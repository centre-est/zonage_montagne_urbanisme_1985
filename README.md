# Zonage Montagne Urbanisme 1985

Actualisation au 1er janvier 2019 de la liste des communes concernées par l'application des dispositions d'urbanisme de la loi Montagne (Loi n° 85-30 du 9 janvier 1985)

Missionné par la DHUP (Bureau de la législation de l'urbanisme), le Cerema a réalisé une actualisation au 1er janvier 2019 de la liste des communes concernées par l'application des dispositions d'urbanisme de la loi Montagne (Loi n° 85-30 du 9 janvier 1985 relative au développement et à la protection de la montagne, Loi n° 2016-1888 du 28 décembre 2016 de modernisation, de développement et de protection des territoires de montagne).

La liste de ces communes est définie par les arrêtés des 20 février 1974 (modifié par le rectificatif du 2 mars 1974), 28 avril 1976, 18 janvier 1977, 13 novembre 1978, 29 janvier 1982, 20 septembre 1983, 14 décembre 1984 et 25 juillet 1985.

Le Cerema livre :

### 01_maj_1_janvier_2019
Par département :
- un tableau de la liste des communes telles qu'elles sont inscrites dans les arrêtés de classement,
- un tableau de la liste des communes au 1er janvier 2019 classées totalement ou partiellement.
- le script SQL départemental permettant de passer de la liste de 1985 à celle des 2019.
### Sur Ceremadata
A l'echelle de la France Métropolitaine :
- une couche cartographique communale des communes au 1er janvier 2019 conforme au Code Officiel Géographie (COG INSEE) du 01/01/2019. Cette table prend en compte les fusions et rétablissements de communes survenus entre les dates des arrêtés de classement et le 1er janvier 2019 :
https://www.cdata.cerema.fr/geonetwork/srv/fre/catalog.search;jsessionid=56B70865B571FFFBE033C4361B203143#/metadata/0de7d0cd-e48e-4b77-bfce-517a4e3db5fc
- le tableau des communes au 1er janvier 2019 conforme au Code Officiel Géographie (COG INSEE) du 01/01/2019: 
https://www.cdata.cerema.fr/geonetwork/srv/fre/catalog.search;jsessionid=56B70865B571FFFBE033C4361B203143#/metadata/84de68e3-8e64-4caf-b000-fbeaa492879e

### 02_travail_infra_communal
- un script SQL par département permettant qui a permis de générer un zonage infracommunale pour les communes classées parties au 1er janvier 2019.
### Sur Ceremadata
- une couche cartographique infra communale d'application de ce zonage par département :
https://www.cdata.cerema.fr/geonetwork/srv/fre/catalog.search;jsessionid=56B70865B571FFFBE033C4361B203143#/metadata/3131c2dc-b882-47ca-b20f-abc2f1ae692b

Ces données ont fait l'objet d'une validation par la Direction Départementale des Territoires du département.
