----- Travail � faire :
select insee_2019, nom_min_2019,agreg_insee_classement,agreg_nom_classement, agreg_commentaires
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
where code_dep = '034' and classement_2019 != 'totalit�';
/*
----------+------------+----------------------+--------------------+------------------------------------------------------------------------------------------------+
34186     |Octon       |34186                 |OCTON               |Territoire de l�ancienne commune de Saint-Martin-des-Combes � ancienne commune rattach�e en 1963|
*/

----> Les pr�fixes des communes sont dans les sections etalab : non 
SELECT id, commune , prefixe FROM r_cadastre_etalab_2021.n_sections_etalab_000_2021
where COMMUNE = '34186';
/*
id        |commune|prefixe|
----------+-------+-------+
341860000A|34186  |000    |
341860000B|34186  |000    |
341860000C|34186  |000    |
341860000D|34186  |000    |
341860000E|34186  |000    |
341860000F|34186  |000    |
341860000G|34186  |000    |
341860000H|34186  |000    |
 */

--- 34186
--- Mais en comparant le scan50 historique qui indique les p�rim�tres des anciennes communes et les sections cadastrales on voit que le p�rim�tre de l'ancienne commune = Section H + section G
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '34186' and (id = '341860000G' or id = '341860000H');
--> OK

---- DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '34186';
---- Ajout 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '34186' and (id = '341860000G' or id = '341860000H')) as geom,
	'true' as finalisee,
	'en comparant le scan50 historique qui indique les p�rim�tres des anciennes communes et les sections cadastrales on voit que le p�rim�tre de l�ancienne commune = Section H + section G' as detail_arrete,
	'Ancien p�rim�tre de la commune retrouv� grace aux pr�fixes de sections cadastrales : select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''34186'' and (id = ''341860000G'' or id = ''341860000H'')' as detail_zinf,
	'21/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_034_2019 as t1
WHERE insee_2019 = '34186'
);
--> Updated Rows	1

---- 13/0/2021
---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_034
where classement_2019 != 'totalit�' order by insee_2019;
/*
insee_2019|nom_cog2019|finalisee|detail_arrete                                                                                                                                                                         |
----------+-----------+---------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
34186     |OCTON      |true     |en comparant le scan50 historique qui indique les p�rim�tres des anciennes communes et les sections cadastrales on voit que le p�rim�tre de l�ancienne commune = Section H + section G|
*/


---- 15/10/2021
---- A] Cr�ation du polygone
---- A.1] Cr�ation du polygone
drop table if exists  a_dcap_actu_arretes_montagne_dhup.temp_perimetre_034;
create table a_dcap_actu_arretes_montagne_dhup.temp_perimetre_034 as 
select
st_multi(ST_UNION(geom))::Geometry('MULTIPOLYGON',2154) as geom
FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_034;
--> Updated Rows	1

---- A.2] Identification des scories :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_034
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|geom        
--+--------+--------+----------+------------
33|       1|      33|      2815|MULTIPOLYGON
 6|       1|       6|      1828|MULTIPOLYGON
17|       1|      17|      1605|MULTIPOLYGON
 3|       1|       3|      1314|MULTIPOLYGON
29|       1|      29|      1153|MULTIPOLYGON
20|       1|      20|       913|MULTIPOLYGON
 1|       1|       1|       795|MULTIPOLYGON
31|       1|      31|       540|MULTIPOLYGON
19|       1|      19|       277|MULTIPOLYGON
26|       1|      26|       243|MULTIPOLYGON
 7|       1|       7|       208|MULTIPOLYGON
18|       1|      18|       161|MULTIPOLYGON
21|       1|      21|       148|MULTIPOLYGON
22|       1|      22|       120|MULTIPOLYGON
14|       1|      14|       105|MULTIPOLYGON
34|       1|      34|        79|MULTIPOLYGON
32|       1|      32|        78|MULTIPOLYGON
10|       1|      10|        50|MULTIPOLYGON
15|       1|      15|        42|MULTIPOLYGON
25|       1|      25|        23|MULTIPOLYGON
13|       1|      13|        22|MULTIPOLYGON
12|       1|      12|        21|MULTIPOLYGON
 5|       1|       5|        17|MULTIPOLYGON
11|       1|      11|        15|MULTIPOLYGON
 9|       1|       9|        14|MULTIPOLYGON
 4|       1|       4|        11|MULTIPOLYGON
24|       1|      24|         7|MULTIPOLYGON
36|       1|      36|         6|MULTIPOLYGON
30|       1|      30|         4|MULTIPOLYGON
 2|       1|       2|         2|MULTIPOLYGON
23|       1|      23|         2|MULTIPOLYGON
35|       1|      35|         1|MULTIPOLYGON
16|       1|      16|         1|MULTIPOLYGON
27|       1|      27|         1|MULTIPOLYGON
 8|       1|       8|         1|MULTIPOLYGON
28|       1|      28|         0|MULTIPOLYGON
 */

---- A.3] Supression des scories :
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable				character varying;
surfacemaxaevider			integer;
req 						text;
BEGIN
---- Les param�tres :
nomduschema :=  'a_dcap_actu_arretes_montagne_dhup';
nomdelatable := 'temp_perimetre_034';
surfacemaxaevider := 2000; ---- si les couches sont en coordonnn�es m�triques : en m2

---- Le Script 
---- Premi�rement : on met le polygone sans les trous
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous;
CREATE TABLE "' || nomduschema || '".table_sans_trous as
	SELECT ROW_NUMBER() OVER() AS id,
	ST_MULTI(st_makepolygon(st_exteriorring(st_geometryn(geom,1))))::geometry(''MULTIPOLYGON'',2154) as geom
	FROM "' || nomduschema || '"."' || nomdelatable || '";
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- Deuxi�mement : On cr�� une TABLE que de trous de plus de 10m2 :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
CREATE TABLE "' || nomduschema || '".table_de_trous_gardes AS (
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
            from "' || nomduschema || '"."' || nomdelatable || '"
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry(''MULTIPOLYGON'',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
where st_area(geom) > ' || surfacemaxaevider || '
);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL : On d�coupe la tache sans trou avec les trous gard�s
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous" AS
select row_number() over() as id,
st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_difference((st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t1.geom))),3))),(st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t2.geom))),3)))))),3))::geometry(''MultiPolygon'',2154) as geom
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
WHERE st_intersects(t1.geom,t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL BIS : On n'oublie pas les derniers polygones qui ne sont pas concern�s par des trous
req := '
INSERT INTO "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous"
select row_number() over() as id, st_multi(t1.geom)
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
where st_intersects(t1.geom,t2.geom) IS FALSE;
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- POUR FAIRE PROPRE : on supprime les tables temporaires :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous;
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous; 
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

---- A.4] V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_034_sans_petits_trous 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 1|       1|       1|      2815|
 */

---- B] Cr�ation de la couche
---- B.1] Cr�ation de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034 as 
with selection as 
			(select	source_geom
			FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_034
			group by source_geom order by source_geom)
select
	'034'::varchar(3) as insee_dep,
	'MASSIF CENTRAL'::varchar(20) as massif,
	'Zone Infra-communale concern�e par la loi n� 85-30 du 9 janvier 1985 relative au d�veloppement et � la protection de la montagne'::varchar(254) as type_zone,
	'cerema au 01/01/2019'::varchar(21) as millesime,
	array_to_string(array_agg(source_geom), ' & ', '*')::varchar(254) as sourc_geom,
	(select geom from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_034_sans_petits_trous)::Geometry('MULTIPOLYGON',2154) as geom
from selection;
--> Updated Rows	1

---- B.2] V�rification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- G�om�trie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034 WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034 WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Polygones non ferm�s :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034 WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Combien de g�om�trie :
select ST_NumGeometries(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034;
-->
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034;
--> 

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034 ADD CONSTRAINT l_loimontagne85_zinf_034_pk PRIMARY KEY (insee_dep);

---- D] Supression d'un polygone en trop
----> 1 polygone
----  V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 */

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_034;
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_034_sans_petits_trous;

---- F] Sauvegarde de la table car corrections manuelles
drop table if exists a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_034_15102021;
create table a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_034_15102021 (like a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034 including all);
insert into a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_034_15102021 select * from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_034;