---- 03/12/2020

---- A] Import de la BDPARCELLAIRE Historique de 2010 via glisser/d�placer de QGIS

---- A.1] Renommage
ALTER TABLE r_bdparcellaire_histo."BATIMENT"
    RENAME TO n_batiment_bdp_063_2012;
ALTER TABLE r_bdparcellaire_histo."COMMUNE"
    RENAME TO n_commune_bdp_063_2012;
ALTER TABLE r_bdparcellaire_histo."DIVCAD"
    RENAME TO n_divcad_bdp_063_2012;
ALTER TABLE r_bdparcellaire_histo."LOCALISANT"
    RENAME TO n_localisant_bdp_063_2012;
ALTER TABLE r_bdparcellaire_histo."PARCELLE"
    RENAME TO n_parcelle_bdp_063_2012;

---- A.2] Corrections
UPDATE r_bdparcellaire_histo.n_divcad_bdp_063_2012 SET geom=
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
--> UPDATE 0 / Requ�te ex�cut�e avec succ�s en 1 s 373 msec.

UPDATE r_bdparcellaire_histo.n_batiment_bdp_063_2012 SET geom=
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
--> UPDATE 0 / Requ�te ex�cut�e avec succ�s en 7 s 559 msec.

UPDATE r_bdparcellaire_histo.n_commune_bdp_063_2012 SET geom=
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
--> UPDATE 0 / Requ�te ex�cut�e avec succ�s en 559 msec.

UPDATE r_bdparcellaire_histo.n_localisant_bdp_063_2012 SET geom=
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
--> UPDATE 0 / Requ�te ex�cut�e avec succ�s en 735 msec.

UPDATE r_bdparcellaire_histo.n_parcelle_bdp_063_2012 SET geom=
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
--> UPDATE 0 / Requ�te ex�cut�e avec succ�s en 15 s 263 msec.

select  * from public.dgfip_dfi
where meres && '63115000ZC%'

---- B] MAJ des 5 communes :
----- Travail � faire :
select insee_2019, nom_min_2019,agreg_insee_classement,agreg_nom_classement, agreg_commentaires
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
where code_dep = '063' and classement_2019 != 'totalit�';
/*
code_dep|insee_2019|nom_cog2019 |nom_min_2019|nb_evenements_cog|agreg_insee_classement|agreg_nom_classement|agreg_arretes|agreg_classement_85|classement_2019|agreg_commentaires                                                                                                    |source_geom                   |geom                                                                                                                                                                                                                                                           |insee_2019|nom_min_2019|agreg_insee_classement|agreg_nom_classement|agreg_commentaires                                                                                                    |
--------+----------+------------+------------+-----------------+----------------------+--------------------+-------------+-------------------+---------------+----------------------------------------------------------------------------------------------------------------------+------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+------------+----------------------+--------------------+----------------------------------------------------------------------------------------------------------------------+
063     |63103     |CHATEL GUYON|Ch�tel-Guyon|                1|{63103}               |{CHATELGUYON}       |{1976-04-28} |{P}                |partie         |{"Section AR (Village du BOURNET)"}                                                                                   |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((702796.3 6535150.4, 702852.1 6535144.4, 702890.8 6535163.8, 702913 6535142.7, 702929.5 6535102.7, 702940 6535097.6, 703052.2 6535094.8, 703112.3 6535097.5, 703168.6 6535110.3, 703196.8 6535137.6, 703235.2 6535156, 703290.8 6535156.7, 70333|63103     |Ch�tel-Guyon|{63103}               |{CHATELGUYON}       |{"Section AR (Village du BOURNET)"}                                                                                   |
063     |63116     |COMBRONDE   |Combronde   |                0|63116                 |COMBRONDE           |1976-04-28   |P                  |partie         |Section G (Village des BALLANGES, BORONTS)�; Section A (Village des JOUFFRETS)                                        |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((706489.4 6540599.7, 706330 6540609, 706250.2 6540639, 706182.8 6540674.5, 706100 6540694.5, 706000.5 6540750.1, 705948.7 6540760.7, 705931.3 6540776.6, 705928.6 6540792.6, 705911.1 6540798.2, 705871.2 6540843.9, 705752.8 6540891, 705713.7 |63116     |Combronde   |63116                 |COMBRONDE           |Section G (Village des BALLANGES, BORONTS)�; Section A (Village des JOUFFRETS)                                        |
063     |63150     |ENVAL       |Enval       |                0|63150                 |ENVAL               |1976-04-28   |P                  |partie         |Section ZD (Village de BEAUVALEIX)                                                                                    |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((702369.4 6532640.3, 702450.9 6532660.4, 702481.9 6532680.6, 702389.2 6532710.3, 702332.8 6532753.1, 702296.3 6532769.5, 702266.1 6532772.9, 702195.7 6532799.9, 702134.8 6532844.7, 702116.2 6532868.5, 702111.1 6532906.8, 702229.8 6533076.7,|63150     |Enval       |63150                 |ENVAL               |Section ZD (Village de BEAUVALEIX)                                                                                    |
063     |63307     |ROMAGNAT    |Romagnat    |                0|63307                 |ROMAGNAT            |1976-04-28   |P                  |partie         |Section G & AL (village de OPME)�; Section I & AM (Village de SAULZET-LE-CHAUD)�; section H (Village de REDON, PRADET)|n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((703349.1 6513014.7, 703396.8 6512918, 703593.6 6512992.6, 703720.3 6513050.1, 703732.2 6513048.1, 703901.4 6512887.1, 703978.2 6512946, 704023.8 6512996.8, 704096.6 6513040.9, 704444.1 6513125.9, 704718.7 6513215.4, 704860.8 6513077.6, 704|63307     |Romagnat    |63307                 |ROMAGNAT            |Section G & AL (village de OPME)�; Section I & AM (Village de SAULZET-LE-CHAUD)�; section H (Village de REDON, PRADET)|
063     |63417     |SAYAT       |Sayat       |                0|63417                 |SAYAT               |1976-04-28   |P                  |partie         |Section A (Village d�ARGNAT)                                                                                          |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((703246.3 6527188.2, 703301.6 6527135.5, 703278.4 6527041.9, 703331.2 6526856.6, 703367.1 6526892.4, 706337.1 6526897.8, 703543.8 6526866.5, 703576.4 6526829.4, 703665.4 6526797.7, 703710 6526796.7, 703796 6526814.7, 703868.8 6526819.7, 704|63417     |Sayat       |63417                 |SAYAT               |Section A (Village d�ARGNAT)                                                                                          |
*/

---- 63103 CHATEL GUYON
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom))))
from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63103' and (code ='AR' or code ='YA');
--> OK

---- Ajout 
DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '63103';
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63103' and (code ='AR' or code ='YA')) as geom,
	'true' as finalisee,
	'Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales' as detail_arrete,
	'select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''63103'' and (code =''AR'' or code =''YA'')' as detail_zinf,
	'26/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 as t1
WHERE insee_2019 = '63103'
);
--> Updated Rows	1

---- 63116 COMBRONDE - version 1
/*
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom))))
from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63116' and (code ='G' or code ='A' or code in ('YA','YB','YC','YN','YM','YL'));
--> OK

---- Ajout
DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '63116';
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63116' and (code ='G' or code ='A' or code in ('YA','YB','YC','YN','YM','YL'))) as geom,
	'true' as finalisee,
	'Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales' as detail_arrete,
	'select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''63116'' and (code =''G'' or code =''A'' or code in (''YA'',''YB'',''YC'',''YN'',''YM'',''YL''));' as detail_zinf,
	'26/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 as t1
WHERE insee_2019 = '63116'
);
--> Updated Rows	1
*/

---- 63116 COMBRONDE - version 2
---- import de la couche transmise par la DDT : a_dcap_actu_arretes_montagne_dhup.n_montagne_zinf_063
select geom from a_dcap_actu_arretes_montagne_dhup.n_montagne_zinf_063 where id = '1';

update a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 set
geom = (select geom from a_dcap_actu_arretes_montagne_dhup.n_montagne_zinf_063 where id = '1'),
source_geom = 'DDT 63',
detail_zinf = 'copie du zonage infra communal fourni par la DDT63 : n_montagne_zinf_063.shp tir� de https://www.archivesdepartementales.puy-de-dome.fr/ark:/72847/vtad9f29a21f2061f92/dao/0/layout:table/idsearch:RECH_8719eaf18e119f5e7a2cfcecdad4234e#id:153369434?gallery=true&brightness=100.00&contrast=100.00&center=5633.049,-3810.103&zoom=7&rotation=0.000 '
where  insee_2019 = '63116';

---- 63150 ENVAL
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63150' and code = 'ZD';
--> OK

---- Ajout 
DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '63150';
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(st_union(geom)) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63150' and code = 'ZD') as geom,
	'true' as finalisee,
	'Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales' as detail_arrete,
	'select ST_Multi(st_union(geom)) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''63150'' and code = ''ZD'';' as detail_zinf,
	'26/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 as t1
WHERE insee_2019 = '63150'
);
--> Updated Rows	1

---- 63307
---- Section G & AL (village de OPME)�; Section I & AM (Village de SAULZET-LE-CHAUD)�; section H (Village de REDON, PRADET)
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom))))
from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63307' and code in ('G','AL','I','AM','H', 'BK','BB','ZA');
--> OK

---- Ajout 
DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '63307';
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63307' and code in ('G','AL','I','AM','H', 'BK','BB','ZA')) as geom,
	'true' as finalisee,
	'Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales' as detail_arrete,
	'select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''63307'' and code in (''G'',''AL'',''I'',''AM'',''H'', ''BK'',''BB'',''ZA'');' as detail_zinf,
	'26/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 as t1
WHERE insee_2019 = '63307'
);
--> Updated Rows	1

---- 63417 SAYAT - version 1
---- Section A (Village d�ARGNAT)
/*  
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom))))
from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63417' and code in ('A','AB','AC','AA','AD','XB','AE');
--> OK

---- Ajout 
DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '63417';
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom))))
from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '63417' and code in ('A','AB','AC','AA','AD','XB','AE')) as geom,
	'true' as finalisee,
	'Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales' as detail_arrete,
	'select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''63417'' and code in (''A'',''AB'',''AC'',''AA'',''AD'',''XB'',''AE'');' as detail_zinf,
	'26/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 as t1
WHERE insee_2019 = '63417'
);
--> Updated Rows	1
*/

---- 63417 SAYAT - version 2
select geom from a_dcap_actu_arretes_montagne_dhup.n_montagne_zinf_063 where id = '2';

update a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 set
geom = (select geom from a_dcap_actu_arretes_montagne_dhup.n_montagne_zinf_063 where id = '2'),
source_geom = 'DDT 63',
detail_zinf = 'copie du zonage infra communal fourni par la DDT63 : n_montagne_zinf_063.shp tir� de https://www.archivesdepartementales.puy-de-dome.fr/ark:/72847/vtac4b4def5d1c48955/dao/0/layout:table/idsearch:RECH_38c226291018ead953a86a16d54aad8e#id:1250473724?gallery=true&brightness=100.00&contrast=100.00&center=5779.429,-3801.429&zoom=5&rotation=0.000'
where  insee_2019 = '63417';

---- 13/0/2021
---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_063
where classement_2019 != 'totalit�' order by insee_2019;
/*
insee_2019|nom_cog2019 |finalisee|detail_arrete                                                                                                                                                     |
----------+------------+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
63103     |CHATEL GUYON|true     |Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales|
63116     |COMBRONDE   |true     |Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales|
63150     |ENVAL       |true     |Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales|
63307     |ROMAGNAT    |true     |Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales|
63417     |SAYAT       |true     |Recherche historique des sections par la DDT s�est faite � partir des planches du cadastre napol�onien diffus�es sur le site internet des archives d�partementales|
*/


---- 15/10/2021
---- A] Cr�ation du polygone
---- A.1] Cr�ation du polygone
drop table if exists  a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063;
create table a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063 as 
select
st_multi(ST_UNION(geom))::Geometry('MULTIPOLYGON',2154) as geom
FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_063;
--> Updated Rows	1

---- A.2] Identification des scories :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk |id_tache|num_trou|surface_m2|
---+--------+--------+----------+
  1|       1|       1|   2370495|
 25|       1|      25|    115609|
 23|       1|      23|      8798|
 46|       1|      46|      8320|
 35|       1|      35|      6903|
 38|       1|      38|      4719|
 48|       1|      48|      3347|
 70|       1|      70|      3135|
110|       1|     110|      2248|
107|       1|     107|      1856|
 55|       1|      55|      1380|
 56|       1|      56|      1318|
 50|       1|      50|      1224|
 85|       1|      85|      1195|
 47|       1|      47|      1190|
106|       1|     106|      1177|
 51|       1|      51|      1133|
136|       1|     136|      1051|
 27|       1|      27|       975|
 73|       1|      73|       971|
138|       1|     138|       961|
111|       1|     111|       896|
 42|       1|      42|       703|
 82|       1|      82|       694|
 40|       1|      40|       691|
127|       1|     127|       619|
 65|       1|      65|       609|
 54|       1|      54|       578|
 81|       1|      81|       540|
 57|       1|      57|       522|
150|       1|     150|       492|
 29|       1|      29|       454|
148|       1|     148|       424|
151|       1|     151|       339|
105|       1|     105|       323|
 20|       1|      20|       322|
 28|       1|      28|       284|
 71|       1|      71|       279|
109|       1|     109|       276|
103|       1|     103|       270|
 34|       1|      34|       264|
  6|       1|       6|       238|
 79|       1|      79|       237|
 58|       1|      58|       222|
  4|       1|       4|       173|
 10|       1|      10|       156|
 72|       1|      72|       152|
 68|       1|      68|       147|
113|       1|     113|       130|
141|       1|     141|       129|
 78|       1|      78|       125|
 21|       1|      21|       121|
153|       1|     153|       120|
 33|       1|      33|       116|
 18|       1|      18|       113|
 44|       1|      44|       113|
 32|       1|      32|       104|
 77|       1|      77|        96|
118|       1|     118|        94|
102|       1|     102|        90|
 87|       1|      87|        86|
 59|       1|      59|        79|
 66|       1|      66|        78|
 41|       1|      41|        77|
 26|       1|      26|        75|
 14|       1|      14|        73|
100|       1|     100|        73|
 95|       1|      95|        66|
 90|       1|      90|        65|
 24|       1|      24|        63|
 12|       1|      12|        62|
 64|       1|      64|        56|
 31|       1|      31|        53|
 17|       1|      17|        51|
 69|       1|      69|        51|
101|       1|     101|        50|
121|       1|     121|        49|
 67|       1|      67|        48|
112|       1|     112|        42|
 53|       1|      53|        41|
 80|       1|      80|        41|
 19|       1|      19|        39|
 39|       1|      39|        39|
142|       1|     142|        35|
119|       1|     119|        31|
 13|       1|      13|        29|
 86|       1|      86|        20|
  3|       1|       3|        19|
116|       1|     116|        19|
131|       1|     131|        19|
 43|       1|      43|        18|
 30|       1|      30|        18|
132|       1|     132|        18|
129|       1|     129|        18|
 60|       1|      60|        15|
 91|       1|      91|        15|
147|       1|     147|        15|
 84|       1|      84|        15|
128|       1|     128|        14|
 52|       1|      52|        14|
 76|       1|      76|        13|
137|       1|     137|        13|
149|       1|     149|        13|
 98|       1|      98|        12|
 45|       1|      45|        11|
146|       1|     146|        10|
 22|       1|      22|        10|
 83|       1|      83|        10|
 37|       1|      37|        10|
 88|       1|      88|        10|
120|       1|     120|         9|
117|       1|     117|         9|
  9|       1|       9|         9|
144|       1|     144|         9|
139|       1|     139|         9|
 15|       1|      15|         7|
130|       1|     130|         7|
126|       1|     126|         5|
 49|       1|      49|         5|
 89|       1|      89|         5|
 96|       1|      96|         4|
143|       1|     143|         4|
  8|       1|       8|         4|
145|       1|     145|         4|
 36|       1|      36|         3|
135|       1|     135|         3|
  7|       1|       7|         3|
115|       1|     115|         2|
 74|       1|      74|         2|
 16|       1|      16|         2|
 61|       1|      61|         2|
124|       1|     124|         2|
 94|       1|      94|         2|
 99|       1|      99|         2|
123|       1|     123|         1|
108|       1|     108|         1|
122|       1|     122|         1|
104|       1|     104|         1|
 92|       1|      92|         1|
133|       1|     133|         1|
134|       1|     134|         1|
 11|       1|      11|         1|
  5|       1|       5|         1|
152|       1|     152|         0|
 93|       1|      93|         0|
125|       1|     125|         0|
 62|       1|      62|         0|
 97|       1|      97|         0|
  2|       1|       2|         0|
 75|       1|      75|         0|
114|       1|     114|         0|
 63|       1|      63|         0|
140|       1|     140|         0|
 */

---- A.3] Supression des scories :
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable				character varying;
surfacemaxaevider			integer;
req 						text;
BEGIN
---- Les param�tres :
nomduschema :=  'a_dcap_actu_arretes_montagne_dhup';
nomdelatable := 'temp_perimetre_063';
surfacemaxaevider := 200000; ---- si les couches sont en coordonnn�es m�triques : en m2

---- Le Script 
---- Premi�rement : on met le polygone sans les trous
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous;
CREATE TABLE "' || nomduschema || '".table_sans_trous as
	SELECT ROW_NUMBER() OVER() AS id,
	ST_MULTI(st_makepolygon(st_exteriorring(st_geometryn(geom,1))))::geometry(''MULTIPOLYGON'',2154) as geom
	FROM "' || nomduschema || '"."' || nomdelatable || '";
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- Deuxi�mement : On cr�� une TABLE que de trous de plus de 10m2 :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
CREATE TABLE "' || nomduschema || '".table_de_trous_gardes AS (
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
            from "' || nomduschema || '"."' || nomdelatable || '"
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry(''MULTIPOLYGON'',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
where st_area(geom) > ' || surfacemaxaevider || '
);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL : On d�coupe la tache sans trou avec les trous gard�s
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous" AS
select row_number() over() as id,
st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_difference((st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t1.geom))),3))),(st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t2.geom))),3)))))),3))::geometry(''MultiPolygon'',2154) as geom
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
WHERE st_intersects(t1.geom,t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL BIS : On n'oublie pas les derniers polygones qui ne sont pas concern�s par des trous
req := '
INSERT INTO "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous"
select row_number() over() as id, st_multi(t1.geom)
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
where st_intersects(t1.geom,t2.geom) IS FALSE;
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- POUR FAIRE PROPRE : on supprime les tables temporaires :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous;
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous; 
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

---- A.4] V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063_sans_petits_trous 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2
--+--------+--------+----------
 1|       1|       1|   2370495
 */

---- B] Cr�ation de la couche
---- B.1] Cr�ation de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063 as 
with deux_parties as (
			(with
			srequete as (
						select st_area((st_dump(geom)).geom::geometry('POLYGON',2154)) as surface_m2,
						(st_dump(geom)).geom::geometry('POLYGON',2154) as geom
						from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063
						)
			select geom from srequete where st_area(geom) = 2156151423.135037)
			union
			(select geom from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063_sans_petits_trous)
			),
selection as 
			(select	source_geom
			FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_063
			group by source_geom order by source_geom)
select 
	'063'::varchar(3) as insee_dep,
	'MASSIF CENTRAL'::varchar(20) as massif,
	'Zone Infra-communale concern�e par la loi n� 85-30 du 9 janvier 1985 relative au d�veloppement et � la protection de la montagne'::varchar(254) as type_zone,
	'cerema au 01/01/2019'::varchar(21) as millesime,
	array_to_string(array_agg(source_geom), ' & ', '*')::varchar(254) as sourc_geom,
	(select st_multi(st_union(geom))::geometry('MULTIPOLYGON',2154) as geom from deux_parties)
from selection;
--> Updated Rows	1

---- B.2] V�rification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- G�om�trie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063 WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063 WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Polygones non ferm�s :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063 WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Combien de g�om�trie :
select ST_NumGeometries(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063;
--> 2
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063;
--> 

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063 ADD CONSTRAINT l_loimontagne85_zinf_063_pk PRIMARY KEY (insee_dep);

---- D] Supression d'un polygone en trop
----> 1 polygone
----  V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 */

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063;
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_063_sans_petits_trous;

---- F] Sauvegarde de la table car corrections manuelles
drop table if exists a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_063_15102021;
create table a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_063_15102021 (like a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063 including all);
insert into a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_063_15102021 select * from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_063;
