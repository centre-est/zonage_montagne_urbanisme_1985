---- 07/07/2020
----- ALPES DE HAUTE PROVENCE ----
---- IMPORT donnes
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 SET partie = 'totalit' WHERE partie IS NULL;
--> Updated Rows	176

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004;
--> 183

---- Quelles sont les communes o il s'est pass quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019     |insee_commune_1985       |nom_commune_1985                                              |arretes                                        |classement_85                        |nb_commune_1985|commentaires                                                                                                                                                                                                 |code_evenement_cog|type_commune_avant|type_commune_aprs|
-------------|------------------------|-------------------------|--------------------------------------------------------------|-----------------------------------------------|-------------------------------------|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|------------------|------------------|

*/

--- Quelles communes n'ont pas changs ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 162 lignes

---- Requte finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchange
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	180

--- Comparaison 1  avec fichier Frdric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '04' ---004
ORDER BY insee_cog2019, insee_com;

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004;
--> Updated Rows	180

---- Identification des rtablissement  corriger  la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
04009 |ARCHAIL|04009     |ARCHAIL   |x       |totalit|1974-02-20|1980-01-01|21 |
 */

---- Identification des oprations faites sur les communes non classe
---- A vrifier sur fusion ou fusion simple et corriger  la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_004)
order by date_eff, com_ap

---- Correction du fichier final : le 07/07/2020

--- Comparaison 2  avec fichier Frdric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_004_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '04' ---004
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                      |count|
-----------------------------------|-----|
Partie de commune                  |    1|
totalit                           |  162|
{totalit}                         |   11|
{totalit} non classe             |    2|
{totalit} Non classe             |    1|
{totalit} non classe, non classe|    1|
{totalit,totalit}                |    3|
 */
---- A] Cration de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019 as
SELECT 
'038'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit' THEN 'totalit'
	WHEN t1.classement_85 = '{totalit}' THEN 'totalit'
	
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit' THEN 'totalit'
	WHEN t1.classement_85 = '{totalit}' THEN 'totalit'
	WHEN t1.classement_85 = '{totalit} non classe' THEN 'partie'
	WHEN t1.classement_85 = '{totalit} Non classe' THEN 'partie'
	WHEN t1.classement_85 = '{totalit} non classe, non classe' THEN 'partie'
	WHEN t1.classement_85 = '{totalit,totalit}' THEN 'totalit'   
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 181

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit       |  176|
partie         |    5|
 */

---- B] Attache  la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019 FOR VALUES IN ('038');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 RENAME TO l_liste_commune_montagne_1985_038_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038_old
GROUP BY partie
ORDER BY partie;
/*
partie           |count|
-----------------|-----|
Partie de commune|    1|
totalit         |  182|
 */
---- A] Cration de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS
SELECT
'038'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit' THEN 'totalit'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038_old
ORDER BY insee_classement;
--> 183

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit       |  182|
partie         |    1|
 */

---- B] Attache  la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 FOR VALUES IN ('038');
--> Updated Rows	0

---- 12/10/2021
---- A] Création du polygone
---- A.1] Création du polygone
drop table if exists  a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004;
create table a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004 as 
select
st_multi(ST_UNION(geom))::Geometry('MULTIPOLYGON',2154) as geom
FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_004;
--> Updated Rows	1

---- A.2] Identification des scories :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk |id_tache|num_trou|surface_m2|
---+--------+--------+----------+
 80|       1|      80|  36633861|
 74|       1|      74|  36190624|
 61|       1|      61|  21079476|
  4|       1|       4|  18419056|
105|       1|     105|  11811399|
177|       1|     177|     90411|
205|       1|     205|     77465|
193|       1|     193|     63673|
149|       1|     149|     47029|
 */

---- A.3] Supression des scories :
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable				character varying;
surfacemaxaevider			integer;
req 						text;
BEGIN
---- Les paramètres :
nomduschema :=  'a_dcap_actu_arretes_montagne_dhup';
nomdelatable := 'temp_perimetre_004';
surfacemaxaevider := 100000; ---- si les couches sont en coordonnnées métriques : en m2

---- Le Script 
---- Premièrement : on met le polygone sans les trous
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous;
CREATE TABLE "' || nomduschema || '".table_sans_trous as
	SELECT ROW_NUMBER() OVER() AS id,
	ST_MULTI(st_makepolygon(st_exteriorring(st_geometryn(geom,1))))::geometry(''MULTIPOLYGON'',2154) as geom
	FROM "' || nomduschema || '"."' || nomdelatable || '";
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- Deuxièmement : On créé une TABLE que de trous de plus de 10m2 :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
CREATE TABLE "' || nomduschema || '".table_de_trous_gardes AS (
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
            from "' || nomduschema || '"."' || nomdelatable || '"
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry(''MULTIPOLYGON'',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
where st_area(geom) > ' || surfacemaxaevider || '
);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL : On découpe la tache sans trou avec les trous gardés
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous" AS
select row_number() over() as id,
st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_difference((st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t1.geom))),3))),(st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t2.geom))),3)))))),3))::geometry(''MultiPolygon'',2154) as geom
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
WHERE st_intersects(t1.geom,t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL BIS : On n'oublie pas les derniers polygones qui ne sont pas concernés par des trous
req := '
INSERT INTO "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous"
select row_number() over() as id, st_multi(t1.geom)
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
where st_intersects(t1.geom,t2.geom) IS FALSE;
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- POUR FAIRE PROPRE : on supprime les tables temporaires :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous;
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous; 
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

---- A.4] Vérification du résultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004_sans_petits_trous 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 1|       1|       1|  36633861|
 4|       1|       4|  36190624|
 5|       1|       5|  21079476|
 3|       1|       3|  18419056|
 2|       1|       2|  11811399|
 */


---- B] Créaton de la couche
---- B.1] Créaton de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004 as 
with selection as 
			(select	source_geom
			FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_004
			group by source_geom order by source_geom)
select
	'004'::varchar(3) as insee_dep,
	'ALPES'::varchar(20) as massif,
	'Zone Infra-communale concernée par la loi n° 85-30 du 9 janvier 1985 relative au développement et à la protection de la montagne'::varchar(254) as type_zone,
	'cerema au 01/01/2019'::varchar(21) as millesime,
	array_to_string(array_agg(source_geom), ' & ', '*')::varchar(254) as sourc_geom,
	(select geom from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004_sans_petits_trous)::Geometry('MULTIPOLYGON',2154) as geom
from selection;
--> Updated Rows	1

---- B.2] Vérification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- Géométrie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004 WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne récupérée.

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004 WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne récupérée.

---- Polygones non fermés :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004 WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne récupérée.

---- Combien de géométrie :
select ST_NumGeometries(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004;
-->
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004 limit 1;
--> 

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004 ADD CONSTRAINT l_loimontagne85_zinf_004_pk PRIMARY KEY (insee_dep);

---- D] Vérification visuelle et correction des pointes de zonages trop importantes

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004;
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004_sans_petits_trous;

---- F] Sauvegarde de la table car corrections manuelles
drop table if exists a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_004_15102021;
create table a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_004_15102021 (like a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004 including all);
insert into a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_004_15102021 select * from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_004;
