----- Travail � faire :
select insee_2019, nom_min_2019,agreg_insee_classement,agreg_nom_classement, agreg_commentaires
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
where code_dep = '02B' and classement_2019 != 'totalit�';
/*
insee_2019|nom_min_2019|agreg_insee_classement|agreg_nom_classement|agreg_commentaires                                                                                                                                                                                                                 |
----------+------------+----------------------+--------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
2B167     |Montegrosso |2B167                 |MONTEGROSSO         |Arr�t� de 1976�: (Montemaggiore) donc partie pour le Cerema / R�ponse DDT : pas en mesure de donner un avis �clair� mais nous pensons que cette distinction n'a plus lieu d'�tre et qu'il conviendrait de classer toute la commune.|
*/

----> Les pr�fixes des communes sont dans les sections etalab : NON 
SELECT id, commune , prefixe FROM r_cadastre_etalab_2021.n_sections_etalab_000_2021
where COMMUNE = '2B167';
/*
id        |commune|prefixe|
----------+-------+-------+
2B1670000B|2B167  |000    |
2B1670000F|2B167  |000    |
2B1670000G|2B167  |000    |
2B167000ZH|2B167  |000    |
2B167000ZI|2B167  |000    |
2B1670000C|2B167  |000    |
2B1670000H|2B167  |000    |
2B1670000I|2B167  |000    |
2B167000ZA|2B167  |000    |
2B167000ZB|2B167  |000    |
2B167000ZC|2B167  |000    |
2B167000ZD|2B167  |000    |
2B167000ZE|2B167  |000    |
2B167000ZK|2B167  |000    |
2B167000ZL|2B167  |000    |
2B167000ZM|2B167  |000    |
2B167000ZN|2B167  |000    |
2B167000ZO|2B167  |000    |
2B167000ZP|2B167  |000    |
 */

select * from "r_cadastre_etalab_2021"."n_lieux_dits_etalab_02b_2021" where commune = '2B167' and nom = 'MONTEMAGGIORE';

--- 12021
select geom from "r_cadastre_etalab_2021"."n_lieux_dits_etalab_02b_2021" where commune = '2B167' and nom = 'MONTEMAGGIORE';
--> OK

---- DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '2B167';
---- Ajout 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_lieux_dits_etalab_02b_2021' as source_geom,
	(select geom from "r_cadastre_etalab_2021"."n_lieux_dits_etalab_02b_2021" where commune = '2B167' and nom = 'MONTEMAGGIORE') as geom,
	'true' as finalisee,
	agreg_commentaires as detail_arrete,
	'Ancien p�rim�tre du hameaux retrouv� grace au lieu-dit du cadastre : select geom from "r_cadastre_etalab_2021"."n_lieux_dits_etalab_02b_2021" where commune = ''2B167'' and nom = ''MONTEMAGGIORE''' as detail_zinf,
	'21/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02B_2019 as t1
WHERE insee_2019 = '2B167'
);
--> Updated Rows	1

---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_02b
where classement_2019 != 'totalit�' order by insee_2019;
/*
insee_2019|nom_cog2019|finalisee|detail_arrete                                                                                                                                                                                                                      |
----------+-----------+---------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
2B167     |MONTEGROSSO|true     |Arr�t� de 1976�: (Montemaggiore) donc partie pour le Cerema / R�ponse DDT : pas en mesure de donner un avis �clair� mais nous pensons que cette distinction n'a plus lieu d'�tre et qu'il conviendrait de classer toute la commune.|
*/


---- 09/11/2021
---- A] Cr�ation du polygone
---- A.1] Cr�ation du polygone
drop table if exists  a_dcap_actu_arretes_montagne_dhup.temp_perimetre_02b;
create table a_dcap_actu_arretes_montagne_dhup.temp_perimetre_02b as 
select
st_multi(ST_UNION(geom))::Geometry('MULTIPOLYGON',2154) as geom
FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_02b;
--> Updated Rows	1

---- A.2] Identification des scories :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_02b
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|geom|
--+--------+--------+----------+----+
 */

---- A.3] Supression des scories :


---- B] Cr�ation de la couche
---- B.1] Cr�ation de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b as 
with selection as 
			(select	source_geom
			FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_02b
			group by source_geom order by source_geom)
select
	'02b'::varchar(3) as insee_dep,
	'MASSIF DE CORSE'::varchar(20) as massif,
	'Zone Infra-communale concern�e par la loi n� 85-30 du 9 janvier 1985 relative au d�veloppement et � la protection de la montagne'::varchar(254) as type_zone,
	'cerema au 01/01/2019'::varchar(21) as millesime,
	array_to_string(array_agg(source_geom), ' & ', '*')::varchar(254) as sourc_geom,
	(select geom from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_02b)::Geometry('MULTIPOLYGON',2154) as geom
from selection;
--> Updated Rows	1

---- B.2] V�rification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- G�om�trie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Polygones non ferm�s :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Combien de g�om�trie :
select ST_NumGeometries(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b;
--> 9
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b;
--> 

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b ADD CONSTRAINT l_loimontagne85_zinf_02b_pk PRIMARY KEY (insee_dep);

---- D] Supression d'un polygone en trop
----> 1 polygone
----  V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_02b 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 */

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_02b;
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_02b_sans_petits_trous;