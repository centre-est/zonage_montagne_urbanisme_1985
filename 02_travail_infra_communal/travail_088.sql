----- Travail � faire :
select *, insee_2019, nom_min_2019,agreg_insee_classement,agreg_nom_classement, agreg_commentaires
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
where code_dep = '088' and classement_2019 != 'totalit�';
/*
code_dep|insee_2019|nom_cog2019      |nom_min_2019     |nb_evenements_cog|agreg_insee_classement               |agreg_nom_classement                                                                     |agreg_arretes                                                      |agreg_classement_85                                                |classement_2019|agreg_commentaires                                                                                                                                                                                                                                             |source_geom                   |geom                                                                                                                                                                                                                                                           |insee_2019|nom_min_2019     |agreg_insee_classement               |agreg_nom_classement                                                                     |agreg_commentaires                                                                                                                                                                                                                                             |
--------+----------+-----------------+-----------------+-----------------+-------------------------------------+-----------------------------------------------------------------------------------------+-------------------------------------------------------------------+-------------------------------------------------------------------+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+-----------------+-------------------------------------+-----------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
088     |81233     |TERRE DE BANCALIE|Terre-de-Bancali�|                6|{81226,81233,81241,81260,81296,81301}|{RONEL,ROUMEGOUX,SAINT-ANTONIN-DE-LACALM,SAINT-LIEUX-LAFENASSE,TERRE-CLAPIER,"LE TRAVET"}|{non class�,non class�,1976-04-28,non class�,non class�,1976-04-28}|{non class�e,non class�e,totalit�,non class�e,non class�e,totalit�}|partie         |01/01/2019 : Roum�goux devient commune d�l�gu�e (chef-lieu) au sein de Terre-de-Bancali� (81233) (commune nouvelle). / 01/01/2019 : Cr�ation de la commune nouvelle de Terre-de-Bancali� en lieu et place des communes de Ronel (81226), de Roum�goux (81233), |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((638883.4 6302108, 638912.9 6302136.2, 639033.7 6302295.8, 639063.8 6302302.7, 639113.4 6302339.8, 639399 6302422.2, 639492.6 6302334, 639551.6 6302301.8, 639605.1 6302311.2, 639727 6302271.4, 639737.8 6302273.9, 639761.8 6302307.1, 639792.|81233     |Terre-de-Bancali�|{81226,81233,81241,81260,81296,81301}|{RONEL,ROUMEGOUX,SAINT-ANTONIN-DE-LACALM,SAINT-LIEUX-LAFENASSE,TERRE-CLAPIER,"LE TRAVET"}|01/01/2019 : Roum�goux devient commune d�l�gu�e (chef-lieu) au sein de Terre-de-Bancali� (81233) (commune nouvelle). / 01/01/2019 : Cr�ation de la commune nouvelle de Terre-de-Bancali� en lieu et place des communes de Ronel (81226), de Roum�goux (81233), |
*/

----> Les pr�fixes des communes sont dans les sections etalab : OUI 
SELECT id, commune , prefixe FROM r_cadastre_etalab_2021.n_sections_etalab_000_2021
where COMMUNE = '81233';
/*
id        |commune|prefixe|
----------+-------+-------+
812330000A|81233  |000    |
812332960A|81233  |296    |
812330000B|81233  |000    |
812330000C|81233  |000    |
812330000D|81233  |000    |
812332260A|81233  |226    |
812332260B|81233  |226    |
812332260C|81233  |226    |
812332410A|81233  |241    |
812332410B|81233  |241    |
812332410C|81233  |241    |
812332410D|81233  |241    |
812332410E|81233  |241    |
812332600A|81233  |260    |
812332600B|81233  |260    |
812332600C|81233  |260    |
812332600D|81233  |260    |
812332600E|81233  |260    |
812332960B|81233  |296    |
812332960C|81233  |296    |
812332960D|81233  |296    |
812333010A|81233  |301    |
812333010B|81233  |301    |
 */

--- 81233
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '81233' and prefixe ='241' or  prefixe ='301';
--> OK

select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_bdtopo_2018.n_commune_bdt_088_2018 where code_insee in ('81241','81301');


---- DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '81233';
---- Ajout 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_commune_bdt_088_2018' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_bdtopo_2018.n_commune_bdt_088_2018 where code_insee in ('81241','81301')) as geom,
	'true' as finalisee,
	agreg_commentaires as detail_arrete,
	'select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_bdtopo_2018.n_commune_bdt_088_2018 where code_insee in (''81241'',''81301'');' as detail_zinf,
	'21/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_088_2019 as t1
WHERE insee_2019 = '81233'
);
--> Updated Rows	1

---- 13/08/2021
---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_088
where classement_2019 != 'totalit�' order by insee_2019;
/*
insee_2019|nom_cog2019           |finalisee|detail_arrete                                                                                                                                                                          |
----------+----------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
88361     |PROVENCHERES ET COLROY|true     |01/01/2016 : Cr�ation de la commune nouvelle de Provench�res-et-Colroy en lieu et place des communes de Colroy-la-Grande (88112) et de Provench�res-sur-Fave (88361) devenues d�l�gu�es|
*/

---- 18/10/2021
---- A] Cr�ation du polygone
---- A.1] Cr�ation du polygone
drop table if exists  a_dcap_actu_arretes_montagne_dhup.temp_perimetre_088;
create table a_dcap_actu_arretes_montagne_dhup.temp_perimetre_088 as 
select
st_multi(ST_UNION(geom))::Geometry('MULTIPOLYGON',2154) as geom
FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_088;
--> Updated Rows	1

---- A.2] Identification des scories :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_088
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 2|       1|       2|  83189172|
 1|       1|       1|     18583|
10|       1|      10|      8251|
 8|       1|       8|      6910|
17|       1|      17|      5958|
13|       1|      13|      4727|
19|       1|      19|      2751|
12|       1|      12|      1955|
14|       1|      14|      1675|
16|       1|      16|      1532|
 7|       1|       7|      1500|
11|       1|      11|       809|
 5|       1|       5|       478|
20|       1|      20|       468|
 6|       1|       6|       418|
 3|       1|       3|       182|
 4|       1|       4|       181|
15|       1|      15|        61|
18|       1|      18|        25|
 9|       1|       9|        11|
 */

---- A.3] Supression des scories :
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable				character varying;
surfacemaxaevider			integer;
req 						text;
BEGIN
---- Les param�tres :
nomduschema :=  'a_dcap_actu_arretes_montagne_dhup';
nomdelatable := 'temp_perimetre_088';
surfacemaxaevider := 20000; ---- si les couches sont en coordonnn�es m�triques : en m2

---- Le Script 
---- Premi�rement : on met le polygone sans les trous
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous;
CREATE TABLE "' || nomduschema || '".table_sans_trous as
	SELECT ROW_NUMBER() OVER() AS id,
	ST_MULTI(st_makepolygon(st_exteriorring(st_geometryn(geom,1))))::geometry(''MULTIPOLYGON'',2154) as geom
	FROM "' || nomduschema || '"."' || nomdelatable || '";
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- Deuxi�mement : On cr�� une TABLE que de trous de plus de 10m2 :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
CREATE TABLE "' || nomduschema || '".table_de_trous_gardes AS (
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
            from "' || nomduschema || '"."' || nomdelatable || '"
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry(''MULTIPOLYGON'',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
where st_area(geom) > ' || surfacemaxaevider || '
);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL : On d�coupe la tache sans trou avec les trous gard�s
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous" AS
select row_number() over() as id,
st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_difference((st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t1.geom))),3))),(st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t2.geom))),3)))))),3))::geometry(''MultiPolygon'',2154) as geom
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
WHERE st_intersects(t1.geom,t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL BIS : On n'oublie pas les derniers polygones qui ne sont pas concern�s par des trous
req := '
INSERT INTO "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous"
select row_number() over() as id, st_multi(t1.geom)
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
where st_intersects(t1.geom,t2.geom) IS FALSE;
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- POUR FAIRE PROPRE : on supprime les tables temporaires :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous;
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous; 
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

---- A.4] V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_088_sans_petits_trous 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 1|       1|       1|  83189172|
 */
*/
---- B] Cr�ation de la couche
---- B.1] Cr�ation de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088 as 
with selection as 
			(select	source_geom
			FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_088
			group by source_geom order by source_geom)
select
	'088'::varchar(3) as insee_dep,
	'MASSIF DES VOSGES'::varchar(20) as massif,
	'Zone Infra-communale concern�e par la loi n� 85-30 du 9 janvier 1985 relative au d�veloppement et � la protection de la montagne'::varchar(254) as type_zone,
	'cerema au 01/01/2019'::varchar(21) as millesime,
	array_to_string(array_agg(source_geom), ' & ', '*')::varchar(254) as sourc_geom,
	(select geom from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_088_sans_petits_trous)::Geometry('MULTIPOLYGON',2154) as geom
from selection;
--> Updated Rows	1

---- B.2] V�rification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- G�om�trie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088 WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088 WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Polygones non ferm�s :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088 WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Combien de g�om�trie :
select ST_NumGeometries(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088;
--> 1
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088;
/*
st_numinteriorrings|
-------------------+
                   |
*/

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088 ADD CONSTRAINT l_loimontagne85_zinf_088_pk PRIMARY KEY (insee_dep);

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_088;
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_088_sans_petits_trous;

---- F] Sauvegarde de la table car corrections manuelles
drop table if exists a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_088_18102021;
create table a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_088_18102021 (like a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088 including all);
insert into a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_088_18102021 select * from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_088;

