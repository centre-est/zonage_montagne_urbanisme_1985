----- Travail � faire :
select insee_2019, nom_min_2019,agreg_insee_classement,agreg_nom_classement, agreg_commentaires
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
where code_dep = '007' and classement_2019 != 'totalit�';
/*
insee_2019|nom_min_2019|agreg_insee_classement|agreg_nom_classement|agreg_commentaires           |
----------+------------+----------------------+--------------------+-----------------------------+
07334     |Les Vans    |07334                 |LES VANS            |Section de Brahic et de Naves|*
*/

----> Les pr�fixes des communes sont dans les sections etalab : oui 
SELECT * FROM r_cadastre_etalab_2021.n_sections_etalab_000_2021
where COMMUNE = '04070';

--- 07334 LES VANS
select * from r_cadastre_etalab_2021.n_lieux_dits_etalab_000_2021 where code_dep = '007' and commune = '07334' and (nom ='BRAHIC' or nom like'%NAVES%');
/*
 commune|nom             |created   |updated   |geom                                                                                                                                                                                                                                                           |code_dep|
-------+----------------+----------+----------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+
07334  |BRAHIC          |2014-08-06|2014-08-06|MULTIPOLYGON (((787783.8121330291 6365652.358666175, 787785.6893075999 6365656.006757248, 787787.4228230723 6365658.530723756, 787789.6196868448 6365660.394551737, 787790.8169335542 6365660.811225565, 787793.2932923129 6365660.345834869, 787801.0732131532|007     |
07334  |VILLAGE DE NAVES|2014-09-01|2014-09-01|MULTIPOLYGON (((788681.1033313561 6367210.8918347275, 788679.1764086614 6367210.84244082, 788672.5195650646 6367208.426568556, 788664.5523698165 6367230.023268512, 788657.5737094425 6367239.135106789, 788654.7331913968 6367249.649607254, 788653.0907285882|007     |
 */

---- DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '07334';
---- Ajout 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_lieux_dits_etalab_000_2021' as source_geom,
	(select st_multi(st_union(geom)) from r_cadastre_etalab_2021.n_lieux_dits_etalab_000_2021 where code_dep = '007' and commune = '07334' and (nom ='BRAHIC' or nom like'%NAVES%')) as geom,
	'true' as finalisee,
	'Lieux dit BRAHIC retrouv� et VILLAGE DE NAVES � la place de NAVES' as detail_arrete,
	'select st_multi(st_union(geom)) from r_cadastre_etalab_2021.n_lieux_dits_etalab_000_2021 where code_dep = ''007'' and commune = ''07334'' and (nom =''BRAHIC'' or nom like ''%NAVES%'')' as detail_zinf,
	'21/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_007_2019 as t1
WHERE insee_2019 = '07334'
);
--> Updated Rows	1

---- 13/0/2021
---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_007
where classement_2019 != 'totalit�' order by insee_2019;
/*
insee_2019|nom_cog2019|finalisee|detail_arrete                                                    |
----------+-----------+---------+-----------------------------------------------------------------+
07334     |VANS       |true     |Lieux dit BRAHIC retrouv� et VILLAGE DE NAVES � la place de NAVES|
*/


----- 23/08/2021
----- Appel de la DDT : St�phane SAUSSAC : en fait il s'agit des anciennes communes et non des hameaux.
SELECT id, commune , prefixe FROM r_cadastre_etalab_2021.n_sections_etalab_000_2021
where COMMUNE = '07334';
/*
id        |commune|prefixe|
----------+-------+-------+
073340000A|07334  |000    |
073340430A|07334  |043    |
073340430B|07334  |043    |
073340430C|07334  |043    |
073340430D|07334  |043    |
07334043AB|07334  |043    |
073340570A|07334  |057    |
073340570B|07334  |057    |
073341640A|07334  |164    |
073341640B|07334  |164    |
 */

select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021
where COMMUNE = '07334' and (prefixe ='043' or  prefixe ='164');
--> ok

DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '07334';
---- Ajout 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021
		where COMMUNE = '07334' and (prefixe ='043' or  prefixe ='164')) as geom,
	'true' as finalisee,
	'Interpr�tation DDT : l arr�t� parle de sections = p�rim�tres des anciennes communes avant fusion le 01/07/1973' as detail_arrete,
	'select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''07334'' and (prefixe =''043'' or  prefixe =''164''))' as detail_zinf,
	'23/08/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_007_2019 as t1
WHERE insee_2019 = '07334'
);

---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_007
where classement_2019 != 'totalit�' order by insee_2019;
/*
insee_2019|nom_cog2019|finalisee|detail_arrete                                                                                                 |
----------+-----------+---------+--------------------------------------------------------------------------------------------------------------+
07334     |VANS       |true     |Interpr�tation DDT : l arr�t� parle de sections = p�rim�tres des anciennes communes avant fusion le 01/07/1973|
*/


---- 15/10/2021
---- A] Cr�ation du polygone
---- A.1] Cr�ation du polygone
drop table if exists  a_dcap_actu_arretes_montagne_dhup.temp_perimetre_007;
create table a_dcap_actu_arretes_montagne_dhup.temp_perimetre_007 as 
select
st_multi(ST_UNION(geom))::Geometry('MULTIPOLYGON',2154) as geom
FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_007;
--> Updated Rows	1

---- A.2] Identification des scories :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_007
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
12|       1|      12|     21122|
33|       1|      33|      2878|
10|       1|      10|      2847|
11|       1|      11|      1958|
13|       1|      13|      1915|
36|       1|      36|      1632|
32|       1|      32|      1548|
31|       1|      31|      1222|
24|       1|      24|      1067|
34|       1|      34|       955|
41|       1|      41|       856|
16|       1|      16|       737|
 2|       1|       2|       688|
29|       1|      29|       435|
28|       1|      28|       408|
22|       1|      22|       378|
17|       1|      17|       280|
30|       1|      30|       225|
 6|       1|       6|       206|
35|       1|      35|       199|
20|       1|      20|       183|
26|       1|      26|       171|
38|       1|      38|       121|
 5|       1|       5|        87|
40|       1|      40|        81|
27|       1|      27|        74|
25|       1|      25|        73|
23|       1|      23|        70|
 4|       1|       4|        64|
18|       1|      18|        43|
19|       1|      19|        42|
37|       1|      37|        37|
14|       1|      14|        30|
39|       1|      39|        15|
15|       1|      15|         4|
 1|       1|       1|         3|
 3|       1|       3|         2|
21|       1|      21|         2|
 7|       1|       7|         1|
 9|       1|       9|         1|
 8|       1|       8|         0|
 */

---- A.3] Supression des scories :
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable				character varying;
surfacemaxaevider			integer;
req 						text;
BEGIN
---- Les param�tres :
nomduschema :=  'a_dcap_actu_arretes_montagne_dhup';
nomdelatable := 'temp_perimetre_007';
surfacemaxaevider := 20000; ---- si les couches sont en coordonnn�es m�triques : en m2

---- Le Script 
---- Premi�rement : on met le polygone sans les trous
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous;
CREATE TABLE "' || nomduschema || '".table_sans_trous as
	SELECT ROW_NUMBER() OVER() AS id,
	ST_MULTI(st_makepolygon(st_exteriorring(st_geometryn(geom,1))))::geometry(''MULTIPOLYGON'',2154) as geom
	FROM "' || nomduschema || '"."' || nomdelatable || '";
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- Deuxi�mement : On cr�� une TABLE que de trous de plus de 10m2 :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
CREATE TABLE "' || nomduschema || '".table_de_trous_gardes AS (
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
            from "' || nomduschema || '"."' || nomdelatable || '"
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry(''MULTIPOLYGON'',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
where st_area(geom) > ' || surfacemaxaevider || '
);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL : On d�coupe la tache sans trou avec les trous gard�s
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous" AS
select row_number() over() as id,
st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_difference((st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t1.geom))),3))),(st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t2.geom))),3)))))),3))::geometry(''MultiPolygon'',2154) as geom
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
WHERE st_intersects(t1.geom,t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL BIS : On n'oublie pas les derniers polygones qui ne sont pas concern�s par des trous
req := '
INSERT INTO "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous"
select row_number() over() as id, st_multi(t1.geom)
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
where st_intersects(t1.geom,t2.geom) IS FALSE;
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- POUR FAIRE PROPRE : on supprime les tables temporaires :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous;
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous; 
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

---- A.4] V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_007_sans_petits_trous 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 1|       1|       1|     21122|
 */

---- B] Cr�aton de la couche
---- B.1] Cr�aton de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007 as 
with selection as 
			(select	source_geom
			FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_007
			group by source_geom order by source_geom)
select
	'007'::varchar(3) as insee_dep,
	'MASSIF CENTRAL'::varchar(20) as massif,
	'Zone Infra-communale concern�e par la loi n� 85-30 du 9 janvier 1985 relative au d�veloppement et � la protection de la montagne'::varchar(254) as type_zone,
	'cerema au 01/01/2019'::varchar(21) as millesime,
	array_to_string(array_agg(source_geom), ' & ', '*')::varchar(254) as sourc_geom,
	(select geom from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_007_sans_petits_trous)::Geometry('MULTIPOLYGON',2154) as geom
from selection;
--> Updated Rows	1

---- B.2] V�rification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- G�om�trie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007 WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007 WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Polygones non ferm�s :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007 WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Combien de g�om�trie :
select ST_NumGeometries(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007;
-->
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007;
--> 

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007 ADD CONSTRAINT l_loimontagne85_zinf_007_pk PRIMARY KEY (insee_dep);

---- D] V�rification visuelle et correction des pointes de zonages trop importantes
----> 1 polygone
----  V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 */

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_007;
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_007_sans_petits_trous;

---- F] Sauvegarde de la table car corrections manuelles
drop table if exists a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_007_15102021;
create table a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_007_15102021 (like a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007 including all);
insert into a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_007_15102021 select * from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_007;

