----- Travail � faire :
select insee_2019, nom_min_2019,agreg_insee_classement,agreg_nom_classement, agreg_commentaires
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
where code_dep = '019' and classement_2019 != 'totalit�';
/*
insee_2019|nom_min_2019         |agreg_insee_classement|agreg_nom_classement             |agreg_commentaires                                                                                                                                                             |
----------+---------------------+----------------------+---------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
19019     |Beaulieu-sur-Dordogne|19019,{19032}         |BEAULIEU SUR DORDOGNE, {BRIVEZAC}|01/01/2019 : Cr�ation de la commune nouvelle de Beaulieu-sur-Dordogne en lieu et place des communes de Beaulieu-sur-Dordogne (19019) et de Brivezac (19032) devenues d�l�gu�es.|
*/

----> Les pr�fixes des communes sont dans les sections etalab : oui 
SELECT id, commune , prefixe FROM r_cadastre_etalab_2021.n_sections_etalab_000_2021
where COMMUNE = '19019';
/*
id        |commune|prefixe|
----------+-------+-------+
19019000AB|19019  |000    |
19019000AC|19019  |000    |
19019000AL|19019  |000    |
19019000AD|19019  |000    |
19019032AE|19019  |032    |
19019000AE|19019  |000    |
19019000AH|19019  |000    |
19019000AI|19019  |000    |
19019000AK|19019  |000    |
19019000ZA|19019  |000    |
19019000ZB|19019  |000    |
19019032AB|19019  |032    |
19019032AC|19019  |032    |
19019032AD|19019  |032    |
19019032AH|19019  |032    |
19019032AI|19019  |032    |
19019032AK|19019  |032    |
19019032AL|19019  |032    |
19019032AM|19019  |032    |
 */

--- 19019
select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '19019' and prefixe = '032';
--> OK

---- DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 = '19019';
---- Ajout 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_sections_etalab_000_2021' as source_geom,
	(select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = '19019' and prefixe = '032') as geom,
	'true' as finalisee,
	'Fusion d�une commune non class�e et d�une commune enti�rement class�e' as detail_arrete,
	'Ancien p�rim�tre de la commune retrouv� grace aux pr�fixes de sections cadastrales : select ST_Multi(ST_MakePolygon(ST_ExteriorRing(st_union(geom)))) from r_cadastre_etalab_2021.n_sections_etalab_000_2021 where COMMUNE = ''19019'' and prefixe = ''032''' as detail_zinf,
	'21/05/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_019_2019 as t1
WHERE insee_2019 = '19019'
);
--> Updated Rows	1

---- 13/0/2021
---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_019
where classement_2019 != 'totalit�' order by insee_2019;
/*
insee_2019|nom_cog2019          |finalisee|detail_arrete                                                        |
----------+---------------------+---------+---------------------------------------------------------------------+
19019     |BEAULIEU SUR DORDOGNE|true     |Fusion d�une commune non class�e et d�une commune enti�rement class�e|
*/


---- 15/10/2021
---- A] Cr�ation du polygone
---- A.1] Cr�ation du polygone
drop table if exists  a_dcap_actu_arretes_montagne_dhup.temp_perimetre_019;
create table a_dcap_actu_arretes_montagne_dhup.temp_perimetre_019 as 
select
st_multi(ST_UNION(geom))::Geometry('MULTIPOLYGON',2154) as geom
FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_019;
--> Updated Rows	1

---- A.2] Identification des scories :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_019
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
22|       1|      22|     36567|
18|       1|      18|      6727|
14|       1|      14|      4588|
20|       1|      20|      3633|
23|       1|      23|      2380|
 2|       1|       2|      1980|
15|       1|      15|      1488|
19|       1|      19|      1407|
12|       1|      12|      1393|
31|       1|      31|       830|
28|       1|      28|       553|
 7|       1|       7|       527|
26|       1|      26|       524|
21|       1|      21|       413|
13|       1|      13|       286|
27|       1|      27|       211|
16|       1|      16|       166|
29|       1|      29|       142|
24|       1|      24|       125|
11|       1|      11|       120|
 6|       1|       6|        95|
30|       1|      30|        72|
 9|       1|       9|        65|
 3|       1|       3|        63|
 5|       1|       5|        48|
 8|       1|       8|        35|
 1|       1|       1|        29|
25|       1|      25|         8|
10|       1|      10|         8|
 4|       1|       4|         6|
17|       1|      17|         0|
 */

---- A.3] Supression des scories :
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable				character varying;
surfacemaxaevider			integer;
req 						text;
BEGIN
---- Les param�tres :
nomduschema :=  'a_dcap_actu_arretes_montagne_dhup';
nomdelatable := 'temp_perimetre_019';
surfacemaxaevider := 10000; ---- si les couches sont en coordonnn�es m�triques : en m2

---- Le Script 
---- Premi�rement : on met le polygone sans les trous
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous;
CREATE TABLE "' || nomduschema || '".table_sans_trous as
	SELECT ROW_NUMBER() OVER() AS id,
	ST_MULTI(st_makepolygon(st_exteriorring(st_geometryn(geom,1))))::geometry(''MULTIPOLYGON'',2154) as geom
	FROM "' || nomduschema || '"."' || nomdelatable || '";
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- Deuxi�mement : On cr�� une TABLE que de trous de plus de 10m2 :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
CREATE TABLE "' || nomduschema || '".table_de_trous_gardes AS (
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
            from "' || nomduschema || '"."' || nomdelatable || '"
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry(''MULTIPOLYGON'',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
where st_area(geom) > ' || surfacemaxaevider || '
);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL : On d�coupe la tache sans trou avec les trous gard�s
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous" AS
select row_number() over() as id,
st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_difference((st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t1.geom))),3))),(st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t2.geom))),3)))))),3))::geometry(''MultiPolygon'',2154) as geom
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
WHERE st_intersects(t1.geom,t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL BIS : On n'oublie pas les derniers polygones qui ne sont pas concern�s par des trous
req := '
INSERT INTO "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous"
select row_number() over() as id, st_multi(t1.geom)
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
where st_intersects(t1.geom,t2.geom) IS FALSE;
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- POUR FAIRE PROPRE : on supprime les tables temporaires :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous;
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous; 
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

---- A.4] V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_019_sans_petits_trous 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 1|       1|       1|     36567|
 */

---- B] Cr�ation de la couche
---- B.1] Cr�ation de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019 as 
with selection as 
			(select	source_geom
			FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_019
			group by source_geom order by source_geom)
select
	'019'::varchar(3) as insee_dep,
	'MASSIF CENTRAL'::varchar(20) as massif,
	'Zone Infra-communale concern�e par la loi n� 85-30 du 9 janvier 1985 relative au d�veloppement et � la protection de la montagne'::varchar(254) as type_zone,
	'cerema au 01/01/2019'::varchar(21) as millesime,
	array_to_string(array_agg(source_geom), ' & ', '*')::varchar(254) as sourc_geom,
	(select geom from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_019_sans_petits_trous)::Geometry('MULTIPOLYGON',2154) as geom
from selection;
--> Updated Rows	1

---- B.2] V�rification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- G�om�trie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019 WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019 WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Polygones non ferm�s :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019 WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne r�cup�r�e.

---- Combien de g�om�trie :
select ST_NumGeometries(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019;
-->
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019;
--> 

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019 ADD CONSTRAINT l_loimontagne85_zinf_019_pk PRIMARY KEY (insee_dep);

---- D] Supression d'un polygone en trop
----> 1 polygone
----  V�rification du r�sultat :
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019 
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk|id_tache|num_trou|surface_m2|
--+--------+--------+----------+
 */

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_019;
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_019_sans_petits_trous;

---- F] Sauvegarde de la table car corrections manuelles
drop table if exists a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_019_15102021;
create table a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_019_15102021 (like a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019 including all);
insert into a_dcap_actu_arretes_montagne_dhup.sauve_l_loimontagne85_zinf_019_15102021 select * from a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_019;