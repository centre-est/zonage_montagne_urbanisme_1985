---- 03/03/2021

---- A] Import de la couche de la DDT 69
---- Import de \\isere\DIRECTIONS\IDA_PCI\Montagne\09_Affaires\2020\DHUP_Actualisation_arretes_montagne\3_Travail\Partie2_couche_carto\069\livraison_2_ddt
---- Par un glisser d�plac� depuis QGIS

ALTER TABLE a_dcap_actu_arretes_montagne_dhup."l_montagne_zinf_069-v2" RENAME TO l_montagne_zinf_069_2020;

with decoup_bdtopo as (
		---- Je d�coupe 
		select t1.code_insee , (ST_DUMP(ST_Intersection(st_transform(t1.geom,2154), t2.geom))).geom as dump_geom
		from r_bdtopo_2020.n_commune_bdt_fra_2020 as t1
		join a_dcap_actu_arretes_montagne_dhup.l_montagne_zinf_069_2020 as t2
		on ST_Intersects(st_transform(t1.geom,2154), t2.geom))
select dump_geom, st_area(dump_geom) from decoup_bdtopo
order by st_area(dump_geom) DESC;


insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 
(with decoup_bdtopo as (
		---- Je d�coupe 
		select t1.code_insee , (ST_DUMP(ST_Intersection(st_transform(t1.geom,2154), t2.geom))).geom as geom
		from r_bdtopo_2020.n_commune_bdt_fra_2020 as t1
		join a_dcap_actu_arretes_montagne_dhup.l_montagne_zinf_069_2020 as t2
		on ST_Intersects(st_transform(t1.geom,2154), t2.geom)
		)
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_commune_bdt_fra_2020' as source_geom,
	t2.geom,
	'true' as finalisee,
	agreg_commentaires as detail_arrete,
	'Compl�t� par l�Arr�t� Pr�fectoral n�1519 du 16 octobre 1985 et zonage cadastral founi par la DDT du Rh�ne' as detail_zinf,
	'03/03/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_069_2019 as t1
join decoup_bdtopo as t2
on t1.insee_2019 = t2.code_insee
where st_area(t2.geom)> 30000 and classement_2019 = 'partie');

---- 17/06/2021
---- Int�gration de la livraison d�finitive
----- Travail � faire :
select *, insee_2019, nom_min_2019,agreg_insee_classement,agreg_nom_classement, agreg_commentaires
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
where code_dep = '069' and classement_2019 != 'totalit�';
/*
code_dep|insee_2019|nom_cog2019          |nom_min_2019         |nb_evenements_cog|agreg_insee_classement   |agreg_nom_classement                                                |agreg_arretes                                          |agreg_classement_85|classement_2019|agreg_commentaires                                                                                                                                                                                                                                             |source_geom                   |geom                                                                                                                                                                                                                                                           |insee_2019|nom_min_2019         |agreg_insee_classement   |agreg_nom_classement                                                |agreg_commentaires                                                                                                                                                                                                                                             |
--------+----------+---------------------+---------------------+-----------------+-------------------------+--------------------------------------------------------------------+-------------------------------------------------------+-------------------+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------+---------------------+-------------------------+--------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
069     |69018     |BEAUJEU              |Beaujeu              |                0|69018                    |BEAUJEU                                                             |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1974�: Sections A, B, E1, E2, F�Arr�t� de 1985�: Sections AR en partie, AP en partie, AO en partie                                                                                                                                                   |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((821613.7 6566857.1, 821623.4 6566858.5, 821985.7 6566485.3, 821982.4 6566473.5, 822204.5 6566033.4, 822247.1 6565980.2, 822305.2 6565924.5, 822409.6 6565858.3, 822511.8 6565805.6, 822601.6 6565774.8, 822655.9 6565770, 822943.3 6565877.4, 8|69018     |Beaujeu              |69018                    |BEAUJEU                                                             |Arr�t� de 1974�: Sections A, B, E1, E2, F�Arr�t� de 1985�: Sections AR en partie, AP en partie, AO en partie                                                                                                                                                   |
069     |69021     |BESSENAY             |Bessenay             |                0|69021                    |BESSENAY                                                            |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1976�: Section F1, F2 et E en partie, Lieux-dits�: Le moulin � Vent, Le Vernay�Arr�t� de 1985�: Sections E, D et A en partie�; C1 en partie, B2 en partie, B1 en partie                                                                              |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((819033.5 6522356.6, 819071.2 6522342.1, 819154.5 6522333.9, 819200.9 6522302.6, 819255.5 6522299.9, 819284 6522316.5, 819300.7 6522308.7, 819325.1 6522276.8, 819409.4 6522273.4, 819420.1 6522264.1, 819425.3 6522231.3, 819479 6522183.7, 819|69021     |Bessenay             |69021                    |BESSENAY                                                            |Arr�t� de 1976�: Section F1, F2 et E en partie, Lieux-dits�: Le moulin � Vent, Le Vernay�Arr�t� de 1985�: Sections E, D et A en partie�; C1 en partie, B2 en partie, B1 en partie                                                                              |
069     |69022     |BIBOST               |Bibost               |                0|69022                    |BIBOST                                                              |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1974�: Sections A2, A3, A4 et A5 en partie, lieudit�: Le Treve�Arr�t� de 1985�: Sections A1, A5 en partie, B1 en partie, B2 en partie, B3 en partie et B4 en partie                                                                                  |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((821903.5 6522418.4, 821867.5 6522426.3, 821837.4 6522381.6, 821762.5 6522361.3, 821708.1 6522313.4, 821681.7 6522362.2, 821662.9 6522363.4, 821619.6 6522304.8, 821600.8 6522297.4, 821557.6 6522298.4, 821487.5 6522242, 821443.5 6522262.2, 8|69022     |Bibost               |69022                    |BIBOST                                                              |Arr�t� de 1974�: Sections A2, A3, A4 et A5 en partie, lieudit�: Le Treve�Arr�t� de 1985�: Sections A1, A5 en partie, B1 en partie, B2 en partie, B3 en partie et B4 en partie                                                                                  |
069     |69051     |CHAUSSAN             |Chaussan             |                0|69051                    |CHAUSSAN                                                            |28/04/1976                                             |P                  |partie         |Sections C, D et B en partie, sauf Hameau Brunzieux                                                                                                                                                                                                            |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((825561.2 6504234.8, 825516.8 6504293.6, 825475.4 6504330.6, 825210 6504634.8, 825436.9 6505349.2, 825378.1 6505670.2, 826065.8 6505926.5, 826384.1 6505994.3, 826737.4 6506175.2, 827338.6 6506748.5, 827755.6 6506951.4, 828122.6 6507035.4, 8|69051     |Chaussan             |69051                    |CHAUSSAN                                                            |Sections C, D et B en partie, sauf Hameau Brunzieux                                                                                                                                                                                                            |
069     |69058     |CHIROUBLES           |Chiroubles           |                0|69058                    |CHIROUBLES                                                          |28/04/1976                                             |P                  |partie         |Section D                                                                                                                                                                                                                                                      |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((828456.9 6567659.9, 828475.8 6567654.9, 828520.7 6567556.3, 828584.6 6567515, 829082.3 6566895.3, 829315.6 6566480.5, 829365.3 6566427.2, 829417.5 6566333, 829527.7 6566108.4, 829630.9 6565537.2, 829541.2 6564938.7, 829346.9 6565015.2, 828|69058     |Chiroubles           |69058                    |CHIROUBLES                                                          |Section D                                                                                                                                                                                                                                                      |
069     |69104     |JULLIE               |Julli�               |                0|69104                    |JULLIE                                                              |28/04/1976                                             |P                  |partie         |Sections A, B1 et B2                                                                                                                                                                                                                                           |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((830724.6 6571594.9, 830478.4 6571384, 830328.5 6571397.1, 830243.8 6571440.7, 830181.6 6571456.4, 830087.1 6571468, 829991.6 6571468.9, 829914.2 6571485.7, 829831.2 6571410.6, 829819.7 6571378.1, 829776 6571313.6, 829765.1 6571308.6, 82972|69104     |Julli�               |69104                    |JULLIE                                                              |Sections A, B1 et B2                                                                                                                                                                                                                                           |
069     |69124     |MARCHAMPT            |Marchampt            |                0|69124                    |MARCHAMPT                                                           |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1976�: Sections A1, C1, D, E1 et E2�Arr�t� de 1985�: Sections�: AC en partie, AI en partie                                                                                                                                                           |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((819838.8 6555133.4, 819906.6 6555257.5, 819872.5 6555413.2, 819853 6555445.1, 819787.5 6555779.4, 819379.7 6556052.6, 819348.6 6556100.1, 819276 6556178.9, 819167.1 6556344, 819129.4 6556433.3, 819122.6 6556433.2, 819117.7 6556445.7, 81912|69124     |Marchampt            |69124                    |MARCHAMPT                                                           |Arr�t� de 1976�: Sections A1, C1, D, E1 et E2�Arr�t� de 1985�: Sections�: AC en partie, AI en partie                                                                                                                                                           |
069     |69154     |POLLIONNAY           |Pollionnay           |                0|69154                    |POLLIONNAY                                                          |28/04/1976                                             |P                  |partie         |Sections AL, AM et AN                                                                                                                                                                                                                                          |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((831646.3 6519219.6, 831683.6 6519183.9, 831730.5 6519160.3, 831751.9 6519138.5, 831890.5 6519062.6, 831911.3 6519040.8, 831919 6519010.3, 831954 6518973.1, 831983.3 6518958.4, 832050.1 6518896.5, 832082.4 6518882.3, 832076.1 6518848.5, 832|69154     |Pollionnay           |69154                    |POLLIONNAY                                                          |Sections AL, AM et AN                                                                                                                                                                                                                                          |
069     |69157     |VINDRY SUR TURDINE   |Vindry-sur-Turdine   |                4|{69157,69073,69147,69223}|{PONTCHARRA-SUR-TURDINE, DAREIZE, LES OLMES, SAINT-LOUP}            |{25/07/1985,28/04/1976 18/01/197,non class�,28/04/1976}|{P,P,non class�e,P}|partie         |{"Section 3 en partie","Arr�t� de 1976�: Sections A1, A2 en partie�: Le Creux","","Section A1 et lieudit�: Le Cret-du-Pay sur les sections A2 et B1"} - 01/01/2019 : Cr�ation de la commune nouvelle de Vindry-sur-Turdine en lieu et place des communes de Dar|n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((817727 6531995.2, 817461.1 6531752.2, 817071.3 6531476.8, 817012.7 6531486.7, 816973.8 6531482.1, 816907.4 6531444.5, 816871 6531437.7, 816850.1 6531384.1, 816846.6 6531342.3, 816830.8 6531326.9, 816821.9 6531207.6, 816806.4 6531136.5, 816|69157     |Vindry-sur-Turdine   |{69157,69073,69147,69223}|{PONTCHARRA-SUR-TURDINE, DAREIZE, LES OLMES, SAINT-LOUP}            |{"Section 3 en partie","Arr�t� de 1976�: Sections A1, A2 en partie�: Le Creux","","Section A1 et lieudit�: Le Cret-du-Pay sur les sections A2 et B1"} - 01/01/2019 : Cr�ation de la commune nouvelle de Vindry-sur-Turdine en lieu et place des communes de Dar|
069     |69167     |RIVOLET              |Rivolet              |                0|69167                    |RIVOLET                                                             |28/04/1976                                             |P                  |partie         |Section A, Section B                                                                                                                                                                                                                                           |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((825826.1 6545679.4, 825804.6 6545658.2, 825687.3 6545482.1, 825699 6545407.3, 825653.1 6545403.7, 825577.1 6545357.8, 825498.4 6545347.1, 825459.9 6545330.2, 825381 6545329.8, 825334.1 6545351.4, 825238.2 6545376.6, 825190.6 6545351.4, 825|69167     |Rivolet              |69167                    |RIVOLET                                                             |Section A, Section B                                                                                                                                                                                                                                           |
069     |69170     |RONTALON             |Rontalon             |                0|69170                    |RONTALON                                                            |28/04/1976�25/07/1985                                  |P                  |partie         |Ar�t� de 1976�: en totalit� sauf les sections AH et AI�Arr�t� de 1985�: Section AI                                                                                                                                                                             |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((823921.4 6506539.9, 823968.7 6506564.7, 824035.5 6506580.3, 824037.9 6506592.7, 824022.6 6506610.1, 823963 6506642, 823918.2 6506769.3, 823928.1 6506789.9, 823996.4 6506851.5, 824025.4 6506908.3, 824023.8 6506935.9, 824002.8 6506976.4, 823|69170     |Rontalon             |69170                    |RONTALON                                                            |Ar�t� de 1976�: en totalit� sauf les sections AH et AI�Arr�t� de 1985�: Section AI                                                                                                                                                                             |
069     |69175     |SAVIGNY              |Savigny              |                0|69175                    |SAVIGNY                                                             |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1976�: Sections E1, E2 et E3�Arr�t� de 1985�: Sections C2 en partie�; C3 en partie�; D2 en partie�; D3, D4, F, F2, F3, F4                                                                                                                            |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((819717.3 6524306.1, 819702 6524354.3, 819555.4 6524589.4, 819519.2 6524682.2, 819458.5 6524774.8, 819441.6 6524784.8, 819411.6 6524788, 819310.7 6524860.3, 819268.7 6524912.5, 819223.1 6524948.2, 819190.6 6524943.9, 819093.1 6524999.8, 818|69175     |Savigny              |69175                    |SAVIGNY                                                             |Arr�t� de 1976�: Sections E1, E2 et E3�Arr�t� de 1985�: Sections C2 en partie�; C3 en partie�; D2 en partie�; D3, D4, F, F2, F3, F4                                                                                                                            |
069     |69177     |SOURCIEUX LES MINES  |Sourcieux-les-Mines  |                0|69177                    |SOURCIEUX-LES-MINES                                                 |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1976�: Sections C2 et D1�Arr�t� de 1985�: Sections B2, C1, D2, D3, et A en partie                                                                                                                                                                    |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((826701.5 6525412.3, 826846 6524743.2, 827201.3 6524356.3, 828624.2 6523356.6, 828738.4 6522626, 828685.7 6522550.5, 828648.3 6522516, 828566.4 6522489.5, 828488.2 6522481.5, 828343.5 6522385.8, 828299.6 6522304.5, 828279.3 6522279.6, 82823|69177     |Sourcieux-les-Mines  |69177                    |SOURCIEUX-LES-MINES                                                 |Arr�t� de 1976�: Sections C2 et D1�Arr�t� de 1985�: Sections B2, C1, D2, D3, et A en partie                                                                                                                                                                    |
069     |69228     |CHANABIERE           |Chabani�re           |                3|{69228,69195,69237}      |{SAINT-MAURICE-SUR-DARGOIRE,SAINT-DIDIER-SOUS-RIVERIE,SAINT-SORLIN}�|{non class�, 28/04/1976 25/07/1985,28/04/1976}�        |{non class�e,P,P}  |partie         |{,"Arr�t� de 1976�: Sections A, C, D, B1, B2 en partie, lieux-dits�: La Ronze, Jorson et Longchamp, parcelles 260 � 280 et B3 en partie, Lieudit�: Les Loys, parcelles 563 � 567�; Les Guerins, parcelles 551 � 553 et 556 � 562","Sections D1, D2, D3, C1, C2,|n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((829479.2 6495127.6, 829457.3 6495073.2, 829379.5 6495001.4, 829318.9 6494974.3, 829203.4 6494952.4, 829121.7 6494947.1, 828987.1 6495027.1, 828875.7 6494859.4, 828867.5 6494836.5, 828831.3 6494813.2, 828810.8 6494787.6, 828791.9 6494781.5,|69228     |Chabani�re           |{69228,69195,69237}      |{SAINT-MAURICE-SUR-DARGOIRE,SAINT-DIDIER-SOUS-RIVERIE,SAINT-SORLIN}�|{,"Arr�t� de 1976�: Sections A, C, D, B1, B2 en partie, lieux-dits�: La Ronze, Jorson et Longchamp, parcelles 260 � 280 et B3 en partie, Lieudit�: Les Loys, parcelles 563 � 567�; Les Guerins, parcelles 551 � 553 et 556 � 562","Sections D1, D2, D3, C1, C2,|
069     |69234     |SAINT ROMAIN DE POPEY|Saint-Romain-de-Popey|                0|69234                    |SAINT-ROMAIN-DE-POPEY                                               |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1974�: Section C3 en partie, lieudit�: Teilloux, Bois de Teilloux, Bois de Varenne, Le Bois de la Combe, Bois-Simon, Le Cret�Arr�t� de 1985�(Ecrit Saint-Romain-en -Popey)�: Sections C1 en partie�; C2 en partie�; C3 en partie, D2 en partie�;     |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((816067.1 6529459.6, 816037 6529761.5, 816223.8 6530437, 816538.6 6530810, 816815.3 6531109.4, 816806.4 6531136.5, 816821.9 6531207.6, 816830.8 6531326.9, 816846.6 6531342.3, 816850.1 6531384.1, 816871 6531437.7, 816907.4 6531444.5, 816973.|69234     |Saint-Romain-de-Popey|69234                    |SAINT-ROMAIN-DE-POPEY                                               |Arr�t� de 1974�: Section C3 en partie, lieudit�: Teilloux, Bois de Teilloux, Bois de Varenne, Le Bois de la Combe, Bois-Simon, Le Cret�Arr�t� de 1985�(Ecrit Saint-Romain-en -Popey)�: Sections C1 en partie�; C2 en partie�; C3 en partie, D2 en partie�;     |
069     |69249     |THURINS              |Thurins              |                0|69249                    |THURINS                                                             |28/04/1976�25/07/1985                                  |P                  |partie         |Arr�t� de 1976�: Sections AR et AT en totalit�, AP en partie, AS en partie, AH en partie, AE en partie, AD en partie�; section AP en partie lieux-dits�: La Martini�re d�en Haut, Roche-Saint-Martin, Bois-Renard, Le Combard�; Section AS en partie, Lieux-dit|n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((830360 6510204.1, 830008.1 6510127.7, 829640.3 6509824.4, 829306.9 6509399.2, 829185.1 6509083.3, 829149.2 6508887.6, 828810.6 6507874.2, 828249.3 6508586.1, 827822.4 6508780.4, 827482.5 6509151.8, 827070.8 6509171.6, 826862 6509207.3, 826|69249     |Thurins              |69249                    |THURINS                                                             |Arr�t� de 1976�: Sections AR et AT en totalit�, AP en partie, AS en partie, AH en partie, AE en partie, AD en partie�; section AP en partie lieux-dits�: La Martini�re d�en Haut, Roche-Saint-Martin, Bois-Renard, Le Combard�; Section AS en partie, Lieux-dit|
069     |69255     |VAUGNERAY            |Vaugneray            |                0|{69255,69221}            |{VAUGNERAY,SAINT-LAURENT-DE-VAUX}                                   |{28/04/1976�25/07/1985,28/04/1976}                     |{P,totalit�}       |partie         |Arr�t� de 1976�: Sections E, G2, I (mal ecrit � v�rifier), K, F1 en partie et H en partie, F1 en partie, lieux-dits�: La�s, Bel-Air, Taconant, L�Eveque, H en partie, lieux-dits�: Au-Dessus de la Chana, Les Roches, Rochetrouille, Clavigny, Combe-Fusil-en-P|n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((832066.4 6516245.1, 831977.7 6516146.9, 831945.7 6516124.5, 831857 6515992, 831814.4 6515996.1, 831712.3 6516037.1, 831670.4 6516024.5, 831655.7 6515989.2, 831672.6 6515881.5, 831670 6515848.5, 831658.1 6515831.9, 831635.7 6515824.5, 83158|69255     |Vaugneray            |{69255,69221}            |{VAUGNERAY,SAINT-LAURENT-DE-VAUX}                                   |Arr�t� de 1976�: Sections E, G2, I (mal ecrit � v�rifier), K, F1 en partie et H en partie, F1 en partie, lieux-dits�: La�s, Bel-Air, Taconant, L�Eveque, H en partie, lieux-dits�: Au-Dessus de la Chana, Les Roches, Rochetrouille, Clavigny, Combe-Fusil-en-P|
069     |69258     |VAUXRENARD           |Vauxrenard           |                0|69258                    |VAUXRENARD                                                          |28/04/1976                                             |P                  |partie         |Sections A, AD, AC, AO, AP, AR, AS et G                                                                                                                                                                                                                        |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((824018.5 6570213, 824057.1 6570390.6, 824026.4 6570478.8, 824026 6570565.6, 824055 6570625.4, 824006.1 6570689.8, 824047.6 6570719.4, 824116.9 6570857, 824158.9 6570912.9, 824389.4 6571445.8, 824621.6 6571487.8, 824689.6 6571484.9, 824780.|69258     |Vauxrenard           |69258                    |VAUXRENARD                                                          |Sections A, AD, AC, AO, AP, AR, AS et G                                                                                                                                                                                                                        |
069     |69267     |VILLIE MORGON        |Villi�-Morgon        |                0|69267                    |VILLIE-MORGON                                                       |25/07/1985                                             |P                  |partie         |Section AB en partie                                                                                                                                                                                                                                           |n_adm_exp_cog_commune_000_2019|MULTIPOLYGON (((829356 6560834.6, 829323.9 6560832.7, 829113.6 6561071.2, 829057.8 6561145.9, 828936.4 6561359, 828894.8 6561448.4, 828877.3 6561508.5, 828853 6561509.8, 827914.3 6562201, 827275.6 6562757.3, 827271.6 6562789.9, 827283.3 6562836.7, 827264.|69267     |Villi�-Morgon        |69267                    |VILLIE-MORGON                                                       |Section AB en partie                                                                                                                                                                                                                                           |
 */

DELETE from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 WHERE insee_2019 like '69%';
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	code_dep,
	insee_2019,
	nom_cog2019,
	nom_min_2019,
	nb_evenements_cog,
	agreg_insee_classement,
	agreg_nom_classement,
	agreg_arretes,
	agreg_classement_85,
	classement_2019,
	agreg_commentaires,
	'n_commune_bdt_fra_2020' as source_geom,
	st_transform(t2.geom,2154),
	'true' as finalisee,
	'Compl�t� par l�Arr�t� Pr�fectoral n�1519 du 16 octobre 1985 et zonage cadastral founi par la DDT du Rh�ne' as detail_zinf,
	null as detail_zinf,
	'17/06/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 as t1
join r_bdtopo_2020.n_commune_bdt_fra_2020 as t2
on t1.insee_2019 = t2.code_insee
where code_dep = '069' and classement_2019 = 'totalit�'
);
--> Updated Rows	77

---- Import de la livraison de la commune par un glisser d�plac� :
select * from  a_dcap_actu_arretes_montagne_dhup.l_montagne_detail_zinf_069 where  right(to_char(code_insee,'99999'),5) like '69%'

insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	t1.code_dep,
	t1.insee_2019,
	t1.nom_cog2019,
	t1.nom_min_2019,
	t1.nb_evenements_cog,
	t1.agreg_insee_classement,
	t1.agreg_nom_classement,
	t1.agreg_arretes,
	t1.agreg_classement_85,
	t1.classement_2019,
	t1.agreg_commentaires,
	t2.sourc_geom as source_geom,
	t2.geom,
	'true' as finalisee,
	'Compl�t� par l�Arr�t� Pr�fectoral n�1519 du 16 octobre 1985 et zonage cadastral founi par la DDT du Rh�ne' as detail_zinf,
	case
	when t2.methode_ = 'Requ�te' and t2.parc_orig is not null and t2.parc_2020 is not null then 'Requ�te : Parcelle origine ' || t2.parc_orig || ' ==> Parcelle 2020 : ' || t2.parc_2020 
	when t2.methode_ = 'Requ�te' and t2.parc_orig is not null and t2.parc_2020 is null then 'Requ�te : Parcelle origine : ' || t2.parc_orig
	when t2.methode_ = 'Requ�te' and t2.parc_orig is null and t2.parc_2020 is not null then 'Requ�te : Parcelle 2020 : ' || t2.parc_2020
	when t2.methode_ = 'Requ�te' and t2.parc_orig is null and t2.parc_2020 is null then 'Requ�te' 
	else t2.methode_
	end as detail_zinf,
	'17/06/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 as t1 
join a_dcap_actu_arretes_montagne_dhup.l_montagne_detail_zinf_069 as t2
on t1.insee_2019 = right(to_char(t2.code_insee,'99999'),5)
where t1.code_dep = '069' and t1.classement_2019 = 'partie'
);
--> Updated Rows	2952

---- {69157,69073,69147,69223} devient 69157 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	t1.code_dep,
	t1.insee_2019,
	t1.nom_cog2019,
	t1.nom_min_2019,
	t1.nb_evenements_cog,
	t1.agreg_insee_classement,
	t1.agreg_nom_classement,
	t1.agreg_arretes,
	t1.agreg_classement_85,
	t1.classement_2019,
	t1.agreg_commentaires,
	t2.sourc_geom as source_geom,
	t2.geom,
	'true' as finalisee,
	'Compl�t� par l�Arr�t� Pr�fectoral n�1519 du 16 octobre 1985 et zonage cadastral founi par la DDT du Rh�ne' as detail_zinf,
	case
	when t2.methode_ = 'Requ�te' and t2.parc_orig is not null and t2.parc_2020 is not null then 'Requ�te : Parcelle origine ' || t2.parc_orig || ' ==> Parcelle 2020 : ' || t2.parc_2020 
	when t2.methode_ = 'Requ�te' and t2.parc_orig is not null and t2.parc_2020 is null then 'Requ�te : Parcelle origine : ' || t2.parc_orig
	when t2.methode_ = 'Requ�te' and t2.parc_orig is null and t2.parc_2020 is not null then 'Requ�te : Parcelle 2020 : ' || t2.parc_2020
	when t2.methode_ = 'Requ�te' and t2.parc_orig is null and t2.parc_2020 is null then 'Requ�te' 
	else t2.methode_
	end as detail_zinf,
	'17/06/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 as t1 
join a_dcap_actu_arretes_montagne_dhup.l_montagne_detail_zinf_069 as t2
on ST_Intersects(t1.geom,ST_Centroid(t2.geom))
where t1.insee_2019 = '69157'
);
--> Updated Rows	251

---- {69228,69195,69237} devient 69228 
insert into a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_000 (
select
	t1.code_dep,
	t1.insee_2019,
	t1.nom_cog2019,
	t1.nom_min_2019,
	t1.nb_evenements_cog,
	t1.agreg_insee_classement,
	t1.agreg_nom_classement,
	t1.agreg_arretes,
	t1.agreg_classement_85,
	t1.classement_2019,
	t1.agreg_commentaires,
	t2.sourc_geom as source_geom,
	t2.geom,
	'true' as finalisee,
	'Compl�t� par l�Arr�t� Pr�fectoral n�1519 du 16 octobre 1985 et zonage cadastral founi par la DDT du Rh�ne' as detail_zinf,
	case
	when t2.methode_ = 'Requ�te' and t2.parc_orig is not null and t2.parc_2020 is not null then 'Requ�te : Parcelle origine ' || t2.parc_orig || ' ==> Parcelle 2020 : ' || t2.parc_2020 
	when t2.methode_ = 'Requ�te' and t2.parc_orig is not null and t2.parc_2020 is null then 'Requ�te : Parcelle origine : ' || t2.parc_orig
	when t2.methode_ = 'Requ�te' and t2.parc_orig is null and t2.parc_2020 is not null then 'Requ�te : Parcelle 2020 : ' || t2.parc_2020
	when t2.methode_ = 'Requ�te' and t2.parc_orig is null and t2.parc_2020 is null then 'Requ�te' 
	else t2.methode_
	end as detail_zinf,
	'17/06/2021' as maj
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 as t1 
join a_dcap_actu_arretes_montagne_dhup.l_montagne_detail_zinf_069 as t2
on ST_Intersects(t1.geom,ST_Centroid(t2.geom))
where t1.insee_2019 = '69228'
);
--> Updated Rows	14

---- 13/08/2021
---- travail r�alis� :
select insee_2019, nom_cog2019, finalisee, detail_arrete--, detail_zinf
from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_zinf_069
where classement_2019 != 'totalit�' order by insee_2019;
/*
imossible � afficher
*/


---- 18/10/2021
---- A] Import de la couche de la DDT 69
---- Import de \\isere\DIRECTIONS\IDA_PCI\Montagne\09_Affaires\2020\DHUP_Actualisation_arretes_montagne\3_Travail\Partie2_couche_carto\069\livraison_2_ddt\l_montagne_zinf_069
---- Par un glisser d�plac� depuis QGIS

---- A] V�rification :
UPDATE a_dcap_actu_arretes_montagne_dhup.l_montagne_zinf_069 SET geom=
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
--> Updated Rows	0

---- B] Cr�aton de la couche
---- B.1] Cr�aton de la couche
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069;
create table a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069 as 
			select
			'069'::varchar(3) as insee_dep,
			'MASSIF CENTRAL'::varchar(20) as massif,
			'Zone Infra-communale concern�e par la loi n� 85-30 du 9 janvier 1985 relative au d�veloppement et � la protection de la montagne'::varchar(254) as type_zone,
			'cerema au 01/01/2019'::varchar(21) as millesime,
			'Cadastre DGFiP et DDT du Rh�ne' as sourc_geom,
			(select st_multi(ST_union(geom))::Geometry('MULTIPOLYGON',2154) as geom from a_dcap_actu_arretes_montagne_dhup.l_montagne_zinf_069)
--> Updated Rows	1

---- B.2] V�rification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
/*
ST_SRID|GeometryType|ST_GeometryType|ST_CoordDim|ST_NDims|count|
-------+------------+---------------+-----------+--------+-----+
   2154|MULTIPOLYGON|ST_MultiPolygon|          2|       2|    1|
 */
---- G�om�trie vide :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069 WHERE ST_IsEmpty(geom);
/*
insee_dep|massif|type_zone|millesime|geom|
---------+------+---------+---------+----+
 */

---- Points anormaux :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069 WHERE NOT ST_IsSimple(geom);
/*
insee_dep|massif|type_zone|millesime|geom|
---------+------+---------+---------+----+
 */

---- Polygones non ferm�s :
SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069 WHERE NOT ST_IsClosed(geom);
/*
insee_dep|massif|type_zone|millesime|geom|
---------+------+---------+---------+----+
*/
---- Combien de g�om�trie :
select sum(ST_NumGeometries(geom)) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069;
--> 3
select ST_NumInteriorRings(geom) FROM a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069;
/*
st_numinteriorrings|
-------------------+
                   |
 */

---- C] Optimisation
---- C.1] Pk
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_loimontagne85_zinf_069 ADD CONSTRAINT l_loimontagne85_zinf_069_pk PRIMARY KEY (insee_dep);

---- D] V�rification visuelle et correction des pointes de zonages trop importantes

---- E] Correction des tables temporaires
drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_069;
----drop table if exists a_dcap_actu_arretes_montagne_dhup.temp_perimetre_069_sans_petits_trous;
