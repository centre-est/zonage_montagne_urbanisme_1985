---- 20/08/2020
----- 068 ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068  WHERE date_decis IS NULL;
--> 0

--UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	90

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068;
--> 98

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019 |insee_commune_arrete_classement|nom_commune_arrete_classement|arrete_classement      |classement_commune |commentaires                                    |nb_evenements_cog|
-------------|--------------------|-------------------------------|-----------------------------|-----------------------|-------------------|------------------------------------------------|-----------------|
68106        |GOLDBACH ALTENBACH  |{68106}                        |{GOLDBACH-ALTENBACH}         |{1974-02-20}           |{totalit�}         |{"Ecrit ALTENBACH-GOLDBACH dans arr�t� de 1974"}|                1|
68201        |MASEVAUX NIEDERBRUCK|{68201,68233}                  |{MASEVAUX,NIEDERBRUCK}       |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                     |                2|
68350        |VOEGTLINSHOFFEN     |{68350}                        |{VOEGTLINSHOFFEN}            |{1982-01-29}           |{P}                |{"Section A"}                                   |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 94 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	97

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '68' ---068
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
68014        |68014         |68014                          |{68014}           |                0|                    1|
68025        |68025         |68025                          |{68025}           |                0|                    1|
68035        |68035         |68035                          |{68035}           |                0|                    1|
68040        |68040         |68040                          |{68040}           |                0|                    1|
68044        |68044         |68044                          |{68044}           |                0|                    1|
68045        |68045         |68045                          |{68045}           |                0|                    1|
68046        |68046         |68046                          |{68046}           |                0|                    1|
68049        |68049         |68049                          |{68049}           |                0|                    1|
68051        |68051         |68051                          |{68051}           |                0|                    1|
68058        |68058         |68058                          |{68058}           |                0|                    1|
68067        |68067         |68067                          |{68067}           |                0|                    1|
68073        |68073         |68073                          |{68073}           |                0|                    1|
68074        |68074         |68074                          |{68074}           |                0|                    1|
68083        |68083         |68083                          |{68083}           |                0|                    1|
68089        |68089         |68089                          |{68089}           |                0|                    1|
68090        |68090         |68090                          |{68090}           |                0|                    1|
68092        |68092         |68092                          |{68092}           |                0|                    1|
68097        |68097         |68097                          |{68097}           |                0|                    1|
68102        |68102         |68102                          |{68102}           |                0|                    1|
68106        |68106         |{68106}                        |{68106}           |                1|                    1|
68109        |68109         |68109                          |{68109}           |                0|                    1|
68111        |68111         |68111                          |{68111}           |                0|                    1|
68112        |68112         |68112                          |{68112}           |                0|                    1|
68117        |68117         |68117                          |{68117}           |                0|                    1|
68123        |68123         |68123                          |{68123}           |                0|                    1|
68142        |68142         |68142                          |{68142}           |                0|                    1|
68151        |68151         |68151                          |{68151}           |                0|                    1|
68165        |68165         |68165                          |{68165}           |                0|                    1|
68167        |68167         |68167                          |{68167}           |                0|                    1|
68169        |68169         |68169                          |{68169}           |                0|                    1|
68171        |68171         |68171                          |{68171}           |                0|                    1|
68173        |68173         |68173                          |{68173}           |                0|                    1|
68175        |68175         |68175                          |{68175}           |                0|                    1|
68177        |68177         |68177                          |{68177}           |                0|                    1|
68178        |68178         |68178                          |{68178}           |                0|                    1|
68181        |68181         |68181                          |{68181}           |                0|                    1|
68184        |68184         |68184                          |{68184}           |                0|                    1|
68185        |68185         |68185                          |{68185}           |                0|                    1|
68186        |68186         |68186                          |{68186}           |                0|                    1|
68188        |68188         |68188                          |{68188}           |                0|                    1|
68190        |68190         |68190                          |{68190}           |                0|                    1|
68193        |68193         |68193                          |{68193}           |                0|                    1|
68194        |68194         |68194                          |{68194}           |                0|                    1|
68199        |68199         |68199                          |{68199}           |                0|                    1|
68201        |68201         |{68201,68233}                  |{68201,68233}     |                2|                    2|
68204        |68204         |68204                          |{68204}           |                0|                    1|
68210        |68210         |68210                          |{68210}           |                0|                    1|
68211        |68211         |68211                          |{68211}           |                0|                    1|
68212        |68212         |68212                          |{68212}           |                0|                    1|
68213        |68213         |68213                          |{68213}           |                0|                    1|
68217        |68217         |68217                          |{68217}           |                0|                    1|
68223        |68223         |68223                          |{68223}           |                0|                    1|
68226        |68226         |68226                          |{68226}           |                0|                    1|
68229        |68229         |68229                          |{68229}           |                0|                    1|
68239        |68239         |68239                          |{68239}           |                0|                    1|
68243        |68243         |68243                          |{68243}           |                0|                    1|
68247        |68247         |68247                          |{68247}           |                0|                    1|
68248        |68248         |68248                          |{68248}           |                0|                    1|
68249        |68249         |68249                          |{68249}           |                0|                    1|
68251        |68251         |68251                          |{68251}           |                0|                    1|
68255        |68255         |68255                          |{68255}           |                0|                    1|
68259        |68259         |68259                          |{68259}           |                0|                    1|
68261        |68261         |68261                          |{68261}           |                0|                    1|
68262        |68262         |68262                          |{68262}           |                0|                    1|
68274        |68274         |68274                          |{68274}           |                0|                    1|
68275        |68275         |68275                          |{68275}           |                0|                    1|
68276        |68276         |68276                          |{68276}           |                0|                    1|
68283        |68283         |68283                          |{68283}           |                0|                    1|
68287        |68287         |68287                          |{68287}           |                0|                    1|
68292        |68292         |68292                          |{68292}           |                0|                    1|
68294        |68294         |68294                          |{68294}           |                0|                    1|
68298        |68298         |68298                          |{68298}           |                0|                    1|
68307        |68307         |68307                          |{68307}           |                0|                    1|
68308        |68308         |68308                          |{68308}           |                0|                    1|
68311        |68311         |68311                          |{68311}           |                0|                    1|
68312        |68312         |68312                          |{68312}           |                0|                    1|
68315        |68315         |68315                          |{68315}           |                0|                    1|
68316        |68316         |68316                          |{68316}           |                0|                    1|
68317        |68317         |68317                          |{68317}           |                0|                    1|
68318        |68318         |68318                          |{68318}           |                0|                    1|
68328        |68328         |68328                          |{68328}           |                0|                    1|
68329        |68329         |68329                          |{68329}           |                0|                    1|
68334        |68334         |68334                          |{68334}           |                0|                    1|
68335        |68335         |68335                          |{68335}           |                0|                    1|
68344        |68344         |68344                          |{68344}           |                0|                    1|
68347        |68347         |68347                          |{68347}           |                0|                    1|
68350        |68350         |{68350}                        |{68350}           |                1|                    1|
68354        |68354         |68354                          |{68354}           |                0|                    1|
68358        |68358         |68358                          |{68358}           |                0|                    1|
68359        |68359         |68359                          |{68359}           |                0|                    1|
68361        |68361         |68361                          |{68361}           |                0|                    1|
68368        |68368         |68368                          |{68368}           |                0|                    1|
68370        |68370         |68370                          |{68370}           |                0|                    1|
68372        |68372         |68372                          |{68372}           |                0|                    1|
68373        |68373         |68373                          |{68373}           |                0|                    1|
68380        |68380         |68380                          |{68380}           |                0|                    1|
68385        |68385         |68385                          |{68385}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068;
--> Updated Rows	97

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068)
order by date_eff, com_ap
/*
com_av|ncc_av   |com_ap|ncc_ap            |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|---------|------|------------------|----------|-----------------|-----------------------------|-------------------------------|
68003 |ALTENBACH|68106 |GOLDBACH ALTENBACH|1973-01-01|{1974-02-20}     |{GOLDBACH-ALTENBACH}         |{68106}                        |
68003 |ALTENBACH|68106 |GOLDBACH ALTENBACH|2015-09-01|{1974-02-20}     |{GOLDBACH-ALTENBACH}         |{68106}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---068
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 21/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
P                  |    7|
{P}                |    1|
totalit�           |   87|
{totalit�}         |    1|
{totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_068_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_068_2019 as
SELECT 
'068'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = 'P' THEN 'partie'	
	WHEN t1.classement_85 = '{P}' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_068_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 97

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_068_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   89|
partie         |    8|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_068_2019 FOR VALUES IN ('068');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 RENAME TO l_liste_commune_montagne_1985_068_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |    8|
totalit�|   90|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 AS
SELECT
'068'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068_old
ORDER BY insee_classement;
--> 98

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   90|
partie         |    8|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_068 FOR VALUES IN ('068');
--> Updated Rows	0






