---- 30/07/2020
----- DROME ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	176

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026;
--> 192

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019 |insee_commune_arrete_classement|nom_commune_arrete_classement         |arrete_classement      |classement_commune |commentaires                                                                                                                                                                         |nb_evenements_cog|
-------------|--------------------|-------------------------------|--------------------------------------|-----------------------|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
26001        |SOLAURE EN DIOIS    |{26001,26187}                  |{AIX-EN-DIOIS,MOLIERES-GLANDAZ}       |{1983-09-20,1983-09-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                                                                                                          |                2|
26055        |BOULC               |{26055,26260}                  |{BOULC-BONNEVAL,RAVEL-ET-FERRIERS}    |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{"Arrété de 1974 cite Boulc puis Bonneval mais le 01/01/1974 : Boulc devient Boulc-Bonneval suite à sa fusion avec Bonneval-en-Diois (26053).",NULL}                              |                2|
26086        |CHATILLON EN DIOIS  |{26086,26354}                  |{CHATILLON-EN-DIOIS,TRESCHENU-CREYERS}|{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                                                                                                          |                2|
26091        |CHAUVAC LAUX MONTAUX|{26091,26158}                  |{CHAUVAC,LAUX-MONTAUX}                |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,"01/01/2002 : Laux-Montaux est rattachée à Chauvac (26091) qui devient Chauvac-Laux-Montaux (fusion simple)."}                                                               |                2|
26262        |RECOUBEAU JANSAC    |{26151,26262}                  |{JANSAC,RECOUBEAU}                    |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{"01/01/1976 : Recoubeau devient Recoubeau-Jansac suite à sa fusion avec Jansac (26151).","01/01/1976 : Recoubeau devient Recoubeau-Jansac suite à sa fusion avec Jansac (26151)."}|                2|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 182 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	187

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '26' ---026
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
26001        |26001         |{26001,26187}                  |{26001,26187}     |                2|                    2|
26003        |26003         |26003                          |{26003}           |                0|                    1|
26012        |26012         |26012                          |{26012}           |                0|                    1|
26013        |26013         |26013                          |{26013}           |                0|                    1|
26015        |26015         |26015                          |{26015}           |                0|                    1|
26016        |26016         |26016                          |{26016}           |                0|                    1|
26017        |26017         |26017                          |{26017}           |                0|                    1|
26018        |26018         |26018                          |{26018}           |                0|                    1|
26019        |26019         |26019                          |{26019}           |                0|                    1|
26022        |26022         |26022                          |{26022}           |                0|                    1|
26023        |26023         |26023                          |{26023}           |                0|                    1|
26024        |26024         |26024                          |{26024}           |                0|                    1|
26025        |26025         |26025                          |{26025}           |                0|                    1|
26026        |26026         |26026                          |{26026}           |                0|                    1|
26027        |26027         |26027                          |{26027}           |                0|                    1|
26030        |26030         |26030                          |{26030}           |                0|                    1|
26032        |26032         |26032                          |{26032}           |                0|                    1|
26035        |26035         |26035                          |{26035}           |                0|                    1|
26036        |26036         |26036                          |{26036}           |                0|                    1|
26039        |26039         |26039                          |{26039}           |                0|                    1|
26040        |26040         |26040                          |{26040}           |                0|                    1|
26043        |26043         |26043                          |{26043}           |                0|                    1|
26046        |26046         |26046                          |{26046}           |                0|                    1|
26047        |26047         |26047                          |{26047}           |                0|                    1|
26048        |26048         |26048                          |{26048}           |                0|                    1|
26050        |26050         |26050                          |{26050}           |                0|                    1|
26051        |26051         |26051                          |{26051}           |                0|                    1|
26055        |26055         |{26055,26260}                  |{26055}           |                2|                    1|
26056        |26056         |26056                          |{26056}           |                0|                    1|
26059        |26059         |26059                          |{26059}           |                0|                    1|
26060        |26060         |26060                          |{26060}           |                0|                    1|
26062        |26062         |26062                          |{26062}           |                0|                    1|
26063        |26063         |26063                          |{26063}           |                0|                    1|
26066        |26066         |26066                          |{26066}           |                0|                    1|
26067        |26067         |26067                          |{26067}           |                0|                    1|
26069        |26069         |26069                          |{26069}           |                0|                    1|
26074        |26074         |26074                          |{26074}           |                0|                    1|
26075        |26075         |26075                          |{26075}           |                0|                    1|
26076        |26076         |26076                          |{26076}           |                0|                    1|
26080        |26080         |26080                          |{26080}           |                0|                    1|
26082        |26082         |26082                          |{26082}           |                0|                    1|
26086        |26086         |{26086,26354}                  |{26354,26086}     |                2|                    2|
26089        |26089         |26089                          |{26089}           |                0|                    1|
26090        |26090         |26090                          |{26090}           |                0|                    1|
26091        |26091         |{26091,26158}                  |{26091}           |                2|                    1|
26098        |26098         |26098                          |{26098}           |                0|                    1|
26100        |26100         |26100                          |{26100}           |                0|                    1|
26101        |26101         |26101                          |{26101}           |                0|                    1|
26103        |26103         |26103                          |{26103}           |                0|                    1|
26104        |26104         |26104                          |{26104}           |                0|                    1|
26105        |26105         |26105                          |{26105}           |                0|                    1|
26111        |26111         |26111                          |{26111}           |                0|                    1|
26112        |26112         |26112                          |{26112}           |                0|                    1|
26113        |26113         |26113                          |{26113}           |                0|                    1|
26114        |26114         |26114                          |{26114}           |                0|                    1|
26117        |26117         |26117                          |{26117}           |                0|                    1|
26122        |26122         |26122                          |{26122}           |                0|                    1|
26123        |26123         |26123                          |{26123}           |                0|                    1|
26126        |26126         |26126                          |{26126}           |                0|                    1|
26127        |26127         |26127                          |{26127}           |                0|                    1|
26128        |26128         |26128                          |{26128}           |                0|                    1|
26130        |26130         |26130                          |{26130}           |                0|                    1|
26131        |26131         |26131                          |{26131}           |                0|                    1|
26134        |26134         |26134                          |{26134}           |                0|                    1|
26135        |26135         |26135                          |{26135}           |                0|                    1|
26136        |26136         |26136                          |{26136}           |                0|                    1|
26137        |26137         |26137                          |{26137}           |                0|                    1|
26141        |26141         |26141                          |{26141}           |                0|                    1|
26142        |26142         |26142                          |{26142}           |                0|                    1|
26147        |26147         |26147                          |{26147}           |                0|                    1|
26149        |26149         |26149                          |{26149}           |                0|                    1|
26150        |26150         |26150                          |{26150}           |                0|                    1|
26152        |26152         |26152                          |{26152}           |                0|                    1|
26153        |26153         |26153                          |{26153}           |                0|                    1|
26154        |26154         |26154                          |{26154}           |                0|                    1|
26159        |26159         |26159                          |{26159}           |                0|                    1|
26161        |26161         |26161                          |{26161}           |                0|                    1|
26163        |26163         |26163                          |{26163}           |                0|                    1|
26164        |26164         |26164                          |{26164}           |                0|                    1|
26167        |26167         |26167                          |{26167}           |                0|                    1|
26168        |26168         |26168                          |{26168}           |                0|                    1|
26175        |26175         |26175                          |{26175}           |                0|                    1|
26178        |26178         |26178                          |{26178}           |                0|                    1|
26181        |26181         |26181                          |{26181}           |                0|                    1|
26183        |26183         |26183                          |{26183}           |                0|                    1|
26186        |26186         |26186                          |{26186}           |                0|                    1|
26189        |26189         |26189                          |{26189}           |                0|                    1|
26190        |26190         |26190                          |{26190}           |                0|                    1|
26193        |26193         |26193                          |{26193}           |                0|                    1|
26195        |26195         |26195                          |{26195}           |                0|                    1|
26199        |26199         |26199                          |{26199}           |                0|                    1|
26200        |26200         |26200                          |{26200}           |                0|                    1|
26201        |26201         |26201                          |{26201}           |                0|                    1|
26202        |26202         |26202                          |{26202}           |                0|                    1|
26204        |26204         |26204                          |{26204}           |                0|                    1|
26205        |26205         |26205                          |{26205}           |                0|                    1|
26209        |26209         |26209                          |{26209}           |                0|                    1|
26214        |26214         |26214                          |{26214}           |                0|                    1|
26215        |26215         |26215                          |{26215}           |                0|                    1|
26221        |26221         |26221                          |{26221}           |                0|                    1|
26222        |26222         |26222                          |{26222}           |                0|                    1|
26223        |26223         |26223                          |{26223}           |                0|                    1|
26224        |26224         |26224                          |{26224}           |                0|                    1|
26227        |26227         |26227                          |{26227}           |                0|                    1|
26228        |26228         |26228                          |{26228}           |                0|                    1|
26229        |26229         |26229                          |{26229}           |                0|                    1|
26232        |26232         |26232                          |{26232}           |                0|                    1|
26234        |26234         |26234                          |{26234}           |                0|                    1|
26236        |26236         |26236                          |{26236}           |                0|                    1|
26238        |26238         |26238                          |{26238}           |                0|                    1|
26239        |26239         |26239                          |{26239}           |                0|                    1|
26240        |26240         |26240                          |{26240}           |                0|                    1|
26241        |26241         |26241                          |{26241}           |                0|                    1|
26242        |26242         |26242                          |{26242}           |                0|                    1|
26243        |26243         |26243                          |{26243}           |                0|                    1|
26244        |26244         |26244                          |{26244}           |                0|                    1|
26245        |26245         |26245                          |{26245}           |                0|                    1|
26246        |26246         |26246                          |{26246}           |                0|                    1|
26248        |26248         |26248                          |{26248}           |                0|                    1|
26249        |26249         |26249                          |{26249}           |                0|                    1|
26253        |26253         |26253                          |{26253}           |                0|                    1|
26254        |26254         |26254                          |{26254}           |                0|                    1|
26255        |26255         |26255                          |{26255}           |                0|                    1|
26256        |26256         |26256                          |{26256}           |                0|                    1|
26262        |26262         |{26151,26262}                  |{26262}           |                2|                    1|
26263        |26263         |26263                          |{26263}           |                0|                    1|
26264        |26264         |26264                          |{26264}           |                0|                    1|
26266        |26266         |26266                          |{26266}           |                0|                    1|
26267        |26267         |26267                          |{26267}           |                0|                    1|
26268        |26268         |26268                          |{26268}           |                0|                    1|
26269        |26269         |26269                          |{26269}           |                0|                    1|
26270        |26270         |26270                          |{26270}           |                0|                    1|
26273        |26273         |26273                          |{26273}           |                0|                    1|
26274        |26274         |26274                          |{26274}           |                0|                    1|
26276        |26276         |26276                          |{26276}           |                0|                    1|
26278        |26278         |26278                          |{26278}           |                0|                    1|
26279        |26279         |26279                          |{26279}           |                0|                    1|
26282        |26282         |26282                          |{26282}           |                0|                    1|
26283        |26283         |26283                          |{26283}           |                0|                    1|
26286        |26286         |26286                          |{26286}           |                0|                    1|
26288        |26288         |26288                          |{26288}           |                0|                    1|
26289        |26289         |26289                          |{26289}           |                0|                    1|
26290        |26290         |26290                          |{26290}           |                0|                    1|
26291        |26291         |26291                          |{26291}           |                0|                    1|
26292        |26292         |26292                          |{26292}           |                0|                    1|
26296        |26296         |26296                          |{26296}           |                0|                    1|
26299        |26299         |26299                          |{26299}           |                0|                    1|
26300        |26300         |26300                          |{26300}           |                0|                    1|
26302        |26302         |26302                          |{26302}           |                0|                    1|
26303        |26303         |26303                          |{26303}           |                0|                    1|
26304        |26304         |26304                          |{26304}           |                0|                    1|
26306        |26306         |26306                          |{26306}           |                0|                    1|
26307        |26307         |26307                          |{26307}           |                0|                    1|
26308        |26308         |26308                          |{26308}           |                0|                    1|
26309        |26309         |26309                          |{26309}           |                0|                    1|
26311        |26311         |26311                          |{26311}           |                0|                    1|
26315        |26315         |26315                          |{26315}           |                0|                    1|
26316        |26316         |26316                          |{26316}           |                0|                    1|
26318        |26318         |26318                          |{26318}           |                0|                    1|
26320        |26320         |26320                          |{26320}           |                0|                    1|
26321        |26321         |26321                          |{26321}           |                0|                    1|
26327        |26327         |26327                          |{26327}           |                0|                    1|
26328        |26328         |26328                          |{26328}           |                0|                    1|
26329        |26329         |26329                          |{26329}           |                0|                    1|
26336        |26336         |26336                          |{26336}           |                0|                    1|
26340        |26340         |26340                          |{26340}           |                0|                    1|
26343        |26343         |26343                          |{26343}           |                0|                    1|
26344        |26344         |26344                          |{26344}           |                0|                    1|
26346        |26346         |26346                          |{26346}           |                0|                    1|
26350        |26350         |26350                          |{26350}           |                0|                    1|
26351        |26351         |26351                          |{26351}           |                0|                    1|
26356        |26356         |26356                          |{26356}           |                0|                    1|
26359        |26359         |26359                          |{26359}           |                0|                    1|
26361        |26361         |26361                          |{26361}           |                0|                    1|
26363        |26363         |26363                          |{26363}           |                0|                    1|
26364        |26364         |26364                          |{26364}           |                0|                    1|
26368        |26368         |26368                          |{26368}           |                0|                    1|
26369        |26369         |26369                          |{26369}           |                0|                    1|
26370        |26370         |26370                          |{26370}           |                0|                    1|
26371        |26371         |26371                          |{26371}           |                0|                    1|
26372        |26372         |26372                          |{26372}           |                0|                    1|
26373        |26373         |26373                          |{26373}           |                0|                    1|
26374        |26374         |26374                          |{26374}           |                0|                    1|
26375        |26375         |26375                          |{26375}           |                0|                    1|
26376        |26376         |26376                          |{26376}           |                0|                    1|
26378        |26378         |26378                          |{26378}           |                0|                    1|
26382        |26382         |26382                          |{26382}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026;
--> Updated Rows	187

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026)
order by date_eff, com_ap
/*
com_av|ncc_av           |com_ap|ncc_ap                      |date_eff  |arrete_classement      |nom_commune_arrete_classement     |insee_commune_
------|-----------------|------|----------------------------|----------|-----------------------|----------------------------------|--------------
26079 |CHARPEY          |26382 |SAINT VINCENT LA COMMANDERIE|1954-12-18|1983-09-20             |SAINT-VINCENT-LA-COMMANDERIE      |26382         
26230 |PETIT PARIS      |26321 |SAINT NAZAIRE LE DESERT     |1966-09-05|1974-02-20             |SAINT-NAZAIRE-LE-DESERT           |26321         
26132 |FARE             |26199 |MONTFERRAND LA FARE         |1970-11-01|1974-02-20             |MONTFERRAND-LA-FARE               |26199         
26120 |ESCOULIN         |26128 |EYGLUY ESCOULIN             |1971-06-01|1974-02-20             |EYGLUY-ESCOULIN                   |26128         
26237 |PILHON           |26136 |VAL MARAVEL                 |1972-08-01|1974-02-20             |VAL-MARAVEL                       |26136         
26029 |BATIE CREMEZIN   |26136 |VAL MARAVEL                 |1972-08-01|1974-02-20             |VAL-MARAVEL                       |26136         
26044 |BECONNE          |26276 |ROCHE SAINT SECRET BECONNE  |1973-01-01|1974-02-20             |ROCHE-SAINT-SECRET-BECONNE        |26276         
26053 |BONNEVAL EN DIOIS|26055 |BOULC BONNEVAL              |1974-01-01|{1974-02-20,1974-02-20}|{BOULC-BONNEVAL,RAVEL-ET-FERRIERS}|{26055,26260} 
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---026
ORDER BY insee_cog2019, insee_com;


------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
totalit�           |  182|
{totalit�,totalit�}|    5|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_026_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_026_2019 as
SELECT 
'026'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	/*
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�' */  
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_026_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 187

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_026_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  187|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_026_2019 FOR VALUES IN ('026');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 RENAME TO l_liste_commune_montagne_1985_026_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|  192|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 AS
SELECT
'026'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026_old
ORDER BY insee_classement;
--> 192

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  192|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_026 FOR VALUES IN ('026');
--> Updated Rows	0




