---- 30/07/2020
----- 031 -
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	86

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031;
--> 86

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019|insee_commune_arrete_classement|nom_commune_arrete_classement|arrete_classement      |classement_commune |commentaires                                                                                                                                                                       |nb_evenements_cog|
-------------|-------------------|-------------------------------|-----------------------------|-----------------------|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
31085        |BOUTX              |{31085,31154}                  |{BOUTX,COULEDOUX}            |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,"01/08/1974 : Couledoux est rattach�e � Boutx (31085) (fusion association)."}                                                                                                |                2|
31129        |CAZARILH LASPENES  |{31129}                        |{CAZARIL-LASPENES}           |{1974-02-20}           |{totalit�}         |{NULL}                                                                                                                                                                             |                1|
31144        |CIERP GAUD         |{31144}                        |{CIERP-GAUD-SIGNAC}          |{1974-02-20}           |{totalit�}         |{"01/01/1974 : Cierp-Gaud devient Cierp-Gaud-Signac suite � sa fusion-association avec Signac (31548).�21/03/1972 : Cierp devient Cierp-Gaud suite � sa fusion avec Gaud (31214)."}|                1|
31471        |SAINT BEAT LEZ     |{31298,31471}                  |{LEZ,SAINT-BEAT}             |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{"Ecrit Leze dans arr�t� de 1974",NULL}                                                                                                                                            |                2|
31548        |SIGNAC             |{31144}                        |{CIERP-GAUD-SIGNAC}          |{1974-02-20}           |{totalit�}         |{"01/01/1974 : Cierp-Gaud devient Cierp-Gaud-Signac suite � sa fusion-association avec Signac (31548).�21/03/1972 : Cierp devient Cierp-Gaud suite � sa fusion avec Gaud (31214)."}|                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 80 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	84

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '31' ---031
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
31009        |31009         |31009                          |{31009}           |                0|                    1|
31010        |31010         |31010                          |{31010}           |                0|                    1|
31011        |31011         |31011                          |{31011}           |                0|                    1|
31012        |31012         |31012                          |{31012}           |                0|                    1|
31014        |31014         |31014                          |{31014}           |                0|                    1|
31015        |31015         |31015                          |{31015}           |                0|                    1|
31015        |31015         |31015                          |{31015}           |                0|                    1|
31017        |31017         |31017                          |{31017}           |                0|                    1|
31019        |31019         |31019                          |{31019}           |                0|                    1|
31020        |31020         |31020                          |{31020}           |                0|                    1|
31040        |31040         |31040                          |{31040}           |                0|                    1|
31041        |31041         |31041                          |{31041}           |                0|                    1|
31042        |31042         |31042                          |{31042}           |                0|                    1|
31046        |31046         |31046                          |{31046}           |                0|                    1|
31064        |31064         |31064                          |{31064}           |                0|                    1|
31067        |31067         |31067                          |{31067}           |                0|                    1|
31068        |31068         |31068                          |{31068}           |                0|                    1|
31081        |31081         |31081                          |{31081}           |                0|                    1|
31085        |31085         |{31085,31154}                  |{31085}           |                2|                    1|
31092        |31092         |31092                          |{31092}           |                0|                    1|
31123        |31123         |31123                          |{31123}           |                0|                    1|
31125        |31125         |31125                          |{31125}           |                0|                    1|
31127        |31127         |31127                          |{31127}           |                0|                    1|
31129        |31129         |{31129}                        |{31129}           |                1|                    1|
31131        |31131         |31131                          |{31131}           |                0|                    1|
31132        |31132         |31132                          |{31132}           |                0|                    1|
31133        |31133         |31133                          |{31133}           |                0|                    1|
31139        |31139         |31139                          |{31139}           |                0|                    1|
31140        |31140         |31140                          |{31140}           |                0|                    1|
31142        |31142         |31142                          |{31142}           |                0|                    1|
31144        |31144         |{31144}                        |{31144}           |                1|                    1|
31146        |31146         |31146                          |{31146}           |                0|                    1|
31176        |31176         |31176                          |{31176}           |                0|                    1|
31177        |31177         |31177                          |{31177}           |                0|                    1|
31190        |31190         |31190                          |{31190}           |                0|                    1|
31191        |31191         |31191                          |{31191}           |                0|                    1|
31195        |31195         |31195                          |{31195}           |                0|                    1|
31199        |31199         |31199                          |{31199}           |                0|                    1|
31200        |31200         |31200                          |{31200}           |                0|                    1|
31207        |31207         |31207                          |{31207}           |                0|                    1|
31213        |31213         |31213                          |{31213}           |                0|                    1|
31217        |31217         |31217                          |{31217}           |                0|                    1|
31221        |31221         |31221                          |{31221}           |                0|                    1|
31222        |31222         |31222                          |{31222}           |                0|                    1|
31235        |31235         |31235                          |{31235}           |                0|                    1|
31236        |31236         |31236                          |{31236}           |                0|                    1|
31241        |31241         |31241                          |{31241}           |                0|                    1|
31242        |31242         |31242                          |{31242}           |                0|                    1|
31244        |31244         |31244                          |{31244}           |                0|                    1|
31245        |31245         |31245                          |{31245}           |                0|                    1|
31290        |31290         |31290                          |{31290}           |                0|                    1|
31306        |31306         |31306                          |{31306}           |                0|                    1|
31308        |31308         |31308                          |{31308}           |                0|                    1|
31313        |31313         |31313                          |{31313}           |                0|                    1|
31316        |31316         |31316                          |{31316}           |                0|                    1|
31335        |31335         |31335                          |{31335}           |                0|                    1|
31337        |31337         |31337                          |{31337}           |                0|                    1|
31342        |31342         |31342                          |{31342}           |                0|                    1|
31348        |31348         |31348                          |{31348}           |                0|                    1|
31360        |31360         |31360                          |{31360}           |                0|                    1|
31369        |31369         |31369                          |{31369}           |                0|                    1|
31394        |31394         |31394                          |{31394}           |                0|                    1|
31404        |31404         |31404                          |{31404}           |                0|                    1|
31405        |31405         |31405                          |{31405}           |                0|                    1|
31408        |31408         |31408                          |{31408}           |                0|                    1|
31431        |31431         |31431                          |{31431}           |                0|                    1|
31432        |31432         |31432                          |{31432}           |                0|                    1|
31434        |31434         |31434                          |{31434}           |                0|                    1|
31447        |31447         |31447                          |{31447}           |                0|                    1|
31465        |31465         |31465                          |{31465}           |                0|                    1|
31470        |31470         |31470                          |{31470}           |                0|                    1|
31471        |31471         |{31298,31471}                  |{31471,31298}     |                2|                    2|
31472        |31472         |31472                          |{31472}           |                0|                    1|
31500        |31500         |31500                          |{31500}           |                0|                    1|
31508        |31508         |31508                          |{31508}           |                0|                    1|
31509        |31509         |31509                          |{31509}           |                0|                    1|
31521        |31521         |31521                          |{31521}           |                0|                    1|
31524        |31524         |31524                          |{31524}           |                0|                    1|
31535        |31535         |31535                          |{31535}           |                0|                    1|
31544        |31544         |31544                          |{31544}           |                0|                    1|
31548        |31548         |{31144}                        |{31548}           |                1|                    1|
31549        |31549         |31549                          |{31549}           |                0|                    1|
31559        |31559         |31559                          |{31559}           |                0|                    1|
31562        |31562         |31562                          |{31562}           |                0|                    1|
31590        |31590         |31590                          |{31590}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031;
--> Updated Rows	84

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap    |insee_comm|nom_commun       |pointage|partie  |date_decis|date_eff  |mod|
------|----------|----------|-----------------|--------|--------|----------|----------|---|
31144 |CIERP GAUD|31144     |CIERP-GAUD-SIGNAC|x       |totalit�|1974-02-20|1983-02-01|21 |
31548 |SIGNAC    |31144     |CIERP-GAUD-SIGNAC|x       |totalit�|1974-02-20|1983-02-01|21 |
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031)
order by date_eff, com_ap
/*
com_av|ncc_av|com_ap|ncc_ap           |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|------|------|-----------------|----------|-----------------|-----------------------------|-------------------------------|
31214 |GAUD  |31144 |CIERP GAUD       |1972-03-21|{1974-02-20}     |{CIERP-GAUD-SIGNAC}          |{31144}                        |
31548 |SIGNAC|31144 |CIERP GAUD SIGNAC|1974-01-01|{1974-02-20}     |{CIERP-GAUD-SIGNAC}          |{31144}                        |
31548 |SIGNAC|31548 |SIGNAC           |1974-01-01|{1974-02-20}     |{CIERP-GAUD-SIGNAC}          |{31144}                        |
31548 |SIGNAC|31548 |SIGNAC           |1983-02-01|{1974-02-20}     |{CIERP-GAUD-SIGNAC}          |{31144}                        |
 */

---- Correction du fichier final :
--- ok

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---031
ORDER BY insee_cog2019, insee_com;


------------------------------------------------------------------------------------------------------------------------------------------------
---- 21/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85               |count|
----------------------------|-----|
totalit�                    |   79|
{totalit�}                  |    3|
{totalit�,totalit�}         |    1|
{totalit�,totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_031_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_031_2019 as
SELECT 
'031'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�' 
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�}' THEN 'totalit�'
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_031_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 84

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_031_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   84|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_031_2019 FOR VALUES IN ('031');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 RENAME TO l_liste_commune_montagne_1985_031_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|   86|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 AS
SELECT
'031'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031_old
ORDER BY insee_classement;
--> 86

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   86|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_031 FOR VALUES IN ('031');
--> Updated Rows	0