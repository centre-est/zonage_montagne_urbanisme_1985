---- 20/08/2020
----- 064 ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064  WHERE date_decis IS NULL;

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 SET date_decis='20/02/1674,28/04/1976' WHERE date_decis IS NULL;
--> Updated Rows	2
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	103

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064;
--> 105

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019|insee_commune_arrete_classement|nom_commune_arrete_classement|arrete_classement      |classement_commune |commentaires|nb_evenements_cog|
-------------|-------------------|-------------------------------|-----------------------------|-----------------------|-------------------|------------|-----------------|
64225        |ANCE FEAS          |{64020,64225}                  |{ANCE,FEAS}                  |{1976-04-28,1976-04-28}|{totalit�,totalit�}|{NULL,NULL} |                2|
64310        |LANNE EN BARETOUS  |{64310}                        |{LANNE}                      |{1974-02-20}           |{totalit�}         |{NULL}      |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 102 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	104

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '64' ---064
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
64006        |64006         |64006                          |{64006}           |                0|                    1|
64008        |64008         |64008                          |{64008}           |                0|                    1|
64011        |64011         |64011                          |{64011}           |                0|                    1|
64014        |64014         |64014                          |{64014}           |                0|                    1|
64015        |64015         |64015                          |{64015}           |                0|                    1|
64016        |64016         |64016                          |{64016}           |                0|                    1|
64017        |64017         |64017                          |{64017}           |                0|                    1|
64026        |64026         |64026                          |{64026}           |                0|                    1|
64029        |64029         |64029                          |{64029}           |                0|                    1|
64040        |64040         |64040                          |{64040}           |                0|                    1|
64047        |64047         |64047                          |{64047}           |                0|                    1|
64058        |64058         |64058                          |{64058}           |                0|                    1|
64062        |64062         |64062                          |{64062}           |                0|                    1|
64064        |64064         |64064                          |{64064}           |                0|                    1|
64066        |64066         |64066                          |{64066}           |                0|                    1|
64068        |64068         |64068                          |{64068}           |                0|                    1|
64069        |64069         |64069                          |{64069}           |                0|                    1|
64081        |64081         |64081                          |{64081}           |                0|                    1|
64085        |64085         |64085                          |{64085}           |                0|                    1|
64092        |64092         |64092                          |{64092}           |                0|                    1|
64093        |64093         |64093                          |{64093}           |                0|                    1|
64104        |64104         |64104                          |{64104}           |                0|                    1|
64107        |64107         |64107                          |{64107}           |                0|                    1|
64110        |64110         |64110                          |{64110}           |                0|                    1|
64116        |64116         |64116                          |{64116}           |                0|                    1|
64124        |64124         |64124                          |{64124}           |                0|                    1|
64127        |64127         |64127                          |{64127}           |                0|                    1|
64128        |64128         |64128                          |{64128}           |                0|                    1|
64136        |64136         |64136                          |{64136}           |                0|                    1|
64150        |64150         |64150                          |{64150}           |                0|                    1|
64154        |64154         |64154                          |{64154}           |                0|                    1|
64155        |64155         |64155                          |{64155}           |                0|                    1|
64162        |64162         |64162                          |{64162}           |                0|                    1|
64166        |64166         |64166                          |{64166}           |                0|                    1|
64175        |64175         |64175                          |{64175}           |                0|                    1|
64185        |64185         |64185                          |{64185}           |                0|                    1|
64188        |64188         |64188                          |{64188}           |                0|                    1|
64204        |64204         |64204                          |{64204}           |                0|                    1|
64206        |64206         |64206                          |{64206}           |                0|                    1|
64213        |64213         |64213                          |{64213}           |                0|                    1|
64217        |64217         |64217                          |{64217}           |                0|                    1|
64218        |64218         |64218                          |{64218}           |                0|                    1|
64222        |64222         |64222                          |{64222}           |                0|                    1|
64223        |64223         |64223                          |{64223}           |                0|                    1|
64225        |64225         |{64020,64225}                  |{64225,64020}     |                2|                    2|
64229        |64229         |64229                          |{64229}           |                0|                    1|
64240        |64240         |64240                          |{64240}           |                0|                    1|
64258        |64258         |64258                          |{64258}           |                0|                    1|
64265        |64265         |64265                          |{64265}           |                0|                    1|
64267        |64267         |64267                          |{64267}           |                0|                    1|
64274        |64274         |64274                          |{64274}           |                0|                    1|
64275        |64275         |64275                          |{64275}           |                0|                    1|
64276        |64276         |64276                          |{64276}           |                0|                    1|
64279        |64279         |64279                          |{64279}           |                0|                    1|
64280        |64280         |64280                          |{64280}           |                0|                    1|
64283        |64283         |64283                          |{64283}           |                0|                    1|
64298        |64298         |64298                          |{64298}           |                0|                    1|
64303        |64303         |64303                          |{64303}           |                0|                    1|
64310        |64310         |{64310}                        |{64310}           |                1|                    1|
64316        |64316         |64316                          |{64316}           |                0|                    1|
64320        |64320         |64320                          |{64320}           |                0|                    1|
64322        |64322         |64322                          |{64322}           |                0|                    1|
64327        |64327         |64327                          |{64327}           |                0|                    1|
64330        |64330         |64330                          |{64330}           |                0|                    1|
64336        |64336         |64336                          |{64336}           |                0|                    1|
64340        |64340         |64340                          |{64340}           |                0|                    1|
64342        |64342         |64342                          |{64342}           |                0|                    1|
64350        |64350         |64350                          |{64350}           |                0|                    1|
64351        |64351         |64351                          |{64351}           |                0|                    1|
64353        |64353         |64353                          |{64353}           |                0|                    1|
64354        |64354         |64354                          |{64354}           |                0|                    1|
64360        |64360         |64360                          |{64360}           |                0|                    1|
64363        |64363         |64363                          |{64363}           |                0|                    1|
64364        |64364         |64364                          |{64364}           |                0|                    1|
64371        |64371         |64371                          |{64371}           |                0|                    1|
64378        |64378         |64378                          |{64378}           |                0|                    1|
64379        |64379         |64379                          |{64379}           |                0|                    1|
64404        |64404         |64404                          |{64404}           |                0|                    1|
64411        |64411         |64411                          |{64411}           |                0|                    1|
64422        |64422         |64422                          |{64422}           |                0|                    1|
64424        |64424         |64424                          |{64424}           |                0|                    1|
64432        |64432         |64432                          |{64432}           |                0|                    1|
64433        |64433         |64433                          |{64433}           |                0|                    1|
64436        |64436         |64436                          |{64436}           |                0|                    1|
64441        |64441         |64441                          |{64441}           |                0|                    1|
64463        |64463         |64463                          |{64463}           |                0|                    1|
64468        |64468         |64468                          |{64468}           |                0|                    1|
64473        |64473         |64473                          |{64473}           |                0|                    1|
64475        |64475         |64475                          |{64475}           |                0|                    1|
64477        |64477         |64477                          |{64477}           |                0|                    1|
64487        |64487         |64487                          |{64487}           |                0|                    1|
64490        |64490         |64490                          |{64490}           |                0|                    1|
64492        |64492         |64492                          |{64492}           |                0|                    1|
64504        |64504         |64504                          |{64504}           |                0|                    1|
64506        |64506         |64506                          |{64506}           |                0|                    1|
64509        |64509         |64509                          |{64509}           |                0|                    1|
64522        |64522         |64522                          |{64522}           |                0|                    1|
64533        |64533         |64533                          |{64533}           |                0|                    1|
64537        |64537         |64537                          |{64537}           |                0|                    1|
64538        |64538         |64538                          |{64538}           |                0|                    1|
64542        |64542         |64542                          |{64542}           |                0|                    1|
64543        |64543         |64543                          |{64543}           |                0|                    1|
 */
---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064;
--> Updated Rows	104

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064)
order by date_eff, com_ap
/*
com_av|ncc_av        |com_ap|ncc_ap     |date_eff  |arrete_classement    |nom_commune_arrete_classement|insee_commune_arrete_classement|
------|--------------|------|-----------|----------|---------------------|-----------------------------|-------------------------------|
64055 |ARROS D OLORON|64064 |ASASP ARROS|1973-01-01|20/02/1674,28/04/1976|ASASP-ARROS                  |64064                          |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---064
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 21/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
P                  |    2|
totalit�           |   98|
{totalit�}         |    1|
{totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_064_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_064_2019 as
SELECT 
'064'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   
	WHEN t1.classement_85 = 'P' THEN 'partie'	
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_064_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 102

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_064_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  100|
partie         |    2|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_064_2019 FOR VALUES IN ('064');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 RENAME TO l_liste_commune_montagne_1985_064_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |    2|
totalit�|  103|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 AS
SELECT
'064'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064_old
ORDER BY insee_classement;
--> 105

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  103|
partie         |    2|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_064 FOR VALUES IN ('064');
--> Updated Rows	0






