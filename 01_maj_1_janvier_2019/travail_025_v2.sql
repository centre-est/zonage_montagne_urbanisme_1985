---- 04 /09 /2020
----- DOUBS ----

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025;
--> 231

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019|insee_commune_arrete_classement      |nom_commune_arrete_classement                                     |arrete_classement                                                  |classement_commune                                     |commentaires                                                                                                                                                                |nb_evenements_cog|
-------------|-------------------|-------------------------------------|------------------------------------------------------------------|-------------------------------------------------------------------|-------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
25288        |FOURNETS LUISANS   |{25288}                              |{FOURNETS-LUISANS}                                                |{20/02/1974}                                                       |{totalit�}                                             |{"Ecrit GRANDFONTAINE-FOURNETS dans arr�t� de 1974 mais le 01/01/1973 : Grandfontaine-Fournets devient Fournets-Luisans suite � sa fusion-association avec Luisans (25352)"}|                1|
25334        |LEVIER             |{25319,25334}                        |{LABERGEMENT-DU-NAVOIS,LEVIER}                                    |{20/02/1974,20/02/1974}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                 |                2|
25424        |PREMIERS SAPINS    |{25028,25128,25302,25424,25480,25585}|{ATHOSE,CHASNANS,HAUTEPIERRE-LE-CHATELET,NODS,RANTECHAUX,VANCLANS}|{20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974}|{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|{NULL,NULL,NULL,NULL,NULL,NULL}                                                                                                                                             |                6|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 222 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	224

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '25' ---025
ORDER BY insee_cog2019, insee_com;
insee_cog2019|fred_insee_com|insee_commune_arrete_classement      |fred_anciens_codes                   |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------------|-------------------------------------|-----------------|---------------------|
25004        |25004         |25004                                |{25004}                              |                0|                    1|
25007        |25007         |25007                                |{25007}                              |                0|                    1|
25012        |25012         |25012                                |{25012}                              |                0|                    1|
25016        |25016         |25016                                |{25016}                              |                0|                    1|
25018        |25018         |25018                                |{25018}                              |                0|                    1|
25024        |25024         |25024                                |{25024}                              |                0|                    1|
25025        |25025         |25025                                |{25025}                              |                0|                    1|
25026        |25026         |25026                                |{25026}                              |                0|                    1|
25029        |25029         |25029                                |{25029}                              |                0|                    1|
25039        |25039         |25039                                |{25039}                              |                0|                    1|
25041        |25041         |25041                                |{25041}                              |                0|                    1|
25042        |25042         |25042                                |{25042}                              |                0|                    1|
25046        |25046         |25046                                |{25046}                              |                0|                    1|
25049        |25049         |25049                                |{25049}                              |                0|                    1|
25050        |25050         |25050                                |{25050}                              |                0|                    1|
25051        |25051         |25051                                |{25051}                              |                0|                    1|
25053        |25053         |25053                                |{25053}                              |                0|                    1|
25060        |25060         |25060                                |{25060}                              |                0|                    1|
25061        |25061         |25061                                |{25061}                              |                0|                    1|
25062        |25062         |25062                                |{25062}                              |                0|                    1|
25070        |25070         |25070                                |{25070}                              |                0|                    1|
25074        |25074         |25074                                |{25074}                              |                0|                    1|
25075        |25075         |25075                                |{25075}                              |                0|                    1|
25077        |25077         |25077                                |{25077}                              |                0|                    1|
25079        |25079         |25079                                |{25079}                              |                0|                    1|
25085        |25085         |25085                                |{25085}                              |                0|                    1|
25091        |25091         |25091                                |{25091}                              |                0|                    1|
25095        |25095         |25095                                |{25095}                              |                0|                    1|
25096        |25096         |25096                                |{25096}                              |                0|                    1|
25099        |25099         |25099                                |{25099}                              |                0|                    1|
25100        |25100         |25100                                |{25100}                              |                0|                    1|
25102        |25102         |25102                                |{25102}                              |                0|                    1|
25108        |25108         |25108                                |{25108}                              |                0|                    1|
25110        |25110         |25110                                |{25110}                              |                0|                    1|
25113        |25113         |25113                                |{25113}                              |                0|                    1|
25114        |25114         |25114                                |{25114}                              |                0|                    1|
25120        |25120         |25120                                |{25120}                              |                0|                    1|
25121        |25121         |25121                                |{25121}                              |                0|                    1|
25122        |25122         |25122                                |{25122}                              |                0|                    1|
25124        |25124         |25124                                |{25124}                              |                0|                    1|
25125        |25125         |25125                                |{25125}                              |                0|                    1|
25127        |25127         |25127                                |{25127}                              |                0|                    1|
25130        |25130         |25130                                |{25130}                              |                0|                    1|
25131        |25131         |25131                                |{25131}                              |                0|                    1|
25138        |25138         |25138                                |{25138}                              |                0|                    1|
25139        |25139         |25139                                |{25139}                              |                0|                    1|
25142        |25142         |25142                                |{25142}                              |                0|                    1|
25148        |25148         |25148                                |{25148}                              |                0|                    1|
25151        |25151         |25151                                |{25151}                              |                0|                    1|
25157        |25157         |25157                                |{25157}                              |                0|                    1|
25160        |25160         |25160                                |{25160}                              |                0|                    1|
25161        |25161         |25161                                |{25161}                              |                0|                    1|
25173        |25173         |25173                                |{25173}                              |                0|                    1|
25174        |25174         |25174                                |{25174}                              |                0|                    1|
25176        |25176         |25176                                |{25176}                              |                0|                    1|
25179        |25179         |25179                                |{25179}                              |                0|                    1|
25180        |25180         |25180                                |{25180}                              |                0|                    1|
25193        |25193         |25193                                |{25193}                              |                0|                    1|
25194        |25194         |25194                                |{25194}                              |                0|                    1|
25199        |25199         |25199                                |{25199}                              |                0|                    1|
25201        |25201         |25201                                |{25201}                              |                0|                    1|
25202        |25202         |25202                                |{25202}                              |                0|                    1|
25203        |25203         |25203                                |{25203}                              |                0|                    1|
25204        |25204         |25204                                |{25204}                              |                0|                    1|
25211        |25211         |25211                                |{25211}                              |                0|                    1|
25213        |25213         |25213                                |{25213}                              |                0|                    1|
25218        |25218         |25218                                |{25218}                              |                0|                    1|
25219        |25219         |25219                                |{25219}                              |                0|                    1|
25227        |25227         |25227                                |{25227}                              |                0|                    1|
25229        |25229         |25229                                |{25229}                              |                0|                    1|
25231        |25231         |25231                                |{25231}                              |                0|                    1|
25233        |25233         |25233                                |{25233}                              |                0|                    1|
25234        |25234         |25234                                |{25234}                              |                0|                    1|
25238        |25238         |25238                                |{25238}                              |                0|                    1|
25239        |25239         |25239                                |{25239}                              |                0|                    1|
25240        |25240         |25240                                |{25240}                              |                0|                    1|
25243        |25243         |25243                                |{25243}                              |                0|                    1|
25244        |25244         |25244                                |{25244}                              |                0|                    1|
25248        |25248         |25248                                |{25248}                              |                0|                    1|
25252        |25252         |25252                                |{25252}                              |                0|                    1|
25254        |25254         |25254                                |{25254}                              |                0|                    1|
25255        |25255         |25255                                |{25255}                              |                0|                    1|
25256        |25256         |25256                                |{25256}                              |                0|                    1|
25259        |25259         |25259                                |{25259}                              |                0|                    1|
25261        |25261         |25261                                |{25261}                              |                0|                    1|
25262        |25262         |25262                                |{25262}                              |                0|                    1|
25263        |25263         |25263                                |{25263}                              |                0|                    1|
25268        |25268         |25268                                |{25268}                              |                0|                    1|
25270        |25270         |25270                                |{25270}                              |                0|                    1|
25271        |25271         |25271                                |{25271}                              |                0|                    1|
25274        |25274         |25274                                |{25274}                              |                0|                    1|
25275        |25275         |25275                                |{25275}                              |                0|                    1|
25280        |25280         |25280                                |{25280}                              |                0|                    1|
25282        |25282         |25282                                |{25282}                              |                0|                    1|
25285        |25285         |25285                                |{25285}                              |                0|                    1|
25286        |25286         |25286                                |{25286}                              |                0|                    1|
25288        |25288         |{25288}                              |{25288}                              |                1|                    1|
25289        |25289         |25289                                |{25289}                              |                0|                    1|
25290        |25290         |25290                                |{25290}                              |                0|                    1|
25293        |25293         |25293                                |{25293}                              |                0|                    1|
25295        |25295         |25295                                |{25295}                              |                0|                    1|
25296        |25296         |25296                                |{25296}                              |                0|                    1|
25301        |25301         |25301                                |{25301}                              |                0|                    1|
25303        |25303         |25303                                |{25303}                              |                0|                    1|
25307        |25307         |25307                                |{25307}                              |                0|                    1|
25308        |25308         |25308                                |{25308}                              |                0|                    1|
25309        |25309         |25309                                |{25309}                              |                0|                    1|
25314        |25314         |25314                                |{25314}                              |                0|                    1|
25318        |25318         |25318                                |{25318}                              |                0|                    1|
25320        |25320         |25320                                |{25320}                              |                0|                    1|
25321        |25321         |25321                                |{25321}                              |                0|                    1|
25325        |25325         |25325                                |{25325}                              |                0|                    1|
25329        |25329         |25329                                |{25329}                              |                0|                    1|
25331        |25331         |25331                                |{25331}                              |                0|                    1|
25333        |25333         |25333                                |{25333}                              |                0|                    1|
25334        |25334         |{25319,25334}                        |{25334,25319}                        |                2|                    2|
25335        |25335         |25335                                |{25335}                              |                0|                    1|
25339        |25339         |25339                                |{25339}                              |                0|                    1|
25342        |25342         |25342                                |{25342}                              |                0|                    1|
25343        |25343         |25343                                |{25343}                              |                0|                    1|
25344        |25344         |25344                                |{25344}                              |                0|                    1|
25346        |25346         |25346                                |{25346}                              |                0|                    1|
25347        |25347         |25347                                |{25347}                              |                0|                    1|
25348        |25348         |25348                                |{25348}                              |                0|                    1|
25349        |25349         |25349                                |{25349}                              |                0|                    1|
25351        |25351         |25351                                |{25351}                              |                0|                    1|
25356        |25356         |25356                                |{25356}                              |                0|                    1|
25357        |25357         |25357                                |{25357}                              |                0|                    1|
25361        |25361         |25361                                |{25361}                              |                0|                    1|
25366        |25366         |25366                                |{25366}                              |                0|                    1|
25373        |25373         |25373                                |{25373}                              |                0|                    1|
25380        |25380         |25380                                |{25380}                              |                0|                    1|
25386        |25386         |25386                                |{25386}                              |                0|                    1|
25387        |25387         |25387                                |{25387}                              |                0|                    1|
25389        |25389         |25389                                |{25389}                              |                0|                    1|
25390        |25390         |25390                                |{25390}                              |                0|                    1|
25391        |25391         |25391                                |{25391}                              |                0|                    1|
25392        |25392         |25392                                |{25392}                              |                0|                    1|
25393        |25393         |25393                                |{25393}                              |                0|                    1|
25398        |25398         |25398                                |{25398}                              |                0|                    1|
25402        |25402         |25402                                |{25402}                              |                0|                    1|
25403        |25403         |25403                                |{25403}                              |                0|                    1|
25404        |25404         |25404                                |{25404}                              |                0|                    1|
25405        |25405         |25405                                |{25405}                              |                0|                    1|
25411        |25411         |25411                                |{25411}                              |                0|                    1|
25413        |25413         |25413                                |{25413}                              |                0|                    1|
25415        |25415         |25415                                |{25415}                              |                0|                    1|
25420        |25420         |25420                                |{25420}                              |                0|                    1|
25421        |25421         |25421                                |{25421}                              |                0|                    1|
25424        |25424         |{25028,25128,25302,25424,25480,25585}|{25128,25480,25424,25302,25028,25585}|                6|                    6|
25425        |25425         |25425                                |{25425}                              |                0|                    1|
25432        |25432         |25432                                |{25432}                              |                0|                    1|
25433        |25433         |25433                                |{25433}                              |                0|                    1|
25440        |25440         |25440                                |{25440}                              |                0|                    1|
25441        |25441         |25441                                |{25441}                              |                0|                    1|
25442        |25442         |25442                                |{25442}                              |                0|                    1|
25447        |25447         |25447                                |{25447}                              |                0|                    1|
25449        |25449         |25449                                |{25449}                              |                0|                    1|
25451        |25451         |25451                                |{25451}                              |                0|                    1|
25452        |25452         |25452                                |{25452}                              |                0|                    1|
25453        |25453         |25453                                |{25453}                              |                0|                    1|
25456        |25456         |25456                                |{25456}                              |                0|                    1|
25457        |25457         |25457                                |{25457}                              |                0|                    1|
25458        |25458         |25458                                |{25458}                              |                0|                    1|
25459        |25459         |25459                                |{25459}                              |                0|                    1|
25462        |25462         |25462                                |{25462}                              |                0|                    1|
25463        |25463         |25463                                |{25463}                              |                0|                    1|
25464        |25464         |25464                                |{25464}                              |                0|                    1|
25471        |25471         |25471                                |{25471}                              |                0|                    1|
25483        |25483         |25483                                |{25483}                              |                0|                    1|
25486        |25486         |25486                                |{25486}                              |                0|                    1|
25487        |25487         |25487                                |{25487}                              |                0|                    1|
25489        |25489         |25489                                |{25489}                              |                0|                    1|
25493        |25493         |25493                                |{25493}                              |                0|                    1|
25494        |25494         |25494                                |{25494}                              |                0|                    1|
25501        |25501         |25501                                |{25501}                              |                0|                    1|
25503        |25503         |25503                                |{25503}                              |                0|                    1|
25504        |25504         |25504                                |{25504}                              |                0|                    1|
25512        |25512         |25512                                |{25512}                              |                0|                    1|
25513        |25513         |25513                                |{25513}                              |                0|                    1|
25514        |25514         |25514                                |{25514}                              |                0|                    1|
25515        |25515         |25515                                |{25515}                              |                0|                    1|
25517        |25517         |25517                                |{25517}                              |                0|                    1|
25519        |25519         |25519                                |{25519}                              |                0|                    1|
25522        |25522         |25522                                |{25522}                              |                0|                    1|
25525        |25525         |25525                                |{25525}                              |                0|                    1|
25529        |25529         |25529                                |{25529}                              |                0|                    1|
25534        |25534         |25534                                |{25534}                              |                0|                    1|
25541        |25541         |25541                                |{25541}                              |                0|                    1|
25545        |25545         |25545                                |{25545}                              |                0|                    1|
25548        |25548         |25548                                |{25548}                              |                0|                    1|
25549        |25549         |25549                                |{25549}                              |                0|                    1|
25550        |25550         |25550                                |{25550}                              |                0|                    1|
25551        |25551         |25551                                |{25551}                              |                0|                    1|
25554        |25554         |25554                                |{25554}                              |                0|                    1|
25559        |25559         |25559                                |{25559}                              |                0|                    1|
25565        |25565         |25565                                |{25565}                              |                0|                    1|
25571        |25571         |25571                                |{25571}                              |                0|                    1|
25573        |25573         |25573                                |{25573}                              |                0|                    1|
25578        |25578         |25578                                |{25578}                              |                0|                    1|
25584        |25584         |25584                                |{25584}                              |                0|                    1|
25588        |25588         |25588                                |{25588}                              |                0|                    1|
25589        |25589         |25589                                |{25589}                              |                0|                    1|
25591        |25591         |25591                                |{25591}                              |                0|                    1|
25592        |25592         |25592                                |{25592}                              |                0|                    1|
25596        |25596         |25596                                |{25596}                              |                0|                    1|
25597        |25597         |25597                                |{25597}                              |                0|                    1|
25600        |25600         |25600                                |{25600}                              |                0|                    1|
25601        |25601         |25601                                |{25601}                              |                0|                    1|
25605        |25605         |25605                                |{25605}                              |                0|                    1|
25607        |25607         |25607                                |{25607}                              |                0|                    1|
25609        |25609         |25609                                |{25609}                              |                0|                    1|
25615        |25615         |25615                                |{25615}                              |                0|                    1|
25617        |25617         |25617                                |{25617}                              |                0|                    1|
25619        |25619         |25619                                |{25619}                              |                0|                    1|
25620        |25620         |25620                                |{25620}                              |                0|                    1|
25621        |25621         |25621                                |{25621}                              |                0|                    1|
25623        |25623         |25623                                |{25623}                              |                0|                    1|
25625        |25625         |25625                                |{25625}                              |                0|                    1|
25627        |25627         |25627                                |{25627}                              |                0|                    1|
25630        |25630         |25630                                |{25630}                              |                0|                    1|
25633        |25633         |25633                                |{25633}                              |                0|                    1|
25634        |25634         |25634                                |{25634}                              |                0|                    1|
25635        |25635         |25635                                |{25635}                              |                0|                    1|
             |25362         |                                     |{25362}                              |                 |                    1|

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025;
--> Updated Rows	226

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025)
order by date_eff, com_ap
/*
com_av|ncc_av                  |com_ap|ncc_ap                   |date_eff  |arrete_classement      |nom_commune_arrete_classement |insee_commune_arrete_classement|
------|------------------------|------|-------------------------|----------|-----------------------|------------------------------|-------------------------------|
25636 |VILLEDIEU LE CAMP       |25601 |VERCEL VILLEDIEU LE CAMP |1962-08-20|20/02/1974             |VERCEL                        |25601                          |
25423 |NEUVIER                 |25138 |TERRES DE CHAUX          |1969-12-01|20/02/1974             |TERRES-DE-CHAUX               |25138                          |
25135 |CHATILLON SOUS MAICHE   |25138 |TERRES DE CHAUX          |1969-12-01|20/02/1974             |TERRES-DE-CHAUX               |25138                          |
25169 |COURCELLES LES CHATILLON|25138 |TERRES DE CHAUX          |1969-12-01|20/02/1974             |TERRES-DE-CHAUX               |25138                          |
25272 |GLAINANS                |25018 |ANTEUIL                  |1973-01-01|28/04/1976�29/01/1982  |ANTEUIL                       |25018                          |
25568 |TOURNEDOZ               |25018 |ANTEUIL                  |1973-01-01|28/04/1976�29/01/1982  |ANTEUIL                       |25018                          |
25206 |DROITFONTAINE           |25051 |BELLEHERBE               |1973-01-01|20/02/1974             |BELLEHERBE                    |25051                          |
25081 |BOULOIS                 |25127 |CHARQUEMONT              |1973-01-01|20/02/1974             |CHARQUEMONT                   |25127                          |
25352 |LUISANS                 |25288 |FOURNETS LUISANS         |1973-01-01|{20/02/1974}           |{FOURNETS-LUISANS}            |{25288}                        |
25294 |GRANGES SAINTE MARIE    |25320 |LABERGEMENT SAINTE MARIE |1973-01-01|20/02/1974             |LABERGEMENT-SAINTE-MARIE      |25320                          |
25260 |FRIOLAIS                |25392 |MONT DE VOUGNEY          |1973-01-01|20/02/1974             |MONT-DE-VOUGNEY               |25392                          |
25064 |BLANCHEFONTAINE         |25433 |ORGEANS BLANCHEFONTAINE  |1973-01-01|20/02/1974             |ORGEANS-BLANCHEFONTAINE       |25433                          |
25603 |VERMONDANS              |25463 |PONT DE ROIDE            |1973-01-01|28/04/1976             |PONT-DE-ROIDE                 |25463                          |
25080 |BOUJEONS                |25486 |REMORAY BOUJEONS         |1973-01-01|20/02/1974             |REMORAY-BOUJEONS              |25486                          |
25412 |MOUILLEVILLERS          |25519 |SAINT HIPPOLYTE          |1973-01-01|28/04/1976             |SAINT-HIPPOLYTE               |25519                          |
25407 |MONTURSIN               |25275 |GLERE                    |1973-07-01|20/02/1974             |GLERE                         |25275                          |
25606 |VERNOIS LE FOL          |25275 |GLERE                    |1973-07-01|20/02/1974             |GLERE                         |25275                          |
25362 |MALPAS                  |25592 |VAUX ET CHANTEGRUE MALPAS|1974-01-01|20/02/1974             |VAUX-ET-CHANTEGRUE-MALPAS     |25592                          |
25292 |GRANGES MAILLOT         |25334 |LEVIER                   |1974-04-01|{20/02/1974,20/02/1974}|{LABERGEMENT-DU-NAVOIS,LEVIER}|{25319,25334}                  |
25352 |LUISANS                 |25288 |FOURNETS LUISANS         |2008-01-01|{20/02/1974}           |{FOURNETS-LUISANS}            |{25288}                        |
25603 |VERMONDANS              |25463 |PONT DE ROIDE VERMONDANS |2014-12-06|28/04/1976             |PONT-DE-ROIDE                 |25463                          |
25530 |SANCEY LE LONG          |25529 |SANCEY                   |2016-01-01|28/04/1976             |SANCEY-LE-GRAND               |25529                          |
25292 |GRANGES MAILLOT         |25334 |LEVIER                   |2017-01-01|{20/02/1974,20/02/1974}|{LABERGEMENT-DU-NAVOIS,LEVIER}|{25319,25334}                  |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---025
ORDER BY insee_cog2019, insee_com;


------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- En attente d'un avis de la DDT
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN

departement := '025';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019 as
	SELECT 
	''' || departement || '''::varchar(3) AS code_dep,
	t1.insee_cog2019::varchar(5) AS insee_2019,
	t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
	t2.nom_com::varchar AS nom_min_2019,
	t1.nb_evenements_cog::integer,
	t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
	''En expertise''::varchar AS agreg_nom_classement , 
	''En expertise''::varchar AS agreg_arretes,
	''En expertise''::varchar AS agreg_classement_85,
	''Expertis''::varchar(8) AS classement_2019,
	commentaires::TEXT AS agreg_commentaires,
	''n_adm_exp_cog_commune_000_2019''::varchar(254) AS source_geom,
	t2.geom::geometry(''MULTIPOLYGON'',2154)
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_' || departement || '_final AS t1
	JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
	ON t1.insee_cog2019 = t2.insee_com
	ORDER BY insee_2019;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
		ATTACH PARTITION a_dcap_act/_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- B] Cr�ation de la table des communes de 1985
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp AS
	SELECT
	''' || departement || '''::varchar(3) AS code_dep,
	insee_comm::varchar(5) AS insee_classement,
	nom_commun::varchar(100) AS nom_commune,
	date_decis::varchar AS arrete_classement,
	''Expertis''::varchar(8) AS classement_1985,
	commentair::TEXT AS commentaires
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '
	ORDER BY insee_classement;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- Une fois avis DDT obtenu : 07/09/2020
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN
departement := '025';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 02/03/2021
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                          |count|
-------------------------------------------------------|-----|
P                                                      |    7|
P, non class�e                                         |    1|
totalit�                                               |  214|
{totalit�}                                             |    1|
{totalit�,totalit�}                                    |    1|
{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|    1|
*/

---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_025_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_025_2019 as
SELECT 
'025'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'                   
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'              
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	
	WHEN t1.classement_85 = 'P, non class�e' THEN 'partie'
	WHEN t1.classement_85 = 'P' THEN 'partie'                  
    
	/*
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   */
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_025_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 225

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_025_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  217|
partie         |    8|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_025_2019 FOR VALUES IN ('025');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 RENAME TO l_liste_commune_montagne_1985_025_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |    8|
totalit�|  223|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 AS
SELECT
'025'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'
	WHEN partie = 'p' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025_old
ORDER BY insee_classement;
--> 231

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  223|
partie         |    8|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_025 FOR VALUES IN ('025');
--> Updated Rows	0








