---- 08/07/2020
----- ARRIEGE ----
---- IMPORT donn�es

ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME COLUMN "D�partement" TO departemen;
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME COLUMN "Code Commune" TO insee_comm;
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME COLUMN "Nom Commune" TO nom_commun;
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME COLUMN "Date de d�cision France (2)" TO date_decis;
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME COLUMN "Pointage 2020" TO pointage;
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME COLUMN "Partie de commune" TO partie;
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME COLUMN "Commentaires" TO commentair;
--> Queries	7
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 SET partie = 'totalit�' WHERE partie ='';
--> Updated Rows	225

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012;
--> 226

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019           |insee_commune_arrete_classement      |nom_commune_arrete_classement                                                           |arrete_classement                                                  |classement_commune                                     |commentaires                                                                                                                                                                                      |nb_evenements_cog|
-------------|------------------------------|-------------------------------------|----------------------------------------------------------------------------------------|-------------------------------------------------------------------|-------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
12076        |CONQUES EN ROUERGUE           |{12076,12114,12218}                  |{CONQUES,GRAND-VABRE,SAINT-CYPRIEN-SUR-DOURDOU}                                         |{20/02/1974,20/02/1974,"20/02/1974�28/04/1976"}                    |{totalit�,totalit�,totalit�}                           |{"","","Arr�t� de 1974�: lieudits Labros, le Gauffour, Lapeyre, Lesverdus, La Carriere, La Salles, GrandVal, Gensac, La Souque, La Gade, Monclas, Las Cases�Arr�t� de 1976�: reste du territoire"}|                3|
12120        |LAISSAC SEVERAC L EGLISE      |{12120,12271}                        |{LAISSAC,SEVERAC-L'EGLISE}                                                              |{20/02/1974,20/02/1974}                                            |{totalit�,totalit�}                                    |{"",""}                                                                                                                                                                                           |                2|
12133        |LUC LA PRIMAUBE               |{12133}                              |{LUC}                                                                                   |{20/02/1974}                                                       |{totalit�}                                             |{""}                                                                                                                                                                                              |                1|
12177        |PALMAS D AVEYRON              |{12081,12087,12177}                  |{COUSSERGUES,CRUEJOULS,PALMAS}                                                          |{20/02/1974,20/02/1974,20/02/1974}                                 |{totalit�,totalit�,totalit�}                           |{"","",""}                                                                                                                                                                                        |                3|
12223        |ARGENCES EN AUBRAC            |{12005,12112,12117,12223,12279,12304}|{ALPUECH,GRAISSAC,LACALM,SAINTE-GENEVIEVE(-SUR-ARGENCE),"LA TERRISSE",VITRAC-EN-VIADENE}|{20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974}|{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|{"","","","SAINTE-GENEVIEVE-SUR-ARGENCE dans fichier d�origine","",""}                                                                                                                            |                6|
12224        |SAINT GENIEZ D OLT ET D AUBRAC|{12014,12224}                        |{AURELLE-VERLAC,SAINT-GENIEZ(-D'OLT)}                                                   |{20/02/1974,20/02/1974}                                            |{totalit�,totalit�}                                    |{"","SAINT-GENIEZ-D'OLT dans fichier d�otigine"}                                                                                                                                                  |                2|
12270        |SEVERAC D AVEYRON             |{12040,12123,12126,12196,12270}      |{BUZEINS,LAPANOUSE-DE-SEVERAC,LAVERNHE,RECOULES-PREVINQUIERES,SEVERAC-LE-CHATEAU}       |{20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974}           |{totalit�,totalit�,totalit�,totalit�,totalit�}         |{"","","","",""}                                                                                                                                                                                  |                5|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 99 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	210

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '12' ---012
ORDER BY insee_cog2019, insee_com;

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012;
--> Updated Rows	210

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012)
order by date_eff, com_ap
/*
com_av|ncc_av        |com_ap|ncc_ap      |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|--------------|------|------------|----------|-----------------|-----------------------------|-------------------------------|
12302 |VILLEVAYRE    |12167 |NAJAC       |1965-02-26|28/04/20176      |NAJAC                        |12167                          |
12306 |VORS          |12056 |BARAQUEVILLE|1973-01-01|20/02/1974       |BARAQUEVILLE                 |12056                          |
12245 |SAINT SALVADOU|12021 |BAS SEGALA  |2016-01-01|28/04/20176      |LA BASTIDE-L'EVEQUE          |12021                          |
12285 |VABRE TIZAC   |12021 |BAS SEGALA  |2016-01-01|28/04/20176      |LA BASTIDE-L'EVEQUE          |12021                          |
 */

---- Correction du fichier final :
--- 08/07/2020

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '12' ---012
ORDER BY insee_cog2019, insee_com;


---- 24/11/2020
---- MAJ des informations sur les tables 
---- l_liste_commune_montagne_2019_012_final
---- l_liste_commune_montagne_1985_012


------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                          |count|
-------------------------------------------------------|-----|
totalit�                                               |  202|
{totalit�}                                             |    1|
totalit�, non class�e, non class�e                     |    1|
{totalit�,totalit�}                                    |    2|
{totalit�,totalit�,totalit�}                           |    1|
{totalit�,totalit�,totalit�,totalit�}                  |    1|
{totalit�,totalit�,totalit�,totalit�,totalit�}         |    1|
{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_012_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_012_2019 as
SELECT 
'012'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�, non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'   
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_012_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 210

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_012_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  209|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_012_2019 FOR VALUES IN ('012');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 RENAME TO l_liste_commune_montagne_1985_012_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|  226|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 AS
SELECT
'012'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012_old
ORDER BY insee_classement;
--> 226

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  226|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_012 FOR VALUES IN ('012');
--> Updated Rows	0
