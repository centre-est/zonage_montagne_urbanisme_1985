---- 31/07/2020
----- LOIRE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042  WHERE date_decis IS NULL;

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	151

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042;
--> 170

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019               |insee_commune_arrete_classement|nom_commune_arrete_classement       |arrete_classement      |classement_commune |commentaires                                                                                                                                                                                                                                         |nb_evenements_cog|
-------------|----------------------------------|-------------------------------|------------------------------------|-----------------------|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
42019        |BOEN SUR LIGNON                   |{42019}                        |{BOEN}                              |{1976-04-28}           |{P}                |{"Par d�cret n�2012-938, le nom de la commune est pass� de Bo�n � Bo�n-sur-Lignon"}                                                                                                                                                                  |                1|
42039        |CHALMAZEL JEANSAGNIERE            |{42039,42114}                  |{CHALMAZEL,JEANSAGNIERE}            |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                                                                                                                                                                          |                2|
42142        |MERLE LEIGNEC                     |{42142}                        |{MERLE}                             |{1974-02-20}           |{totalit�}         |{"01/04/1993 : Merle devient Merle-Leignec."}                                                                                                                                                                                                        |                1|
42191        |ROISEY                            |{42191}                        |{ROISEY}                            |{1974-02-20}           |{totalit�}         |{"Ecrit Roizey dans l�arr�t� de 1974"}                                                                                                                                                                                                               |                1|
42239        |SAINT JEAN SAINT MAURICE SUR LOIRE|{42239}                        |{SAINT-JEAN-SAINT-MAURICE-SUR-LOIRE}|{1976-04-28}           |{totalit�}         |{"Ecrit Saint-Jean-le-Puy et aussi Saint-Maurice-sur-Loire sur l�arr�t� mais une seule commune en 1976 car le 01/04/1974 la commune devient Saint-Jean-Saint-Maurice-sur-Loire suite � sa fusion-association avec Saint-Maurice-sur-Loire (42263). "}|                1|
42245        |VETRE SUR ANZON                   |{42245,42291}                  |{SAINT-JULIEN-LA-VETRE,SAINT-THURIN}|{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                                                                                                                                                                          |                2|
42321        |VALLA SUR ROCHEFORT               |{42321}                        |{"LA VALLA"}                        |{1974-02-20}           |{totalit�}         |{NULL}                                                                                                                                                                                                                                               |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 161 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	169

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '42' ---042
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
42001        |42001         |42001                          |{42001}           |                0|                    1|
42002        |42002         |42002                          |{42002}           |                0|                    1|
42003        |42003         |42003                          |{42003}           |                0|                    1|
42006        |42006         |42006                          |{42006}           |                0|                    1|
42007        |42007         |42007                          |{42007}           |                0|                    1|
42008        |42008         |42008                          |{42008}           |                0|                    1|
42010        |42010         |42010                          |{42010}           |                0|                    1|
42012        |42012         |42012                          |{42012}           |                0|                    1|
42014        |42014         |42014                          |{42014}           |                0|                    1|
42015        |42015         |42015                          |{42015}           |                0|                    1|
42017        |42017         |42017                          |{42017}           |                0|                    1|
42019        |42019         |{42019}                        |{42019}           |                1|                    1|
42023        |42023         |42023                          |{42023}           |                0|                    1|
42027        |42027         |42027                          |{42027}           |                0|                    1|
42028        |42028         |42028                          |{42028}           |                0|                    1|
42029        |42029         |42029                          |{42029}           |                0|                    1|
42031        |42031         |42031                          |{42031}           |                0|                    1|
42032        |42032         |42032                          |{42032}           |                0|                    1|
42033        |42033         |42033                          |{42033}           |                0|                    1|
42034        |42034         |42034                          |{42034}           |                0|                    1|
42035        |42035         |42035                          |{42035}           |                0|                    1|
42036        |42036         |42036                          |{42036}           |                0|                    1|
42039        |42039         |{42039,42114}                  |{42039,42114}     |                2|                    2|
42040        |42040         |42040                          |{42040}           |                0|                    1|
42042        |42042         |42042                          |{42042}           |                0|                    1|
42045        |42045         |42045                          |{42045}           |                0|                    1|
42047        |42047         |42047                          |{42047}           |                0|                    1|
42050        |42050         |42050                          |{42050}           |                0|                    1|
42051        |42051         |42051                          |{42051}           |                0|                    1|
42053        |42053         |42053                          |{42053}           |                0|                    1|
42054        |42054         |42054                          |{42054}           |                0|                    1|
42055        |42055         |42055                          |{42055}           |                0|                    1|
42058        |42058         |42058                          |{42058}           |                0|                    1|
42059        |42059         |42059                          |{42059}           |                0|                    1|
42060        |42060         |42060                          |{42060}           |                0|                    1|
42061        |42061         |42061                          |{42061}           |                0|                    1|
42062        |42062         |42062                          |{42062}           |                0|                    1|
42063        |42063         |42063                          |{42063}           |                0|                    1|
42064        |42064         |42064                          |{42064}           |                0|                    1|
42067        |42067         |42067                          |{42067}           |                0|                    1|
42072        |42072         |42072                          |{42072}           |                0|                    1|
42073        |42073         |42073                          |{42073}           |                0|                    1|
42076        |42076         |42076                          |{42076}           |                0|                    1|
42079        |42079         |42079                          |{42079}           |                0|                    1|
42084        |42084         |42084                          |{42084}           |                0|                    1|
42085        |42085         |42085                          |{42085}           |                0|                    1|
42086        |42086         |42086                          |{42086}           |                0|                    1|
42089        |42089         |42089                          |{42089}           |                0|                    1|
42090        |42090         |42090                          |{42090}           |                0|                    1|
42091        |42091         |42091                          |{42091}           |                0|                    1|
42093        |42093         |42093                          |{42093}           |                0|                    1|
42096        |42096         |42096                          |{42096}           |                0|                    1|
42100        |42100         |42100                          |{42100}           |                0|                    1|
42101        |42101         |42101                          |{42101}           |                0|                    1|
42102        |42102         |42102                          |{42102}           |                0|                    1|
42107        |42107         |42107                          |{42107}           |                0|                    1|
42109        |42109         |42109                          |{42109}           |                0|                    1|
42113        |42113         |42113                          |{42113}           |                0|                    1|
42115        |42115         |42115                          |{42115}           |                0|                    1|
42116        |42116         |42116                          |{42116}           |                0|                    1|
42117        |42117         |42117                          |{42117}           |                0|                    1|
42119        |42119         |42119                          |{42119}           |                0|                    1|
42121        |42121         |42121                          |{42121}           |                0|                    1|
42122        |42122         |42122                          |{42122}           |                0|                    1|
42125        |42125         |42125                          |{42125}           |                0|                    1|
42126        |42126         |42126                          |{42126}           |                0|                    1|
42128        |42128         |42128                          |{42128}           |                0|                    1|
42133        |42133         |42133                          |{42133}           |                0|                    1|
42134        |42134         |42134                          |{42134}           |                0|                    1|
42136        |42136         |42136                          |{42136}           |                0|                    1|
42137        |42137         |42137                          |{42137}           |                0|                    1|
42138        |42138         |42138                          |{42138}           |                0|                    1|
42139        |42139         |42139                          |{42139}           |                0|                    1|
42140        |42140         |42140                          |{42140}           |                0|                    1|
42142        |42142         |{42142}                        |{42142}           |                1|                    1|
42146        |42146         |42146                          |{42146}           |                0|                    1|
42148        |42148         |42148                          |{42148}           |                0|                    1|
42158        |42158         |42158                          |{42158}           |                0|                    1|
42159        |42159         |42159                          |{42159}           |                0|                    1|
42164        |42164         |42164                          |{42164}           |                0|                    1|
42165        |42165         |42165                          |{42165}           |                0|                    1|
42167        |42167         |42167                          |{42167}           |                0|                    1|
42168        |42168         |42168                          |{42168}           |                0|                    1|
42169        |42169         |42169                          |{42169}           |                0|                    1|
42172        |42172         |42172                          |{42172}           |                0|                    1|
42179        |42179         |42179                          |{42179}           |                0|                    1|
42182        |42182         |42182                          |{42182}           |                0|                    1|
42188        |42188         |42188                          |{42188}           |                0|                    1|
42191        |42191         |{42191}                        |{42191}           |                1|                    1|
42192        |42192         |42192                          |{42192}           |                0|                    1|
42193        |42193         |42193                          |{42193}           |                0|                    1|
42195        |42195         |42195                          |{42195}           |                0|                    1|
42196        |42196         |42196                          |{42196}           |                0|                    1|
42198        |42198         |42198                          |{42198}           |                0|                    1|
42199        |42199         |42199                          |{42199}           |                0|                    1|
42201        |42201         |42201                          |{42201}           |                0|                    1|
42202        |42202         |42202                          |{42202}           |                0|                    1|
42203        |42203         |42203                          |{42203}           |                0|                    1|
42204        |42204         |42204                          |{42204}           |                0|                    1|
42205        |42205         |42205                          |{42205}           |                0|                    1|
42207        |42207         |42207                          |{42207}           |                0|                    1|
42208        |42208         |42208                          |{42208}           |                0|                    1|
42209        |42209         |42209                          |{42209}           |                0|                    1|
42210        |42210         |42210                          |{42210}           |                0|                    1|
42213        |42213         |42213                          |{42213}           |                0|                    1|
42216        |42216         |42216                          |{42216}           |                0|                    1|
42217        |42217         |42217                          |{42217}           |                0|                    1|
42218        |42218         |42218                          |{42218}           |                0|                    1|
42224        |42224         |42224                          |{42224}           |                0|                    1|
42225        |42225         |42225                          |{42225}           |                0|                    1|
42227        |42227         |42227                          |{42227}           |                0|                    1|
42229        |42229         |42229                          |{42229}           |                0|                    1|
42232        |42232         |42232                          |{42232}           |                0|                    1|
42233        |42233         |42233                          |{42233}           |                0|                    1|
42234        |42234         |42234                          |{42234}           |                0|                    1|
42235        |42235         |42235                          |{42235}           |                0|                    1|
42238        |42238         |42238                          |{42238}           |                0|                    1|
42239        |42239         |{42239}                        |{42239}           |                1|                    1|
42240        |42240         |42240                          |{42240}           |                0|                    1|
42245        |42245         |{42245,42291}                  |{42291,42245}     |                2|                    2|
42246        |42246         |42246                          |{42246}           |                0|                    1|
42247        |42247         |42247                          |{42247}           |                0|                    1|
42248        |42248         |42248                          |{42248}           |                0|                    1|
42252        |42252         |42252                          |{42252}           |                0|                    1|
42255        |42255         |42255                          |{42255}           |                0|                    1|
42259        |42259         |42259                          |{42259}           |                0|                    1|
42260        |42260         |42260                          |{42260}           |                0|                    1|
42261        |42261         |42261                          |{42261}           |                0|                    1|
42262        |42262         |42262                          |{42262}           |                0|                    1|
42264        |42264         |42264                          |{42264}           |                0|                    1|
42266        |42266         |42266                          |{42266}           |                0|                    1|
42270        |42270         |42270                          |{42270}           |                0|                    1|
42271        |42271         |42271                          |{42271}           |                0|                    1|
42274        |42274         |42274                          |{42274}           |                0|                    1|
42276        |42276         |42276                          |{42276}           |                0|                    1|
42278        |42278         |42278                          |{42278}           |                0|                    1|
42280        |42280         |42280                          |{42280}           |                0|                    1|
42281        |42281         |42281                          |{42281}           |                0|                    1|
42282        |42282         |42282                          |{42282}           |                0|                    1|
42283        |42283         |42283                          |{42283}           |                0|                    1|
42286        |42286         |42286                          |{42286}           |                0|                    1|
42287        |42287         |42287                          |{42287}           |                0|                    1|
42288        |42288         |42288                          |{42288}           |                0|                    1|
42293        |42293         |42293                          |{42293}           |                0|                    1|
42295        |42295         |42295                          |{42295}           |                0|                    1|
42297        |42297         |42297                          |{42297}           |                0|                    1|
42298        |42298         |42298                          |{42298}           |                0|                    1|
42300        |42300         |42300                          |{42300}           |                0|                    1|
42301        |42301         |42301                          |{42301}           |                0|                    1|
42302        |42302         |42302                          |{42302}           |                0|                    1|
42306        |42306         |42306                          |{42306}           |                0|                    1|
42308        |42308         |42308                          |{42308}           |                0|                    1|
42310        |42310         |42310                          |{42310}           |                0|                    1|
42312        |42312         |42312                          |{42312}           |                0|                    1|
42313        |42313         |42313                          |{42313}           |                0|                    1|
42314        |42314         |42314                          |{42314}           |                0|                    1|
42318        |42318         |42318                          |{42318}           |                0|                    1|
42320        |42320         |42320                          |{42320}           |                0|                    1|
42321        |42321         |{42321}                        |{42321}           |                1|                    1|
42322        |42322         |42322                          |{42322}           |                0|                    1|
42326        |42326         |42326                          |{42326}           |                0|                    1|
42328        |42328         |42328                          |{42328}           |                0|                    1|
42329        |42329         |42329                          |{42329}           |                0|                    1|
42331        |42331         |42331                          |{42331}           |                0|                    1|
42334        |42334         |42334                          |{42334}           |                0|                    1|
42335        |42335         |42335                          |{42335}           |                0|                    1|
42336        |42336         |42336                          |{42336}           |                0|                    1|
42339        |42339         |42339                          |{42339}           |                0|                    1|
             |42104         |                               |{42104}           |                 |                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042;
--> Updated Rows	168

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042)
order by date_eff, com_ap
/*
com_av|ncc_av                   |com_ap|ncc_ap                            |date_eff  |arrete_classement|nom_commune_arrete_classement       |insee_commune_arrete_classement|
------|-------------------------|------|----------------------------------|----------|-----------------|------------------------------------|-------------------------------|
42258 |SAINT MARTIN EN COAILLEUX|42207 |SAINT CHAMOND                     |1964-03-14|28/04/1976       |SAINT-CHAMOND                       |42207                          |
42111 |IZIEUX                   |42207 |SAINT CHAMOND                     |1964-03-14|28/04/1976       |SAINT-CHAMOND                       |42207                          |
42244 |SAINT JULIEN EN JAREZ    |42207 |SAINT CHAMOND                     |1964-03-14|28/04/1976       |SAINT-CHAMOND                       |42207                          |
42292 |SAINT VICTOR SUR LOIRE   |42218 |SAINT ETIENNE SUR LOIRE           |1969-10-18|1974-02-20       |SAINT-ETIENNE                       |42218                          |
42309 |TERRENOIRE               |42218 |SAINT ETIENNE                     |1970-01-01|1974-02-20       |SAINT-ETIENNE                       |42218                          |
42190 |ROCHETAILLEE             |42218 |SAINT ETIENNE                     |1973-01-01|1974-02-20       |SAINT-ETIENNE                       |42218                          |
42080 |CULA                     |42225 |GENILAC                           |1973-06-01|1976-04-28       |GENILAC                             |42225                          |
42263 |SAINT MAURICE SUR LOIRE  |42239 |SAINT JEAN SAINT MAURICE SUR LOIRE|1974-04-01|{1976-04-28}     |{SAINT-JEAN-SAINT-MAURICE-SUR-LOIRE}|{42239}                        |
42263 |SAINT MAURICE SUR LOIRE  |42239 |SAINT JEAN SAINT MAURICE SUR LOIRE|2008-01-01|{1976-04-28}     |{SAINT-JEAN-SAINT-MAURICE-SUR-LOIRE}|{42239}                        |
 */

------------------------------------------------------------------------------------------------------------------------------------------------
---- 21/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
P                  |   18|
{P}                |    1|
totalit�           |  144|
{totalit�}         |    4|
{totalit�,totalit�}|    2|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_042_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_042_2019 as
SELECT 
'042'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	
	WHEN t1.classement_85 = 'P' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{P}' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_042_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 169

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_042_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  150|
partie         |   19|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_042_2019 FOR VALUES IN ('042');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 RENAME TO l_liste_commune_montagne_1985_042_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |   19|
totalit�|  152|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 AS
SELECT
'042'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042_old
ORDER BY insee_classement;
--> 171

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  152|
partie         |   19|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_042 FOR VALUES IN ('042');
--> Updated Rows	0