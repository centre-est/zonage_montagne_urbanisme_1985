
---- 31/07/2020
----- JURA ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039  WHERE date_decis IS NULL;
/*
id |departemen|insee_comm|nom_commun |date_decis|pointage|partie|commentair                                                                                                   |
---|----------|----------|-----------|----------|--------|------|-------------------------------------------------------------------------------------------------------------|
112|39        |39436     |PONT-D'HERY|          |x       |      |1974�: Sections de Fonteny et de Pont-d�Hery�1976�: Reste du territoire non class� par l�arr�t� du 20/02/1974|
*/

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 SET date_decis='20/02/1974,28/04/1976' WHERE date_decis IS NULL;
--> Updated Rows	1

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	153

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039;
--> 153

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019    |insee_commune_arrete_classement|nom_commune_arrete_classement                            |arrete_classement                            |classement_commune                   |commentaires                                                                                                     |nb_evenements_cog|
-------------|-----------------------|-------------------------------|---------------------------------------------------------|---------------------------------------------|-------------------------------------|-----------------------------------------------------------------------------------------------------------------|-----------------|
39130        |NANCHEZ                |{39130,39417,39442,39562}      |{CHAUX-DES-PRES,"LES PIARDS",PRENOVEL,VILLARD-SUR-BIENNE}|{1974-02-20,1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�,totalit�}|{NULL,NULL,NULL,NULL}                                                                                            |                4|
39258        |GRANDE RIVIERE CHATEAU |{39115,39258}                  |{CHATEAU-DES-PRES,GRANDE-RIVIERE}                        |{1974-02-20,1974-02-20}                      |{totalit�,totalit�}                  |{NULL,NULL}                                                                                                      |                2|
39286        |LAVANS LES SAINT CLAUDE|{39286,39438,39440}            |{LAVANS-LES-SAINT-CLAUDE,PONTHOUX,PRATZ}                 |{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{NULL,NULL,NULL}                                                                                                 |                3|
39329        |MIEGES                 |{39213,39329,39340}            |{ESSERVAL-COMBE,MIEGES,MOLPRE}                           |{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{NULL,NULL,NULL}                                                                                                 |                3|
39331        |MIGNOVILLARD           |{39161,39331}                  |{COMMUNAILLES-EN-MONTAGNE,MIGNOVILLARD}                  |{1974-02-20,1974-02-20}                      |{totalit�,totalit�}                  |{NULL,NULL}                                                                                                      |                2|
39339        |CHASSAL MOLINGES       |{39113,39339}                  |{CHASSAL,MOLINGES}                                       |{1976-04-28,1976-04-28}                      |{totalit�,totalit�}                  |{NULL,NULL}                                                                                                      |                2|
39367        |MORBIER                |{39367,39524}                  |{MORBIER,TANCUA}                                         |{1974-02-20,1974-02-20}                      |{totalit�,totalit�}                  |{NULL,"commune non pr�sente dans fichier d�origine"}                                                             |                2|
39368        |HAUTS DE BIENNE        |{39294,39368,39371}            |{LEZAT,MOREZ,"LA MOUILLE"}                               |{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{NULL,NULL,NULL}                                                                                                 |                3|
39372        |MOURNANS CHARBONNY     |{39372}                        |{MOURNANS}                                               |{1974-02-20}                                 |{totalit�}                           |{"MOURNANS-CHARBONNY dans le fichier d�origine"}                                                                 |                1|
39436        |PONT D HERY            |{39436}                        |{PONT-D'HERY}                                            |{"20/02/1974,28/04/1976"}                    |{totalit�}                           |{"1974�: Sections de Fonteny et de Pont-d�Hery�1976�: Reste du territoire non class� par l�arr�t� du 20/02/1974"}|                1|
39491        |COTEAUX DU LIZON       |{39186,39491}                  |{CUTTURA,SAINT-LUPICIN}                                  |{1974-02-20,1974-02-20}                      |{totalit�,totalit�}                  |{NULL,NULL}                                                                                                      |                2|
39510        |SEPTMONCEL LES MOLUNES |{39341,39510}                  |{"LES MOLUNES",SEPTMONCEL}                               |{1974-02-20,1974-02-20}                      |{totalit�,totalit�}                  |{NULL,NULL}                                                                                                      |                2|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 126 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	138

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '39' ---039
ORDER BY insee_cog2019, insee_com;

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039;
--> Updated Rows	22

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039)
order by date_eff, com_ap
/*
com_av|ncc_av                 |com_ap|ncc_ap                    |date_eff  |arrete_classement        |nom_commune_arrete_classement          |insee_commune_arrete_classement|
------|-----------------------|------|--------------------------|----------|-------------------------|---------------------------------------|-------------------------------|
39257 |GRAND CHATEL           |39561 |VILLARDS D HERIA          |1947-04-12|1974-02-20               |VILLARDS-D'HERIA                       |39561                          |
39516 |SIEGES                 |39579 |VIRY                      |1947-04-12|1974-02-20               |VIRY                                   |39579                          |
39374 |MOUTAINE ARESCHES      |39586 |ARESCHES                  |1950-11-11|1974-02-20               |ARESCHES                               |39586                          |
39416 |PETIT VILLARD          |39331 |MIGNOVILLARD PETIT VILLARD|1966-01-01|{1974-02-20,1974-02-20}  |{COMMUNAILLES-EN-MONTAGNE,MIGNOVILLARD}|{39161,39331}                  |
39098 |CHAMPAGNY              |39133 |CHAUX CHAMPAGNY           |1973-01-01|1974-02-20               |CHAUX-CHAMPAGNY                        |39133                          |
39242 |FROIDEFONTAINE         |39331 |MIGNOVILLARD              |1973-01-01|{1974-02-20,1974-02-20}  |{COMMUNAILLES-EN-MONTAGNE,MIGNOVILLARD}|{39161,39331}                  |
39212 |ESSAVILLY              |39331 |MIGNOVILLARD              |1973-01-01|{1974-02-20,1974-02-20}  |{COMMUNAILLES-EN-MONTAGNE,MIGNOVILLARD}|{39161,39331}                  |
39410 |PERRENA                |39424 |PLANCHES EN MONTAGNE      |1973-01-01|1974-02-20               |LES PLANCHES-EN-MONTAGNE               |39424                          |
39231 |FONTENY                |39436 |PONT D HERY               |1973-01-01|{"20/02/1974,28/04/1976"}|{PONT-D'HERY}                          |{39436}                        |
39374 |MOUTAINE               |39436 |PONT D HERY               |1973-01-01|{"20/02/1974,28/04/1976"}|{PONT-D'HERY}                          |{39436}                        |
39181 |CRILLAT                |39493 |SAINT MAURICE CRILLAT     |1973-01-01|1974-02-20               |SAINT-MAURICE-CRILLAT                  |39493                          |
39536 |TREFFAY                |39517 |SIROD                     |1973-01-01|1974-02-20               |SIROD                                  |39517                          |
39459 |RIVIERE DEVANT         |39258 |GRANDE RIVIERE            |1973-03-01|{1974-02-20,1974-02-20}  |{CHATEAU-DES-PRES,GRANDE-RIVIERE}      |{39115,39258}                  |
39541 |VALFIN LES SAINT CLAUDE|39478 |SAINT CLAUDE              |1974-01-01|1974-02-20               |SAINT-CLAUDE                           |39478                          |
39152 |CINQUETRAL             |39478 |SAINT CLAUDE              |1974-01-01|1974-02-20               |SAINT-CLAUDE                           |39478                          |
39144 |CHEVRY                 |39478 |SAINT CLAUDE              |1974-01-01|1974-02-20               |SAINT-CLAUDE                           |39478                          |
39450 |RANCHETTE              |39478 |SAINT CLAUDE              |1974-01-01|1974-02-20               |SAINT-CLAUDE                           |39478                          |
39125 |CHAUMONT               |39478 |SAINT CLAUDE              |1974-01-01|1974-02-20               |SAINT-CLAUDE                           |39478                          |
39242 |FROIDEFONTAINE         |39331 |MIGNOVILLARD              |1977-03-01|{1974-02-20,1974-02-20}  |{COMMUNAILLES-EN-MONTAGNE,MIGNOVILLARD}|{39161,39331}                  |
39212 |ESSAVILLY              |39331 |MIGNOVILLARD              |1977-03-01|{1974-02-20,1974-02-20}  |{COMMUNAILLES-EN-MONTAGNE,MIGNOVILLARD}|{39161,39331}                  |
39374 |MOUTAINE               |39436 |PONT D HERY               |2006-01-01|{"20/02/1974,28/04/1976"}|{PONT-D'HERY}                          |{39436}                        |
39231 |FONTENY                |39436 |PONT D HERY               |2006-01-01|{"20/02/1974,28/04/1976"}|{PONT-D'HERY}                          |{39436}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---039
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 22/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                        |count|
-------------------------------------|-----|
totalit�                             |  126|
{totalit�}                           |    2|
{totalit�,totalit�}                  |    6|
{totalit�,totalit�,totalit�}         |    3|
{totalit�,totalit�,totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_039_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_039_2019 as
SELECT 
'039'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
'totalit�'::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_039_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 138

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_039_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  138|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_039_2019 FOR VALUES IN ('039');
--> Updated Rows	0

---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 RENAME TO l_liste_commune_montagne_1985_039_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|  153|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 AS
SELECT
'039'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039_old
ORDER BY insee_classement;
--> 153

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  153|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_039 FOR VALUES IN ('039');
--> Updated Rows	0
