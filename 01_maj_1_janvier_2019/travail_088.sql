---- 25/08/2020
----- VOSGES ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088  WHERE date_decis IS NULL;

--UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	93

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088;
--> 93

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019   |insee_commune_arrete_classement|nom_commune_arrete_classement  |arrete_classement      |classement_commune |commentaires|nb_evenements_cog|
-------------|----------------------|-------------------------------|-------------------------------|-----------------------|-------------------|------------|-----------------|
88106        |BAN SUR MEURTHE CLEFCY|{88106}                        |{CLEFCY}                       |{1974-02-20}           |{totalit�}         |{""}        |                1|
88218        |GRANGES AUMONTZEY     |{88018,88218}                  |{AUMONTZEY,GRANGES-SUR-VOLOGNE}|{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL} |                2|
88361        |PROVENCHERES ET COLROY|{88112}                        |{COLROY-LA-GRANDE}             |{1983-09-20}           |{totalit�}         |{NULL}      |                1|
88413        |SAINT DIE DES VOSGES  |{88413}                        |{SAINT-DIE}                    |{1983-09-20}           |{totalit�}         |{NULL}      |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	91

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '88' ---088
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
88005        |88005         |88005                          |{88005}           |                0|                    1|
88009        |88009         |88009                          |{88009}           |                0|                    1|
88014        |88014         |88014                          |{88014}           |                0|                    1|
88032        |88032         |88032                          |{88032}           |                0|                    1|
88033        |88033         |88033                          |{88033}           |                0|                    1|
88035        |88035         |88035                          |{88035}           |                0|                    1|
88037        |88037         |88037                          |{88037}           |                0|                    1|
88053        |88053         |88053                          |{88053}           |                0|                    1|
88059        |88059         |88059                          |{88059}           |                0|                    1|
88064        |88064         |88064                          |{88064}           |                0|                    1|
88075        |88075         |88075                          |{88075}           |                0|                    1|
88081        |88081         |88081                          |{88081}           |                0|                    1|
88082        |88082         |88082                          |{88082}           |                0|                    1|
88085        |88085         |88085                          |{88085}           |                0|                    1|
88089        |88089         |88089                          |{88089}           |                0|                    1|
88093        |88093         |88093                          |{88093}           |                0|                    1|
88106        |88106         |{88034,88106}                  |{88106}           |                2|                    1|
88109        |88109         |88109                          |{88109}           |                0|                    1|
88115        |88115         |88115                          |{88115}           |                0|                    1|
88116        |88116         |88116                          |{88116}           |                0|                    1|
88120        |88120         |88120                          |{88120}           |                0|                    1|
88148        |88148         |88148                          |{88148}           |                0|                    1|
88158        |88158         |88158                          |{88158}           |                0|                    1|
88159        |88159         |88159                          |{88159}           |                0|                    1|
88167        |88167         |88167                          |{88167}           |                0|                    1|
88170        |88170         |88170                          |{88170}           |                0|                    1|
88177        |88177         |88177                          |{88177}           |                0|                    1|
88181        |88181         |88181                          |{88181}           |                0|                    1|
88188        |88188         |88188                          |{88188}           |                0|                    1|
88193        |88193         |88193                          |{88193}           |                0|                    1|
88196        |88196         |88196                          |{88196}           |                0|                    1|
88197        |88197         |88197                          |{88197}           |                0|                    1|
88198        |88198         |88198                          |{88198}           |                0|                    1|
88205        |88205         |88205                          |{88205}           |                0|                    1|
88213        |88213         |88213                          |{88213}           |                0|                    1|
88215        |88215         |88215                          |{88215}           |                0|                    1|
88218        |88218         |{88018,88218}                  |{88218,88018}     |                2|                    2|
88240        |88240         |88240                          |{88240}           |                0|                    1|
88244        |88244         |88244                          |{88244}           |                0|                    1|
88256        |88256         |88256                          |{88256}           |                0|                    1|
88263        |88263         |88263                          |{88263}           |                0|                    1|
88268        |88268         |88268                          |{88268}           |                0|                    1|
88269        |88269         |88269                          |{88269}           |                0|                    1|
88275        |88275         |88275                          |{88275}           |                0|                    1|
88276        |88276         |88276                          |{88276}           |                0|                    1|
88277        |88277         |88277                          |{88277}           |                0|                    1|
88284        |88284         |88284                          |{88284}           |                0|                    1|
88300        |88300         |88300                          |{88300}           |                0|                    1|
88302        |88302         |88302                          |{88302}           |                0|                    1|
88306        |88306         |88306                          |{88306}           |                0|                    1|
88317        |88317         |88317                          |{88317}           |                0|                    1|
88320        |88320         |88320                          |{88320}           |                0|                    1|
88345        |88345         |88345                          |{88345}           |                0|                    1|
88346        |88346         |88346                          |{88346}           |                0|                    1|
88349        |88349         |88349                          |{88349}           |                0|                    1|
88361        |88361         |{88112}                        |{88112}           |                1|                    1|
88362        |88362         |88362                          |{88362}           |                0|                    1|
88369        |88369         |88369                          |{88369}           |                0|                    1|
88373        |88373         |88373                          |{88373}           |                0|                    1|
88380        |88380         |88380                          |{88380}           |                0|                    1|
88391        |88391         |88391                          |{88391}           |                0|                    1|
88398        |88398         |88398                          |{88398}           |                0|                    1|
88408        |88408         |88408                          |{88408}           |                0|                    1|
88409        |88409         |88409                          |{88409}           |                0|                    1|
88413        |88413         |{88413}                        |{88413}           |                1|                    1|
88415        |88415         |88415                          |{88415}           |                0|                    1|
88419        |88419         |88419                          |{88419}           |                0|                    1|
88426        |88426         |88426                          |{88426}           |                0|                    1|
88436        |88436         |88436                          |{88436}           |                0|                    1|
88442        |88442         |88442                          |{88442}           |                0|                    1|
88444        |88444         |88444                          |{88444}           |                0|                    1|
88447        |88447         |88447                          |{88447}           |                0|                    1|
88451        |88451         |88451                          |{88451}           |                0|                    1|
88462        |88462         |88462                          |{88462}           |                0|                    1|
88463        |88463         |88463                          |{88463}           |                0|                    1|
88464        |88464         |88464                          |{88464}           |                0|                    1|
88467        |88467         |88467                          |{88467}           |                0|                    1|
88468        |88468         |88468                          |{88468}           |                0|                    1|
88470        |88470         |88470                          |{88470}           |                0|                    1|
88486        |88486         |88486                          |{88486}           |                0|                    1|
88487        |88487         |88487                          |{88487}           |                0|                    1|
88492        |88492         |88492                          |{88492}           |                0|                    1|
88498        |88498         |88498                          |{88498}           |                0|                    1|
88500        |88500         |88500                          |{88500}           |                0|                    1|
88501        |88501         |88501                          |{88501}           |                0|                    1|
88503        |88503         |88503                          |{88503}           |                0|                    1|
88505        |88505         |88505                          |{88505}           |                0|                    1|
88506        |88506         |88506                          |{88506}           |                0|                    1|
88526        |88526         |88526                          |{88526}           |                0|                    1|
88528        |88528         |88528                          |{88528}           |                0|                    1|
88531        |88531         |88531                          |{88531}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088;
--> Updated Rows	91

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088)
order by date_eff, com_ap
/*
com_av|ncc_av               |com_ap|ncc_ap                |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|---------------------|------|----------------------|----------|-----------------|-----------------------------|-------------------------------|
88361 |PROVENCHERES SUR FAVE|88361 |PROVENCHERES ET COLROY|2016-01-01|{1983-09-20}     |{COLROY-LA-GRANDE}           |{88112}                        |
88361 |PROVENCHERES SUR FAVE|88361 |PROVENCHERES SUR FAVE |2016-01-01|{1983-09-20}     |{COLROY-LA-GRANDE}           |{88112}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---088
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- En attente d'un avis de la DDT
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN

departement := '088';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019 as
	SELECT 
	''' || departement || '''::varchar(3) AS code_dep,
	t1.insee_cog2019::varchar(5) AS insee_2019,
	t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
	t2.nom_com::varchar AS nom_min_2019,
	t1.nb_evenements_cog::integer,
	t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
	''En expertise''::varchar AS agreg_nom_classement , 
	''En expertise''::varchar AS agreg_arretes,
	''En expertise''::varchar AS agreg_classement_85,
	''Expertis''::varchar(8) AS classement_2019,
	commentaires::TEXT AS agreg_commentaires,
	''n_adm_exp_cog_commune_000_2019''::varchar(254) AS source_geom,
	t2.geom::geometry(''MULTIPOLYGON'',2154)
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_' || departement || '_final AS t1
	JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
	ON t1.insee_cog2019 = t2.insee_com
	ORDER BY insee_2019;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- B] Cr�ation de la table des communes de 1985
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp AS
	SELECT
	''' || departement || '''::varchar(3) AS code_dep,
	insee_comm::varchar(5) AS insee_classement,
	nom_commun::varchar(100) AS nom_commune,
	date_decis::varchar AS arrete_classement,
	''Expertis''::varchar(8) AS classement_1985,
	commentair::TEXT AS commentaires
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '
	ORDER BY insee_classement;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- Une fois avis DDT obtenu : 02/03/2021
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN
departement := '088';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 02/03/2021
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
totalit�           |   87|
{totalit�}         |    2|
{totalit�,totalit�}|    2|
*/

---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_088_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_088_2019 as
SELECT 
'088'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'                     
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = 'P, non class�e' THEN 'partie'
	WHEN t1.classement_85 = 'P' THEN 'partie'                  
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_088_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 91

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_088_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   91|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_088_2019 FOR VALUES IN ('088');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 RENAME TO l_liste_commune_montagne_1985_088_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|   93|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 AS
SELECT
'088'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'
	WHEN partie = 'p' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088_old
ORDER BY insee_classement;
--> 93

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   93|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_088 FOR VALUES IN ('088');
--> Updated Rows	0
