---- 31/07/2020
----- ISERE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038  WHERE date_decis IS NULL;


UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	178

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038;
--> 228

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019       |insee_commune_arrete_classement|nom_commune_arrete_classement                          |arrete_classement                 |classement_commune          |commentaires                                                                                                                                                                                       |nb_evenements_cog|
-------------|--------------------------|-------------------------------|-------------------------------------------------------|----------------------------------|----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
38073        |CHANTEPERIER              |{38073,38302}                  |{CHANTELOUVE,"LE PERIER"}                              |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                        |                2|
38163        |HAUT BREDA                |{38163,38306}                  |{"LA FERRIERE",PINSOT}                                 |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                        |                2|
38216        |MALLEVAL EN VERCORS       |{38216}                        |{MALLEVAL}                                             |{1974-02-20}                      |{totalit�}                  |{NULL}                                                                                                                                                                                             |                1|
38225        |AUTRANS MEAUDRE EN VERCORS|{38021,38225}                  |{AUTRANS,MEAUDRE}                                      |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                        |                2|
38253        |DEUX ALPES                |{38253,38534}                  |{MONT-DE-LANS,VENOSC}                                  |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                        |                2|
38280        |NOTRE DAME DE VAULX       |{38280}                        |{NOTRE-DAME-DE-VAUX}                                   |{1974-02-20}                      |{totalit�}                  |{NULL}                                                                                                                                                                                             |                1|
38395        |PLATEAU DES PETITES ROCHES|{38367,38395,38435}            |{SAINT-BERNARD,SAINT-HILAIRE-DU-TOUVET,SAINT-PANCRASSE}|{1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�}|{"Ecrit Saint-Bernad-du-Touvet dans l�arr�t� de 1974","SAINT-HILAIRE dans fichier d�origine",NULL}                                                                                                 |                3|
38407        |SURE EN CHARTREUSE        |{38312,38407}                  |{POMMIERS-LA-PLACETTE,SAINT-JULIEN-DE-RAZ}             |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                        |                2|
38418        |SAINTE MARIE DU MONT      |{38466}                        |{SAINT-VINCENT-DE-MERCUZE-SAINTE-MARIE-DU-MONT}        |{1974-02-20}                      |{p}                         |{"01/01/1973 : Sainte-Marie-du-Mont est rattach�e � Saint-Vincent-de-Mercuze (38466) (fusion association) qui devient Saint-Vincent-de-Mercuze-Sainte-Marie-du-Mont. �Section Saint-Marie du Mont"}|                1|
38422        |SAINT MARTIN D URIAGE     |{38422}                        |{SAINT-MARTIN-D'URIAGE}                                |{1974-02-20}                      |{totalit�}                  |{NULL}                                                                                                                                                                                             |                1|
38439        |CRETS EN BELLEDONNE       |{38262,38439}                  |{MORETEL-DE-MAILLES,SAINT-PIERRE-D'ALLEVARD}           |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                        |                2|
38456        |CHATEL EN TRIEVES         |{38125,38456}                  |{CORDEAC,SAINT-SEBASTIEN}                              |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                        |                2|
38466        |SAINT VINCENT DE MERCUZE  |{38466}                        |{SAINT-VINCENT-DE-MERCUZE-SAINTE-MARIE-DU-MONT}        |{1974-02-20}                      |{p}                         |{"01/01/1973 : Sainte-Marie-du-Mont est rattach�e � Saint-Vincent-de-Mercuze (38466) (fusion association) qui devient Saint-Vincent-de-Mercuze-Sainte-Marie-du-Mont. �Section Saint-Marie du Mont"}|                1|
38478        |SECHILIENNE               |{38478}                        |{SECHILIENNE}                                          |{1974-02-20}                      |{totalit�}                  |{NULL}                                                                                                                                                                                             |                1|
38529        |VAULNAVEYS LE HAUT        |{38529}                        |{VAULNAVEYS-LE-HAUT}                                   |{1974-02-20}                      |{totalit�}                  |{NULL}                                                                                                                                                                                             |                1|
38567        |CHAMROUSSE                |{38422,38478,38529}            |{SAINT-MARTIN-D'URIAGE,SECHILIENNE,VAULNAVEYS-LE-HAUT} |{1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�}|{NULL,NULL,NULL}                                                                                                                                                                                   |                3|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 205 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	221

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '38' ---038
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|-------------------|-----------------|---------------------|
38002        |38002         |38002                          |{38002}            |                0|                    1|
38005        |38005         |38005                          |{38005}            |                0|                    1|
38006        |38006         |38006                          |{38006}            |                0|                    1|
38008        |38008         |38008                          |{38008}            |                0|                    1|
38020        |38020         |38020                          |{38020}            |                0|                    1|
38023        |38023         |38023                          |{38023}            |                0|                    1|
38031        |38031         |38031                          |{38031}            |                0|                    1|
38036        |38036         |38036                          |{38036}            |                0|                    1|
38040        |38040         |38040                          |{38040}            |                0|                    1|
38043        |38043         |38043                          |{38043}            |                0|                    1|
38045        |38045         |38045                          |{38045}            |                0|                    1|
38052        |38052         |38052                          |{38052}            |                0|                    1|
38060        |38060         |38060                          |{38060}            |                0|                    1|
38061        |38061         |38061                          |{38061}            |                0|                    1|
38070        |38070         |38070                          |{38070}            |                0|                    1|
38073        |38073         |{38073,38302}                  |{38073,38302}      |                2|                    2|
38075        |38075         |38075                          |{38075}            |                0|                    1|
38078        |38078         |38078                          |{38078}            |                0|                    1|
38082        |38082         |38082                          |{38082}            |                0|                    1|
38086        |38086         |38086                          |{38086}            |                0|                    1|
38090        |38090         |38090                          |{38090}            |                0|                    1|
38092        |38092         |38092                          |{38092}            |                0|                    1|
38100        |38100         |38100                          |{38100}            |                0|                    1|
38103        |38103         |38103                          |{38103}            |                0|                    1|
38105        |38105         |38105                          |{38105}            |                0|                    1|
38106        |38106         |38106                          |{38106}            |                0|                    1|
38108        |38108         |38108                          |{38108}            |                0|                    1|
38111        |38111         |38111                          |{38111}            |                0|                    1|
38112        |38112         |38112                          |{38112}            |                0|                    1|
38113        |38113         |38113                          |{38113}            |                0|                    1|
38115        |38115         |38115                          |{38115}            |                0|                    1|
38116        |38116         |38116                          |{38116}            |                0|                    1|
38117        |38117         |38117                          |{38117}            |                0|                    1|
38120        |38120         |38120                          |{38120}            |                0|                    1|
38126        |38126         |38126                          |{38126}            |                0|                    1|
38127        |38127         |38127                          |{38127}            |                0|                    1|
38128        |38128         |38128                          |{38128}            |                0|                    1|
38129        |38129         |38129                          |{38129}            |                0|                    1|
38132        |38132         |38132                          |{38132}            |                0|                    1|
38133        |38133         |38133                          |{38133}            |                0|                    1|
38137        |38137         |38137                          |{38137}            |                0|                    1|
38150        |38150         |38150                          |{38150}            |                0|                    1|
38153        |38153         |38153                          |{38153}            |                0|                    1|
38154        |38154         |38154                          |{38154}            |                0|                    1|
38155        |38155         |38155                          |{38155}            |                0|                    1|
38163        |38163         |{38163,38306}                  |{38306,38163}      |                2|                    2|
38171        |38171         |38171                          |{38171}            |                0|                    1|
38173        |38173         |38173                          |{38173}            |                0|                    1|
38175        |38175         |38175                          |{38175}            |                0|                    1|
38177        |38177         |38177                          |{38177}            |                0|                    1|
38181        |38181         |38181                          |{38181}            |                0|                    1|
38186        |38186         |38186                          |{38186}            |                0|                    1|
38187        |38187         |38187                          |{38187}            |                0|                    1|
38188        |38188         |38188                          |{38188}            |                0|                    1|
38191        |38191         |38191                          |{38191}            |                0|                    1|
38192        |38192         |38192                          |{38192}            |                0|                    1|
38195        |38195         |38195                          |{38195}            |                0|                    1|
38203        |38203         |38203                          |{38203}            |                0|                    1|
38204        |38204         |38204                          |{38204}            |                0|                    1|
38205        |38205         |38205                          |{38205}            |                0|                    1|
38206        |38206         |38206                          |{38206}            |                0|                    1|
38207        |38207         |38207                          |{38207}            |                0|                    1|
38208        |38208         |38208                          |{38208}            |                0|                    1|
38212        |38212         |38212                          |{38212}            |                0|                    1|
38216        |38216         |{38216}                        |{38216}            |                1|                    1|
38217        |38217         |38217                          |{38217}            |                0|                    1|
38222        |38222         |38222                          |{38222}            |                0|                    1|
38224        |38224         |38224                          |{38224}            |                0|                    1|
38225        |38225         |{38021,38225}                  |{38225,38021}      |                2|                    2|
38226        |38226         |38226                          |{38226}            |                0|                    1|
38228        |38228         |38228                          |{38228}            |                0|                    1|
38235        |38235         |38235                          |{38235}            |                0|                    1|
38236        |38236         |38236                          |{38236}            |                0|                    1|
38237        |38237         |38237                          |{38237}            |                0|                    1|
38241        |38241         |38241                          |{38241}            |                0|                    1|
38242        |38242         |38242                          |{38242}            |                0|                    1|
38243        |38243         |38243                          |{38243}            |                0|                    1|
38248        |38248         |38248                          |{38248}            |                0|                    1|
38252        |38252         |38252                          |{38252}            |                0|                    1|
38253        |38253         |{38253,38534}                  |{38253,38534}      |                2|                    2|
38254        |38254         |38254                          |{38254}            |                0|                    1|
38256        |38256         |38256                          |{38256}            |                0|                    1|
38258        |38258         |38258                          |{38258}            |                0|                    1|
38263        |38263         |38263                          |{38263}            |                0|                    1|
38264        |38264         |38264                          |{38264}            |                0|                    1|
38265        |38265         |38265                          |{38265}            |                0|                    1|
38266        |38266         |38266                          |{38266}            |                0|                    1|
38268        |38268         |38268                          |{38268}            |                0|                    1|
38269        |38269         |38269                          |{38269}            |                0|                    1|
38271        |38271         |38271                          |{38271}            |                0|                    1|
38272        |38272         |38272                          |{38272}            |                0|                    1|
38273        |38273         |38273                          |{38273}            |                0|                    1|
38275        |38275         |38275                          |{38275}            |                0|                    1|
38277        |38277         |38277                          |{38277}            |                0|                    1|
38278        |38278         |38278                          |{38278}            |                0|                    1|
38279        |38279         |38279                          |{38279}            |                0|                    1|
38280        |38280         |{38280}                        |{38280}            |                1|                    1|
38281        |38281         |38281                          |{38281}            |                0|                    1|
38283        |38283         |38283                          |{38283}            |                0|                    1|
38285        |38285         |38285                          |{38285}            |                0|                    1|
38286        |38286         |38286                          |{38286}            |                0|                    1|
38289        |38289         |38289                          |{38289}            |                0|                    1|
38299        |38299         |38299                          |{38299}            |                0|                    1|
38301        |38301         |38301                          |{38301}            |                0|                    1|
38304        |38304         |38304                          |{38304}            |                0|                    1|
38308        |38308         |38308                          |{38308}            |                0|                    1|
38313        |38313         |38313                          |{38313}            |                0|                    1|
38314        |38314         |38314                          |{38314}            |                0|                    1|
38319        |38319         |38319                          |{38319}            |                0|                    1|
38321        |38321         |38321                          |{38321}            |                0|                    1|
38322        |38322         |38322                          |{38322}            |                0|                    1|
38325        |38325         |38325                          |{38325}            |                0|                    1|
38326        |38326         |38326                          |{38326}            |                0|                    1|
38328        |38328         |38328                          |{38328}            |                0|                    1|
38329        |38329         |38329                          |{38329}            |                0|                    1|
38330        |38330         |38330                          |{38330}            |                0|                    1|
38333        |38333         |38333                          |{38333}            |                0|                    1|
38334        |38334         |38334                          |{38334}            |                0|                    1|
38338        |38338         |38338                          |{38338}            |                0|                    1|
38342        |38342         |38342                          |{38342}            |                0|                    1|
38345        |38345         |38345                          |{38345}            |                0|                    1|
38350        |38350         |38350                          |{38350}            |                0|                    1|
38355        |38355         |38355                          |{38355}            |                0|                    1|
38356        |38356         |38356                          |{38356}            |                0|                    1|
38361        |38361         |38361                          |{38361}            |                0|                    1|
38362        |38362         |38362                          |{38362}            |                0|                    1|
38364        |38364         |38364                          |{38364}            |                0|                    1|
38366        |38366         |38366                          |{38366}            |                0|                    1|
38372        |38372         |38372                          |{38372}            |                0|                    1|
38375        |38375         |38375                          |{38375}            |                0|                    1|
38376        |38376         |38376                          |{38376}            |                0|                    1|
38383        |38383         |38383                          |{38383}            |                0|                    1|
38386        |38386         |38386                          |{38386}            |                0|                    1|
38387        |38387         |38387                          |{38387}            |                0|                    1|
38388        |38388         |38388                          |{38388}            |                0|                    1|
38390        |38390         |38390                          |{38390}            |                0|                    1|
38391        |38391         |38391                          |{38391}            |                0|                    1|
38395        |38395         |{38367,38395,38435}            |{38435,38367,38395}|                3|                    3|
38396        |38396         |38396                          |{38396}            |                0|                    1|
38397        |38397         |38397                          |{38397}            |                0|                    1|
38402        |38402         |38402                          |{38402}            |                0|                    1|
38403        |38403         |38403                          |{38403}            |                0|                    1|
38404        |38404         |38404                          |{38404}            |                0|                    1|
38405        |38405         |38405                          |{38405}            |                0|                    1|
38407        |38407         |{38312,38407}                  |{38312,38407}      |                2|                    2|
38412        |38412         |38412                          |{38412}            |                0|                    1|
38413        |38413         |38413                          |{38413}            |                0|                    1|
38414        |38414         |38414                          |{38414}            |                0|                    1|
38418        |38418         |{38466}                        |{38418}            |                1|                    1|
38419        |38419         |38419                          |{38419}            |                0|                    1|
38420        |38420         |38420                          |{38420}            |                0|                    1|
38421        |38421         |38421                          |{38421}            |                0|                    1|
38422        |38422         |{38422}                        |{38422}            |                1|                    1|
38423        |38423         |38423                          |{38423}            |                0|                    1|
38424        |38424         |38424                          |{38424}            |                0|                    1|
38426        |38426         |38426                          |{38426}            |                0|                    1|
38427        |38427         |38427                          |{38427}            |                0|                    1|
38428        |38428         |38428                          |{38428}            |                0|                    1|
38429        |38429         |38429                          |{38429}            |                0|                    1|
38430        |38430         |38430                          |{38430}            |                0|                    1|
38432        |38432         |38432                          |{38432}            |                0|                    1|
38433        |38433         |38433                          |{38433}            |                0|                    1|
38437        |38437         |38437                          |{38437}            |                0|                    1|
38438        |38438         |38438                          |{38438}            |                0|                    1|
38439        |38439         |{38262,38439}                  |{38439,38262}      |                2|                    2|
38442        |38442         |38442                          |{38442}            |                0|                    1|
38443        |38443         |38443                          |{38443}            |                0|                    1|
38444        |38444         |38444                          |{38444}            |                0|                    1|
38445        |38445         |38445                          |{38445}            |                0|                    1|
38446        |38446         |38446                          |{38446}            |                0|                    1|
38453        |38453         |38453                          |{38453}            |                0|                    1|
38456        |38456         |{38125,38456}                  |{38125,38456}      |                2|                    2|
38460        |38460         |38460                          |{38460}            |                0|                    1|
38462        |38462         |38462                          |{38462}            |                0|                    1|
38463        |38463         |38463                          |{38463}            |                0|                    1|
38466        |38466         |{38466}                        |{38466}            |                1|                    1|
38469        |38469         |38469                          |{38469}            |                0|                    1|
38470        |38470         |38470                          |{38470}            |                0|                    1|
38471        |38471         |38471                          |{38471}            |                0|                    1|
38472        |38472         |38472                          |{38472}            |                0|                    1|
38474        |38474         |38474                          |{38474}            |                0|                    1|
38478        |38478         |{38478}                        |{38478}            |                1|                    1|
38485        |38485         |38485                          |{38485}            |                0|                    1|
38489        |38489         |38489                          |{38489}            |                0|                    1|
38492        |38492         |38492                          |{38492}            |                0|                    1|
38497        |38497         |38497                          |{38497}            |                0|                    1|
38499        |38499         |38499                          |{38499}            |                0|                    1|
38501        |38501         |38501                          |{38501}            |                0|                    1|
38503        |38503         |38503                          |{38503}            |                0|                    1|
38504        |38504         |38504                          |{38504}            |                0|                    1|
38511        |38511         |38511                          |{38511}            |                0|                    1|
38513        |38513         |38513                          |{38513}            |                0|                    1|
38514        |38514         |38514                          |{38514}            |                0|                    1|
38516        |38516         |38516                          |{38516}            |                0|                    1|
38517        |38517         |38517                          |{38517}            |                0|                    1|
38518        |38518         |38518                          |{38518}            |                0|                    1|
38521        |38521         |38521                          |{38521}            |                0|                    1|
38522        |38522         |38522                          |{38522}            |                0|                    1|
38523        |38523         |38523                          |{38523}            |                0|                    1|
38524        |38524         |38524                          |{38524}            |                0|                    1|
38526        |38526         |38526                          |{38526}            |                0|                    1|
38527        |38527         |38527                          |{38527}            |                0|                    1|
38528        |38528         |38528                          |{38528}            |                0|                    1|
38529        |38529         |{38529}                        |{38529}            |                1|                    1|
38531        |38531         |38531                          |{38531}            |                0|                    1|
38533        |38533         |38533                          |{38533}            |                0|                    1|
38538        |38538         |38538                          |{38538}            |                0|                    1|
38540        |38540         |38540                          |{38540}            |                0|                    1|
38545        |38545         |38545                          |{38545}            |                0|                    1|
38547        |38547         |38547                          |{38547}            |                0|                    1|
38548        |38548         |38548                          |{38548}            |                0|                    1|
38549        |38549         |38549                          |{38549}            |                0|                    1|
38550        |38550         |38550                          |{38550}            |                0|                    1|
38551        |38551         |38551                          |{38551}            |                0|                    1|
38552        |38552         |38552                          |{38552}            |                0|                    1|
38559        |38559         |38559                          |{38559}            |                0|                    1|
38562        |38562         |38562                          |{38562}            |                0|                    1|
38563        |38563         |38563                          |{38563}            |                0|                    1|
38564        |38564         |38564                          |{38564}            |                0|                    1|
38565        |38565         |38565                          |{38565}            |                0|                    1|
38567        |38567         |{38422,38478,38529}            |{38567}            |                3|                    1|
             |38209         |                               |{38209}            |                 |                    1|
             |38561         |                               |{38561}            |                 |                    1|
 */


---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038;
--> Updated Rows	221

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap                  |insee_comm|nom_commun                                   |pointage|partie  |date_decis|date_eff  |mod|
------|------------------------|----------|---------------------------------------------|--------|--------|----------|----------|---|
38418 |SAINTE MARIE DU MONT    |38418     |SAINT-VINCENT-DE-MERCUZE-SAINTE-MARIE-DU-MONT|x       |p       |1974-02-20|1984-01-01|21 |
38418 |SAINTE MARIE DU MONT    |38466     |SAINT-VINCENT-DE-MERCUZE                     |x       |totalit�|1974-02-20|1984-01-01|21 |
38466 |SAINT VINCENT DE MERCUZE|38466     |SAINT-VINCENT-DE-MERCUZE                     |x       |totalit�|1974-02-20|1984-01-01|21 |
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038)
order by date_eff, com_ap
/*
com_av|ncc_av            |com_ap|ncc_ap                   |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|------------------|------|-------------------------|----------|-----------------|-----------------------------|-------------------------------|
38007 |ALLIERES ET RISSET|38524 |VARCES ALLIERES ET RISSET|1955-07-01|1974-02-20       |VARCES-ALLIERES-ET-RISSET    |38524                          |
38477 |SAVEL             |38224 |MAYRES SAVEL             |1965-03-01|1974-02-20       |MAYRES-SAVEL                 |38224                          |
38385 |SAINT GENIS       |38226 |MENS                     |1973-01-01|1974-02-20       |MENS                         |38226                          |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2 avec fichier ddt38
--- Import du fichier : a_dcap_actu_arretes_montagne_dhup."N_MONTAGNE_ZSUP_038"

:
SELECT insee_cog2019, insee AS ddt_insee_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup."N_MONTAGNE_ZSUP_038"
ON insee_cog2019 = insee
--WHERE LEFT(insee_com,2) = 'dd' ---038
ORDER BY insee_cog2019, insee;
/*
insee_cog2019|ddt_insee_com|
-------------|-------------|
38002        |38002        |
38005        |38005        |
38006        |38006        |
38008        |38008        |
38020        |38020        |
38023        |38023        |
38031        |38031        |
38036        |38036        |
38040        |38040        |
38043        |38043        |
38045        |38045        |
38052        |38052        |
38060        |38060        |
38061        |38061        |
38070        |38070        |
38073        |38073        |
38075        |38075        |
38078        |38078        |
38082        |38082        |
38086        |38086        |
38090        |38090        |
38092        |38092        |
38100        |38100        |
38103        |38103        |
38105        |38105        |
38106        |38106        |
38108        |38108        |
38111        |38111        |
38112        |38112        |
38113        |38113        |
38115        |38115        |
38116        |38116        |
38117        |38117        |
38120        |38120        |
38126        |38126        |
38127        |38127        |
38128        |38128        |
38129        |38129        |
38132        |38132        |
38133        |38133        |
38137        |38137        |
38150        |38150        |
38153        |38153        |
38154        |38154        |
38155        |38155        |
38163        |38163        |
38171        |38171        |
38173        |38173        |
38175        |38175        |
38177        |38177        |
38181        |38181        |
38186        |38186        |
38187        |38187        |
38188        |38188        |
38191        |38191        |
38192        |38192        |
38195        |38195        |
38203        |38203        |
38204        |38204        |
38205        |38205        |
38206        |38206        |
38207        |38207        |
38208        |38208        |
38212        |38212        |
38216        |38216        |
38217        |38217        |
38222        |38222        |
38224        |38224        |
38225        |38225        |
38226        |38226        |
38228        |38228        |
38235        |38235        |
38236        |38236        |
38237        |38237        |
38241        |38241        |
38242        |38242        |
38243        |38243        |
38248        |38248        |
38252        |38252        |
38253        |38253        |
38254        |38254        |
38256        |38256        |
38258        |38258        |
38263        |38263        |
38264        |38264        |
38265        |38265        |
38266        |38266        |
38268        |38268        |
38269        |38269        |
38271        |38271        |
38272        |38272        |
38273        |38273        |
38275        |38275        |
38277        |38277        |
38278        |38278        |
38279        |38279        |
38280        |38280        |
38281        |38281        |
38283        |38283        |
38285        |38285        |
38286        |38286        |
38289        |38289        |
38299        |38299        |
38301        |38301        |
38304        |38304        |
38308        |38308        |
38313        |38313        |
38314        |38314        |
38319        |38319        |
38321        |38321        |
38322        |38322        |
38325        |38325        |
38326        |38326        |
38328        |38328        |
38329        |38329        |
38330        |38330        |
38333        |38333        |
38334        |38334        |
38338        |38338        |
38342        |38342        |
38345        |38345        |
38350        |38350        |
38355        |38355        |
38356        |38356        |
38361        |38361        |
38362        |38362        |
38364        |38364        |
38366        |38366        |
38372        |38372        |
38375        |38375        |
38376        |38376        |
38383        |38383        |
38386        |38386        |
38387        |38387        |
38388        |38388        |
38390        |38390        |
38391        |38391        |
38395        |38395        |
38396        |38396        |
38397        |38397        |
38402        |38402        |
38403        |38403        |
38404        |38404        |
38405        |38405        |
38407        |38407        |
38412        |38412        |
38413        |38413        |
38414        |38414        |
38418        |38418        |
38419        |38419        |
38420        |38420        |
38421        |38421        |
38422        |38422        |
38423        |38423        |
38424        |38424        |
38426        |38426        |
38427        |38427        |
38428        |38428        |
38429        |38429        |
38430        |38430        |
38432        |38432        |
38433        |38433        |
38437        |38437        |
38438        |38438        |
38439        |38439        |
38442        |38442        |
38443        |38443        |
38444        |38444        |
38445        |38445        |
38446        |38446        |
38453        |38453        |
38456        |38456        |
38460        |38460        |
38462        |38462        |
38463        |38463        |
38466        |             |
38469        |38469        |
38470        |38470        |
38471        |38471        |
38472        |38472        |
38474        |38474        |
38478        |38478        |
38485        |38485        |
38489        |38489        |
38492        |38492        |
38497        |38497        |
38499        |38499        |
38501        |38501        |
38503        |38503        |
38504        |38504        |
38511        |38511        |
38513        |38513        |
38514        |38514        |
38516        |38516        |
38517        |38517        |
38518        |38518        |
38521        |38521        |
38522        |38522        |
38523        |38523        |
38524        |38524        |
38526        |38526        |
38527        |38527        |
38528        |38528        |
38529        |38529        |
38531        |38531        |
38533        |38533        |
38538        |38538        |
38540        |38540        |
38545        |38545        |
38547        |38547        |
38548        |38548        |
38549        |38549        |
38550        |38550        |
38551        |38551        |
38552        |38552        |
38559        |38559        |
38562        |38562        |
38563        |38563        |
38564        |38564        |
38565        |38565        |
38567        |38567        |
             |38125        |
             |38302        |
             |38306        |
             |38312        |
             |38367        |
             |38435        |
             |38534        |
 */


------------------------------------------------------------------------------------------------------------------------------------------------
---- 25/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85               |count|
----------------------------|-----|
P                           |   49|
totalit�                    |  157|
{totalit�}                  |    5|
{totalit�,totalit�}         |    7|
{totalit�,totalit�,totalit�}|    2|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019 as
SELECT 
'038'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = 'P' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�}' THEN 'totalit�'
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_038_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 220

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  171|
partie         |   49|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_038_2019 FOR VALUES IN ('038');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 RENAME TO l_liste_commune_montagne_1985_038_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
p       |    1|
P       |   49|
totalit�|  178|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 AS
SELECT
'038'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'
	WHEN partie = 'p' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038_old
ORDER BY insee_classement;
--> 228

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  178|
partie         |   50|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_038 FOR VALUES IN ('038');
--> Updated Rows	0



