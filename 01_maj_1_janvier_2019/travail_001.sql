---- 08/07/2020
----- AIN ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	176

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001;
-->130

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019     |insee_commune_1985       |nom_commune_1985                                              |arretes                                        |classement_85                        |nb_commune_1985|commentaires                                                                                                                                                                                                 |code_evenement_cog|type_commune_avant|type_commune_apr�s|
-------------|------------------------|-------------------------|--------------------------------------------------------------|-----------------------------------------------|-------------------------------------|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|------------------|------------------|

*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 99 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	113

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '01' ---001
ORDER BY insee_cog2019, insee_com;

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001;
--> Updated Rows	113

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001)
order by date_eff, com_ap
/*
com_av|ncc_av        |com_ap|ncc_ap                  |date_eff  |arrete_classement                              |nom_commune_arrete_classement                               |insee_commune_arrete_classement|
------|--------------|------|------------------------|----------|-----------------------------------------------|------------------------------------------------------------|-------------------------------|
01164 |FORENS        |01104 |CHEZERY FORENS          |1962-08-27|1974-02-20                                     |CHEZERY-FORENS                                              |01104                          |
01201 |LACOUX        |01185 |HAUTEVILLE LOMPNES      |1964-09-01|{1974-02-20,1974-02-20,1974-02-20,1974-02-20}  |{CORMARANCHE-EN-BUGEY,HAUTEVILLE-LOMPNES,HOSTIAS,THEZILLIEU}|{01122,01185,01186,01417}      |
01222 |LONGECOMBE    |01185 |HAUTEVILLE LOMPNES      |1964-09-01|{1974-02-20,1974-02-20,1974-02-20,1974-02-20}  |{CORMARANCHE-EN-BUGEY,HAUTEVILLE-LOMPNES,HOSTIAS,THEZILLIEU}|{01122,01185,01186,01417}      |
01438 |VESENEX CRASSY|01143 |DIVONNE LES BAINS       |1965-02-15|1974-02-20                                     |DIVONNE-LES-BAINS                                           |01143                          |
01126 |COUPY         |01033 |BELLEGARDE SUR VALSERINE|1966-03-23|{"1974-02-20 1976-04-28",1974-02-20,1974-02-20}|{BELLEGARDE-SUR-VALSERINE,CHATILLON-EN-MICHAILLE,LANCRANS}  |{01033,01091,01205}            |
01018 |ARLOD         |01033 |BELLEGARDE SUR VALSERINE|1971-01-01|{"1974-02-20 1976-04-28",1974-02-20,1974-02-20}|{BELLEGARDE-SUR-VALSERINE,CHATILLON-EN-MICHAILLE,LANCRANS}  |{01033,01091,01205}            |
01217 |LILIGNOD      |01079 |CHAMPAGNE EN VALROMEY   |1973-01-01|{1976-04-28}                                   |{CHAMPAGNE-EN-VALROMEY}                                     |{01079}                        |
01287 |PASSIN        |01079 |CHAMPAGNE EN VALROMEY   |1973-01-01|{1976-04-28}                                   |{CHAMPAGNE-EN-VALROMEY}                                     |{01079}                        |
01131 |CRAZ          |01189 |INJOUX GENISSIAT        |1973-01-01|1974-02-20                                     |INJOUX-GENISSIAT                                            |01189                          |
01178 |GRANGES       |01240 |MATAFELON GRANGES       |1973-01-01|1974-02-20 1976-04-28                          |MATAFELON-GRANGES                                           |01240                          |
01440 |VEYZIAT       |01283 |OYONNAX                 |1973-01-01|{1974-02-20}                                   |{OYONNAX}                                                   |{01283}                        |
01055 |BOUVENT       |01283 |OYONNAX                 |1973-01-01|{1974-02-20}                                   |{OYONNAX}                                                   |{01283}                        |
01455 |VOLOGNAT      |01267 |NURIEUX VOLOGNAT        |1973-03-01|1974-02-20                                     |NURIEUX-VOLOGNAT                                            |01267                          |
01270 |NAPT          |01410 |SONTHONNAX LA MONTAGNE  |1974-01-01|1974-02-20                                     |SONTHONNAX-LA-MONTAGNE                                      |01410                          |
01226 |LUTHEZIEU     |01036 |BELMONT LUTHEZIEU       |1974-11-01|{1974-02-20,1974-02-20,1974-02-20,1976-04-28}  |{BELMONT-LUTHEZIEU,LOMPNIEU,SUTRIEU,VIEU}                   |{01036,01221,01414,01442}      |
01217 |LILIGNOD      |01079 |CHAMPAGNE EN VALROMEY   |1997-01-01|{1976-04-28}                                   |{CHAMPAGNE-EN-VALROMEY}                                     |{01079}                        |
01287 |PASSIN        |01079 |CHAMPAGNE EN VALROMEY   |1997-01-01|{1976-04-28}                                   |{CHAMPAGNE-EN-VALROMEY}                                     |{01079}                        |
01226 |LUTHEZIEU     |01036 |BELMONT LUTHEZIEU       |1997-12-01|{1974-02-20,1974-02-20,1974-02-20,1976-04-28}  |{BELMONT-LUTHEZIEU,LOMPNIEU,SUTRIEU,VIEU}                   |{01036,01221,01414,01442}      |
01440 |VEYZIAT       |01283 |OYONNAX                 |2015-01-01|{1974-02-20}                                   |{OYONNAX}                                                   |{01283}                        |
01015 |ARBIGNIEU     |01015 |ARBIGNIEU               |2016-01-01|{1974-02-20}                                   |{SAINT-BOIS}                                                |{01340}                        |
01015 |ARBIGNIEU     |01015 |ARBOYS EN BUGEY         |2016-01-01|{1974-02-20}                                   |{SAINT-BOIS}                                                |{01340}                        |
 */

---- Correction du fichier final :
--- le 07/07/2020

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '01' ---001
ORDER BY insee_cog2019, insee_com;




















------------------------------------------------------------------------------------------------------------------------------------------------
---- 23/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                 |count|
----------------------------------------------|-----|
Partie de commune                             |    1|
totalit�                                      |   98|
{totalit�}                                    |    5|
{totalit�} non class�e                        |    1|
{totalit�,totalit�}                           |    3|
{totalit�,totalit�,totalit�}                  |    1|
{totalit�,totalit�,totalit�,totalit�}         |    3|
{totalit�,totalit�,totalit�,totalit�} totalit�|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_001_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_001_2019 as
SELECT 
'001'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'	
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�} totalit�' THEN 'totalit�'
	/*
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	*/   
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_001_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 113

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_001_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  111|
partie         |    2|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_001_2019 FOR VALUES IN ('001');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 RENAME TO l_liste_commune_montagne_1985_001_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001_old
GROUP BY partie
ORDER BY partie;
/*
partie           |count|
-----------------|-----|
Partie de commune|    1|
totalit�         |  129|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 AS
SELECT
'001'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001_old
ORDER BY insee_classement;
--> 130

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  182|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_001 FOR VALUES IN ('001');
--> Updated Rows	0