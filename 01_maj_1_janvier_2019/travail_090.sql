---- 25/08/2020
---- 090
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090  WHERE date_decis IS NULL;

--UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	12

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090;
--> 12

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019     |insee_commune_1985       |nom_commune_1985                                              |arretes                                        |classement_85                        |nb_commune_1985|commentaires                                                                                                                                                                                                 |code_evenement_cog|type_commune_avant|type_commune_apr�s|
-------------|------------------------|-------------------------|--------------------------------------------------------------|-----------------------------------------------|-------------------------------------|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|------------------|------------------|

*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 12 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	12

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '90' ---090
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
90005        |90005         |90005                          |{90005}           |                0|                    1|
90006        |90006         |90006                          |{90006}           |                0|                    1|
90041        |90041         |90041                          |{90041}           |                0|                    1|
90052        |90052         |90052                          |{90052}           |                0|                    1|
90054        |90054         |90054                          |{90054}           |                0|                    1|
90061        |90061         |90061                          |{90061}           |                0|                    1|
90065        |90065         |90065                          |{90065}           |                0|                    1|
90079        |90079         |90079                          |{90079}           |                0|                    1|
90085        |90085         |90085                          |{90085}           |                0|                    1|
90088        |90088         |90088                          |{90088}           |                0|                    1|
90089        |90089         |90089                          |{90089}           |                0|                    1|
90102        |90102         |90102                          |{90102}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090;
--> Updated Rows	12

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090)
order by date_eff, com_ap
/*
com_av|ncc_av       |com_ap|ncc_ap   |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|-------------|------|---------|----------|-----------------|-----------------------------|-------------------------------|
90040 |ETUEFFONT BAS|90041 |ETUEFFONT|1973-07-01|1974-02-20       |ETUEFFONT                    |90041                          |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---090
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- En attente d'un avis de la DDT
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN

departement := '090';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019 as
	SELECT 
	''' || departement || '''::varchar(3) AS code_dep,
	t1.insee_cog2019::varchar(5) AS insee_2019,
	t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
	t2.nom_com::varchar AS nom_min_2019,
	t1.nb_evenements_cog::integer,
	t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
	''En expertise''::varchar AS agreg_nom_classement , 
	''En expertise''::varchar AS agreg_arretes,
	''En expertise''::varchar AS agreg_classement_85,
	''Expertis''::varchar(8) AS classement_2019,
	commentaires::TEXT AS agreg_commentaires,
	''n_adm_exp_cog_commune_000_2019''::varchar(254) AS source_geom,
	t2.geom::geometry(''MULTIPOLYGON'',2154)
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_' || departement || '_final AS t1
	JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
	ON t1.insee_cog2019 = t2.insee_com
	ORDER BY insee_2019;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- B] Cr�ation de la table des communes de 1985
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp AS
	SELECT
	''' || departement || '''::varchar(3) AS code_dep,
	insee_comm::varchar(5) AS insee_classement,
	nom_commun::varchar(100) AS nom_commune,
	date_decis::varchar AS arrete_classement,
	''Expertis''::varchar(8) AS classement_1985,
	commentair::TEXT AS commentaires
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '
	ORDER BY insee_classement;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- Une fois avis DDT obtenu : 
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN
departement := '090';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 03/03/2021
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85|count|
-------------|-----|
totalit�     |   12|
*/

---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_090_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_090_2019 as
SELECT 
'090'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'                   
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'              
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	
	WHEN t1.classement_85 = 'P, non class�e' THEN 'partie'
	WHEN t1.classement_85 = 'P' THEN 'partie'                  
    
	/*
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   */
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_090_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 12

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_090_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   12|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_090_2019 FOR VALUES IN ('090');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 RENAME TO l_liste_commune_montagne_1985_090_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|   12|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 AS
SELECT
'090'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'
	WHEN partie = 'p' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090_old
ORDER BY insee_classement;
--> 12

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   12|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_090 FOR VALUES IN ('090');
--> Updated Rows	0





