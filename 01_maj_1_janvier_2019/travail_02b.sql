---- 28/07
----- 2b ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup."l_liste_commune_montagne_1985_02B" RENAME TO l_liste_commune_montagne_1985_02b;

ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	207

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b;
--> 208

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019     |insee_commune_1985       |nom_commune_1985                                              |arretes                                        |classement_85                        |nb_commune_1985|commentaires                                                                                                                                                                                                 |code_evenement_cog|type_commune_avant|type_commune_apr�s|
-------------|------------------------|-------------------------|--------------------------------------------------------------|-----------------------------------------------|-------------------------------------|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|------------------|------------------|

*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 207 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	208

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '2B' ---02b
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
2B002        |2B002         |2B002                          |{2B002}           |                0|                    1|
2B003        |2B003         |2B003                          |{2B003}           |                0|                    1|
2B005        |2B005         |2B005                          |{2B005}           |                0|                    1|
2B007        |2B007         |2B007                          |{2B007}           |                0|                    1|
2B010        |2B010         |2B010                          |{2B010}           |                0|                    1|
2B012        |2B012         |2B012                          |{2B012}           |                0|                    1|
2B013        |2B013         |2B013                          |{2B013}           |                0|                    1|
2B015        |2B015         |2B015                          |{2B015}           |                0|                    1|
2B016        |2B016         |2B016                          |{2B016}           |                0|                    1|
2B020        |2B020         |2B020                          |{2B020}           |                0|                    1|
2B023        |2B023         |2B023                          |{2B023}           |                0|                    1|
2B025        |2B025         |2B025                          |{2B025}           |                0|                    1|
2B029        |2B029         |2B029                          |{2B029}           |                0|                    1|
2B030        |2B030         |2B030                          |{2B030}           |                0|                    1|
2B034        |2B034         |2B034                          |{2B034}           |                0|                    1|
2B036        |2B036         |2B036                          |{2B036}           |                0|                    1|
2B039        |2B039         |2B039                          |{2B039}           |                0|                    1|
2B043        |2B043         |2B043                          |{2B043}           |                0|                    1|
2B045        |2B045         |2B045                          |{2B045}           |                0|                    1|
2B046        |2B046         |2B046                          |{2B046}           |                0|                    1|
2B047        |2B047         |2B047                          |{2B047}           |                0|                    1|
2B049        |2B049         |2B049                          |{2B049}           |                0|                    1|
2B051        |2B051         |2B051                          |{2B051}           |                0|                    1|
2B052        |2B052         |2B052                          |{2B052}           |                0|                    1|
2B053        |2B053         |2B053                          |{2B053}           |                0|                    1|
2B054        |2B054         |2B054                          |{2B054}           |                0|                    1|
2B055        |2B055         |2B055                          |{2B055}           |                0|                    1|
2B058        |2B058         |2B058                          |{2B058}           |                0|                    1|
2B059        |2B059         |2B059                          |{2B059}           |                0|                    1|
2B063        |2B063         |2B063                          |{2B063}           |                0|                    1|
2B067        |2B067         |2B067                          |{2B067}           |                0|                    1|
2B068        |2B068         |2B068                          |{2B068}           |                0|                    1|
2B069        |2B069         |2B069                          |{2B069}           |                0|                    1|
2B072        |2B072         |2B072                          |{2B072}           |                0|                    1|
2B073        |2B073         |2B073                          |{2B073}           |                0|                    1|
2B074        |2B074         |2B074                          |{2B074}           |                0|                    1|
2B075        |2B075         |2B075                          |{2B075}           |                0|                    1|
2B078        |2B078         |2B078                          |{2B078}           |                0|                    1|
2B079        |2B079         |2B079                          |{2B079}           |                0|                    1|
2B080        |2B080         |2B080                          |{2B080}           |                0|                    1|
2B081        |2B081         |2B081                          |{2B081}           |                0|                    1|
2B082        |2B082         |2B082                          |{2B082}           |                0|                    1|
2B083        |2B083         |2B083                          |{2B083}           |                0|                    1|
2B084        |2B084         |2B084                          |{2B084}           |                0|                    1|
2B086        |2B086         |2B086                          |{2B086}           |                0|                    1|
2B088        |2B088         |2B088                          |{2B088}           |                0|                    1|
2B093        |2B093         |2B093                          |{2B093}           |                0|                    1|
2B095        |2B095         |2B095                          |{2B095}           |                0|                    1|
2B096        |2B096         |2B096                          |{2B096}           |                0|                    1|
2B097        |2B097         |2B097                          |{2B097}           |                0|                    1|
2B101        |2B101         |2B101                          |{2B101}           |                0|                    1|
2B102        |2B102         |2B102                          |{2B102}           |                0|                    1|
2B105        |2B105         |2B105                          |{2B105}           |                0|                    1|
2B106        |2B106         |2B106                          |{2B106}           |                0|                    1|
2B107        |2B107         |2B107                          |{2B107}           |                0|                    1|
2B110        |2B110         |2B110                          |{2B110}           |                0|                    1|
2B111        |2B111         |2B111                          |{2B111}           |                0|                    1|
2B112        |2B112         |2B112                          |{2B112}           |                0|                    1|
2B113        |2B113         |2B113                          |{2B113}           |                0|                    1|
2B116        |2B116         |2B116                          |{2B116}           |                0|                    1|
2B121        |2B121         |2B121                          |{2B121}           |                0|                    1|
2B122        |2B122         |2B122                          |{2B122}           |                0|                    1|
2B124        |2B124         |2B124                          |{2B124}           |                0|                    1|
2B125        |2B125         |2B125                          |{2B125}           |                0|                    1|
2B126        |2B126         |2B126                          |{2B126}           |                0|                    1|
2B135        |2B135         |2B135                          |{2B135}           |                0|                    1|
2B136        |2B136         |2B136                          |{2B136}           |                0|                    1|
2B137        |2B137         |2B137                          |{2B137}           |                0|                    1|
2B138        |2B138         |2B138                          |{2B138}           |                0|                    1|
2B140        |2B140         |2B140                          |{2B140}           |                0|                    1|
2B147        |2B147         |2B147                          |{2B147}           |                0|                    1|
2B149        |2B149         |2B149                          |{2B149}           |                0|                    1|
2B150        |2B150         |2B150                          |{2B150}           |                0|                    1|
2B152        |2B152         |2B152                          |{2B152}           |                0|                    1|
2B153        |2B153         |2B153                          |{2B153}           |                0|                    1|
2B155        |2B155         |2B155                          |{2B155}           |                0|                    1|
2B156        |2B156         |2B156                          |{2B156}           |                0|                    1|
2B157        |2B157         |2B157                          |{2B157}           |                0|                    1|
2B159        |2B159         |2B159                          |{2B159}           |                0|                    1|
2B161        |2B161         |2B161                          |{2B161}           |                0|                    1|
2B162        |2B162         |2B162                          |{2B162}           |                0|                    1|
2B164        |2B164         |2B164                          |{2B164}           |                0|                    1|
2B165        |2B165         |2B165                          |{2B165}           |                0|                    1|
2B166        |2B166         |2B166                          |{2B166}           |                0|                    1|
2B167        |2B167         |2B167                          |{2B167}           |                0|                    1|
2B168        |2B168         |2B168                          |{2B168}           |                0|                    1|
2B169        |2B169         |2B169                          |{2B169}           |                0|                    1|
2B170        |2B170         |2B170                          |{2B170}           |                0|                    1|
2B171        |2B171         |2B171                          |{2B171}           |                0|                    1|
2B172        |2B172         |2B172                          |{2B172}           |                0|                    1|
2B173        |2B173         |2B173                          |{2B173}           |                0|                    1|
2B175        |2B175         |2B175                          |{2B175}           |                0|                    1|
2B176        |2B176         |2B176                          |{2B176}           |                0|                    1|
2B177        |2B177         |2B177                          |{2B177}           |                0|                    1|
2B178        |2B178         |2B178                          |{2B178}           |                0|                    1|
2B179        |2B179         |2B179                          |{2B179}           |                0|                    1|
2B180        |2B180         |2B180                          |{2B180}           |                0|                    1|
2B182        |2B182         |2B182                          |{2B182}           |                0|                    1|
2B183        |2B183         |2B183                          |{2B183}           |                0|                    1|
2B184        |2B184         |2B184                          |{2B184}           |                0|                    1|
2B185        |2B185         |2B185                          |{2B185}           |                0|                    1|
2B187        |2B187         |2B187                          |{2B187}           |                0|                    1|
2B188        |2B188         |2B188                          |{2B188}           |                0|                    1|
2B190        |2B190         |2B190                          |{2B190}           |                0|                    1|
2B192        |2B192         |2B192                          |{2B192}           |                0|                    1|
2B193        |2B193         |2B193                          |{2B193}           |                0|                    1|
2B194        |2B194         |2B194                          |{2B194}           |                0|                    1|
2B195        |2B195         |2B195                          |{2B195}           |                0|                    1|
2B199        |2B199         |2B199                          |{2B199}           |                0|                    1|
2B201        |2B201         |2B201                          |{2B201}           |                0|                    1|
2B202        |2B202         |2B202                          |{2B202}           |                0|                    1|
2B206        |2B206         |2B206                          |{2B206}           |                0|                    1|
2B208        |2B208         |2B208                          |{2B208}           |                0|                    1|
2B210        |2B210         |2B210                          |{2B210}           |                0|                    1|
2B213        |2B213         |2B213                          |{2B213}           |                0|                    1|
2B214        |2B214         |2B214                          |{2B214}           |                0|                    1|
2B216        |2B216         |2B216                          |{2B216}           |                0|                    1|
2B217        |2B217         |2B217                          |{2B217}           |                0|                    1|
2B218        |2B218         |2B218                          |{2B218}           |                0|                    1|
2B219        |2B219         |2B219                          |{2B219}           |                0|                    1|
2B220        |2B220         |2B220                          |{2B220}           |                0|                    1|
2B221        |2B221         |2B221                          |{2B221}           |                0|                    1|
2B222        |2B222         |2B222                          |{2B222}           |                0|                    1|
2B223        |2B223         |2B223                          |{2B223}           |                0|                    1|
2B224        |2B224         |2B224                          |{2B224}           |                0|                    1|
2B225        |2B225         |2B225                          |{2B225}           |                0|                    1|
2B226        |2B226         |2B226                          |{2B226}           |                0|                    1|
2B227        |2B227         |2B227                          |{2B227}           |                0|                    1|
2B229        |2B229         |2B229                          |{2B229}           |                0|                    1|
2B230        |2B230         |2B230                          |{2B230}           |                0|                    1|
2B231        |2B231         |2B231                          |{2B231}           |                0|                    1|
2B233        |2B233         |2B233                          |{2B233}           |                0|                    1|
2B234        |2B234         |2B234                          |{2B234}           |                0|                    1|
2B235        |2B235         |2B235                          |{2B235}           |                0|                    1|
2B236        |2B236         |2B236                          |{2B236}           |                0|                    1|
2B238        |2B238         |2B238                          |{2B238}           |                0|                    1|
2B239        |2B239         |2B239                          |{2B239}           |                0|                    1|
2B241        |2B241         |2B241                          |{2B241}           |                0|                    1|
2B243        |2B243         |2B243                          |{2B243}           |                0|                    1|
2B244        |2B244         |2B244                          |{2B244}           |                0|                    1|
2B245        |2B245         |2B245                          |{2B245}           |                0|                    1|
2B246        |2B246         |2B246                          |{2B246}           |                0|                    1|
2B248        |2B248         |2B248                          |{2B248}           |                0|                    1|
2B250        |2B250         |2B250                          |{2B250}           |                0|                    1|
2B251        |2B251         |2B251                          |{2B251}           |                0|                    1|
2B252        |2B252         |2B252                          |{2B252}           |                0|                    1|
2B255        |2B255         |2B255                          |{2B255}           |                0|                    1|
2B256        |2B256         |2B256                          |{2B256}           |                0|                    1|
2B257        |2B257         |2B257                          |{2B257}           |                0|                    1|
2B260        |2B260         |2B260                          |{2B260}           |                0|                    1|
2B261        |2B261         |2B261                          |{2B261}           |                0|                    1|
2B263        |2B263         |2B263                          |{2B263}           |                0|                    1|
2B264        |2B264         |2B264                          |{2B264}           |                0|                    1|
2B265        |2B265         |2B265                          |{2B265}           |                0|                    1|
2B267        |2B267         |2B267                          |{2B267}           |                0|                    1|
2B273        |2B273         |2B273                          |{2B273}           |                0|                    1|
2B274        |2B274         |2B274                          |{2B274}           |                0|                    1|
2B275        |2B275         |2B275                          |{2B275}           |                0|                    1|
2B277        |2B277         |2B277                          |{2B277}           |                0|                    1|
2B280        |2B280         |2B280                          |{2B280}           |                0|                    1|
2B281        |2B281         |2B281                          |{2B281}           |                0|                    1|
2B283        |2B283         |2B283                          |{2B283}           |                0|                    1|
2B287        |2B287         |2B287                          |{2B287}           |                0|                    1|
2B289        |2B289         |2B289                          |{2B289}           |                0|                    1|
2B290        |2B290         |2B290                          |{2B290}           |                0|                    1|
2B291        |2B291         |2B291                          |{2B291}           |                0|                    1|
2B292        |2B292         |2B292                          |{2B292}           |                0|                    1|
2B293        |2B293         |2B293                          |{2B293}           |                0|                    1|
2B296        |2B296         |2B296                          |{2B296}           |                0|                    1|
2B297        |2B297         |2B297                          |{2B297}           |                0|                    1|
2B299        |2B299         |2B299                          |{2B299}           |                0|                    1|
2B301        |2B301         |2B301                          |{2B301}           |                0|                    1|
2B302        |2B302         |2B302                          |{2B302}           |                0|                    1|
2B304        |2B304         |2B304                          |{2B304}           |                0|                    1|
2B305        |2B305         |2B305                          |{2B305}           |                0|                    1|
2B306        |2B306         |2B306                          |{2B306}           |                0|                    1|
2B309        |2B309         |2B309                          |{2B309}           |                0|                    1|
2B314        |2B314         |2B314                          |{2B314}           |                0|                    1|
2B315        |2B315         |2B315                          |{2B315}           |                0|                    1|
2B316        |2B316         |2B316                          |{2B316}           |                0|                    1|
2B317        |2B317         |2B317                          |{2B317}           |                0|                    1|
2B320        |2B320         |2B320                          |{2B320}           |                0|                    1|
2B321        |2B321         |2B321                          |{2B321}           |                0|                    1|
2B327        |2B327         |2B327                          |{2B327}           |                0|                    1|
2B328        |2B328         |2B328                          |{2B328}           |                0|                    1|
2B329        |2B329         |2B329                          |{2B329}           |                0|                    1|
2B332        |2B332         |2B332                          |{2B332}           |                0|                    1|
2B333        |2B333         |2B333                          |{2B333}           |                0|                    1|
2B334        |2B334         |2B334                          |{2B334}           |                0|                    1|
2B337        |2B337         |2B337                          |{2B337}           |                0|                    1|
2B338        |2B338         |2B338                          |{2B338}           |                0|                    1|
2B339        |2B339         |2B339                          |{2B339}           |                0|                    1|
2B340        |2B340         |2B340                          |{2B340}           |                0|                    1|
2B341        |2B341         |2B341                          |{2B341}           |                0|                    1|
2B342        |2B342         |2B342                          |{2B342}           |                0|                    1|
2B344        |2B344         |2B344                          |{2B344}           |                0|                    1|
2B347        |2B347         |2B347                          |{2B347}           |                0|                    1|
2B350        |2B350         |2B350                          |{2B350}           |                0|                    1|
2B352        |2B352         |2B352                          |{2B352}           |                0|                    1|
2B353        |2B353         |2B353                          |{2B353}           |                0|                    1|
2B354        |2B354         |2B354                          |{2B354}           |                0|                    1|
2B355        |2B355         |2B355                          |{2B355}           |                0|                    1|
2B356        |2B356         |2B356                          |{2B356}           |                0|                    1|
2B361        |2B361         |2B361                          |{2B361}           |                0|                    1|
2B364        |2B364         |2B364                          |{2B364}           |                0|                    1|
2B365        |2B365         |2B365                          |{2B365}           |                0|                    1|
2B366        |2B366         |2B366                          |{2B366}           |                0|                    1|
             |2B145         |                               |{2B145}           |                 |                    1|
 */
---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b;
--> Updated Rows	208

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02b AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02b)
order by date_eff, com_ap
/*
com_av|ncc_av                   |com_ap|ncc_ap                   |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|-------------------------|------|-------------------------|----------|-----------------|-----------------------------|-------------------------------|
20002 |AGHIONE                  |2B002 |AGHIONE                  |1976-01-01|1974-02-20       |AGHIONE                      |2B002                          |
20003 |AITI                     |2B003 |AITI                     |1976-01-01|1974-02-20       |AITI                         |2B003                          |
20005 |ALANDO                   |2B005 |ALANDO                   |1976-01-01|1974-02-20       |ALANDO                       |2B005                          |
20007 |ALBERTACCE               |2B007 |ALBERTACCE               |1976-01-01|1974-02-20       |ALBERTACCE                   |2B007                          |
20010 |ALGAJOLA                 |2B010 |ALGAJOLA                 |1976-01-01|1976-04-28       |ALGAJOLA                     |2B010                          |
20012 |ALTIANI                  |2B012 |ALTIANI                  |1976-01-01|1974-02-20       |ALTIANI                      |2B012                          |
20013 |ALZI                     |2B013 |ALZI                     |1976-01-01|1974-02-20       |ALZI                         |2B013                          |
20015 |AMPRIANI                 |2B015 |AMPRIANI                 |1976-01-01|1974-02-20       |AMPRIANI                     |2B015                          |
20016 |ANTISANTI                |2B016 |ANTISANTI                |1976-01-01|1974-02-20       |ANTISANTI                    |2B016                          |
20020 |AREGNO                   |2B020 |AREGNO                   |1976-01-01|1976-04-28       |AREGNO                       |2B020                          |
20023 |ASCO                     |2B023 |ASCO                     |1976-01-01|1974-02-20       |ASCO                         |2B023                          |
20025 |AVAPESSA                 |2B025 |AVAPESSA                 |1976-01-01|1976-04-28       |AVAPESSA                     |2B025                          |
20029 |BARBAGGIO                |2B029 |BARBAGGIO                |1976-01-01|1976-04-28       |BARBAGGIO                    |2B029                          |
20030 |BARRETTALI               |2B030 |BARRETTALI               |1976-01-01|1976-04-28       |BARRETTALI                   |2B030                          |
20034 |BELGODERE                |2B034 |BELGODERE                |1976-01-01|1976-04-28       |BELGODERE                    |2B034                          |
20036 |BIGORNO                  |2B036 |BIGORNO                  |1976-01-01|1974-02-20       |BIGORNO                      |2B036                          |
20039 |BISINCHI                 |2B039 |BISINCHI                 |1976-01-01|1974-02-20       |BISINCHI                     |2B039                          |
20043 |BRANDO                   |2B043 |BRANDO                   |1976-01-01|1976-04-28       |BRANDO                       |2B043                          |
20045 |BUSTANICO                |2B045 |BUSTANICO                |1976-01-01|1974-02-20       |BUSTANICO                    |2B045                          |
20046 |CAGNANO                  |2B046 |CAGNANO                  |1976-01-01|1976-04-28       |CAGNANO                      |2B046                          |
20047 |CALACUCCIA               |2B047 |CALACUCCIA               |1976-01-01|1974-02-20       |CALACUCCIA                   |2B047                          |
20049 |CALENZANA                |2B049 |CALENZANA                |1976-01-01|1976-04-28       |CALENZANA                    |2B049                          |
20051 |CAMBIA                   |2B051 |CAMBIA                   |1976-01-01|1974-02-20       |CAMBIA                       |2B051                          |
20052 |CAMPANA                  |2B052 |CAMPANA                  |1976-01-01|1974-02-20       |CAMPANA                      |2B052                          |
20053 |CAMPI                    |2B053 |CAMPI                    |1976-01-01|1974-02-20       |CAMPI                        |2B053                          |
20054 |CAMPILE                  |2B054 |CAMPILE                  |1976-01-01|1974-02-20       |CAMPILE                      |2B054                          |
20055 |CAMPITELLO               |2B055 |CAMPITELLO               |1976-01-01|1974-02-20       |CAMPITELLO                   |2B055                          |
20058 |CANARI                   |2B058 |CANARI                   |1976-01-01|1976-04-28       |CANARI                       |2B058                          |
20059 |CANAVAGGIA               |2B059 |CANAVAGGIA               |1976-01-01|1974-02-20       |CANAVAGGIA                   |2B059                          |
20063 |CARCHETO BRUSTICO        |2B063 |CARCHETO BRUSTICO        |1976-01-01|1974-02-20       |CARCHETO-BRUSTICO            |2B063                          |
20067 |CARPINETO                |2B067 |CARPINETO                |1976-01-01|1974-02-20       |CARPINETO                    |2B067                          |
20068 |CARTICASI                |2B068 |CARTICASI                |1976-01-01|1974-02-20       |CARTICASI                    |2B068                          |
20069 |CASABIANCA               |2B069 |CASABIANCA               |1976-01-01|1974-02-20       |CASABIANCA                   |2B069                          |
20072 |CASALTA                  |2B072 |CASALTA                  |1976-01-01|1976-04-28       |CASALTA                      |2B072                          |
20073 |CASAMACCIOLI             |2B073 |CASAMACCIOLI             |1976-01-01|1974-02-20       |CASAMACCIOLI                 |2B073                          |
20074 |CASANOVA                 |2B074 |CASANOVA                 |1976-01-01|1974-02-20       |CASANOVA                     |2B074                          |
20075 |CASEVECCHIE              |2B075 |CASEVECCHIE              |1976-01-01|1974-02-20       |CASEVECCHIE                  |2B075                          |
20078 |CASTELLARE DI MERCURIO   |2B078 |CASTELLARE DI MERCURIO   |1976-01-01|1974-02-20       |CASTELLARE-DI-MERCURIO       |2B078                          |
20079 |CASTELLO DI ROSTINO      |2B079 |CASTELLO DI ROSTINO      |1976-01-01|1974-02-20       |CASTELLO-DI-ROSTINO          |2B079                          |
20080 |CASTIFAO                 |2B080 |CASTIFAO                 |1976-01-01|1974-02-20       |CASTIFAO                     |2B080                          |
20081 |CASTIGLIONE              |2B081 |CASTIGLIONE              |1976-01-01|1974-02-20       |CASTIGLIONE                  |2B081                          |
20082 |CASTINETA                |2B082 |CASTINETA                |1976-01-01|1974-02-20       |CASTINETA                    |2B082                          |
20083 |CASTIRLA                 |2B083 |CASTIRLA                 |1976-01-01|1974-02-20       |CASTIRLA                     |2B083                          |
20084 |CATERI                   |2B084 |CATERI                   |1976-01-01|1976-04-28       |CATERI                       |2B084                          |
20086 |CENTURI                  |2B086 |CENTURI                  |1976-01-01|1976-04-28       |CENTURI                      |2B086                          |
20088 |CHIATRA                  |2B088 |CHIATRA                  |1976-01-01|1974-02-20       |CHIATRA                      |2B088                          |
20093 |CORBARA                  |2B093 |CORBARA                  |1976-01-01|1976-04-28       |CORBARA                      |2B093                          |
20095 |CORSCIA                  |2B095 |CORSCIA                  |1976-01-01|1974-02-20       |CORSCIA                      |2B095                          |
20096 |CORTE                    |2B096 |CORTE                    |1976-01-01|1974-02-20       |CORTE                        |2B096                          |
20097 |COSTA                    |2B097 |COSTA                    |1976-01-01|1976-04-28       |COSTA                        |2B097                          |
20101 |CROCE                    |2B101 |CROCE                    |1976-01-01|1974-02-20       |CROCE                        |2B101                          |
20102 |CROCICCHIA               |2B102 |CROCICCHIA               |1976-01-01|1974-02-20       |CROCICCHIA                   |2B102                          |
20105 |ERBAJOLO                 |2B105 |ERBAJOLO                 |1976-01-01|1974-02-20       |ERBAJOLO                     |2B105                          |
20106 |ERONE                    |2B106 |ERONE                    |1976-01-01|1974-02-20       |ERONE                        |2B106                          |
20107 |ERSA                     |2B107 |ERSA                     |1976-01-01|1976-04-28       |ERSA                         |2B107                          |
20110 |FAVALELLO                |2B110 |FAVALELLO                |1976-01-01|1974-02-20       |FAVALELLO                    |2B110                          |
20111 |FELCE                    |2B111 |FELCE                    |1976-01-01|1974-02-20       |FELCE                        |2B111                          |
20112 |FELICETO                 |2B112 |FELICETO                 |1976-01-01|1976-04-28       |FELICETO                     |2B112                          |
20113 |FICAJA                   |2B113 |FICAJA                   |1976-01-01|1974-02-20       |FICAJA                       |2B113                          |
20116 |FOCICCHIA                |2B116 |FOCICCHIA                |1976-01-01|1974-02-20       |FOCICCHIA                    |2B116                          |
20121 |GALERIA                  |2B121 |GALERIA                  |1976-01-01|1976-04-28       |GALERIA                      |2B121                          |
20122 |GAVIGNANO                |2B122 |GAVIGNANO                |1976-01-01|1974-02-20       |GAVIGNANO                    |2B122                          |
20124 |GHISONI                  |2B124 |GHISONI                  |1976-01-01|1974-02-20       |GHISONI                      |2B124                          |
20125 |GIOCATOJO                |2B125 |GIOCATOJO                |1976-01-01|1974-02-20       |GIOCATOJO                    |2B125                          |
20126 |GIUNCAGGIO               |2B126 |GIUNCAGGIO               |1976-01-01|1974-02-20       |GIUNCAGGIO                   |2B126                          |
20135 |ISOLACCIO DI FIUMORBO    |2B135 |ISOLACCIO DI FIUMORBO    |1976-01-01|1974-02-20       |ISOLACCIO-DI-FIUMORBO        |2B135                          |
20136 |LAMA                     |2B136 |LAMA                     |1976-01-01|1976-04-28       |LAMA                         |2B136                          |
20137 |LANO                     |2B137 |LANO                     |1976-01-01|1974-02-20       |LANO                         |2B137                          |
20138 |LAVATOGGIO               |2B138 |LAVATOGGIO               |1976-01-01|1976-04-28       |LAVATOGGIO                   |2B138                          |
20140 |LENTO                    |2B140 |LENTO                    |1976-01-01|1974-02-20       |LENTO                        |2B140                          |
20147 |LOZZI                    |2B147 |LOZZI                    |1976-01-01|1974-02-20       |LOZZI                        |2B147                          |
20149 |LUGO DI NAZZA            |2B149 |LUGO DI NAZZA            |1976-01-01|1974-02-20       |LUGO-DI-NAZZA                |2B149                          |
20150 |LUMIO                    |2B150 |LUMIO                    |1976-01-01|1976-04-28       |LUMIO                        |2B150                          |
20152 |LURI                     |2B152 |LURI                     |1976-01-01|1976-04-28       |LURI                         |2B152                          |
20153 |MANSO                    |2B153 |MANSO                    |1976-01-01|1974-02-20       |MANSO                        |2B153                          |
20155 |MATRA                    |2B155 |MATRA                    |1976-01-01|1974-02-20       |MATRA                        |2B155                          |
20156 |MAUSOLEO                 |2B156 |MAUSOLEO                 |1976-01-01|1974-02-20       |MAUSOLEO                     |2B156                          |
20157 |MAZZOLA                  |2B157 |MAZZOLA                  |1976-01-01|1974-02-20       |MAZZOLA                      |2B157                          |
20159 |MERIA                    |2B159 |MERIA                    |1976-01-01|1976-04-28       |MERIA                        |2B159                          |
20161 |MOITA                    |2B161 |MOITA                    |1976-01-01|1974-02-20       |MOITA                        |2B161                          |
20162 |MOLTIFAO                 |2B162 |MOLTIFAO                 |1976-01-01|1974-02-20       |MOLTIFAO                     |2B162                          |
20164 |MONACIA D OREZZA         |2B164 |MONACIA D OREZZA         |1976-01-01|1974-02-20       |MONACIA-D'OREZZA             |2B164                          |
20165 |MONCALE                  |2B165 |MONCALE                  |1976-01-01|1976-04-28       |MONCALE                      |2B165                          |
20166 |MONTE                    |2B166 |MONTE                    |1976-01-01|1974-02-20       |MONTE                        |2B166                          |
20167 |MONTEGROSSO              |2B167 |MONTEGROSSO              |1976-01-01|1976-04-28       |MONTEGROSSO                  |2B167                          |
20168 |MONTICELLO               |2B168 |MONTICELLO               |1976-01-01|1976-04-28       |MONTICELLO                   |2B168                          |
20169 |MOROSAGLIA               |2B169 |MOROSAGLIA               |1976-01-01|1974-02-20       |MOROSAGLIA                   |2B169                          |
20170 |MORSIGLIA                |2B170 |MORSIGLIA                |1976-01-01|1976-04-28       |MORSIGLIA                    |2B170                          |
20171 |MURACCIOLE               |2B171 |MURACCIOLE               |1976-01-01|1974-02-20       |MURACCIOLE                   |2B171                          |
20172 |MURATO                   |2B172 |MURATO                   |1976-01-01|1974-02-20       |MURATO                       |2B172                          |
20173 |MURO                     |2B173 |MURO                     |1976-01-01|1976-04-28       |MURO                         |2B173                          |
20175 |NESSA                    |2B175 |NESSA                    |1976-01-01|1976-04-28       |NESSA                        |2B175                          |
20176 |NOCARIO                  |2B176 |NOCARIO                  |1976-01-01|1974-02-20       |NOCARIO                      |2B176                          |
20177 |NOCETA                   |2B177 |NOCETA                   |1976-01-01|1974-02-20       |NOCETA                       |2B177                          |
20178 |NONZA                    |2B178 |NONZA                    |1976-01-01|1976-04-28       |NONZA                        |2B178                          |
20179 |NOVALE                   |2B179 |NOVALE                   |1976-01-01|1974-02-20       |NOVALE                       |2B179                          |
20180 |NOVELLA                  |2B180 |NOVELLA                  |1976-01-01|1976-04-28       |NOVELLA                      |2B180                          |
20182 |OCCHIATANA               |2B182 |OCCHIATANA               |1976-01-01|1976-04-28       |OCCHIATANA                   |2B182                          |
20183 |OGLIASTRO                |2B183 |OGLIASTRO                |1976-01-01|1976-04-28       |OGLIASTRO                    |2B183                          |
20184 |OLCANI                   |2B184 |OLCANI                   |1976-01-01|1976-04-28       |OLCANI                       |2B184                          |
20185 |OLETTA                   |2B185 |OLETTA                   |1976-01-01|1976-04-28       |OLETTA                       |2B185                          |
20187 |OLMETA DI CAPOCORSO      |2B187 |OLMETA DI CAPOCORSO      |1976-01-01|1976-04-28       |OLMETA-DI-CAPOCORSO          |2B187                          |
20188 |OLMETA DI TUDA           |2B188 |OLMETA DI TUDA           |1976-01-01|1976-04-28       |OLMETA-DI-TUDA               |2B188                          |
20190 |OLMI CAPPELLA            |2B190 |OLMI CAPPELLA            |1976-01-01|1974-02-20       |OLMI-CAPPELLA                |2B190                          |
20192 |OLMO                     |2B192 |OLMO                     |1976-01-01|1974-02-20       |OLMO                         |2B192                          |
20193 |OMESSA                   |2B193 |OMESSA                   |1976-01-01|1974-02-20       |OMESSA                       |2B193                          |
20194 |ORTALE                   |2B194 |ORTALE                   |1976-01-01|1974-02-20       |ORTALE                       |2B194                          |
20195 |ORTIPORIO                |2B195 |ORTIPORIO                |1976-01-01|1974-02-20       |ORTIPORIO                    |2B195                          |
20199 |PALASCA                  |2B199 |PALASCA                  |1976-01-01|1976-04-28       |PALASCA                      |2B199                          |
20201 |PANCHERACCIA             |2B201 |PANCHERACCIA             |1976-01-01|1974-02-20       |PANCHERACCIA                 |2B201                          |
20202 |PARATA                   |2B202 |PARATA                   |1976-01-01|1974-02-20       |PARATA                       |2B202                          |
20206 |PENTA ACQUATELLA         |2B206 |PENTA ACQUATELLA         |1976-01-01|1974-02-20       |PENTA-ACQUATELLA             |2B206                          |
20208 |PERELLI                  |2B208 |PERELLI                  |1976-01-01|1974-02-20       |PERELLI                      |2B208                          |
20210 |PERO CASEVECCHIE         |2B210 |PERO CASEVECCHIE         |1976-01-01|1976-04-28       |PERO-CASEVECCHIE             |2B210                          |
20213 |PIANELLO                 |2B213 |PIANELLO                 |1976-01-01|1974-02-20       |PIANELLO                     |2B213                          |
20214 |PIANO                    |2B214 |PIANO                    |1976-01-01|1974-02-20       |PIANO                        |2B214                          |
20216 |PIAZZALI                 |2B216 |PIAZZALI                 |1976-01-01|1974-02-20       |PIAZZALI                     |2B216                          |
20217 |PIAZZOLE                 |2B217 |PIAZZOLE                 |1976-01-01|1974-02-20       |PIAZZOLE                     |2B217                          |
20218 |PIEDICORTE DI GAGGIO     |2B218 |PIEDICORTE DI GAGGIO     |1976-01-01|1974-02-20       |PIEDICORTE-DI-GAGGIO         |2B218                          |
20219 |PIEDICROCE               |2B219 |PIEDICROCE               |1976-01-01|1974-02-20       |PIEDICROCE                   |2B219                          |
20220 |PIEDIGRIGGIO             |2B220 |PIEDIGRIGGIO             |1976-01-01|1974-02-20       |PIEDIGRIGGIO                 |2B220                          |
20221 |PIEDIPARTINO             |2B221 |PIEDIPARTINO             |1976-01-01|1974-02-20       |PIEDIPARTINO                 |2B221                          |
20222 |PIE D OREZZA             |2B222 |PIE D OREZZA             |1976-01-01|1974-02-20       |PIE-D'OREZZA                 |2B222                          |
20223 |PIETRALBA                |2B223 |PIETRALBA                |1976-01-01|1974-02-20       |PIETRALBA                    |2B223                          |
20224 |PIETRACORBARA            |2B224 |PIETRACORBARA            |1976-01-01|1976-04-28       |PIETRACORBARA                |2B224                          |
20225 |PIETRA DI VERDE          |2B225 |PIETRA DI VERDE          |1976-01-01|1974-02-20       |PIETRA-DI-VERDE              |2B225                          |
20226 |PIETRASERENA             |2B226 |PIETRASERENA             |1976-01-01|1974-02-20       |PIETRASERENA                 |2B226                          |
20227 |PIETRICAGGIO             |2B227 |PIETRICAGGIO             |1976-01-01|1974-02-20       |PIETRICAGGIO                 |2B227                          |
20229 |PIETROSO                 |2B229 |PIETROSO                 |1976-01-01|1974-02-20       |PIETROSO                     |2B229                          |
20230 |PIEVE                    |2B230 |PIEVE                    |1976-01-01|1974-02-20       |PIEVE                        |2B230                          |
20231 |PIGNA                    |2B231 |PIGNA                    |1976-01-01|1976-04-28       |PIGNA                        |2B231                          |
20233 |PINO                     |2B233 |PINO                     |1976-01-01|1976-04-28       |PINO                         |2B233                          |
20234 |PIOBETTA                 |2B234 |PIOBETTA                 |1976-01-01|1974-02-20       |PIOBETTA                     |2B234                          |
20235 |PIOGGIOLA                |2B235 |PIOGGIOLA                |1976-01-01|1974-02-20       |PIOGGIOLA                    |2B235                          |
20236 |POGGIO DI NAZZA          |2B236 |POGGIO DI NAZZA          |1976-01-01|1974-02-20       |POGGIO-DI-NAZZA              |2B236                          |
20238 |POGGIO DI VENACO         |2B238 |POGGIO DI VENACO         |1976-01-01|1974-02-20       |POGGIO-DI-VENACO             |2B238                          |
20239 |POGGIO D OLETTA          |2B239 |POGGIO D OLETTA          |1976-01-01|1976-04-28       |POGGIO-D'OLETTA              |2B239                          |
20241 |POGGIO MARINACCIO        |2B241 |POGGIO MARINACCIO        |1976-01-01|1974-02-20       |POGGIO-MARINACCIO            |2B241                          |
20243 |POLVEROSO                |2B243 |POLVEROSO                |1976-01-01|1974-02-20       |POLVEROSO                    |2B243                          |
20244 |POPOLASCA                |2B244 |POPOLASCA                |1976-01-01|1974-02-20       |POPOLASCA                    |2B244                          |
20245 |PORRI                    |2B245 |PORRI                    |1976-01-01|1976-04-28       |PORRI                        |2B245                          |
20246 |PORTA                    |2B246 |PORTA                    |1976-01-01|1974-02-20       |LA PORTA                     |2B246                          |
20248 |PRATO DI GIOVELLINA      |2B248 |PRATO DI GIOVELLINA      |1976-01-01|1974-02-20       |PRATO                        |2B248                          |
20250 |PRUNELLI DI CASACCONI    |2B250 |PRUNELLI DI CASACCONI    |1976-01-01|1976-04-28       |PRUNELLI-DI-CASACCONI        |2B250                          |
20251 |PRUNELLI DI FIUMORBO     |2B251 |PRUNELLI DI FIUMORBO     |1976-01-01|1976-04-28       |PRUNELLI-DI-FIUMORBO         |2B251                          |
20252 |PRUNO                    |2B252 |PRUNO                    |1976-01-01|1976-04-28       |PRUNO                        |2B252                          |
20255 |QUERCITELLO              |2B255 |QUERCITELLO              |1976-01-01|1974-02-20       |QUERCITELLO                  |2B255                          |
20256 |RAPAGGIO                 |2B256 |RAPAGGIO                 |1976-01-01|1974-02-20       |RAPAGGIO                     |2B256                          |
20257 |RAPALE                   |2B257 |RAPALE                   |1976-01-01|1974-02-20       |RAPALE                       |2B257                          |
20260 |RIVENTOSA                |2B260 |RIVENTOSA                |1976-01-01|1974-02-20       |RIVENTOSA                    |2B260                          |
20261 |ROGLIANO                 |2B261 |ROGLIANO                 |1976-01-01|1976-04-28       |ROGLIANO                     |2B261                          |
20263 |ROSPIGLIANI              |2B263 |ROSPIGLIANI              |1976-01-01|1974-02-20       |ROSPIGLIANI                  |2B263                          |
20264 |RUSIO                    |2B264 |RUSIO                    |1976-01-01|1974-02-20       |RUSIO                        |2B264                          |
20265 |RUTALI                   |2B265 |RUTALI                   |1976-01-01|1974-02-20       |RUTALI                       |2B265                          |
20267 |SALICETO                 |2B267 |SALICETO                 |1976-01-01|1974-02-20       |SALICETO                     |2B267                          |
20273 |SCATA                    |2B273 |SCATA                    |1976-01-01|1976-04-28       |SCATA                        |2B273                          |
20274 |SCOLCA                   |2B274 |SCOLCA                   |1976-01-01|1976-04-28       |SCOLCA                       |2B274                          |
20275 |SERMANO                  |2B275 |SERMANO                  |1976-01-01|1974-02-20       |SERMANO                      |2B275                          |
20277 |SERRA DI FIUMORBO        |2B277 |SERRA DI FIUMORBO        |1976-01-01|1974-02-20       |SERRA-DI-FIUMORBO            |2B277                          |
20280 |SILVARECCIO              |2B280 |SILVARECCIO              |1976-01-01|1974-02-20       |SILVARECCIO                  |2B280                          |
20281 |SISCO                    |2B281 |SISCO                    |1976-01-01|1976-04-28       |SISCO                        |2B281                          |
20283 |SOLARO                   |2B283 |SOLARO                   |1976-01-01|1976-04-28       |SOLARO                       |2B283                          |
20287 |SORIO                    |2B287 |SORIO                    |1976-01-01|1976-04-28       |SORIO                        |2B287                          |
20289 |SOVERIA                  |2B289 |SOVERIA                  |1976-01-01|1974-02-20       |SOVERIA                      |2B289                          |
20290 |SPELONCATO               |2B290 |SPELONCATO               |1976-01-01|1976-04-28       |SPELONCATO                   |2B290                          |
20291 |STAZZONA                 |2B291 |STAZZONA                 |1976-01-01|1974-02-20       |STAZZONA                     |2B291                          |
20292 |SANT ANDREA DI BOZIO     |2B292 |SANT ANDREA DI BOZIO     |1976-01-01|1974-02-20       |SANT'ANDREA-DI-BOZIO         |2B292                          |
20293 |SANT ANDREA DI COTONE    |2B293 |SANT ANDREA DI COTONE    |1976-01-01|1976-04-28       |SANT'ANDREA-DI-COTONE        |2B293                          |
20296 |SANT ANTONINO            |2B296 |SANT ANTONINO            |1976-01-01|1976-04-28       |SANT'ANTONINO                |2B296                          |
20297 |SAN DAMIANO              |2B297 |SAN DAMIANO              |1976-01-01|1974-02-20       |SAN-DAMIANO                  |2B297                          |
20299 |SAN GAVINO D AMPUGNANI   |2B299 |SAN GAVINO D AMPUGNANI   |1976-01-01|1976-04-28       |SAN-GAVINO-D'AMPUGNANI       |2B299                          |
20301 |SAN GAVINO DI TENDA      |2B301 |SAN GAVINO DI TENDA      |1976-01-01|1976-04-28       |SAN-GAVINO-DI-TENDA          |2B301                          |
20302 |SAN GIOVANNI DI MORIANI  |2B302 |SAN GIOVANNI DI MORIANI  |1976-01-01|1976-04-28       |SAN-GIOVANNI                 |2B302                          |
20304 |SAN LORENZO              |2B304 |SAN LORENZO              |1976-01-01|1974-02-20       |SAN-LORENZO                  |2B304                          |
20305 |SAN MARTINO DI LOTA      |2B305 |SAN MARTINO DI LOTA      |1976-01-01|1976-04-28       |SAN-MARTINO-DI-LOTA          |2B305                          |
20306 |SANTA LUCIA DI MERCURIO  |2B306 |SANTA LUCIA DI MERCURIO  |1976-01-01|1974-02-20       |SANTA-LUCIA-DI-MERCURIO      |2B306                          |
20309 |SANTA MARIA DI LOTA      |2B309 |SANTA MARIA DI LOTA      |1976-01-01|1976-04-28       |SANTA-MARIA-DI-LOTA          |2B309                          |
20314 |SANTO PIETRO DI TENDA    |2B314 |SANTO PIETRO DI TENDA    |1976-01-01|1976-04-28       |SANTO-PIETRO-DI-TENDA        |2B314                          |
20315 |SANTO PIETRO DI VENACO   |2B315 |SANTO PIETRO DI VENACO   |1976-01-01|1974-02-20       |SANTO-PIETRO-DI-VENACO       |2B315                          |
20316 |SANTA REPARATA DI BALAGNA|2B316 |SANTA REPARATA DI BALAGNA|1976-01-01|1976-04-28       |SANTA-REPARATA-DI-BALAGNA    |2B316                          |
20317 |SANTA REPARATA DI MORIANI|2B317 |SANTA REPARATA DI MORIANI|1976-01-01|1974-02-20       |SANTA-REPARATA-DI-MORIANI    |2B317                          |
20320 |TALLONE                  |2B320 |TALLONE                  |1976-01-01|1976-04-28       |TALLONE                      |2B320                          |
20321 |TARRANO                  |2B321 |TARRANO                  |1976-01-01|1974-02-20       |TARRANO                      |2B321                          |
20327 |TOMINO                   |2B327 |TOMINO                   |1976-01-01|1976-04-28       |TOMINO                       |2B327                          |
20328 |TOX                      |2B328 |TOX                      |1976-01-01|1976-04-28       |TOX                          |2B328                          |
20329 |TRALONCA                 |2B329 |TRALONCA                 |1976-01-01|1974-02-20       |TRALONCA                     |2B329                          |
20332 |URTACA                   |2B332 |URTACA                   |1976-01-01|1976-04-28       |URTACA                       |2B332                          |
20333 |VALLECALLE               |2B333 |VALLECALLE               |1976-01-01|1976-04-28       |VALLECALLE                   |2B333                          |
20334 |VALLE D ALESANI          |2B334 |VALLE D ALESANI          |1976-01-01|1974-02-20       |VALLE-D'ALESANI              |2B334                          |
20337 |VALLE DI ROSTINO         |2B337 |VALLE DI ROSTINO         |1976-01-01|1974-02-20       |VALLE-DI-ROSTINO             |2B337                          |
20338 |VALLE D OREZZA           |2B338 |VALLE D OREZZA           |1976-01-01|1974-02-20       |VALLE-D'OREZZA               |2B338                          |
20339 |VALLICA                  |2B339 |VALLICA                  |1976-01-01|1974-02-20       |VALLICA                      |2B339                          |
20340 |VELONE ORNETO            |2B340 |VELONE ORNETO            |1976-01-01|1976-04-28       |VELONE-ORNETO                |2B340                          |
20341 |VENACO                   |2B341 |VENACO                   |1976-01-01|1974-02-20       |VENACO                       |2B341                          |
20342 |VENTISERI                |2B342 |VENTISERI                |1976-01-01|1976-04-28       |VENTISERI                    |2B342                          |
20344 |VERDESE                  |2B344 |VERDESE                  |1976-01-01|1974-02-20       |VERDESE                      |2B344                          |
20347 |VEZZANI                  |2B347 |VEZZANI                  |1976-01-01|1974-02-20       |VEZZANI                      |2B347                          |
20350 |VIGNALE                  |2B350 |VIGNALE                  |1976-01-01|1976-04-28       |VIGNALE                      |2B350                          |
20352 |VILLE DI PARASO          |2B352 |VILLE DI PARASO          |1976-01-01|1976-04-28       |VILLE-DI-PARASO              |2B352                          |
20353 |VILLE DI PIETRABUGNO     |2B353 |VILLE DI PIETRABUGNO     |1976-01-01|1976-04-28       |VILLE-DI-PIETRABUGNO         |2B353                          |
20354 |VIVARIO                  |2B354 |VIVARIO                  |1976-01-01|1974-02-20       |VIVARIO                      |2B354                          |
20355 |VOLPAJOLA                |2B355 |VOLPAJOLA                |1976-01-01|1976-04-28       |VOLPAJOLA                    |2B355                          |
20356 |ZALANA                   |2B356 |ZALANA                   |1976-01-01|1974-02-20       |ZALANA                       |2B356                          |
20361 |ZILIA                    |2B361 |ZILIA                    |1976-01-01|1974-02-20       |ZILIA                        |2B361                          |
20364 |ZUANI                    |2B364 |ZUANI                    |1976-01-01|1974-02-20       |ZUANI                        |2B364                          |
20365 |SAN GAVINO DI FIUMORBO   |2B365 |SAN GAVINO DI FIUMORBO   |1976-01-01|1974-02-20       |SAN-GAVINO-DI-FIUMORBO       |2B365                          |
20366 |CHISA                    |2B366 |CHISA                    |1976-01-01|1974-02-20       |CHISA                        |2B366                          |
 */

---- Correction du fichier final :
--- pas besoin

------------------------------------------------------------------------------------------------------------------------------------------------
---- 23/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02B_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85|count|
-------------|-----|
partie       |    1|
totalit�     |  207|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02B_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02B_2019 as
SELECT 
'02B'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = 'partie' THEN 'partie'
  
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02B_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 208

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02B_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  207|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02B_2019 FOR VALUES IN ('02B');
--> Updated Rows	0

---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02B RENAME TO l_liste_commune_montagne_1985_02B_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02B_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
partie  |    1|
totalit�|  207|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02B;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02B AS
SELECT
'02B'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'partie' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02B_old
ORDER BY insee_classement;
--> 208

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02B GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  207|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02B FOR VALUES IN ('02B');
--> Updated Rows	0



