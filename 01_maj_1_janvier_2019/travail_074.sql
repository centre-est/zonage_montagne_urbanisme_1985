---- 24/08/2020
----- HAUTE-SAVOIE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074  WHERE date_decis IS NULL;

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 SET date_decis='20/02/1974,28/04/1976' WHERE date_decis IS NULL;
--> Updated Rows	4
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	173

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074;
--> 91

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019 |insee_commune_arrete_classement|nom_commune_arrete_classement                                         |arrete_classement                                       |classement_commune                            |commentaires                                                                                                                                                                   |nb_evenements_cog|
-------------|--------------------|-------------------------------|----------------------------------------------------------------------|--------------------------------------------------------|----------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
74010        |ANNECY              |{74011,74217}                  |{ANNECY-LE-VIEUX,PRINGY-FERRIERES}                                    |{1976-04-28,1974-02-20}                                 |{P,????}                                      |{"Hameaux�: Sur Les Bois, Chez le Roy,Chez Chappet, Chez Rosset","Arr�t� de 1974 ne cite que FERRIERES"}                                                                       |                2|
74014        |ARACHES LA FRASSE   |{74014}                        |{ARACHES}                                                             |{1974-02-20}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74045        |BOUCHET MONT CHARVIN|{74045}                        |{"LE BOUCHET-MONT-CHARVIN"}                                           |{1974-02-20}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74077        |CLARAFOND ARCINE    |{74077}                        |{CLARAFOND-ARCINE}                                                    |{1976-04-28}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74106        |DRAILLANT           |{74210}                        |{PERRIGNIER}                                                          |{1974-02-20}                                            |{P}                                           |{"Section Draillant"}                                                                                                                                                          |                1|
74123        |FAVERGES SEYTHENEX  |{74123,74270}                  |{FAVERGES,SEYTHENEX}                                                  |{"20/02/1974,28/04/1976",1974-02-20}                    |{totalit�,totalit�}                           |{"Arr�t� de 1974�: Hameaux de Mont-Bogon, Glaise, Bellecombe, Chantcovert, Frontenex, Verdi�res�Arr�t� de 1976�: reste du territoire non class� par arr�t� du 20/02/1974",NULL}|                2|
74167        |VAL DE CHAISE       |{74084,74167}                  |{CONS-SAINTE-COLOMBE,MARLENS}                                         |{1974-02-20,1974-02-20}                                 |{totalit�,totalit�}                           |{NULL,NULL}                                                                                                                                                                    |                2|
74203        |NOVEL               |{74237}                        |{SAINT-GINGOLPH}                                                      |{1974-02-20}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74210        |PERRIGNIER          |{74210}                        |{PERRIGNIER}                                                          |{1974-02-20}                                            |{P}                                           |{"Section Draillant"}                                                                                                                                                          |                1|
74212        |GLIERES VAL DE BORNE|{74110,74212}                  |{ENTREMONT,"LE PETIT-BORNAND-LES-GLIERES"}                            |{1974-02-20,1974-02-20}                                 |{totalit�,totalit�}                           |{NULL,"Ecrit le Petit Bonnant dans l�arr�t� de 1974"}                                                                                                                          |                2|
74220        |REIGNIER ESERY      |{74220}                        |{REIGNIER}                                                            |{1974-02-20}                                            |{P}                                           |{"Hameaux de Yvre, Moussy, Gratteloup, La Cretaz"}                                                                                                                             |                1|
74237        |SAINT GINGOLPH      |{74237}                        |{SAINT-GINGOLPH}                                                      |{1974-02-20}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74256        |SALLANCHES          |{74246,74256}                  |{SAINT-MARTIN-SUR-ARVE,SALLANCHES}                                    |{1974-02-20,1974-02-20}                                 |{totalit�,totalit�}                           |{NULL,NULL}                                                                                                                                                                    |                2|
74267        |SEVRIER             |{74267}                        |{SEVRIER}                                                             |{1976-04-28}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74273        |SIXT FER CHEVAL     |{74273}                        |{SIXT}                                                                |{1974-02-20}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74275        |TALLOIRES MONTMIN   |{74187,74275}                  |{MONTMIN,TALLOIRES}                                                   |{1974-02-20,1974-02-20}                                 |{totalit�,totalit�}                           |{NULL,NULL}                                                                                                                                                                    |                2|
74279        |THOLLON LES MEMISES |{74279}                        |{THOLLON}                                                             |{1974-02-20}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                         |                1|
74282        |FILLIERE            |{74022,74120,74204,74245,74282}|{AVIERNOZ,EVIRES,"LES OLLIERES",SAINT-MARTIN-BELLEVUE,THORENS-GLIERES}|{1974-02-20,1974-02-20,1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�,totalit�,totalit�}|{NULL,NULL,NULL,NULL,NULL}                                                                                                                                                     |                5|
74289        |VALLIERES SUR FIER  |{74274}                        |{VAL-DE-FIER}                                                         |{1974-02-20}                                            |{????}                                        |{"Arr�t� de 1974 cite seulement SAINT-ANDRE-VAL-DE-FIER"}                                                                                                                      |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 164 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	183

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '74' ---074
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes             |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|-------------------------------|-----------------|---------------------|
74001        |74001         |74001                          |{74001}                        |                0|                    1|
74003        |74003         |74003                          |{74003}                        |                0|                    1|
74004        |74004         |74004                          |{74004}                        |                0|                    1|
74006        |74006         |74006                          |{74006}                        |                0|                    1|
74009        |74009         |74009                          |{74009}                        |                0|                    1|
74010        |74010         |{74011,74217}                  |{74217,74011}                  |                2|                    2|
74014        |74014         |{74014}                        |{74014}                        |                1|                    1|
74015        |74015         |74015                          |{74015}                        |                0|                    1|
74016        |74016         |74016                          |{74016}                        |                0|                    1|
74020        |74020         |74020                          |{74020}                        |                0|                    1|
74024        |74024         |74024                          |{74024}                        |                0|                    1|
74027        |74027         |74027                          |{74027}                        |                0|                    1|
74030        |74030         |74030                          |{74030}                        |                0|                    1|
74031        |74031         |74031                          |{74031}                        |                0|                    1|
74032        |74032         |74032                          |{74032}                        |                0|                    1|
74033        |74033         |74033                          |{74033}                        |                0|                    1|
74034        |74034         |74034                          |{74034}                        |                0|                    1|
74036        |74036         |74036                          |{74036}                        |                0|                    1|
74037        |74037         |74037                          |{74037}                        |                0|                    1|
74038        |74038         |74038                          |{74038}                        |                0|                    1|
74040        |74040         |74040                          |{74040}                        |                0|                    1|
74041        |74041         |74041                          |{74041}                        |                0|                    1|
74042        |74042         |74042                          |{74042}                        |                0|                    1|
74043        |74043         |74043                          |{74043}                        |                0|                    1|
74045        |74045         |{74045}                        |{74045}                        |                1|                    1|
74048        |74048         |74048                          |{74048}                        |                0|                    1|
74049        |74049         |74049                          |{74049}                        |                0|                    1|
74050        |74050         |74050                          |{74050}                        |                0|                    1|
74051        |74051         |74051                          |{74051}                        |                0|                    1|
74052        |74052         |74052                          |{74052}                        |                0|                    1|
74053        |74053         |74053                          |{74053}                        |                0|                    1|
74056        |74056         |74056                          |{74056}                        |                0|                    1|
74057        |74057         |74057                          |{74057}                        |                0|                    1|
74058        |74058         |74058                          |{74058}                        |                0|                    1|
74059        |74059         |74059                          |{74059}                        |                0|                    1|
74060        |74060         |74060                          |{74060}                        |                0|                    1|
74062        |74062         |74062                          |{74062}                        |                0|                    1|
74063        |74063         |74063                          |{74063}                        |                0|                    1|
74064        |74064         |74064                          |{74064}                        |                0|                    1|
74065        |74065         |74065                          |{74065}                        |                0|                    1|
74072        |74072         |74072                          |{74072}                        |                0|                    1|
74073        |74073         |74073                          |{74073}                        |                0|                    1|
74074        |74074         |74074                          |{74074}                        |                0|                    1|
74076        |74076         |74076                          |{74076}                        |                0|                    1|
74077        |74077         |{74077}                        |{74077}                        |                1|                    1|
74079        |74079         |74079                          |{74079}                        |                0|                    1|
74080        |74080         |74080                          |{74080}                        |                0|                    1|
74082        |74082         |74082                          |{74082}                        |                0|                    1|
74083        |74083         |74083                          |{74083}                        |                0|                    1|
74085        |74085         |74085                          |{74085}                        |                0|                    1|
74088        |74088         |74088                          |{74088}                        |                0|                    1|
74089        |74089         |74089                          |{74089}                        |                0|                    1|
74091        |74091         |74091                          |{74091}                        |                0|                    1|
74094        |74094         |74094                          |{74094}                        |                0|                    1|
74095        |74095         |74095                          |{74095}                        |                0|                    1|
74096        |74096         |74096                          |{74096}                        |                0|                    1|
74097        |74097         |74097                          |{74097}                        |                0|                    1|
74098        |74098         |74098                          |{74098}                        |                0|                    1|
74099        |74099         |74099                          |{74099}                        |                0|                    1|
74101        |74101         |74101                          |{74101}                        |                0|                    1|
74102        |74102         |74102                          |{74102}                        |                0|                    1|
74103        |74103         |74103                          |{74103}                        |                0|                    1|
74104        |74104         |74104                          |{74104}                        |                0|                    1|
74106        |74106         |{74210}                        |{74106}                        |                1|                    1|
74107        |74107         |74107                          |{74107}                        |                0|                    1|
74108        |74108         |74108                          |{74108}                        |                0|                    1|
74111        |74111         |74111                          |{74111}                        |                0|                    1|
74114        |74114         |74114                          |{74114}                        |                0|                    1|
74116        |74116         |74116                          |{74116}                        |                0|                    1|
74122        |74122         |74122                          |{74122}                        |                0|                    1|
74123        |74123         |{74123,74270}                  |{74123,74270}                  |                2|                    2|
74126        |74126         |74126                          |{74126}                        |                0|                    1|
74127        |74127         |74127                          |{74127}                        |                0|                    1|
74128        |74128         |74128                          |{74128}                        |                0|                    1|
74129        |74129         |74129                          |{74129}                        |                0|                    1|
74134        |74134         |74134                          |{74134}                        |                0|                    1|
74135        |74135         |74135                          |{74135}                        |                0|                    1|
74136        |74136         |74136                          |{74136}                        |                0|                    1|
74137        |74137         |74137                          |{74137}                        |                0|                    1|
74138        |74138         |74138                          |{74138}                        |                0|                    1|
74139        |74139         |74139                          |{74139}                        |                0|                    1|
74140        |74140         |74140                          |{74140}                        |                0|                    1|
74143        |74143         |74143                          |{74143}                        |                0|                    1|
74146        |74146         |74146                          |{74146}                        |                0|                    1|
74147        |74147         |74147                          |{74147}                        |                0|                    1|
74148        |74148         |74148                          |{74148}                        |                0|                    1|
74151        |74151         |74151                          |{74151}                        |                0|                    1|
74153        |74153         |74153                          |{74153}                        |                0|                    1|
74154        |74154         |74154                          |{74154}                        |                0|                    1|
74155        |74155         |74155                          |{74155}                        |                0|                    1|
74157        |74157         |74157                          |{74157}                        |                0|                    1|
74159        |74159         |74159                          |{74159}                        |                0|                    1|
74160        |74160         |74160                          |{74160}                        |                0|                    1|
74162        |74162         |74162                          |{74162}                        |                0|                    1|
74164        |74164         |74164                          |{74164}                        |                0|                    1|
74167        |74167         |{74084,74167}                  |{74084,74167}                  |                2|                    2|
74170        |74170         |74170                          |{74170}                        |                0|                    1|
74173        |74173         |74173                          |{74173}                        |                0|                    1|
74174        |74174         |74174                          |{74174}                        |                0|                    1|
74175        |74175         |74175                          |{74175}                        |                0|                    1|
74176        |74176         |74176                          |{74176}                        |                0|                    1|
74177        |74177         |74177                          |{74177}                        |                0|                    1|
74183        |74183         |74183                          |{74183}                        |                0|                    1|
74185        |74185         |74185                          |{74185}                        |                0|                    1|
74188        |74188         |74188                          |{74188}                        |                0|                    1|
74189        |74189         |74189                          |{74189}                        |                0|                    1|
74190        |74190         |74190                          |{74190}                        |                0|                    1|
74191        |74191         |74191                          |{74191}                        |                0|                    1|
74192        |74192         |74192                          |{74192}                        |                0|                    1|
74193        |74193         |74193                          |{74193}                        |                0|                    1|
74195        |74195         |74195                          |{74195}                        |                0|                    1|
74196        |74196         |74196                          |{74196}                        |                0|                    1|
74198        |74198         |74198                          |{74198}                        |                0|                    1|
74203        |74203         |{74237}                        |{74203}                        |                1|                    1|
74205        |74205         |74205                          |{74205}                        |                0|                    1|
74206        |74206         |74206                          |{74206}                        |                0|                    1|
74208        |74208         |74208                          |{74208}                        |                0|                    1|
74209        |74209         |74209                          |{74209}                        |                0|                    1|
74211        |74211         |74211                          |{74211}                        |                0|                    1|
74212        |74212         |{74110,74212}                  |{74212,74110}                  |                2|                    2|
74215        |74215         |74215                          |{74215}                        |                0|                    1|
74216        |74216         |74216                          |{74216}                        |                0|                    1|
74219        |74219         |74219                          |{74219}                        |                0|                    1|
74220        |74220         |{74220}                        |{74220}                        |                1|                    1|
74221        |74221         |74221                          |{74221}                        |                0|                    1|
74222        |74222         |74222                          |{74222}                        |                0|                    1|
74223        |74223         |74223                          |{74223}                        |                0|                    1|
74224        |74224         |74224                          |{74224}                        |                0|                    1|
74226        |74226         |74226                          |{74226}                        |                0|                    1|
74228        |74228         |74228                          |{74228}                        |                0|                    1|
74229        |74229         |74229                          |{74229}                        |                0|                    1|
74232        |74232         |74232                          |{74232}                        |                0|                    1|
74234        |74234         |74234                          |{74234}                        |                0|                    1|
74236        |74236         |74236                          |{74236}                        |                0|                    1|
74237        |74237         |{74237}                        |{74237}                        |                1|                    1|
74238        |74238         |74238                          |{74238}                        |                0|                    1|
74239        |74239         |74239                          |{74239}                        |                0|                    1|
74240        |74240         |74240                          |{74240}                        |                0|                    1|
74241        |74241         |74241                          |{74241}                        |                0|                    1|
74242        |74242         |74242                          |{74242}                        |                0|                    1|
74244        |74244         |74244                          |{74244}                        |                0|                    1|
74249        |74249         |74249                          |{74249}                        |                0|                    1|
74250        |74250         |74250                          |{74250}                        |                0|                    1|
74252        |74252         |74252                          |{74252}                        |                0|                    1|
74253        |74253         |74253                          |{74253}                        |                0|                    1|
74256        |74256         |{74246,74256}                  |{74256}                        |                2|                    1|
74258        |74258         |74258                          |{74258}                        |                0|                    1|
74259        |74259         |74259                          |{74259}                        |                0|                    1|
74260        |74260         |74260                          |{74260}                        |                0|                    1|
74261        |74261         |74261                          |{74261}                        |                0|                    1|
74265        |74265         |74265                          |{74265}                        |                0|                    1|
74266        |74266         |74266                          |{74266}                        |                0|                    1|
74267        |74267         |{74267}                        |{74267}                        |                1|                    1|
74269        |74269         |74269                          |{74269}                        |                0|                    1|
74271        |74271         |74271                          |{74271}                        |                0|                    1|
74273        |74273         |{74273}                        |{74273}                        |                1|                    1|
74275        |74275         |{74187,74275}                  |{74275,74187}                  |                2|                    2|
74276        |74276         |74276                          |{74276}                        |                0|                    1|
74278        |74278         |74278                          |{74278}                        |                0|                    1|
74279        |74279         |{74279}                        |{74279}                        |                1|                    1|
74280        |74280         |74280                          |{74280}                        |                0|                    1|
74282        |74282         |{74022,74120,74204,74245,74282}|{74204,74282,74022,74120,74245}|                5|                    5|
74284        |74284         |74284                          |{74284}                        |                0|                    1|
74286        |74286         |74286                          |{74286}                        |                0|                    1|
74287        |74287         |74287                          |{74287}                        |                0|                    1|
74289        |74289         |{74274}                        |{74274}                        |                1|                    1|
74290        |74290         |74290                          |{74290}                        |                0|                    1|
74291        |74291         |74291                          |{74291}                        |                0|                    1|
74294        |74294         |74294                          |{74294}                        |                0|                    1|
74295        |74295         |74295                          |{74295}                        |                0|                    1|
74296        |74296         |74296                          |{74296}                        |                0|                    1|
74299        |74299         |74299                          |{74299}                        |                0|                    1|
74301        |74301         |74301                          |{74301}                        |                0|                    1|
74302        |74302         |74302                          |{74302}                        |                0|                    1|
74303        |74303         |74303                          |{74303}                        |                0|                    1|
74304        |74304         |74304                          |{74304}                        |                0|                    1|
74306        |74306         |74306                          |{74306}                        |                0|                    1|
74307        |74307         |74307                          |{74307}                        |                0|                    1|
74308        |74308         |74308                          |{74308}                        |                0|                    1|
74310        |74310         |74310                          |{74310}                        |                0|                    1|
74311        |74311         |74311                          |{74311}                        |                0|                    1|
74313        |74313         |74313                          |{74313}                        |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074;
--> Updated Rows	183

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap        |insee_comm|nom_commun    |pointage|partie  |date_decis|date_eff  |mod|
------|--------------|----------|--------------|--------|--------|----------|----------|---|
74106 |DRAILLANT     |74210     |PERRIGNIER    |x       |P       |1974-02-20|2001-02-19|21 |
74203 |NOVEL         |74237     |SAINT-GINGOLPH|x       |totalit�|1974-02-20|1983-01-01|21 |
74210 |PERRIGNIER    |74210     |PERRIGNIER    |x       |P       |1974-02-20|2001-02-19|21 |
74237 |SAINT GINGOLPH|74237     |SAINT-GINGOLPH|x       |totalit�|1974-02-20|1983-01-01|21 |
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074)
order by date_eff, com_ap
/*
com_av|ncc_av                  |com_ap|ncc_ap                  |date_eff  |arrete_classement      |nom_commune_arrete_classement     |insee_commune_arrete_classement|
------|------------------------|------|------------------------|----------|-----------------------|----------------------------------|-------------------------------|
74214 |PONTCHY                 |74042 |BONNEVILLE              |1961-08-07|1974-02-20             |BONNEVILLE                        |74042                          |
74092 |COTE D HYOT             |74042 |BONNEVILLE              |1964-12-17|1974-02-20             |BONNEVILLE                        |74042                          |
74247 |SAINT MAURICE DE RUMILLY|74250 |SAINT PIERRE EN FAUCIGNY|1965-01-01|1974-02-20             |SAINT-PIERRE-EN-FAUCIGNY          |74250                          |
74207 |PASSEIRIER              |74250 |SAINT PIERRE EN FAUCIGNY|1965-01-01|1974-02-20             |SAINT-PIERRE-EN-FAUCIGNY          |74250                          |
74230 |SAINT DIDIER EN CHABLAIS|74043 |BONS EN CHABLAIS        |1966-07-01|1974-02-20             |BONS-EN-CHABLAIS                  |74043                          |
74047 |BRENS                   |74043 |BONS EN CHABLAIS        |1966-07-01|1974-02-20             |BONS-EN-CHABLAIS                  |74043                          |
74251 |SAINT ROCH              |74256 |SALLANCHES              |1972-01-01|{1974-02-20,1974-02-20}|{SAINT-MARTIN-SUR-ARVE,SALLANCHES}|{74246,74256}                  |
74023 |AVREGNY                 |74006 |ALLONZIER LA CAILLE     |1973-01-01|1974-02-20             |ALLONZIER-LA-CAILLE               |74006                          |
74149 |LOEX                    |74040 |BONNE                   |1973-01-01|1974-02-20             |BONNE                             |74040                          |
74132 |FRASSE                  |74014 |ARACHES                 |1974-01-01|{1974-02-20}           |{ARACHES}                         |{74014}                        |
74017 |ARCINE                  |74077 |CLARAFOND               |1974-01-01|{1976-04-28}           |{CLARAFOND-ARCINE}                |{74077}                        |
74039 |BONNEGUETE              |74095 |CREMPIGNY BONNEGUETE    |1974-01-01|1976-04-28             |CREMPIGNY-BONNEGUETE              |74095                          |
74106 |DRAILLANT               |74106 |DRAILLANT               |1974-01-01|{1974-02-20}           |{PERRIGNIER}                      |{74210}                        |
74115 |ESSERTS SALEVE          |74185 |MONNETIER MORNEX        |1974-01-01|1974-02-20             |MONNETIER-MORNEX                  |74185                          |
74203 |NOVEL                   |74203 |NOVEL                   |1974-01-01|{1974-02-20}           |{SAINT-GINGOLPH}                  |{74237}                        |
74106 |DRAILLANT               |74210 |PERRIGNIER              |1974-01-01|{1974-02-20}           |{PERRIGNIER}                      |{74210}                        |
74113 |ESERY                   |74220 |REIGNIER                |1974-01-01|{1974-02-20}           |{REIGNIER}                        |{74220}                        |
74248 |SAINT NICOLAS DE VEROCE |74236 |SAINT GERVAIS LES BAINS |1974-01-01|1974-02-20             |SAINT-GERVAIS-LES-BAINS           |74236                          |
74203 |NOVEL                   |74237 |SAINT GINGOLPH          |1974-01-01|{1974-02-20}           |{SAINT-GINGOLPH}                  |{74237}                        |
74203 |NOVEL                   |74203 |NOVEL                   |1983-01-01|{1974-02-20}           |{SAINT-GINGOLPH}                  |{74237}                        |
74132 |FRASSE                  |74014 |ARACHES                 |1995-06-01|{1974-02-20}           |{ARACHES}                         |{74014}                        |
74106 |DRAILLANT               |74106 |DRAILLANT               |2001-02-19|{1974-02-20}           |{PERRIGNIER}                      |{74210}                        |
74017 |ARCINE                  |74077 |CLARAFOND ARCINE        |2005-07-01|{1976-04-28}           |{CLARAFOND-ARCINE}                |{74077}                        |
74113 |ESERY                   |74220 |REIGNIER ESERY          |2007-04-01|{1974-02-20}           |{REIGNIER}                        |{74220}                        |
74010 |ANNECY                  |74010 |ANNECY                  |2017-01-01|{1976-04-28,1974-02-20}|{ANNECY-LE-VIEUX,PRINGY-FERRIERES}|{74011,74217}                  |
74093 |CRAN GEVRIER            |74010 |ANNECY                  |2017-01-01|{1976-04-28,1974-02-20}|{ANNECY-LE-VIEUX,PRINGY-FERRIERES}|{74011,74217}                  |
74182 |MEYTHET                 |74010 |ANNECY                  |2017-01-01|{1976-04-28,1974-02-20}|{ANNECY-LE-VIEUX,PRINGY-FERRIERES}|{74011,74217}                  |
74268 |SEYNOD                  |74010 |ANNECY                  |2017-01-01|{1976-04-28,1974-02-20}|{ANNECY-LE-VIEUX,PRINGY-FERRIERES}|{74011,74217}                  |
74289 |VALLIERES               |74289 |VALLIERES SUR FIER      |2019-01-01|{1974-02-20}           |{VAL-DE-FIER}                     |{74274}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---074
ORDER BY insee_cog2019, insee_com;




------------------------------------------------------------------------------------------------------------------------------------------------
---- 25/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                               |count|
------------------------------------------------------------|-----|
non class�e                                                 |    1|
{Non class�e,P,Non class�e,Non class�e,totalit�,non class�e}|    1|
P                                                           |   13|
{P}                                                         |    1|
{P, non class�e}                                            |    1|
totalit�                                                    |  152|
{totalit�}                                                  |    8|
{totalit�,totalit�}                                         |    5|
{totalit�,totalit�,totalit�,totalit�,totalit�}              |    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_074_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_074_2019 as
SELECT 
'074'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
case
	WHEN t1.classement_85 = 'non class�e' THEN NULL
	WHEN t1.classement_85 = '{Non class�e,P,Non class�e,Non class�e,totalit�,non class�e}' THEN 'partie'
	WHEN t1.classement_85 = 'P' THEN 'partie'
	WHEN t1.classement_85 = '{P}' THEN 'partie'
	WHEN t1.classement_85 = '{P, non class�e}' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_074_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 183

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_074_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
               |    1|
totalit�       |  166|
partie         |   16|
 */

---- Suppression de la commune non class�e :
delete from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_074_2019 where classement_2019 is null;
--> Updated Rows	1

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_074_2019 FOR VALUES IN ('074');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 RENAME TO l_liste_commune_montagne_1985_074_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |   18|
totalit�|  173|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 AS
SELECT
'074'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074_old
ORDER BY insee_classement;
--> 191

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  173|
partie         |   18|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_074 FOR VALUES IN ('074');
--> Updated Rows	0













