---- 20/08/2020
----- PUY DE DOME ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063  WHERE date_decis IS NULL;

--UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	283

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063;
--> 288

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019     |insee_commune_arrete_classement|nom_commune_arrete_classement|arrete_classement      |classement_commune |commentaires                                                                                                               |nb_evenements_cog|
-------------|------------------------|-------------------------------|-----------------------------|-----------------------|-------------------|---------------------------------------------------------------------------------------------------------------------------|-----------------|
63103        |CHATEL GUYON            |{63103}                        |{CHATELGUYON}                |{1976-04-28}           |{P}                |{"Section AR (Village du BOURNET)"}                                                                                        |                1|
63303        |ROCHE CHARLES LA MAYRAND|{63217,63303}                  |{"LA MEYRAND",ROCHE-CHARLES} |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,"01/01/1976 : Roche-Charles devient Roche-Charles-la-Mayrand suite � sa fusion-association avec La Mayrand (63217)."}|                2|
63335        |SAINT DIERY             |{63127,63335}                  |{CRESTE,SAINT-DIERY}         |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                                                |                2|
63448        |LE VERNET CHAMEANE      |{63078,63448}                  |{CHAMEANE,VERNET-LA-VARENNE} |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                                                |                1|
63463        |VISCOMTAT               |{63463}                        |{VISCOMTAT}                  |{1974-02-20}           |{totalit�}         |{NULL}                                                                                                                     |                0|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 280 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	285

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '63' ---063
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
63002        |63002         |63002                          |{63002}           |                0|                    1|
63003        |63003         |63003                          |{63003}           |                0|                    1|
63004        |63004         |63004                          |{63004}           |                0|                    1|
63006        |63006         |63006                          |{63006}           |                0|                    1|
63007        |63007         |63007                          |{63007}           |                0|                    1|
63008        |63008         |63008                          |{63008}           |                0|                    1|
63009        |63009         |63009                          |{63009}           |                0|                    1|
63010        |63010         |63010                          |{63010}           |                0|                    1|
63011        |63011         |63011                          |{63011}           |                0|                    1|
63015        |63015         |63015                          |{63015}           |                0|                    1|
63016        |63016         |63016                          |{63016}           |                0|                    1|
63017        |63017         |63017                          |{63017}           |                0|                    1|
63020        |63020         |63020                          |{63020}           |                0|                    1|
63023        |63023         |63023                          |{63023}           |                0|                    1|
63024        |63024         |63024                          |{63024}           |                0|                    1|
63025        |63025         |63025                          |{63025}           |                0|                    1|
63026        |63026         |63026                          |{63026}           |                0|                    1|
63027        |63027         |63027                          |{63027}           |                0|                    1|
63028        |63028         |63028                          |{63028}           |                0|                    1|
63029        |63029         |63029                          |{63029}           |                0|                    1|
63037        |63037         |63037                          |{63037}           |                0|                    1|
63038        |63038         |63038                          |{63038}           |                0|                    1|
63039        |63039         |63039                          |{63039}           |                0|                    1|
63041        |63041         |63041                          |{63041}           |                0|                    1|
63043        |63043         |63043                          |{63043}           |                0|                    1|
63047        |63047         |63047                          |{63047}           |                0|                    1|
63048        |63048         |63048                          |{63048}           |                0|                    1|
63053        |63053         |63053                          |{63053}           |                0|                    1|
63055        |63055         |63055                          |{63055}           |                0|                    1|
63056        |63056         |63056                          |{63056}           |                0|                    1|
63057        |63057         |63057                          |{63057}           |                0|                    1|
63060        |63060         |63060                          |{63060}           |                0|                    1|
63062        |63062         |63062                          |{63062}           |                0|                    1|
63064        |63064         |63064                          |{63064}           |                0|                    1|
63065        |63065         |63065                          |{63065}           |                0|                    1|
63066        |63066         |63066                          |{63066}           |                0|                    1|
63067        |63067         |63067                          |{63067}           |                0|                    1|
63071        |63071         |63071                          |{63071}           |                0|                    1|
63072        |63072         |63072                          |{63072}           |                0|                    1|
63076        |63076         |63076                          |{63076}           |                0|                    1|
63077        |63077         |63077                          |{63077}           |                0|                    1|
63079        |63079         |63079                          |{63079}           |                0|                    1|
63081        |63081         |63081                          |{63081}           |                0|                    1|
63083        |63083         |63083                          |{63083}           |                0|                    1|
63084        |63084         |63084                          |{63084}           |                0|                    1|
63085        |63085         |63085                          |{63085}           |                0|                    1|
63086        |63086         |63086                          |{63086}           |                0|                    1|
63087        |63087         |63087                          |{63087}           |                0|                    1|
63088        |63088         |63088                          |{63088}           |                0|                    1|
63092        |63092         |63092                          |{63092}           |                0|                    1|
63093        |63093         |63093                          |{63093}           |                0|                    1|
63094        |63094         |63094                          |{63094}           |                0|                    1|
63097        |63097         |63097                          |{63097}           |                0|                    1|
63098        |63098         |63098                          |{63098}           |                0|                    1|
63100        |63100         |63100                          |{63100}           |                0|                    1|
63101        |63101         |63101                          |{63101}           |                0|                    1|
63102        |63102         |63102                          |{63102}           |                0|                    1|
63103        |63103         |{63103}                        |{63103}           |                1|                    1|
63104        |63104         |63104                          |{63104}           |                0|                    1|
63105        |63105         |63105                          |{63105}           |                0|                    1|
63110        |63110         |63110                          |{63110}           |                0|                    1|
63111        |63111         |63111                          |{63111}           |                0|                    1|
63115        |63115         |63115                          |{63115}           |                0|                    1|
63116        |63116         |63116                          |{63116}           |                0|                    1|
63117        |63117         |63117                          |{63117}           |                0|                    1|
63118        |63118         |63118                          |{63118}           |                0|                    1|
63119        |63119         |63119                          |{63119}           |                0|                    1|
63122        |63122         |63122                          |{63122}           |                0|                    1|
63123        |63123         |63123                          |{63123}           |                0|                    1|
63129        |63129         |63129                          |{63129}           |                0|                    1|
63130        |63130         |63130                          |{63130}           |                0|                    1|
63132        |63132         |63132                          |{63132}           |                0|                    1|
63134        |63134         |63134                          |{63134}           |                0|                    1|
63136        |63136         |63136                          |{63136}           |                0|                    1|
63137        |63137         |63137                          |{63137}           |                0|                    1|
63139        |63139         |63139                          |{63139}           |                0|                    1|
63140        |63140         |63140                          |{63140}           |                0|                    1|
63141        |63141         |63141                          |{63141}           |                0|                    1|
63142        |63142         |63142                          |{63142}           |                0|                    1|
63144        |63144         |63144                          |{63144}           |                0|                    1|
63145        |63145         |63145                          |{63145}           |                0|                    1|
63147        |63147         |63147                          |{63147}           |                0|                    1|
63150        |63150         |63150                          |{63150}           |                0|                    1|
63151        |63151         |63151                          |{63151}           |                0|                    1|
63152        |63152         |63152                          |{63152}           |                0|                    1|
63153        |63153         |63153                          |{63153}           |                0|                    1|
63155        |63155         |63155                          |{63155}           |                0|                    1|
63156        |63156         |63156                          |{63156}           |                0|                    1|
63157        |63157         |63157                          |{63157}           |                0|                    1|
63158        |63158         |63158                          |{63158}           |                0|                    1|
63159        |63159         |63159                          |{63159}           |                0|                    1|
63161        |63161         |63161                          |{63161}           |                0|                    1|
63162        |63162         |63162                          |{63162}           |                0|                    1|
63163        |63163         |63163                          |{63163}           |                0|                    1|
63165        |63165         |63165                          |{63165}           |                0|                    1|
63169        |63169         |63169                          |{63169}           |                0|                    1|
63170        |63170         |63170                          |{63170}           |                0|                    1|
63171        |63171         |63171                          |{63171}           |                0|                    1|
63172        |63172         |63172                          |{63172}           |                0|                    1|
63173        |63173         |63173                          |{63173}           |                0|                    1|
63174        |63174         |63174                          |{63174}           |                0|                    1|
63175        |63175         |63175                          |{63175}           |                0|                    1|
63176        |63176         |63176                          |{63176}           |                0|                    1|
63177        |63177         |63177                          |{63177}           |                0|                    1|
63179        |63179         |63179                          |{63179}           |                0|                    1|
63183        |63183         |63183                          |{63183}           |                0|                    1|
63184        |63184         |63184                          |{63184}           |                0|                    1|
63186        |63186         |63186                          |{63186}           |                0|                    1|
63187        |63187         |63187                          |{63187}           |                0|                    1|
63189        |63189         |63189                          |{63189}           |                0|                    1|
63190        |63190         |63190                          |{63190}           |                0|                    1|
63191        |63191         |63191                          |{63191}           |                0|                    1|
63192        |63192         |63192                          |{63192}           |                0|                    1|
63197        |63197         |63197                          |{63197}           |                0|                    1|
63198        |63198         |63198                          |{63198}           |                0|                    1|
63199        |63199         |63199                          |{63199}           |                0|                    1|
63202        |63202         |63202                          |{63202}           |                0|                    1|
63205        |63205         |63205                          |{63205}           |                0|                    1|
63206        |63206         |63206                          |{63206}           |                0|                    1|
63207        |63207         |63207                          |{63207}           |                0|                    1|
63211        |63211         |63211                          |{63211}           |                0|                    1|
63216        |63216         |63216                          |{63216}           |                0|                    1|
63218        |63218         |63218                          |{63218}           |                0|                    1|
63219        |63219         |63219                          |{63219}           |                0|                    1|
63220        |63220         |63220                          |{63220}           |                0|                    1|
63221        |63221         |63221                          |{63221}           |                0|                    1|
63223        |63223         |63223                          |{63223}           |                0|                    1|
63225        |63225         |63225                          |{63225}           |                0|                    1|
63228        |63228         |63228                          |{63228}           |                0|                    1|
63230        |63230         |63230                          |{63230}           |                0|                    1|
63231        |63231         |63231                          |{63231}           |                0|                    1|
63233        |63233         |63233                          |{63233}           |                0|                    1|
63234        |63234         |63234                          |{63234}           |                0|                    1|
63236        |63236         |63236                          |{63236}           |                0|                    1|
63237        |63237         |63237                          |{63237}           |                0|                    1|
63238        |63238         |63238                          |{63238}           |                0|                    1|
63239        |63239         |63239                          |{63239}           |                0|                    1|
63243        |63243         |63243                          |{63243}           |                0|                    1|
63246        |63246         |63246                          |{63246}           |                0|                    1|
63247        |63247         |63247                          |{63247}           |                0|                    1|
63248        |63248         |63248                          |{63248}           |                0|                    1|
63251        |63251         |63251                          |{63251}           |                0|                    1|
63256        |63256         |63256                          |{63256}           |                0|                    1|
63257        |63257         |63257                          |{63257}           |                0|                    1|
63258        |63258         |63258                          |{63258}           |                0|                    1|
63259        |63259         |63259                          |{63259}           |                0|                    1|
63260        |63260         |63260                          |{63260}           |                0|                    1|
63263        |63263         |63263                          |{63263}           |                0|                    1|
63264        |63264         |63264                          |{63264}           |                0|                    1|
63267        |63267         |63267                          |{63267}           |                0|                    1|
63271        |63271         |63271                          |{63271}           |                0|                    1|
63274        |63274         |63274                          |{63274}           |                0|                    1|
63277        |63277         |63277                          |{63277}           |                0|                    1|
63279        |63279         |63279                          |{63279}           |                0|                    1|
63280        |63280         |63280                          |{63280}           |                0|                    1|
63281        |63281         |63281                          |{63281}           |                0|                    1|
63283        |63283         |63283                          |{63283}           |                0|                    1|
63285        |63285         |63285                          |{63285}           |                0|                    1|
63286        |63286         |63286                          |{63286}           |                0|                    1|
63289        |63289         |63289                          |{63289}           |                0|                    1|
63290        |63290         |63290                          |{63290}           |                0|                    1|
63291        |63291         |63291                          |{63291}           |                0|                    1|
63292        |63292         |63292                          |{63292}           |                0|                    1|
63293        |63293         |63293                          |{63293}           |                0|                    1|
63294        |63294         |63294                          |{63294}           |                0|                    1|
63298        |63298         |63298                          |{63298}           |                0|                    1|
63299        |63299         |63299                          |{63299}           |                0|                    1|
63301        |63301         |63301                          |{63301}           |                0|                    1|
63303        |63303         |{63217,63303}                  |{63303}           |                2|                    1|
63304        |63304         |63304                          |{63304}           |                0|                    1|
63305        |63305         |63305                          |{63305}           |                0|                    1|
63307        |63307         |63307                          |{63307}           |                0|                    1|
63309        |63309         |63309                          |{63309}           |                0|                    1|
63310        |63310         |63310                          |{63310}           |                0|                    1|
63312        |63312         |63312                          |{63312}           |                0|                    1|
63313        |63313         |63313                          |{63313}           |                0|                    1|
63314        |63314         |63314                          |{63314}           |                0|                    1|
63318        |63318         |63318                          |{63318}           |                0|                    1|
63319        |63319         |63319                          |{63319}           |                0|                    1|
63320        |63320         |63320                          |{63320}           |                0|                    1|
63323        |63323         |63323                          |{63323}           |                0|                    1|
63324        |63324         |63324                          |{63324}           |                0|                    1|
63326        |63326         |63326                          |{63326}           |                0|                    1|
63328        |63328         |63328                          |{63328}           |                0|                    1|
63329        |63329         |63329                          |{63329}           |                0|                    1|
63331        |63331         |63331                          |{63331}           |                0|                    1|
63334        |63334         |63334                          |{63334}           |                0|                    1|
63335        |63335         |{63127,63335}                  |{63127,63335}     |                2|                    2|
63336        |63336         |63336                          |{63336}           |                0|                    1|
63337        |63337         |63337                          |{63337}           |                0|                    1|
63338        |63338         |63338                          |{63338}           |                0|                    1|
63339        |63339         |63339                          |{63339}           |                0|                    1|
63340        |63340         |63340                          |{63340}           |                0|                    1|
63341        |63341         |63341                          |{63341}           |                0|                    1|
63342        |63342         |63342                          |{63342}           |                0|                    1|
63343        |63343         |63343                          |{63343}           |                0|                    1|
63344        |63344         |63344                          |{63344}           |                0|                    1|
63345        |63345         |63345                          |{63345}           |                0|                    1|
63346        |63346         |63346                          |{63346}           |                0|                    1|
63348        |63348         |63348                          |{63348}           |                0|                    1|
63349        |63349         |63349                          |{63349}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063;
--> Updated Rows	285

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063)
order by date_eff, com_ap
/*
com_av|ncc_av         |com_ap|ncc_ap                  |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|---------------|------|------------------------|----------|-----------------|-----------------------------|-------------------------------|
63361 |SAINT HIPPOLYTE|63103 |CHATELGUYON             |1973-01-01|{1976-04-28}     |{CHATELGUYON}                |{63103}                        |
63316 |SAINT ANASTAISE|63038 |BESSE ET SAINT ANASTAISE|1973-07-01|1974-02-20       |BESSE-ET-SAINT-ANASTAISE     |63038                          |
63361 |SAINT HIPPOLYTE|63103 |CHATEL GUYON            |2007-07-01|{1976-04-28}     |{CHATELGUYON}                |{63103}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---063
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 21/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
P                  |    4|
{P}                |    1|
totalit�           |  276|
{totalit�}         |    1|
{totalit�,totalit�}|    3|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 as
SELECT 
'063'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'  	
	WHEN t1.classement_85 = 'P' THEN 'partie'
	WHEN t1.classement_85 = '{P}' THEN 'partie'
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_063_final AS t1
LEFT JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 285

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  280|
partie         |    5|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_063_2019 FOR VALUES IN ('063');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 RENAME TO l_liste_commune_montagne_1985_063_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |    5|
totalit�|  283|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 AS
SELECT
'063'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063_old
ORDER BY insee_classement;
--> 288

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  283|
partie         |    5|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_063 FOR VALUES IN ('063');
--> Updated Rows	0
