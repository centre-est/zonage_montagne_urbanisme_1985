---- 24/08
---- 081 ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081  WHERE date_decis IS NULL;

--UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	78

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081;
--> 78

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019|insee_commune_arrete_classement|nom_commune_arrete_classement                |arrete_classement                 |classement_commune          |commentaires    |nb_evenements_cog|
-------------|-------------------|-------------------------------|---------------------------------------------|----------------------------------|----------------------------|----------------|-----------------|
81062        |FONTRIEU           |{81062,81091,81153}            |{CASTELNAU-DE-BRASSAC,FERRIERES,"LE MARGNES"}|{1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�}|{NULL,NULL,NULL}|                3|
81233        |TERRE DE BANCALIE  |{81241,81301}                  |{SAINT-ANTONIN-DE-LACALM,"LE TRAVET"}        |{1976-04-28,1976-04-28}           |{totalit�,totalit�}         |{NULL,NULL}     |                2|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 73 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	75

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '81' ---081
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|-------------------|-----------------|---------------------|
81002        |81002         |81002                          |{81002}            |                0|                    1|
81003        |81003         |81003                          |{81003}            |                0|                    1|
81005        |81005         |81005                          |{81005}            |                0|                    1|
81010        |81010         |81010                          |{81010}            |                0|                    1|
81014        |81014         |81014                          |{81014}            |                0|                    1|
81016        |81016         |81016                          |{81016}            |                0|                    1|
81017        |81017         |81017                          |{81017}            |                0|                    1|
81019        |81019         |81019                          |{81019}            |                0|                    1|
81021        |81021         |81021                          |{81021}            |                0|                    1|
81023        |81023         |81023                          |{81023}            |                0|                    1|
81028        |81028         |81028                          |{81028}            |                0|                    1|
81031        |81031         |81031                          |{81031}            |                0|                    1|
81034        |81034         |81034                          |{81034}            |                0|                    1|
81036        |81036         |81036                          |{81036}            |                0|                    1|
81037        |81037         |81037                          |{81037}            |                0|                    1|
81042        |81042         |81042                          |{81042}            |                0|                    1|
81047        |81047         |81047                          |{81047}            |                0|                    1|
81053        |81053         |81053                          |{81053}            |                0|                    1|
81055        |81055         |81055                          |{81055}            |                0|                    1|
81062        |81062         |{81062,81091,81153}            |{81062,81091,81153}|                3|                    3|
81071        |81071         |81071                          |{81071}            |                0|                    1|
81077        |81077         |81077                          |{81077}            |                0|                    1|
81081        |81081         |81081                          |{81081}            |                0|                    1|
81082        |81082         |81082                          |{81082}            |                0|                    1|
81083        |81083         |81083                          |{81083}            |                0|                    1|
81084        |81084         |81084                          |{81084}            |                0|                    1|
81085        |81085         |81085                          |{81085}            |                0|                    1|
81086        |81086         |81086                          |{81086}            |                0|                    1|
81094        |81094         |81094                          |{81094}            |                0|                    1|
81096        |81096         |81096                          |{81096}            |                0|                    1|
81103        |81103         |81103                          |{81103}            |                0|                    1|
81110        |81110         |81110                          |{81110}            |                0|                    1|
81115        |81115         |81115                          |{81115}            |                0|                    1|
81120        |81120         |81120                          |{81120}            |                0|                    1|
81121        |81121         |81121                          |{81121}            |                0|                    1|
81124        |81124         |81124                          |{81124}            |                0|                    1|
81125        |81125         |81125                          |{81125}            |                0|                    1|
81128        |81128         |81128                          |{81128}            |                0|                    1|
81134        |81134         |81134                          |{81134}            |                0|                    1|
81137        |81137         |81137                          |{81137}            |                0|                    1|
81158        |81158         |81158                          |{81158}            |                0|                    1|
81160        |81160         |81160                          |{81160}            |                0|                    1|
81161        |81161         |81161                          |{81161}            |                0|                    1|
81163        |81163         |81163                          |{81163}            |                0|                    1|
81167        |81167         |81167                          |{81167}            |                0|                    1|
81180        |81180         |81180                          |{81180}            |                0|                    1|
81182        |81182         |81182                          |{81182}            |                0|                    1|
81183        |81183         |81183                          |{81183}            |                0|                    1|
81188        |81188         |81188                          |{81188}            |                0|                    1|
81192        |81192         |81192                          |{81192}            |                0|                    1|
81193        |81193         |81193                          |{81193}            |                0|                    1|
81203        |81203         |81203                          |{81203}            |                0|                    1|
81209        |81209         |81209                          |{81209}            |                0|                    1|
81221        |81221         |81221                          |{81221}            |                0|                    1|
81223        |81223         |81223                          |{81223}            |                0|                    1|
81231        |81231         |81231                          |{81231}            |                0|                    1|
81233        |81233         |{81241,81301}                  |{81301,81241}      |                2|                    2|
81237        |81237         |81237                          |{81237}            |                0|                    1|
81238        |81238         |81238                          |{81238}            |                0|                    1|
81239        |81239         |81239                          |{81239}            |                0|                    1|
81240        |81240         |81240                          |{81240}            |                0|                    1|
81245        |81245         |81245                          |{81245}            |                0|                    1|
81264        |81264         |81264                          |{81264}            |                0|                    1|
81267        |81267         |81267                          |{81267}            |                0|                    1|
81268        |81268         |81268                          |{81268}            |                0|                    1|
81269        |81269         |81269                          |{81269}            |                0|                    1|
81278        |81278         |81278                          |{81278}            |                0|                    1|
81282        |81282         |81282                          |{81282}            |                0|                    1|
81288        |81288         |81288                          |{81288}            |                0|                    1|
81295        |81295         |81295                          |{81295}            |                0|                    1|
81303        |81303         |81303                          |{81303}            |                0|                    1|
81305        |81305         |81305                          |{81305}            |                0|                    1|
81312        |81312         |81312                          |{81312}            |                0|                    1|
81314        |81314         |81314                          |{81314}            |                0|                    1|
81321        |81321         |81321                          |{81321}            |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081;
--> Updated Rows	75

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081)
order by date_eff, com_ap
/*
com_av|ncc_av               |com_ap|ncc_ap           |date_eff  |arrete_classement      |nom_commune_arrete_classement        |insee_commune_arrete_classement|
------|---------------------|------|-----------------|----------|-----------------------|-------------------------------------|-------------------------------|
81194 |NARTHOUX             |81245 |SAINT CHRISTOPHE |1973-01-01|1976-04-28             |SAINT-CHRISTOPHE-NARTHOUX            |81245                          |
81226 |RONEL                |81233 |TERRE DE BANCALIE|2019-01-01|{1976-04-28,1976-04-28}|{SAINT-ANTONIN-DE-LACALM,"LE TRAVET"}|{81241,81301}                  |
81233 |ROUMEGOUX            |81233 |ROUMEGOUX        |2019-01-01|{1976-04-28,1976-04-28}|{SAINT-ANTONIN-DE-LACALM,"LE TRAVET"}|{81241,81301}                  |
81233 |ROUMEGOUX            |81233 |TERRE DE BANCALIE|2019-01-01|{1976-04-28,1976-04-28}|{SAINT-ANTONIN-DE-LACALM,"LE TRAVET"}|{81241,81301}                  |
81260 |SAINT LIEUX LAFENASSE|81233 |TERRE DE BANCALIE|2019-01-01|{1976-04-28,1976-04-28}|{SAINT-ANTONIN-DE-LACALM,"LE TRAVET"}|{81241,81301}                  |
81296 |TERRE CLAPIER        |81233 |TERRE DE BANCALIE|2019-01-01|{1976-04-28,1976-04-28}|{SAINT-ANTONIN-DE-LACALM,"LE TRAVET"}|{81241,81301}                  |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---081
ORDER BY insee_cog2019, insee_com;


------------------------------------------------------------------------------------------------------------------------------------------------
---- 25/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                                      |count|
-------------------------------------------------------------------|-----|
{non class�e,non class�e,totalit�,non class�e,non class�e,totalit�}|    1|
totalit�                                                           |   73|
{totalit�,totalit�,totalit�}                                       |    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_081_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_081_2019 as
SELECT 
'081'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{non class�e,non class�e,totalit�,non class�e,non class�e,totalit�}' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�}' THEN 'totalit�'  
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_081_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 75

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_081_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   74|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_081_2019 FOR VALUES IN ('081');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 RENAME TO l_liste_commune_montagne_1985_081_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|   78|
 */

---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 AS
SELECT
'081'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081_old
ORDER BY insee_classement;
--> 78

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   78|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_081 FOR VALUES IN ('081');
--> Updated Rows	0




