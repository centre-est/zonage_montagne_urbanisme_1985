---- 08/07/2020
----- RHONE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069  WHERE date_decis IS NULL;

--UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 SET partie = 'totalit�' WHERE partie IS NULL;
--> 0
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 SET partie = 'totalit�' WHERE partie ='';
--> Updated Rows	88

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069;
--> 133

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019|insee_commune_arrete_classement            |nom_commune_arrete_classement                                                        |arrete_classement                                                             |classement_commune                                              |commentaires                                                                           |nb_evenements_cog|
-------------|-------------------|-------------------------------------------|-------------------------------------------------------------------------------------|------------------------------------------------------------------------------|----------------------------------------------------------------|---------------------------------------------------------------------------------------|-----------------|
69066        |COURS              |{69066,69158,69247,69262}                  |{COURS,PONT-TRAMBOUZE,THEL,"LA VILLE"}                                               |{20/02/1974,20/02/1974,20/02/1974,20/02/1974}                                 |{totalit�,totalit�,totalit�,totalit�}                           |{"Cours-la-Ville dans fichier d�origine","","",""}                                     |                4|
69135        |DEUX GROSNES       |{69015,69135,69150,69185,69210,69224,69251}|{AVENAS,MONSOLS,OUROUX,SAINT-CHRISTOPHE,SAINT-JACQUES-DES-ARRETS,SAINT-MAMERT,TRADES}|{20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974}|{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|{"","","","","","",""}                                                                 |                7|
69248        |THIZY LES BOURGS   |{69041,69128,69129}                        |{"LA CHAPELLE-DE-MARDORE",MARDORE,MARNAND}                                           |{20/02/1974,20/02/1974,20/02/1974}                                            |{totalit�,totalit�,totalit�}                                    |{"Pas pr�sent dans fichier d�origine","Commune non pr�sente dans fichier d�origine",""}|                3|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 98 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	95

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '69' ---069
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement            |fred_anciens_codes                         |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------------------|-------------------------------------------|-----------------|---------------------|
69001        |69001         |69001                                      |{69001}                                    |                0|                    1|
69002        |69002         |69002                                      |{69002}                                    |                0|                    1|
69006        |69006         |69006                                      |{69006}                                    |                0|                    1|
69008        |69008         |69008                                      |{69008}                                    |                0|                    1|
69012        |69012         |69012                                      |{69012}                                    |                0|                    1|
69014        |69014         |69014                                      |{69014}                                    |                0|                    1|
69016        |69016         |69016                                      |{69016}                                    |                0|                    1|
69018        |69018         |69018                                      |{69018}                                    |                0|                    1|
69021        |69021         |69021                                      |{69021}                                    |                0|                    1|
69022        |69022         |69022                                      |{69022}                                    |                0|                    1|
69030        |69030         |69030                                      |{69030}                                    |                0|                    1|
69031        |69031         |69031                                      |{69031}                                    |                0|                    1|
69035        |69035         |69035                                      |{69035}                                    |                0|                    1|
69037        |69037         |69037                                      |{69037}                                    |                0|                    1|
69038        |69038         |69038                                      |{69038}                                    |                0|                    1|
69039        |69039         |69039                                      |{69039}                                    |                0|                    1|
69042        |69042         |69042                                      |{69042}                                    |                0|                    1|
69051        |69051         |69051                                      |{69051}                                    |                0|                    1|
69054        |69054         |69054                                      |{69054}                                    |                0|                    1|
69057        |69057         |69057                                      |{69057}                                    |                0|                    1|
69058        |69058         |69058                                      |{69058}                                    |                0|                    1|
69060        |69060         |69060                                      |{69060}                                    |                0|                    1|
69062        |69062         |69062                                      |{69062}                                    |                0|                    1|
69066        |69066         |{69066,69158,69247,69262}                  |{69247,69066,69158}                        |                4|                    3|
69067        |69067         |69067                                      |{69067}                                    |                0|                    1|
69070        |69070         |69070                                      |{69070}                                    |                0|                    1|
69075        |69075         |69075                                      |{69075}                                    |                0|                    1|
69078        |69078         |69078                                      |{69078}                                    |                0|                    1|
69093        |69093         |69093                                      |{69093}                                    |                0|                    1|
69095        |69095         |69095                                      |{69095}                                    |                0|                    1|
69098        |69098         |69098                                      |{69098}                                    |                0|                    1|
69099        |69099         |69099                                      |{69099}                                    |                0|                    1|
69102        |69102         |69102                                      |{69102}                                    |                0|                    1|
69104        |69104         |69104                                      |{69104}                                    |                0|                    1|
69107        |69107         |69107                                      |{69107}                                    |                0|                    1|
69110        |69110         |69110                                      |{69110}                                    |                0|                    1|
69113        |69113         |69113                                      |{69113}                                    |                0|                    1|
69119        |69119         |69119                                      |{69119}                                    |                0|                    1|
69120        |69120         |69120                                      |{69120}                                    |                0|                    1|
69124        |69124         |69124                                      |{69124}                                    |                0|                    1|
69130        |69130         |69130                                      |{69130}                                    |                0|                    1|
69132        |69132         |69132                                      |{69132}                                    |                0|                    1|
69135        |69135         |{69015,69135,69150,69185,69210,69224,69251}|{69150,69210,69251,69135,69224,69015,69185}|                7|                    7|
69138        |69138         |69138                                      |{69138}                                    |                0|                    1|
69139        |69139         |69139                                      |{69139}                                    |                0|                    1|
69154        |69154         |69154                                      |{69154}                                    |                0|                    1|
69155        |69155         |69155                                      |{69155}                                    |                0|                    1|
69157        |69157         |69157                                      |{69157,69223,69073}                        |                0|                    3|
69160        |69160         |69160                                      |{69160}                                    |                0|                    1|
69161        |69161         |69161                                      |{69161}                                    |                0|                    1|
69164        |69164         |69164                                      |{69164}                                    |                0|                    1|
69166        |69166         |69166                                      |{69166}                                    |                0|                    1|
69167        |69167         |69167                                      |{69167}                                    |                0|                    1|
69169        |69169         |69169                                      |{69169}                                    |                0|                    1|
69170        |69170         |69170                                      |{69170}                                    |                0|                    1|
69174        |69174         |69174                                      |{69174}                                    |                0|                    1|
69175        |69175         |69175                                      |{69175}                                    |                0|                    1|
69177        |69177         |69177                                      |{69177}                                    |                0|                    1|
69178        |69178         |69178                                      |{69178}                                    |                0|                    1|
69180        |69180         |69180                                      |{69180}                                    |                0|                    1|
69181        |69181         |69181                                      |{69181}                                    |                0|                    1|
69182        |69182         |69182                                      |{69182}                                    |                0|                    1|
69183        |69183         |69183                                      |{69183}                                    |                0|                    1|
69184        |69184         |69184                                      |{69184}                                    |                0|                    1|
69186        |69186         |69186                                      |{69186}                                    |                0|                    1|
69187        |69187         |69187                                      |{69187}                                    |                0|                    1|
69188        |69188         |69188                                      |{69188}                                    |                0|                    1|
69192        |69192         |69192                                      |{69192}                                    |                0|                    1|
69196        |69196         |69196                                      |{69196}                                    |                0|                    1|
69200        |69200         |69200                                      |{69200}                                    |                0|                    1|
69201        |69201         |69201                                      |{69201}                                    |                0|                    1|
69203        |69203         |69203                                      |{69203}                                    |                0|                    1|
69209        |69209         |69209                                      |{69209}                                    |                0|                    1|
69214        |69214         |69214                                      |{69214}                                    |                0|                    1|
69216        |69216         |69216                                      |{69216}                                    |                0|                    1|
69217        |69217         |69217                                      |{69217}                                    |                0|                    1|
69220        |69220         |69220                                      |{69220}                                    |                0|                    1|
69225        |69225         |69225                                      |{69225}                                    |                0|                    1|
69227        |69227         |69227                                      |{69227}                                    |                0|                    1|
69229        |69229         |69229                                      |{69229}                                    |                0|                    1|
69230        |69230         |69230                                      |{69230}                                    |                0|                    1|
69231        |69231         |69231                                      |{69231}                                    |                0|                    1|
69234        |69234         |69234                                      |{69234}                                    |                0|                    1|
69238        |69238         |69238                                      |{69238}                                    |                0|                    1|
69240        |69240         |69240                                      |{69240}                                    |                0|                    1|
69243        |69243         |69243                                      |{69243}                                    |                0|                    1|
69248        |69248         |{69041,69128,69129}                        |{69248}                                    |                3|                    1|
69249        |69249         |69249                                      |{69249}                                    |                0|                    1|
69254        |69254         |69254                                      |{69254}                                    |                0|                    1|
69255        |69255         |69255                                      |{69255,69221}                              |                0|                    2|
69258        |69258         |69258                                      |{69258}                                    |                0|                    1|
69261        |69261         |69261                                      |{69261}                                    |                0|                    1|
69263        |69263         |69263                                      |{69263}                                    |                0|                    1|
69267        |69267         |69267                                      |{69267}                                    |                0|                    1|
69269        |69269         |69269                                      |{69269}                                    |                0|                    1|
             |69228         |                                           |{69195,69237}                              |                 |                    2|
 */
---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069;
--> Updated Rows	95

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069)
order by date_eff, com_ap
/*
com_av|ncc_av|com_ap|ncc_ap            |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|------|------|------------------|----------|-----------------|-----------------------------|-------------------------------|
69147 |OLMES |69157 |VINDRY SUR TURDINE|2019-01-01|25/07/1985       |PONTCHARRA-SUR-TURDINE       |69157                          |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---069
ORDER BY insee_cog2019, insee_com;


---- 24/11/2020
----- RHONE ----
---- Apr�s Correction des erreurs

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069;
--> 113

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019|insee_commune_arrete_classement            |nom_commune_arrete_classement                                                        |arrete_classement                                                             |classement_commune                                              |commentaires                                                                           |nb_evenements_cog|
-------------|-------------------|-------------------------------------------|-------------------------------------------------------------------------------------|------------------------------------------------------------------------------|----------------------------------------------------------------|---------------------------------------------------------------------------------------|-----------------|
69066        |COURS              |{69066,69158,69247,69262}                  |{COURS,PONT-TRAMBOUZE,THEL,"LA VILLE"}                                               |{20/02/1974,20/02/1974,20/02/1974,20/02/1974}                                 |{totalit�,totalit�,totalit�,totalit�}                           |{"Cours-la-Ville dans fichier d�origine","","",""}                                     |                4|
69135        |DEUX GROSNES       |{69015,69135,69150,69185,69210,69224,69251}|{AVENAS,MONSOLS,OUROUX,SAINT-CHRISTOPHE,SAINT-JACQUES-DES-ARRETS,SAINT-MAMERT,TRADES}|{20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974}|{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|{"","","","","","",""}                                                                 |                7|
69248        |THIZY LES BOURGS   |{69041,69128,69129}                        |{"LA CHAPELLE-DE-MARDORE",MARDORE,MARNAND}                                           |{20/02/1974,20/02/1974,20/02/1974}                                            |{totalit�,totalit�,totalit�}                                    |{"Pas pr�sent dans fichier d�origine","Commune non pr�sente dans fichier d�origine",""}|                3|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 98 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	95

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '69' ---069
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement            |fred_anciens_codes                         |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------------------|-------------------------------------------|-----------------|---------------------|
69001        |69001         |69001                                      |{69001}                                    |                0|                    1|
69002        |69002         |69002                                      |{69002}                                    |                0|                    1|
69006        |69006         |69006                                      |{69006}                                    |                0|                    1|
69008        |69008         |69008                                      |{69008}                                    |                0|                    1|
69012        |69012         |69012                                      |{69012}                                    |                0|                    1|
69014        |69014         |69014                                      |{69014}                                    |                0|                    1|
69016        |69016         |69016                                      |{69016}                                    |                0|                    1|
69018        |69018         |69018                                      |{69018}                                    |                0|                    1|
69021        |69021         |69021                                      |{69021}                                    |                0|                    1|
69022        |69022         |69022                                      |{69022}                                    |                0|                    1|
69030        |69030         |69030                                      |{69030}                                    |                0|                    1|
69031        |69031         |69031                                      |{69031}                                    |                0|                    1|
69035        |69035         |69035                                      |{69035}                                    |                0|                    1|
69037        |69037         |69037                                      |{69037}                                    |                0|                    1|
69038        |69038         |69038                                      |{69038}                                    |                0|                    1|
69039        |69039         |69039                                      |{69039}                                    |                0|                    1|
69042        |69042         |69042                                      |{69042}                                    |                0|                    1|
69051        |69051         |69051                                      |{69051}                                    |                0|                    1|
69054        |69054         |69054                                      |{69054}                                    |                0|                    1|
69057        |69057         |69057                                      |{69057}                                    |                0|                    1|
69058        |69058         |69058                                      |{69058}                                    |                0|                    1|
69060        |69060         |69060                                      |{69060}                                    |                0|                    1|
69062        |69062         |69062                                      |{69062}                                    |                0|                    1|
69066        |69066         |{69066,69158,69247,69262}                  |{69247,69066,69158}                        |                4|                    3|
69067        |69067         |69067                                      |{69067}                                    |                0|                    1|
69070        |69070         |69070                                      |{69070}                                    |                0|                    1|
69075        |69075         |69075                                      |{69075}                                    |                0|                    1|
69078        |69078         |69078                                      |{69078}                                    |                0|                    1|
69093        |69093         |69093                                      |{69093}                                    |                0|                    1|
69095        |69095         |69095                                      |{69095}                                    |                0|                    1|
69098        |69098         |69098                                      |{69098}                                    |                0|                    1|
69099        |69099         |69099                                      |{69099}                                    |                0|                    1|
69102        |69102         |69102                                      |{69102}                                    |                0|                    1|
69104        |69104         |69104                                      |{69104}                                    |                0|                    1|
69107        |69107         |69107                                      |{69107}                                    |                0|                    1|
69110        |69110         |69110                                      |{69110}                                    |                0|                    1|
69113        |69113         |69113                                      |{69113}                                    |                0|                    1|
69119        |69119         |69119                                      |{69119}                                    |                0|                    1|
69120        |69120         |69120                                      |{69120}                                    |                0|                    1|
69124        |69124         |69124                                      |{69124}                                    |                0|                    1|
69130        |69130         |69130                                      |{69130}                                    |                0|                    1|
69132        |69132         |69132                                      |{69132}                                    |                0|                    1|
69135        |69135         |{69015,69135,69150,69185,69210,69224,69251}|{69150,69210,69251,69135,69224,69015,69185}|                7|                    7|
69138        |69138         |69138                                      |{69138}                                    |                0|                    1|
69139        |69139         |69139                                      |{69139}                                    |                0|                    1|
69154        |69154         |69154                                      |{69154}                                    |                0|                    1|
69155        |69155         |69155                                      |{69155}                                    |                0|                    1|
69157        |69157         |69157                                      |{69157,69223,69073}                        |                0|                    3|
69160        |69160         |69160                                      |{69160}                                    |                0|                    1|
69161        |69161         |69161                                      |{69161}                                    |                0|                    1|
69164        |69164         |69164                                      |{69164}                                    |                0|                    1|
69166        |69166         |69166                                      |{69166}                                    |                0|                    1|
69167        |69167         |69167                                      |{69167}                                    |                0|                    1|
69169        |69169         |69169                                      |{69169}                                    |                0|                    1|
69170        |69170         |69170                                      |{69170}                                    |                0|                    1|
69174        |69174         |69174                                      |{69174}                                    |                0|                    1|
69175        |69175         |69175                                      |{69175}                                    |                0|                    1|
69177        |69177         |69177                                      |{69177}                                    |                0|                    1|
69178        |69178         |69178                                      |{69178}                                    |                0|                    1|
69180        |69180         |69180                                      |{69180}                                    |                0|                    1|
69181        |69181         |69181                                      |{69181}                                    |                0|                    1|
69182        |69182         |69182                                      |{69182}                                    |                0|                    1|
69183        |69183         |69183                                      |{69183}                                    |                0|                    1|
69184        |69184         |69184                                      |{69184}                                    |                0|                    1|
69186        |69186         |69186                                      |{69186}                                    |                0|                    1|
69187        |69187         |69187                                      |{69187}                                    |                0|                    1|
69188        |69188         |69188                                      |{69188}                                    |                0|                    1|
69192        |69192         |69192                                      |{69192}                                    |                0|                    1|
69196        |69196         |69196                                      |{69196}                                    |                0|                    1|
69200        |69200         |69200                                      |{69200}                                    |                0|                    1|
69201        |69201         |69201                                      |{69201}                                    |                0|                    1|
69203        |69203         |69203                                      |{69203}                                    |                0|                    1|
69209        |69209         |69209                                      |{69209}                                    |                0|                    1|
69214        |69214         |69214                                      |{69214}                                    |                0|                    1|
69216        |69216         |69216                                      |{69216}                                    |                0|                    1|
69217        |69217         |69217                                      |{69217}                                    |                0|                    1|
69220        |69220         |69220                                      |{69220}                                    |                0|                    1|
69225        |69225         |69225                                      |{69225}                                    |                0|                    1|
69227        |69227         |69227                                      |{69227}                                    |                0|                    1|
69229        |69229         |69229                                      |{69229}                                    |                0|                    1|
69230        |69230         |69230                                      |{69230}                                    |                0|                    1|
69231        |69231         |69231                                      |{69231}                                    |                0|                    1|
69234        |69234         |69234                                      |{69234}                                    |                0|                    1|
69238        |69238         |69238                                      |{69238}                                    |                0|                    1|
69240        |69240         |69240                                      |{69240}                                    |                0|                    1|
69243        |69243         |69243                                      |{69243}                                    |                0|                    1|
69248        |69248         |{69041,69128,69129}                        |{69248}                                    |                3|                    1|
69249        |69249         |69249                                      |{69249}                                    |                0|                    1|
69254        |69254         |69254                                      |{69254}                                    |                0|                    1|
69255        |69255         |69255                                      |{69255,69221}                              |                0|                    2|
69258        |69258         |69258                                      |{69258}                                    |                0|                    1|
69261        |69261         |69261                                      |{69261}                                    |                0|                    1|
69263        |69263         |69263                                      |{69263}                                    |                0|                    1|
69267        |69267         |69267                                      |{69267}                                    |                0|                    1|
69269        |69269         |69269                                      |{69269}                                    |                0|                    1|
             |69228         |                                           |{69195,69237}                              |                 |                    2|
             */
---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069;
--> Updated Rows	95

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069)
order by date_eff, com_ap
/*
com_av|ncc_av|com_ap|ncc_ap            |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|------|------|------------------|----------|-----------------|-----------------------------|-------------------------------|
69147 |OLMES |69157 |VINDRY SUR TURDINE|2019-01-01|25/07/1985       |PONTCHARRA-SUR-TURDINE       |69157                          |
 */

---- MAJ de la base 2019 : 25/11/2020


------------------------------------------------------------------------------------------------------------------------------------------------
---- 25/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                                   |count|
----------------------------------------------------------------|-----|
{non class�e,P,P}�                                              |    1|
P                                                               |   16|
{P,P,non class�e,P}                                             |    1|
{P,totalit�}                                                    |    1|
totalit�                                                        |   74|
{totalit�,totalit�,totalit�,totalit�}                           |    1|
{totalit�,totalit�,totalit�,totalit�,totalit�}�                 |    1|
{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_069_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_069_2019 as
SELECT 
'069'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'                           
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'              
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'
	
	WHEN t1.classement_85 = '{non class�e,P,P}' THEN 'partie'
	WHEN t1.classement_85 = 'P' THEN 'partie'                  
	WHEN t1.classement_85 = '{P,P,non class�e,P}' THEN 'partie'
	WHEN t1.classement_85 = '{P,totalit�}' THEN 'partie'       
	/*
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   */
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_069_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 96

select classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_069_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   77|
partie         |   19|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_069_2019 FOR VALUES IN ('069');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 RENAME TO l_liste_commune_montagne_1985_069_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |   22|
totalit�|   91|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 AS
SELECT
'069'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069_old
ORDER BY insee_classement;
--> 113

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   91|
partie         |   22|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_069 FOR VALUES IN ('069');
--> Updated Rows	0





















