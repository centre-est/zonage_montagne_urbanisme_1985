---- 19/08/2020
----- HAUTES-LOIRE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043  WHERE date_decis IS NULL;

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	0
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	249

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043;
--> 249

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019  |insee_commune_arrete_classement|nom_commune_arrete_classement                  |arrete_classement      |classement_commune |commentaires                                                                                        |nb_evenements_cog|
-------------|---------------------|-------------------------------|-----------------------------------------------|-----------------------|-------------------|----------------------------------------------------------------------------------------------------|-----------------|
43013        |VISSAC AUTEYRAC      |{43013}                        |{VISSAC-AUTEYRAC}                              |{1974-02-20}           |{totalit�}         |{NULL}                                                                                              |                1|
43090        |ESPLANTAS VAZEILLES  |{43090,43255}                  |{ESPLANTAS,VAZEILLES-PRES-SAUGUES}             |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                         |                2|
43131        |MAZERAT AUROUZE      |{43131}                        |{MAZERAT-AUROUZE}                              |{1974-02-20}           |{totalit�}         |{NULL}                                                                                              |                1|
43132        |MAZEYRAT D ALLIER    |{43132}                        |{MAZEYRAT-D'ALLIER}                            |{1974-02-20}           |{totalit�}         |{NULL}                                                                                              |                1|
43157        |PUY EN VELAY         |{43157}                        |{"LE PUY"}                                     |{1974-02-20}           |{totalit�}         |{"Ecrit le LE PUY-EN-VELAY dans fichier d�origine"}                                                 |                1|
43221        |SAINT PRIVAT D ALLIER|{43176,43221}                  |{SAINT-DIDIER-D'ALLIER,SAINT-PRIVAT-D'ALLIER}  |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                         |                2|
43239        |SIAUGUES SAINTE MARIE|{43209,43239}                  |{SAINTE-MARIE-DES-CHAZES,SIAUGUES-SAINT-ROMAIN}|{1974-02-20,1974-02-20}|{totalit�,totalit�}|{"commune non pr�sente dans fichier d�origine","Ecrit SIAUGUES-SAINTE-MARIE dans fichier d�origine"}|                2|
43245        |THORAS               |{43081,43245}                  |{CROISANCES,THORAS}                            |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                                                         |                2|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 237 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	245

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '43' ---043
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
43001        |43001         |43001                          |{43001}           |                0|                    1|
43002        |43002         |43002                          |{43002}           |                0|                    1|
43003        |43003         |43003                          |{43003}           |                0|                    1|
43004        |43004         |43004                          |{43004}           |                0|                    1|
43005        |43005         |43005                          |{43005}           |                0|                    1|
43006        |43006         |43006                          |{43006}           |                0|                    1|
43007        |43007         |43007                          |{43007}           |                0|                    1|
43008        |43008         |43008                          |{43008}           |                0|                    1|
43009        |43009         |43009                          |{43009}           |                0|                    1|
43010        |43010         |43010                          |{43010}           |                0|                    1|
43011        |43011         |43011                          |{43011}           |                0|                    1|
43012        |43012         |43012                          |{43012}           |                0|                    1|
43013        |43013         |{43013}                        |{43013}           |                1|                    1|
43014        |43014         |43014                          |{43014}           |                0|                    1|
43015        |43015         |43015                          |{43015}           |                0|                    1|
43016        |43016         |43016                          |{43016}           |                0|                    1|
43018        |43018         |43018                          |{43018}           |                0|                    1|
43019        |43019         |43019                          |{43019}           |                0|                    1|
43020        |43020         |43020                          |{43020}           |                0|                    1|
43021        |43021         |43021                          |{43021}           |                0|                    1|
43023        |43023         |43023                          |{43023}           |                0|                    1|
43024        |43024         |43024                          |{43024}           |                0|                    1|
43025        |43025         |43025                          |{43025}           |                0|                    1|
43026        |43026         |43026                          |{43026}           |                0|                    1|
43027        |43027         |43027                          |{43027}           |                0|                    1|
43028        |43028         |43028                          |{43028}           |                0|                    1|
43029        |43029         |43029                          |{43029}           |                0|                    1|
43030        |43030         |43030                          |{43030}           |                0|                    1|
43031        |43031         |43031                          |{43031}           |                0|                    1|
43032        |43032         |43032                          |{43032}           |                0|                    1|
43033        |43033         |43033                          |{43033}           |                0|                    1|
43034        |43034         |43034                          |{43034}           |                0|                    1|
43035        |43035         |43035                          |{43035}           |                0|                    1|
43036        |43036         |43036                          |{43036}           |                0|                    1|
43037        |43037         |43037                          |{43037}           |                0|                    1|
43039        |43039         |43039                          |{43039}           |                0|                    1|
43041        |43041         |43041                          |{43041}           |                0|                    1|
43042        |43042         |43042                          |{43042}           |                0|                    1|
43043        |43043         |43043                          |{43043}           |                0|                    1|
43044        |43044         |43044                          |{43044}           |                0|                    1|
43045        |43045         |43045                          |{43045}           |                0|                    1|
43046        |43046         |43046                          |{43046}           |                0|                    1|
43047        |43047         |43047                          |{43047}           |                0|                    1|
43048        |43048         |43048                          |{43048}           |                0|                    1|
43049        |43049         |43049                          |{43049}           |                0|                    1|
43050        |43050         |43050                          |{43050}           |                0|                    1|
43051        |43051         |43051                          |{43051}           |                0|                    1|
43052        |43052         |43052                          |{43052}           |                0|                    1|
43053        |43053         |43053                          |{43053}           |                0|                    1|
43054        |43054         |43054                          |{43054}           |                0|                    1|
43055        |43055         |43055                          |{43055}           |                0|                    1|
43056        |43056         |43056                          |{43056}           |                0|                    1|
43057        |43057         |43057                          |{43057}           |                0|                    1|
43058        |43058         |43058                          |{43058}           |                0|                    1|
43059        |43059         |43059                          |{43059}           |                0|                    1|
43060        |43060         |43060                          |{43060}           |                0|                    1|
43061        |43061         |43061                          |{43061}           |                0|                    1|
43062        |43062         |43062                          |{43062}           |                0|                    1|
43063        |43063         |43063                          |{43063}           |                0|                    1|
43064        |43064         |43064                          |{43064}           |                0|                    1|
43065        |43065         |43065                          |{43065}           |                0|                    1|
43066        |43066         |43066                          |{43066}           |                0|                    1|
43067        |43067         |43067                          |{43067}           |                0|                    1|
43068        |43068         |43068                          |{43068}           |                0|                    1|
43069        |43069         |43069                          |{43069}           |                0|                    1|
43070        |43070         |43070                          |{43070}           |                0|                    1|
43071        |43071         |43071                          |{43071}           |                0|                    1|
43072        |43072         |43072                          |{43072}           |                0|                    1|
43073        |43073         |43073                          |{43073}           |                0|                    1|
43075        |43075         |43075                          |{43075}           |                0|                    1|
43076        |43076         |43076                          |{43076}           |                0|                    1|
43077        |43077         |43077                          |{43077}           |                0|                    1|
43078        |43078         |43078                          |{43078}           |                0|                    1|
43079        |43079         |43079                          |{43079}           |                0|                    1|
43080        |43080         |43080                          |{43080}           |                0|                    1|
43082        |43082         |43082                          |{43082}           |                0|                    1|
43083        |43083         |43083                          |{43083}           |                0|                    1|
43084        |43084         |43084                          |{43084}           |                0|                    1|
43085        |43085         |43085                          |{43085}           |                0|                    1|
43086        |43086         |43086                          |{43086}           |                0|                    1|
43087        |43087         |43087                          |{43087}           |                0|                    1|
43088        |43088         |43088                          |{43088}           |                0|                    1|
43089        |43089         |43089                          |{43089}           |                0|                    1|
43090        |43090         |{43090,43255}                  |{43090,43255}     |                2|                    2|
43091        |43091         |43091                          |{43091}           |                0|                    1|
43092        |43092         |43092                          |{43092}           |                0|                    1|
43093        |43093         |43093                          |{43093}           |                0|                    1|
43094        |43094         |43094                          |{43094}           |                0|                    1|
43095        |43095         |43095                          |{43095}           |                0|                    1|
43097        |43097         |43097                          |{43097}           |                0|                    1|
43098        |43098         |43098                          |{43098}           |                0|                    1|
43100        |43100         |43100                          |{43100}           |                0|                    1|
43101        |43101         |43101                          |{43101}           |                0|                    1|
43102        |43102         |43102                          |{43102}           |                0|                    1|
43103        |43103         |43103                          |{43103}           |                0|                    1|
43104        |43104         |43104                          |{43104}           |                0|                    1|
43105        |43105         |43105                          |{43105}           |                0|                    1|
43106        |43106         |43106                          |{43106}           |                0|                    1|
43107        |43107         |43107                          |{43107}           |                0|                    1|
43108        |43108         |43108                          |{43108}           |                0|                    1|
43109        |43109         |43109                          |{43109}           |                0|                    1|
43111        |43111         |43111                          |{43111}           |                0|                    1|
43112        |43112         |43112                          |{43112}           |                0|                    1|
43113        |43113         |43113                          |{43113}           |                0|                    1|
43114        |43114         |43114                          |{43114}           |                0|                    1|
43115        |43115         |43115                          |{43115}           |                0|                    1|
43116        |43116         |43116                          |{43116}           |                0|                    1|
43117        |43117         |43117                          |{43117}           |                0|                    1|
43118        |43118         |43118                          |{43118}           |                0|                    1|
43119        |43119         |43119                          |{43119}           |                0|                    1|
43121        |43121         |43121                          |{43121}           |                0|                    1|
43122        |43122         |43122                          |{43122}           |                0|                    1|
43123        |43123         |43123                          |{43123}           |                0|                    1|
43124        |43124         |43124                          |{43124}           |                0|                    1|
43125        |43125         |43125                          |{43125}           |                0|                    1|
43126        |43126         |43126                          |{43126}           |                0|                    1|
43127        |43127         |43127                          |{43127}           |                0|                    1|
43128        |43128         |43128                          |{43128}           |                0|                    1|
43129        |43129         |43129                          |{43129}           |                0|                    1|
43130        |43130         |43130                          |{43130}           |                0|                    1|
43131        |43131         |{43131}                        |{43131}           |                1|                    1|
43132        |43132         |{43132}                        |{43132}           |                1|                    1|
43133        |43133         |43133                          |{43133}           |                0|                    1|
43134        |43134         |43134                          |{43134}           |                0|                    1|
43135        |43135         |43135                          |{43135}           |                0|                    1|
43136        |43136         |43136                          |{43136}           |                0|                    1|
43137        |43137         |43137                          |{43137}           |                0|                    1|
43138        |43138         |43138                          |{43138}           |                0|                    1|
43139        |43139         |43139                          |{43139}           |                0|                    1|
43140        |43140         |43140                          |{43140}           |                0|                    1|
43141        |43141         |43141                          |{43141}           |                0|                    1|
43142        |43142         |43142                          |{43142}           |                0|                    1|
43143        |43143         |43143                          |{43143}           |                0|                    1|
43144        |43144         |43144                          |{43144}           |                0|                    1|
43145        |43145         |43145                          |{43145}           |                0|                    1|
43148        |43148         |43148                          |{43148}           |                0|                    1|
43149        |43149         |43149                          |{43149}           |                0|                    1|
43150        |43150         |43150                          |{43150}           |                0|                    1|
43151        |43151         |43151                          |{43151}           |                0|                    1|
43152        |43152         |43152                          |{43152}           |                0|                    1|
43153        |43153         |43153                          |{43153}           |                0|                    1|
43154        |43154         |43154                          |{43154}           |                0|                    1|
43155        |43155         |43155                          |{43155}           |                0|                    1|
43156        |43156         |43156                          |{43156}           |                0|                    1|
43157        |43157         |{43157}                        |{43157}           |                1|                    1|
43158        |43158         |43158                          |{43158}           |                0|                    1|
43159        |43159         |43159                          |{43159}           |                0|                    1|
43160        |43160         |43160                          |{43160}           |                0|                    1|
43162        |43162         |43162                          |{43162}           |                0|                    1|
43163        |43163         |43163                          |{43163}           |                0|                    1|
43164        |43164         |43164                          |{43164}           |                0|                    1|
43165        |43165         |43165                          |{43165}           |                0|                    1|
43166        |43166         |43166                          |{43166}           |                0|                    1|
43167        |43167         |43167                          |{43167}           |                0|                    1|
43168        |43168         |43168                          |{43168}           |                0|                    1|
43169        |43169         |43169                          |{43169}           |                0|                    1|
43170        |43170         |43170                          |{43170}           |                0|                    1|
43171        |43171         |43171                          |{43171}           |                0|                    1|
43172        |43172         |43172                          |{43172}           |                0|                    1|
43173        |43173         |43173                          |{43173}           |                0|                    1|
43174        |43174         |43174                          |{43174}           |                0|                    1|
43175        |43175         |43175                          |{43175}           |                0|                    1|
43177        |43177         |43177                          |{43177}           |                0|                    1|
43178        |43178         |43178                          |{43178}           |                0|                    1|
43180        |43180         |43180                          |{43180}           |                0|                    1|
43181        |43181         |43181                          |{43181}           |                0|                    1|
43182        |43182         |43182                          |{43182}           |                0|                    1|
43183        |43183         |43183                          |{43183}           |                0|                    1|
43184        |43184         |43184                          |{43184}           |                0|                    1|
43186        |43186         |43186                          |{43186}           |                0|                    1|
43187        |43187         |43187                          |{43187}           |                0|                    1|
43188        |43188         |43188                          |{43188}           |                0|                    1|
43189        |43189         |43189                          |{43189}           |                0|                    1|
43190        |43190         |43190                          |{43190}           |                0|                    1|
43191        |43191         |43191                          |{43191}           |                0|                    1|
43192        |43192         |43192                          |{43192}           |                0|                    1|
43193        |43193         |43193                          |{43193}           |                0|                    1|
43194        |43194         |43194                          |{43194}           |                0|                    1|
43195        |43195         |43195                          |{43195}           |                0|                    1|
43196        |43196         |43196                          |{43196}           |                0|                    1|
43197        |43197         |43197                          |{43197}           |                0|                    1|
43198        |43198         |43198                          |{43198}           |                0|                    1|
43199        |43199         |43199                          |{43199}           |                0|                    1|
43200        |43200         |43200                          |{43200}           |                0|                    1|
43201        |43201         |43201                          |{43201}           |                0|                    1|
43202        |43202         |43202                          |{43202}           |                0|                    1|
43203        |43203         |43203                          |{43203}           |                0|                    1|
43204        |43204         |43204                          |{43204}           |                0|                    1|
43205        |43205         |43205                          |{43205}           |                0|                    1|
43206        |43206         |43206                          |{43206}           |                0|                    1|
43207        |43207         |43207                          |{43207}           |                0|                    1|
43208        |43208         |43208                          |{43208}           |                0|                    1|
43210        |43210         |43210                          |{43210}           |                0|                    1|
43211        |43211         |43211                          |{43211}           |                0|                    1|
43212        |43212         |43212                          |{43212}           |                0|                    1|
43213        |43213         |43213                          |{43213}           |                0|                    1|
43214        |43214         |43214                          |{43214}           |                0|                    1|
43215        |43215         |43215                          |{43215}           |                0|                    1|
43216        |43216         |43216                          |{43216}           |                0|                    1|
43217        |43217         |43217                          |{43217}           |                0|                    1|
43218        |43218         |43218                          |{43218}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043;
--> Updated Rows	245

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043)
order by date_eff, com_ap
/*
com_av|ncc_av             |com_ap|ncc_ap           |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|-------------------|------|-----------------|----------|-----------------|-----------------------------|-------------------------------|
43243 |TAULHAC PRES LE PUY|43157 |PUY              |1965-02-20|{1974-02-20}     |{"LE PUY"}                   |{43157}                        |
43146 |OURS MONS          |43157 |PUY              |1965-02-25|{1974-02-20}     |{"LE PUY"}                   |{43157}                        |
43248 |VABRES             |43005 |ALLEYRAS         |1966-01-01|1974-02-20       |ALLEYRAS                     |43005                          |
43235 |SAUVETAT           |43111 |LANDOS           |1966-01-01|1974-02-20       |LANDOS                       |43111                          |
43266 |VISSAC             |43013 |VISSAC AUTEYRAC  |1972-09-01|{1974-02-20}     |{VISSAC-AUTEYRAC}            |{43013}                        |
43179 |SAINT EBLE         |43132 |MAZEYRAT D ALLIER|1973-01-01|{1974-02-20}     |{MAZEYRAT-D'ALLIER}          |{43132}                        |
43161 |REILHAC            |43132 |MAZEYRAT D ALLIER|1973-01-01|{1974-02-20}     |{MAZEYRAT-D'ALLIER}          |{43132}                        |
43266 |VISSAC             |43013 |VISSAC AUTEYRAC  |1983-01-01|{1974-02-20}     |{VISSAC-AUTEYRAC}            |{43013}                        |
43179 |SAINT EBLE         |43132 |MAZEYRAT D ALLIER|1990-07-01|{1974-02-20}     |{MAZEYRAT-D'ALLIER}          |{43132}                        |
43161 |REILHAC            |43132 |MAZEYRAT D ALLIER|1990-07-01|{1974-02-20}     |{MAZEYRAT-D'ALLIER}          |{43132}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---043
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- En attente d'un avis de la DDT
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN

departement := '043';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019 as
	SELECT 
	''' || departement || '''::varchar(3) AS code_dep,
	t1.insee_cog2019::varchar(5) AS insee_2019,
	t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
	t2.nom_com::varchar AS nom_min_2019,
	t1.nb_evenements_cog::integer,
	t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
	''En expertise''::varchar AS agreg_nom_classement , 
	''En expertise''::varchar AS agreg_arretes,
	''En expertise''::varchar AS agreg_classement_85,
	''Expertis''::varchar(8) AS classement_2019,
	commentaires::TEXT AS agreg_commentaires,
	''n_adm_exp_cog_commune_000_2019''::varchar(254) AS source_geom,
	t2.geom::geometry(''MULTIPOLYGON'',2154)
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_' || departement || '_final AS t1
	JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
	ON t1.insee_cog2019 = t2.insee_com
	ORDER BY insee_2019;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- B] Cr�ation de la table des communes de 1985
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp AS
	SELECT
	''' || departement || '''::varchar(3) AS code_dep,
	insee_comm::varchar(5) AS insee_classement,
	nom_commun::varchar(100) AS nom_commune,
	date_decis::varchar AS arrete_classement,
	''Expertis''::varchar(8) AS classement_1985,
	commentair::TEXT AS commentaires
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '
	ORDER BY insee_classement;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- Une fois avis DDT obtenu : 23/04/2021
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN
departement := '043';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- Une fois avis DDT obtenu : 23/04/2021 -- pas d'avis mais c'est bon
------------------------------------------------------------------------------------------------------------------------------------------------
---- 23/04/2021
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------+-----+
totalit�           |  237|
{totalit�}         |    4|
{totalit�,totalit�}|    4|
*/

---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_043_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_043_2019 as
SELECT 
'043'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'P' THEN 'partie'                  
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_043_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 245

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_043_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------+-----+
totalit�       |  245|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_043_2019 FOR VALUES IN ('043');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 RENAME TO l_liste_commune_montagne_1985_043_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------+-----+
totalit�|  249|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 AS
SELECT
'043'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'
	WHEN partie = 'p' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043_old
ORDER BY insee_classement;
--> 249

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 GROUP BY classement_1985;
/*
classement_1985|count|
---------------+-----+
totalit�       |  249|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_043 FOR VALUES IN ('043');
--> Updated Rows	0