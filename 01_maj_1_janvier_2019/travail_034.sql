---- 31/07/2020
----- 034 ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	87

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034;
--> 88

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019    |insee_commune_arrete_classement|nom_commune_arrete_classement|arrete_classement|classement_commune|commentaires                                                |nb_evenements_cog|
-------------|-----------------------|-------------------------------|-----------------------------|-----------------|------------------|------------------------------------------------------------|-----------------|
34284        |SAINT PONS DE THOMIERES|{34284}                        |{SAINT-PONS}                 |{1974-02-20}     |{totalit�}        |{"15/06/1979 : Saint-Pons devient Saint-Pons-de-Thomi�res."}|                1|
34335        |VILLEMAGNE L ARGENTIERE|{34335}                        |{VILLEMAGNE-L'ARGENTIERE}    |{1976-04-28}     |{totalit�}        |{"Ecrit seulement VILLEMAGNE dans l�arr�t� de 1976"}        |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 86 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	88

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '34' ---034
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
34005        |34005         |34005                          |{34005}           |                0|                    1|
34008        |34008         |34008                          |{34008}           |                0|                    1|
34019        |34019         |34019                          |{34019}           |                0|                    1|
34028        |34028         |34028                          |{34028}           |                0|                    1|
34034        |34034         |34034                          |{34034}           |                0|                    1|
34038        |34038         |34038                          |{34038}           |                0|                    1|
34040        |34040         |34040                          |{34040}           |                0|                    1|
34042        |34042         |34042                          |{34042}           |                0|                    1|
34046        |34046         |34046                          |{34046}           |                0|                    1|
34049        |34049         |34049                          |{34049}           |                0|                    1|
34053        |34053         |34053                          |{34053}           |                0|                    1|
34054        |34054         |34054                          |{34054}           |                0|                    1|
34055        |34055         |34055                          |{34055}           |                0|                    1|
34060        |34060         |34060                          |{34060}           |                0|                    1|
34064        |34064         |34064                          |{34064}           |                0|                    1|
34067        |34067         |34067                          |{34067}           |                0|                    1|
34071        |34071         |34071                          |{34071}           |                0|                    1|
34080        |34080         |34080                          |{34080}           |                0|                    1|
34083        |34083         |34083                          |{34083}           |                0|                    1|
34086        |34086         |34086                          |{34086}           |                0|                    1|
34091        |34091         |34091                          |{34091}           |                0|                    1|
34093        |34093         |34093                          |{34093}           |                0|                    1|
34098        |34098         |34098                          |{34098}           |                0|                    1|
34100        |34100         |34100                          |{34100}           |                0|                    1|
34106        |34106         |34106                          |{34106}           |                0|                    1|
34107        |34107         |34107                          |{34107}           |                0|                    1|
34111        |34111         |34111                          |{34111}           |                0|                    1|
34115        |34115         |34115                          |{34115}           |                0|                    1|
34117        |34117         |34117                          |{34117}           |                0|                    1|
34119        |34119         |34119                          |{34119}           |                0|                    1|
34121        |34121         |34121                          |{34121}           |                0|                    1|
34126        |34126         |34126                          |{34126}           |                0|                    1|
34128        |34128         |34128                          |{34128}           |                0|                    1|
34132        |34132         |34132                          |{34132}           |                0|                    1|
34133        |34133         |34133                          |{34133}           |                0|                    1|
34142        |34142         |34142                          |{34142}           |                0|                    1|
34144        |34144         |34144                          |{34144}           |                0|                    1|
34160        |34160         |34160                          |{34160}           |                0|                    1|
34171        |34171         |34171                          |{34171}           |                0|                    1|
34174        |34174         |34174                          |{34174}           |                0|                    1|
34186        |34186         |34186                          |{34186}           |                0|                    1|
34187        |34187         |34187                          |{34187}           |                0|                    1|
34188        |34188         |34188                          |{34188}           |                0|                    1|
34193        |34193         |34193                          |{34193}           |                0|                    1|
34195        |34195         |34195                          |{34195}           |                0|                    1|
34196        |34196         |34196                          |{34196}           |                0|                    1|
34205        |34205         |34205                          |{34205}           |                0|                    1|
34211        |34211         |34211                          |{34211}           |                0|                    1|
34212        |34212         |34212                          |{34212}           |                0|                    1|
34216        |34216         |34216                          |{34216}           |                0|                    1|
34219        |34219         |34219                          |{34219}           |                0|                    1|
34228        |34228         |34228                          |{34228}           |                0|                    1|
34229        |34229         |34229                          |{34229}           |                0|                    1|
34230        |34230         |34230                          |{34230}           |                0|                    1|
34231        |34231         |34231                          |{34231}           |                0|                    1|
34233        |34233         |34233                          |{34233}           |                0|                    1|
34235        |34235         |34235                          |{34235}           |                0|                    1|
34238        |34238         |34238                          |{34238}           |                0|                    1|
34243        |34243         |34243                          |{34243}           |                0|                    1|
34250        |34250         |34250                          |{34250}           |                0|                    1|
34251        |34251         |34251                          |{34251}           |                0|                    1|
34252        |34252         |34252                          |{34252}           |                0|                    1|
34253        |34253         |34253                          |{34253}           |                0|                    1|
34257        |34257         |34257                          |{34257}           |                0|                    1|
34260        |34260         |34260                          |{34260}           |                0|                    1|
34261        |34261         |34261                          |{34261}           |                0|                    1|
34264        |34264         |34264                          |{34264}           |                0|                    1|
34271        |34271         |34271                          |{34271}           |                0|                    1|
34273        |34273         |34273                          |{34273}           |                0|                    1|
34277        |34277         |34277                          |{34277}           |                0|                    1|
34278        |34278         |34278                          |{34278}           |                0|                    1|
34283        |34283         |34283                          |{34283}           |                0|                    1|
34284        |34284         |{34284}                        |{34284}           |                1|                    1|
34286        |34286         |34286                          |{34286}           |                0|                    1|
34291        |34291         |34291                          |{34291}           |                0|                    1|
34293        |34293         |34293                          |{34293}           |                0|                    1|
34303        |34303         |34303                          |{34303}           |                0|                    1|
34304        |34304         |34304                          |{34304}           |                0|                    1|
34305        |34305         |34305                          |{34305}           |                0|                    1|
34306        |34306         |34306                          |{34306}           |                0|                    1|
34308        |34308         |34308                          |{34308}           |                0|                    1|
34312        |34312         |34312                          |{34312}           |                0|                    1|
34316        |34316         |34316                          |{34316}           |                0|                    1|
34317        |34317         |34317                          |{34317}           |                0|                    1|
34326        |34326         |34326                          |{34326}           |                0|                    1|
34331        |34331         |34331                          |{34331}           |                0|                    1|
34334        |34334         |34334                          |{34334}           |                0|                    1|
34335        |34335         |{34335}                        |{34335}           |                1|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034;
--> Updated Rows	22

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034)
order by date_eff, com_ap
/*
com_av|ncc_av                 |com_ap|ncc_ap|date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|-----------------------|------|------|----------|-----------------|-----------------------------|-------------------------------|
34275 |SAINT MARTIN DES COMBES|34186 |OCTON |1965-01-01|1976-04-28       |OCTON                        |34186                          |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---034
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 22/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85|count|
-------------|-----|
P            |    1|
totalit�     |   85|
{totalit�}   |    2|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_034_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_034_2019 as
SELECT 
'034'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'	
	WHEN t1.classement_85 = 'P' THEN 'partie'   
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_034_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 88

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_034_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   87|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_034_2019 FOR VALUES IN ('034');
--> Updated Rows	0

---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 RENAME TO l_liste_commune_montagne_1985_034_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |    1|
totalit�|   87|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 AS
SELECT
'034'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034_old
ORDER BY insee_classement;
--> 88

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   87|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_034 FOR VALUES IN ('034');
--> Updated Rows	0
