---- 08/07/2020
----- HAUTES-ALPES ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	176

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005;
--> 178

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019      |insee_commune_arrete_classement|nom_commune_arrete_classement                                     |arrete_classement                            |classement_commune                   |commentaires                                                                                           |nb_evenements_cog|
-------------|-------------------------|-------------------------------|------------------------------------------------------------------|---------------------------------------------|-------------------------------------|-------------------------------------------------------------------------------------------------------|-----------------|
05001        |ABRIES RISTOLAS          |{05001,05120}                  |{ABRIES,RISTOLAS}                                                 |{1974-02-20,1974-02-20}                      |{totalit�,totalit�}                  |{NULL,NULL}                                                                                            |                2|
05014        |BARRET SUR MEOUGE        |{05014}                        |{BARRET-LE-BAS}                                                   |{1974-02-20}                                 |{totalit�}                           |{NULL}                                                                                                 |                1|
05023        |BRIANCON                 |{05023,05109}                  |{BRIANCON,PUY-SAINT-PIERRE}                                       |{1974-02-20,1974-02-20}                      |{P�:�????,totalit�}                  |{"1974�: Biranson Nord + 1974 Briancon Sud = tout briancon�?",NULL}                                    |                2|
05024        |VALDOULE                 |{05024,05088,05150}            |{BRUIS,MONTMORIN,SAINTE-MARIE}                                    |{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{NULL,NULL,NULL}                                                                                       |                3|
05036        |CHATEAUROUX LES ALPES    |{05036}                        |{CHATEAUROUX(-LES-ALPES)}                                         |{1974-02-20}                                 |{totalit�}                           |{"Seulement Chateauroux dans arr�t� de 1974"}                                                          |                1|
05039        |AUBESSAGNE               |{05039,05043,05141}            |{CHAUFFAYER,"LES COSTES",SAINT-EUSEBE-EN-CHAMPSAUR}               |{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{NULL,NULL,"Arr�t� de 1974�: juste SAINT-EUSEBE"}                                                      |                3|
05053        |GARDE COLOMBE            |{05053,05069,05143}            |{EYGUIANS,LAGRAND,SAINT-GENIS}                                    |{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{NULL,NULL,NULL}                                                                                       |                3|
05058        |FREISSINIERES            |{05058}                        |{FREISSINIERES}                                                   |{1974-02-20}                                 |{totalit�}                           |{NULL}                                                                                                 |                1|
05061        |GAP                      |{05061,05125}                  |{GAP,ROMETTE}                                                     |{1974-02-20,1974-02-20}                      |{P�????,totalit�}                    |{"Arr�t� de 1974�: Gap � Campagne / Gap Centre / Gap Ouest / Gap Est","Oubli� dans fichier d�origine"} |                2|
05101        |VALLOUISE PELVOUX        |{05101,05175}                  |{PELVOUX,VALLOUISE}                                               |{1974-02-20,1974-02-20}                      |{totalit�,totalit�}                  |{NULL,NULL}                                                                                            |                2|
05109        |PUY SAINT PIERRE         |{05023,05109}                  |{BRIANCON,PUY-SAINT-PIERRE}                                       |{1974-02-20,1974-02-20}                      |{P�:�????,totalit�}                  |{"1974�: Biranson Nord + 1974 Briancon Sud = tout briancon�?",NULL}                                    |                2|
05112        |RABOU                    |{05123}                        |{"LA ROCHE-DES-ARNAUDS"}                                          |{1974-02-20}                                 |{totalit�}                           |{NULL}                                                                                                 |                1|
05118        |VAL BUECH MEOUGE         |{05005,05034,05118}            |{ANTONAVES,CHATEAUNEUF-DE-CHABRE,RIBIERS}                         |{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{NULL,NULL,NULL}                                                                                       |                3|
05123        |ROCHE DES ARNAUDS        |{05123}                        |{"LA ROCHE-DES-ARNAUDS"}                                          |{1974-02-20}                                 |{totalit�}                           |{NULL}                                                                                                 |                1|
05132        |SAINT BONNET EN CHAMPSAUR|{05020,05067,05132}            |{BENEVENT-ET-CHARBILLAC,"LES INFOURNAS",SAINT-BONNET-EN-CHAMPSAUR}|{1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�}         |{"Oubli� dans fichier d�origine","Oubli� dans fichier d�origine","Arr�t� de 1974�: juste SAINT-BONNET"}|                3|
05134        |SAINT CLEMENT SUR DURANCE|{05134}                        |{SAINT-CLEMENT(-SUR-DURANCE)}                                     |{1974-02-20}                                 |{totalit�}                           |{"1974�: Saint-Cl�ment seulement"}                                                                     |                1|
05139        |DEVOLUY                  |{05002,05042,05138,05139}      |{AGNIERES,"LA CLUSE",SAINT-DISDIER,SAINT-ETIENNE-EN-DEVOLUY}      |{1974-02-20,1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�,totalit�}|{"Oubli� dans fichier d�origine","Oubli� dans fichier d�origine","Oubli� dans fichier d�origine",NULL} |                4|
05161        |SALLE LES ALPES          |{05161}                        |{"LA SALLE(-LES-ALPES)"}                                          |{1974-02-20}                                 |{totalit�}                           |{"1974�: Ecrit La Salle seulement"}                                                                    |                1|
05163        |SAUZE DU LAC             |{05163}                        |{"LE SAUZE(-DU-LAC)"}                                             |{1974-02-20}                                 |{totalit�}                           |{"Arr�t� de 1974�: LE SAUZE-DU-LAC"}                                                                   |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 99 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	162

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '05' ---005
ORDER BY insee_cog2019, insee_com;

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005;
--> Updated Rows	162

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap           |insee_comm|nom_commun          |pointage|partie  |date_decis|date_eff  |mod|
------|-----------------|----------|--------------------|--------|--------|----------|----------|---|
05023 |BRIANCON         |05023     |BRIANCON            |x       |P�:�????|1974-02-20|1989-01-01|21 |
05109 |PUY SAINT PIERRE |05023     |BRIANCON            |x       |P�:�????|1974-02-20|1989-01-01|21 |
05109 |PUY SAINT PIERRE |05109     |PUY-SAINT-PIERRE    |x       |totalit�|1974-02-20|1989-01-01|21 |
05112 |RABOU            |05123     |LA ROCHE-DES-ARNAUDS|x       |totalit�|1974-02-20|1983-02-14|21 |
05123 |ROCHE DES ARNAUDS|05123     |LA ROCHE-DES-ARNAUDS|x       |totalit�|1974-02-20|1983-02-14|21 |
 */
--> MAJ le 08/07/2020

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005)
order by date_eff, com_ap
/*
com_av|ncc_av|com_ap|ncc_ap|date_eff|arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|------|------|------|--------|-----------------|-----------------------------|-------------------------------|
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '05' ---005
ORDER BY insee_cog2019, insee_com;




















------------------------------------------------------------------------------------------------------------------------------------------------
---- 23/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                        |count|
-------------------------------------|-----|
totalit�                             |  145|
{totalit�}                           |    8|
{totalit�,totalit�}                  |    3|
{totalit�,totalit�,totalit�}         |    5|
{totalit�,totalit�,totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_005_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_005_2019 as
SELECT 
'005'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
/*CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   
	ELSE NULL
END::varchar(8) AS classement_2019,*/
'totalit�'::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_005_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 162

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_005_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  162|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_005_2019 FOR VALUES IN ('005');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 RENAME TO l_liste_commune_montagne_1985_005_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|  178|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 AS
SELECT
'005'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005_old
ORDER BY insee_classement;
--> 178

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  178|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_005 FOR VALUES IN ('005');
--> Updated Rows	0



