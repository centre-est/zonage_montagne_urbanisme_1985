---- 08/07/2020
----- HAUTES-ALPES ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	85

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030;
--> 85

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019           |insee_commune_arrete_classement|nom_commune_arrete_classement          |arrete_classement      |classement_commune |commentaires                                                              |nb_evenements_cog|
-------------|------------------------------|-------------------------------|---------------------------------------|-----------------------|-------------------|--------------------------------------------------------------------------|-----------------|
30052        |BREAU MARS                    |{30052,30157}                  |{BREAU-ET-SALAGOSSE,MARS}              |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                               |                2|
30195        |PEYROLLES                     |{30195}                        |{PEYROLES}                             |{1974-02-20}           |{totalit�}         |{NULL}                                                                    |                1|
30216        |ROBIAC ROCHESSADOULE          |{30216}                        |{ROBIAC}                               |{1976-04-28}           |{totalit�}         |{"17/04/1983 : Robiac devient Robiac-Rochessadoule."}                     |                1|
30297        |SAINT SAUVEUR CAMPRIEU        |{30297}                        |{"SAINT-SAUVEUR-DES POURCILS"}         |{1974-02-20}           |{totalit�}         |{"SAINT-SAUVEUR-CAMPRIEU dans fichier d’origine"}                       |                1|
30298        |SAINT SEBASTIEN D AIGREFEUILLE|{30298}                        |{SAINT-SEBASTIEN}                      |{1976-04-28}           |{totalit�}         |{"05/05/1976 : Saint-Sébastien devient Saint-Sébastien-d'Aigrefeuille."}|                1|
30339        |VAL D AIGOUAL                 |{30190,30339}                  |{NOTRE-DAME-DE-LA-ROUVIERE,VALLERAUGUE}|{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                                               |                2|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 77 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	84

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '30' ---030
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
30009        |30009         |30009                          |{30009}           |                0|                    1|
30015        |30015         |30015                          |{30015}           |                0|                    1|
30016        |30016         |30016                          |{30016}           |                0|                    1|
30017        |30017         |30017                          |{30017}           |                0|                    1|
30024        |30024         |30024                          |{30024}           |                0|                    1|
30025        |30025         |30025                          |{30025}           |                0|                    1|
30026        |30026         |30026                          |{30026}           |                0|                    1|
30037        |30037         |30037                          |{30037}           |                0|                    1|
30038        |30038         |30038                          |{30038}           |                0|                    1|
30040        |30040         |30040                          |{30040}           |                0|                    1|
30044        |30044         |30044                          |{30044}           |                0|                    1|
30045        |30045         |30045                          |{30045}           |                0|                    1|
30051        |30051         |30051                          |{30051}           |                0|                    1|
30052        |30052         |{30052,30157}                  |{30052,30157}     |                2|                    2|
30064        |30064         |30064                          |{30064}           |                0|                    1|
30074        |30074         |30074                          |{30074}           |                0|                    1|
30077        |30077         |30077                          |{30077}           |                0|                    1|
30079        |30079         |30079                          |{30079}           |                0|                    1|
30080        |30080         |30080                          |{30080}           |                0|                    1|
30087        |30087         |30087                          |{30087}           |                0|                    1|
30090        |30090         |30090                          |{30090}           |                0|                    1|
30094        |30094         |30094                          |{30094}           |                0|                    1|
30099        |30099         |30099                          |{30099}           |                0|                    1|
30105        |30105         |30105                          |{30105}           |                0|                    1|
30108        |30108         |30108                          |{30108}           |                0|                    1|
30120        |30120         |30120                          |{30120}           |                0|                    1|
30129        |30129         |30129                          |{30129}           |                0|                    1|
30130        |30130         |30130                          |{30130}           |                0|                    1|
30132        |30132         |30132                          |{30132}           |                0|                    1|
30137        |30137         |30137                          |{30137}           |                0|                    1|
30139        |30139         |30139                          |{30139}           |                0|                    1|
30140        |30140         |30140                          |{30140}           |                0|                    1|
30142        |30142         |30142                          |{30142}           |                0|                    1|
30153        |30153         |30153                          |{30153}           |                0|                    1|
30154        |30154         |30154                          |{30154}           |                0|                    1|
30159        |30159         |30159                          |{30159}           |                0|                    1|
30167        |30167         |30167                          |{30167}           |                0|                    1|
30168        |30168         |30168                          |{30168}           |                0|                    1|
30170        |30170         |30170                          |{30170}           |                0|                    1|
30171        |30171         |30171                          |{30171}           |                0|                    1|
30172        |30172         |30172                          |{30172}           |                0|                    1|
30176        |30176         |30176                          |{30176}           |                0|                    1|
30194        |30194         |30194                          |{30194}           |                0|                    1|
30195        |30195         |{30195}                        |{30195}           |                1|                    1|
30198        |30198         |30198                          |{30198}           |                0|                    1|
30199        |30199         |30199                          |{30199}           |                0|                    1|
30201        |30201         |30201                          |{30201}           |                0|                    1|
30203        |30203         |30203                          |{30203}           |                0|                    1|
30213        |30213         |30213                          |{30213}           |                0|                    1|
30216        |30216         |{30216}                        |{30216}           |                1|                    1|
30219        |30219         |30219                          |{30219}           |                0|                    1|
30220        |30220         |30220                          |{30220}           |                0|                    1|
30229        |30229         |30229                          |{30229}           |                0|                    1|
30231        |30231         |30231                          |{30231}           |                0|                    1|
30236        |30236         |30236                          |{30236}           |                0|                    1|
30238        |30238         |30238                          |{30238}           |                0|                    1|
30239        |30239         |30239                          |{30239}           |                0|                    1|
30246        |30246         |30246                          |{30246}           |                0|                    1|
30252        |30252         |30252                          |{30252}           |                0|                    1|
30253        |30253         |30253                          |{30253}           |                0|                    1|
30268        |30268         |30268                          |{30268}           |                0|                    1|
30269        |30269         |30269                          |{30269}           |                0|                    1|
30270        |30270         |30270                          |{30270}           |                0|                    1|
30272        |30272         |30272                          |{30272}           |                0|                    1|
30280        |30280         |30280                          |{30280}           |                0|                    1|
30283        |30283         |30283                          |{30283}           |                0|                    1|
30291        |30291         |30291                          |{30291}           |                0|                    1|
30296        |30296         |30296                          |{30296}           |                0|                    1|
30297        |30297         |{30297}                        |{30297}           |                1|                    1|
30298        |30298         |{30298}                        |{30298}           |                1|                    1|
30307        |30307         |30307                          |{30307}           |                0|                    1|
30310        |30310         |30310                          |{30310}           |                0|                    1|
30316        |30316         |30316                          |{30316}           |                0|                    1|
30322        |30322         |30322                          |{30322}           |                0|                    1|
30323        |30323         |30323                          |{30323}           |                0|                    1|
30325        |30325         |30325                          |{30325}           |                0|                    1|
30329        |30329         |30329                          |{30329}           |                0|                    1|
30332        |30332         |30332                          |{30332}           |                0|                    1|
30335        |30335         |30335                          |{30335}           |                0|                    1|
30339        |30339         |{30190,30339}                  |{30190,30339}     |                2|                    2|
30345        |30345         |30345                          |{30345}           |                0|                    1|
30350        |30350         |30350                          |{30350}           |                0|                    1|
30353        |30353         |30353                          |{30353}           |                0|                    1|
             |30022         |                               |{30022}           |                 |                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030;
--> Updated Rows	83

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030)
order by date_eff, com_ap
/*
com_av|ncc_av        |com_ap|ncc_ap  |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|--------------|------|--------|----------|-----------------|-----------------------------|-------------------------------|
30078 |CEZAS         |30325 |SUMENE  |1959-02-21|1974-02-20       |SUMENE                       |30325                          |
30118 |FOUSSIGNARGUES|30037 |BESSEGES|1972-07-01|1976-04-28       |BESSEGES                     |30037                          |
 */

---- Correction du fichier final :
--- ajout 30022 AUJAX

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---030
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
totalit�           |   78|
{totalit�}         |    4|
{totalit�,totalit�}|    2|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_030_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_030_2019 as
SELECT 
'030'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�' /*	
	WHEN t1.classement_85 = 'Partie de commune' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'   */
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_030_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 84

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_030_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |   84|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_030_2019 FOR VALUES IN ('030');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 RENAME TO l_liste_commune_montagne_1985_030_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|   86|
 */

---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 AS
SELECT
'030'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030_old
ORDER BY insee_classement;
--> 86

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |   86|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_030 FOR VALUES IN ('030');
--> Updated Rows	0





