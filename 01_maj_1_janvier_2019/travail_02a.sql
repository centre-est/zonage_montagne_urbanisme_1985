---- 28/07/2020
----- 2A ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup."l_liste_commune_montagne_1985_02A" RENAME TO l_liste_commune_montagne_1985_02a;

ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;


UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	122

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a;
--> 122

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019  |insee_commune_arrete_classement|nom_commune_arrete_classement|arrete_classement|classement_commune|commentaires|nb_evenements_cog|
-------------|---------------------|-------------------------------|-----------------------------|-----------------|------------------|------------|-----------------|
2A215        |PIANOTTOLI CALDARELLO|{2A215}                        |{PIANOTOLLI-CALDARELLO}      |{1974-02-20}     |{totalit�}        |{NULL}      |                1|
2A269        |SARI SOLENZARA       |{2A269}                        |{SARI-DI-PORTO-VECCHIO}      |{1976-04-28}     |{totalit�}        |{NULL}      |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 120 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	123

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '2A' ---02a
ORDER BY insee_cog2019, insee_com;
---- Ajout de la commune 2a064 � la main

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a;
--> Updated Rows	22

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a)
order by date_eff, com_ap
/*
com_av|ncc_av                 |com_ap|ncc_ap                 |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|-----------------------|------|-----------------------|----------|-----------------|-----------------------------|-------------------------------|
20001 |AFA                    |2A001 |AFA                    |1976-01-01|1976-04-28       |AFA                          |2A001                          |
20006 |ALATA                  |2A006 |ALATA                  |1976-01-01|1976-04-28       |ALATA                        |2A006                          |
20008 |ALBITRECCIA            |2A008 |ALBITRECCIA            |1976-01-01|1974-02-20       |ALBITRECCIA                  |2A008                          |
20011 |ALTAGENE               |2A011 |ALTAGENE               |1976-01-01|1974-02-20       |ALTAGENE                     |2A011                          |
20014 |AMBIEGNA               |2A014 |AMBIEGNA               |1976-01-01|1976-04-28       |AMBIEGNA                     |2A014                          |
20017 |APPIETTO               |2A017 |APPIETTO               |1976-01-01|1976-04-28       |APPIETTO                     |2A017                          |
20018 |ARBELLARA              |2A018 |ARBELLARA              |1976-01-01|1976-04-28       |ARBELLARA                    |2A018                          |
20019 |ARBORI                 |2A019 |ARBORI                 |1976-01-01|1976-04-28       |ARBORI                       |2A019                          |
20021 |ARGIUSTA MORICCIO      |2A021 |ARGIUSTA MORICCIO      |1976-01-01|1974-02-20       |ARGIUSTA-MORICCIO            |2A021                          |
20022 |ARRO                   |2A022 |ARRO                   |1976-01-01|1976-04-28       |ARRO                         |2A022                          |
20024 |AULLENE                |2A024 |AULLENE                |1976-01-01|1974-02-20       |AULLENE                      |2A024                          |
20026 |AZILONE AMPAZA         |2A026 |AZILONE AMPAZA         |1976-01-01|1974-02-20       |AZILONE-AMPAZA               |2A026                          |
20027 |AZZANA                 |2A027 |AZZANA                 |1976-01-01|1974-02-20       |AZZANA                       |2A027                          |
20028 |BALOGNA                |2A028 |BALOGNA                |1976-01-01|1974-02-20       |BALOGNA                      |2A028                          |
20031 |BASTELICA              |2A031 |BASTELICA              |1976-01-01|1974-02-20       |BASTELICA                    |2A031                          |
20032 |BASTELICACCIA          |2A032 |BASTELICACCIA          |1976-01-01|1976-04-28       |BASTELICACCIA                |2A032                          |
20035 |BELVEDERE CAMPOMORO    |2A035 |BELVEDERE CAMPOMORO    |1976-01-01|1976-04-28       |BELVEDERE-CAMPOMORO          |2A035                          |
20038 |BILIA                  |2A038 |BILIA                  |1976-01-01|1976-04-28       |BILIA                        |2A038                          |
20040 |BOCOGNANO              |2A040 |BOCOGNANO              |1976-01-01|1974-02-20       |BOCOGNANO                    |2A040                          |
20041 |BONIFACIO              |2A041 |BONIFACIO              |1976-01-01|1976-04-28       |BONIFACIO                    |2A041                          |
20048 |CALCATOGGIO            |2A048 |CALCATOGGIO            |1976-01-01|1976-04-28       |CALCATOGGIO                  |2A048                          |
20056 |CAMPO                  |2A056 |CAMPO                  |1976-01-01|1974-02-20       |CAMPO                        |2A056                          |
20060 |CANNELLE               |2A060 |CANNELLE               |1976-01-01|1976-04-28       |CANNELLE                     |2A060                          |
20061 |CARBINI                |2A061 |CARBINI                |1976-01-01|1974-02-20       |CARBINI                      |2A061                          |
20062 |CARBUCCIA              |2A062 |CARBUCCIA              |1976-01-01|1974-02-20       |CARBUCCIA                    |2A062                          |
20064 |CARDO TORGIA           |2A064 |CARDO TORGIA           |1976-01-01|1974-02-20       |CARDO-TORGIA                 |2A064                          |
20065 |CARGESE                |2A065 |CARGESE                |1976-01-01|1976-04-28       |CARGESE                      |2A065                          |
20066 |CARGIACA               |2A066 |CARGIACA               |1976-01-01|1974-02-20       |CARGIACA                     |2A066                          |
20070 |CASAGLIONE             |2A070 |CASAGLIONE             |1976-01-01|1976-04-28       |CASAGLIONE                   |2A070                          |
20071 |CASALABRIVA            |2A071 |CASALABRIVA            |1976-01-01|1974-02-20       |CASALABRIVA                  |2A071                          |
20085 |CAURO                  |2A085 |CAURO                  |1976-01-01|1976-04-28       |CAURO                        |2A085                          |
20089 |CIAMANNACCE            |2A089 |CIAMANNACCE            |1976-01-01|1974-02-20       |CIAMANNACCE                  |2A089                          |
20090 |COGGIA                 |2A090 |COGGIA                 |1976-01-01|1976-04-28       |COGGIA                       |2A090                          |
20091 |COGNOCOLI MONTICCHI    |2A091 |COGNOCOLI MONTICCHI    |1976-01-01|1976-04-28       |COGNOCOLI-MONTICCHI          |2A091                          |
20092 |CONCA                  |2A092 |CONCA                  |1976-01-01|1976-04-28       |CONCA                        |2A092                          |
20094 |CORRANO                |2A094 |CORRANO                |1976-01-01|1974-02-20       |CORRANO                      |2A094                          |
20098 |COTI CHIAVARI          |2A098 |COTI CHIAVARI          |1976-01-01|1976-04-28       |COTI-CHIAVARI                |2A098                          |
20099 |COZZANO                |2A099 |COZZANO                |1976-01-01|1974-02-20       |COZZANO                      |2A099                          |
20100 |CRISTINACCE            |2A100 |CRISTINACCE            |1976-01-01|1974-02-20       |CRISTINACCE                  |2A100                          |
20103 |CUTTOLI CORTICCHIATO   |2A103 |CUTTOLI CORTICCHIATO   |1976-01-01|1976-04-28       |CUTTOLI-CORTICCHIATO         |2A103                          |
20104 |ECCICA SUARELLA        |2A104 |ECCICA SUARELLA        |1976-01-01|1976-04-28       |ECCICA-SUARELLA              |2A104                          |
20108 |EVISA                  |2A108 |EVISA                  |1976-01-01|1974-02-20       |EVISA                        |2A108                          |
20114 |FIGARI                 |2A114 |FIGARI                 |1976-01-01|1976-04-28       |FIGARI                       |2A114                          |
20115 |FOCE                   |2A115 |FOCE                   |1976-01-01|1985-08-27       |FOCE                         |2A115                          |
20117 |FORCIOLO               |2A117 |FORCIOLO               |1976-01-01|1976-04-28       |FORCIOLO                     |2A117                          |
20118 |FOZZANO                |2A118 |FOZZANO                |1976-01-01|1974-02-20       |FOZZANO                      |2A118                          |
20119 |FRASSETO               |2A119 |FRASSETO               |1976-01-01|1974-02-20       |FRASSETO                     |2A119                          |
20127 |GIUNCHETO              |2A127 |GIUNCHETO              |1976-01-01|1976-04-28       |GIUNCHETO                    |2A127                          |
20128 |GRANACE                |2A128 |GRANACE                |1976-01-01|1974-02-20       |GRANACE                      |2A128                          |
20129 |GROSSA                 |2A129 |GROSSA                 |1976-01-01|1976-04-28       |GROSSA                       |2A129                          |
20130 |GROSSETO PRUGNA        |2A130 |GROSSETO PRUGNA        |1976-01-01|1976-04-28       |GROSSETO-PRUGNA              |2A130                          |
20131 |GUAGNO                 |2A131 |GUAGNO                 |1976-01-01|1974-02-20       |GUAGNO                       |2A131                          |
20132 |GUARGUALE              |2A132 |GUARGUALE              |1976-01-01|1976-04-28       |GUARGUALE                    |2A132                          |
20133 |GUITERA LES BAINS      |2A133 |GUITERA LES BAINS      |1976-01-01|1974-02-20       |GUITERA(-LES-BAINS)          |2A133                          |
20139 |LECCI                  |2A139 |LECCI                  |1976-01-01|1976-04-28       |LECCI                        |2A139                          |
20141 |LETIA                  |2A141 |LETIA                  |1976-01-01|1974-02-20       |LETIA                        |2A141                          |
20142 |LEVIE                  |2A142 |LEVIE                  |1976-01-01|1974-02-20       |LEVIE                        |2A142                          |
20144 |LOPIGNA                |2A144 |LOPIGNA                |1976-01-01|1976-04-28       |LOPIGNA                      |2A144                          |
20146 |LORETO DI TALLANO      |2A146 |LORETO DI TALLANO      |1976-01-01|1974-02-20       |LORETO-DI-TALLANO            |2A146                          |
20154 |MARIGNANA              |2A154 |MARIGNANA              |1976-01-01|1974-02-20       |MARIGNANA                    |2A154                          |
20158 |MELA                   |2A158 |MELA                   |1976-01-01|1974-02-20       |MELA                         |2A158                          |
20160 |MOCA CROCE             |2A160 |MOCA CROCE             |1976-01-01|1974-02-20       |MOCA-CROCE                   |2A160                          |
20163 |MONACIA D AULLENE      |2A163 |MONACIA D AULLENE      |1976-01-01|1974-02-20       |MONACIA-D'AULLENE            |2A163                          |
20174 |MURZO                  |2A174 |MURZO                  |1976-01-01|1974-02-20       |MURZO                        |2A174                          |
20181 |OCANA                  |2A181 |OCANA                  |1976-01-01|1976-04-28       |OCANA                        |2A181                          |
20186 |OLIVESE                |2A186 |OLIVESE                |1976-01-01|1974-02-20       |OLIVESE                      |2A186                          |
20189 |OLMETO                 |2A189 |OLMETO                 |1976-01-01|1976-04-28       |OLMETO                       |2A189                          |
20191 |OLMICCIA               |2A191 |OLMICCIA               |1976-01-01|1974-02-20       |OLMICCIA                     |2A191                          |
20196 |ORTO                   |2A196 |ORTO                   |1976-01-01|1974-02-20       |ORTO                         |2A196                          |
20197 |OSANI                  |2A197 |OSANI                  |1976-01-01|1976-04-28       |OSANI                        |2A197                          |
20198 |OTA                    |2A198 |OTA                    |1976-01-01|1976-04-28       |OTA                          |2A198                          |
20200 |PALNECA                |2A200 |PALNECA                |1976-01-01|1974-02-20       |PALNECA                      |2A200                          |
20203 |PARTINELLO             |2A203 |PARTINELLO             |1976-01-01|1976-04-28       |PARTINELLO                   |2A203                          |
20204 |PASTRICCIOLA           |2A204 |PASTRICCIOLA           |1976-01-01|1974-02-20       |PASTRICCIOLA                 |2A204                          |
20209 |PERI                   |2A209 |PERI                   |1976-01-01|1976-04-28       |PERI                         |2A209                          |
20211 |PETRETO BICCHISANO     |2A211 |PETRETO BICCHISANO     |1976-01-01|1974-02-20       |PETRETO-BICCHISANO           |2A211                          |
20212 |PIANA                  |2A212 |PIANA                  |1976-01-01|1976-04-28       |PIANA                        |2A212                          |
20215 |PIANOTOLLI CALDARELLO  |2A215 |PIANOTOLLI CALDARELLO  |1976-01-01|{1974-02-20}     |{PIANOTOLLI-CALDARELLO}      |{2A215}                        |
20228 |PIETROSELLA            |2A228 |PIETROSELLA            |1976-01-01|1976-04-28       |PIETROSELLA                  |2A228                          |
20232 |PILA CANALE            |2A232 |PILA CANALE            |1976-01-01|1976-04-28       |PILA-CANALE                  |2A232                          |
20240 |POGGIOLO               |2A240 |POGGIOLO               |1976-01-01|1974-02-20       |POGGIOLO                     |2A240                          |
20247 |PORTO VECCHIO          |2A247 |PORTO VECCHIO          |1976-01-01|1976-04-28       |PORTO-VECCHIO                |2A247                          |
20249 |PROPRIANO              |2A249 |PROPRIANO              |1976-01-01|1976-04-28       |PROPRIANO                    |2A249                          |
20253 |QUASQUARA              |2A253 |QUASQUARA              |1976-01-01|1974-02-20       |QUASQUARA                    |2A253                          |
20254 |QUENZA                 |2A254 |QUENZA                 |1976-01-01|1974-02-20       |QUENZA                       |2A254                          |
20258 |RENNO                  |2A258 |RENNO                  |1976-01-01|1974-02-20       |RENNO                        |2A258                          |
20259 |REZZA                  |2A259 |REZZA                  |1976-01-01|1974-02-20       |REZZA                        |2A259                          |
20262 |ROSAZIA                |2A262 |ROSAZIA                |1976-01-01|1974-02-20       |ROSAZIA                      |2A262                          |
20266 |SALICE                 |2A266 |SALICE                 |1976-01-01|1974-02-20       |SALICE                       |2A266                          |
20268 |SAMPOLO                |2A268 |SAMPOLO                |1976-01-01|1974-02-20       |SAMPOLO                      |2A268                          |
20269 |SARI DI PORTO VECCHIO  |2A269 |SARI DI PORTO VECCHIO  |1976-01-01|{1976-04-28}     |{SARI-DI-PORTO-VECCHIO}      |{2A269}                        |
20270 |SARI D ORCINO          |2A270 |SARI D ORCINO          |1976-01-01|1976-04-28       |SARI-D'ORCINO                |2A270                          |
20271 |SARROLA CARCOPINO      |2A271 |SARROLA CARCOPINO      |1976-01-01|1976-04-28       |SARROLA-CARCOPINO            |2A271                          |
20272 |SARTENE                |2A272 |SARTENE                |1976-01-01|1976-04-28       |SARTENE                      |2A272                          |
20276 |SERRA DI FERRO         |2A276 |SERRA DI FERRO         |1976-01-01|1976-04-28       |SERRA-DI-FERRO               |2A276                          |
20278 |SERRA DI SCOPAMENE     |2A278 |SERRA DI SCOPAMENE     |1976-01-01|1974-02-20       |SERRA-DI-SCOPAMENE           |2A278                          |
20279 |SERRIERA               |2A279 |SERRIERA               |1976-01-01|1976-04-28       |SERRIERA                     |2A279                          |
20282 |SOCCIA                 |2A282 |SOCCIA                 |1976-01-01|1974-02-20       |SOCCIA                       |2A282                          |
20284 |SOLLACARO              |2A284 |SOLLACARO              |1976-01-01|1976-04-28       |SOLLACARO                    |2A284                          |
20285 |SORBOLLANO             |2A285 |SORBOLLANO             |1976-01-01|1974-02-20       |SORBOLLANO                   |2A285                          |
20288 |SOTTA                  |2A288 |SOTTA                  |1976-01-01|1974-02-20       |SOTTA                        |2A288                          |
20295 |SANT ANDREA D ORCINO   |2A295 |SANT ANDREA D ORCINO   |1976-01-01|1976-04-28       |SANT'ANDREA-D'ORCINO         |2A295                          |
20300 |SAN GAVINO DI CARBINI  |2A300 |SAN GAVINO DI CARBINI  |1976-01-01|1974-02-20       |SAN-GAVINO-DI-CARBINI        |2A300                          |
20308 |SAINTE LUCIE DE TALLANO|2A308 |SAINTE LUCIE DE TALLANO|1976-01-01|1974-02-20       |SAINTE-LUCIE-DE-TALLANO      |2A308                          |
20310 |SANTA MARIA FIGANIELLA |2A310 |SANTA MARIA FIGANIELLA |1976-01-01|1974-02-20       |SANTA-MARIA-FIGANIELLA       |2A310                          |
20312 |SANTA MARIA SICHE      |2A312 |SANTA MARIA SICHE      |1976-01-01|1974-02-20       |SAINTE-MARIE-SICCHE          |2A312                          |
20322 |TASSO                  |2A322 |TASSO                  |1976-01-01|1974-02-20       |TASSO                        |2A322                          |
20323 |TAVACO                 |2A323 |TAVACO                 |1976-01-01|1976-04-28       |TAVACO                       |2A323                          |
20324 |TAVERA                 |2A324 |TAVERA                 |1976-01-01|1974-02-20       |TAVERA                       |2A324                          |
20326 |TOLLA                  |2A326 |TOLLA                  |1976-01-01|1974-02-20       |TOLLA                        |2A326                          |
20330 |UCCIANI                |2A330 |UCCIANI                |1976-01-01|1974-02-20       |UCCIANI                      |2A330                          |
20331 |URBALACONE             |2A331 |URBALACONE             |1976-01-01|1976-04-28       |URBALACONE                   |2A331                          |
20336 |VALLE DI MEZZANA       |2A336 |VALLE DI MEZZANA       |1976-01-01|1976-04-28       |VALLE-DI-MEZZANA             |2A336                          |
20345 |VERO                   |2A345 |VERO                   |1976-01-01|1974-02-20       |VERO                         |2A345                          |
20348 |VICO                   |2A348 |VICO                   |1976-01-01|1974-02-20       |VICO                         |2A348                          |
20349 |VIGGIANELLO            |2A349 |VIGGIANELLO            |1976-01-01|1976-04-28       |VIGGIANELLO                  |2A349                          |
20351 |VILLANOVA              |2A351 |VILLANOVA              |1976-01-01|1976-04-28       |VILLANOVA                    |2A351                          |
20357 |ZERUBIA                |2A357 |ZERUBIA                |1976-01-01|1974-02-20       |ZERUBIA                      |2A357                          |
20358 |ZEVACO                 |2A358 |ZEVACO                 |1976-01-01|1974-02-20       |ZEVACO                       |2A358                          |
20359 |ZICAVO                 |2A359 |ZICAVO                 |1976-01-01|1974-02-20       |ZICAVO                       |2A359                          |
20360 |ZIGLIARA               |2A360 |ZIGLIARA               |1976-01-01|1976-04-28       |ZIGLIARA                     |2A360                          |
20362 |ZONZA                  |2A362 |ZONZA                  |1976-01-01|1974-02-20       |ZONZA                        |2A362                          |
20363 |ZOZA                   |2A363 |ZOZA                   |1976-01-01|1974-02-20       |ZOZA                         |2A363                          |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---02a
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 21/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85|count|
-------------|-----|
totalit�     |  121|
{totalit�}   |    2|
 */

---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02a_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02a_2019 as
SELECT 
'02A'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�' 
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_02a_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 123

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02a_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  123|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_02a_2019 FOR VALUES IN ('02A');
--> Updated Rows	


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a RENAME TO l_liste_commune_montagne_1985_02a_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|  123|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a AS
SELECT
'02A'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'Partie de commune' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a_old
ORDER BY insee_classement;
--> 123

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  123|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_02a FOR VALUES IN ('02A');
--> Updated Rows	0
