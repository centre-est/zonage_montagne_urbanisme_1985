---- 08/07/2020
----- LOZERE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048  WHERE date_decis IS NULL;

--UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24

ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 RENAME COLUMN partie_de_ TO partie;
--> Updated Rows	0
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 ALTER COLUMN partie TYPE varchar(17) USING partie::varchar;
--> Updated Rows	0

UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	186

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048;
--> 186

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019             |insee_commune_arrete_classement      |nom_commune_arrete_classement                                                                         |arrete_classement                                                  |classement_commune                                     |commentaires                                                                                                                                                                                                                                            |nb_evenements_cog|
-------------|--------------------------------|-------------------------------------|------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|-------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
48009        |PEYRE EN AUBRAC                 |{48009,48047,48060,48076,48142,48183}|{AUMONT-AUBRAC,"LA CHAZE-DE-PEYRE",FAU-DE-PEYRE,JAVOLS,SAINTE-COLOMBE-DE-PEYRE,SAINT-SAUVEUR-DE-PEYRE}|{1974-02-20,1974-02-20,1974-02-20,1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|{NULL,"Ecrit LA CHAZE mais 02/06/1972 : La Chaze devient La Chaze-de-Peyre",NULL,NULL,NULL,NULL}                                                                                                                                                        |                6|
48012        |MONTS VERTS                     |{48012}                              |{"LES MONTS-VERTS"}                                                                                   |{1974-02-20}                                                       |{totalit�}                                             |{NULL}                                                                                                                                                                                                                                                  |                1|
48017        |BANASSAC CANILHAC               |{48017,48033}                        |{BANASSAC,CANILHAC}                                                                                   |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48027        |MONT LOZERE ET GOULET           |{48014,48023,48027,48040,48093,48164}|{BAGNOLS-LES-BAINS,BELVEZET,"LE BLEYMARD",CHASSERADES,MAS-D'ORCIERES,SAINT-JULIEN-DU-TOURNEL}         |{1974-02-20,1974-02-20,1974-02-20,1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|{NULL,NULL,NULL,NULL,NULL,NULL}                                                                                                                                                                                                                         |                6|
48038        |BEL AIR VAL D ANCE              |{48038,48184}                        |{CHAMBON-LE-CHATEAU,SAINT-SYMPHORIEN}                                                                 |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48050        |BEDOUES COCURES                 |{48022,48050}                        |{BEDOUES,COCURES}                                                                                     |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48061        |FLORAC TROIS RIVIERES           |{48061,48186}                        |{FLORAC,"LA SALLE-PRUNET"}                                                                            |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48087        |PRINSUEJOLS MALBOUZON           |{48087,48120}                        |{MALBOUZON,PRINSUEJOLS}                                                                               |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48094        |MASSEGROS CAUSSES GORGES        |{48094,48125,48154,48180,48195}      |{"LE MASSEGROS","LE RECOUX",SAINT-GEORGES-DE-LEVEJAC,SAINT-ROME-DE-DOLAN,"LES VIGNES"}                |{1974-02-20,1974-02-20,1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�,totalit�,totalit�}         |{NULL,NULL,NULL,NULL,NULL}                                                                                                                                                                                                                              |                5|
48099        |BOURGS SUR COLAGNE              |{48049,48099,48113}                  |{CHIRAC,"LE MONASTIER",PIN-MORIES}                                                                    |{1974-02-20,1974-02-20,1974-02-20}                                 |{totalit�,totalit�,totalit�}                           |{NULL,"01/03/1974 : Pin-Mori�s est rattach�e � Le Monastier (48099) (fusion association) qui devient Le Monastier-Pin-Mori�s.","01/03/1974 : Pin-Mori�s est rattach�e � Le Monastier (48099) (fusion association) qui devient Le Monastier-Pin-Mori�s."}|                3|
48105        |NAUSSAC FONTANES                |{48062,48105}                        |{FONTANES,NAUSSAC}                                                                                    |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48116        |PONT DE MONTVERT SUD MONT LOZERE|{48066,48116,48172}                  |{FRAISSINET-DE-LOZERE,"LE PONT-DE-MONTVERT",SAINT-MAURICE-DE-VENTALON}                                |{1974-02-20,1974-02-20,1974-02-20}                                 |{totalit�,totalit�,totalit�}                           |{NULL,NULL,NULL}                                                                                                                                                                                                                                        |                3|
48126        |LACHAMP RIBENNES                |{48078,48126}                        |{LACHAMP,RIBENNES}                                                                                    |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48127        |MONTS DE RANDON                 |{48057,48127,48133,48189,48197}      |{ESTABLES,RIEUTORT-DE-RANDON,SAINT-AMANS,SERVIERES,"LA VILLEDIEU"}                                    |{1974-02-20,1974-02-20,1974-02-20,1974-02-20,1974-02-20}           |{totalit�,totalit�,totalit�,totalit�,totalit�}         |{NULL,NULL,NULL,NULL,NULL}                                                                                                                                                                                                                              |                5|
48139        |SAINT BONNET LAVAL              |{48084,48139}                        |{LAVAL-ATGER,SAINT-BONNET-DE-MONTAUROUX}                                                              |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48146        |GORGES DU TARN CAUSSES          |{48101,48122,48146}                  |{MONTBRUN,QUEZAC,SAINTE-ENIMIE}                                                                       |{1974-02-20,1974-02-20,1974-02-20}                                 |{totalit�,totalit�,totalit�}                           |{NULL,NULL,NULL}                                                                                                                                                                                                                                        |                3|
48152        |VENTALON EN CEVENNES            |{48134,48152}                        |{SAINT-ANDEOL-DE-CLERGUEMORT,SAINT-FREZAL-DE-VENTALON}                                                |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
48166        |CANS ET CEVENNES                |{48162,48166}                        |{SAINT-JULIEN-D'ARPAON,SAINT-LAURENT-DE-TREVES}                                                       |{1974-02-20,1974-02-20}                                            |{totalit�,totalit�}                                    |{NULL,NULL}                                                                                                                                                                                                                                             |                2|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 134 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	152

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '48' ---048
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement      |fred_anciens_codes                   |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------------|-------------------------------------|-----------------|---------------------|
48001        |48001         |48001                                |{48001}                              |                0|                    1|
48002        |48002         |48002                                |{48002}                              |                0|                    1|
48003        |48003         |48003                                |{48003}                              |                0|                    1|
48004        |48004         |48004                                |{48004}                              |                0|                    1|
48005        |48005         |48005                                |{48005}                              |                0|                    1|
48007        |48007         |48007                                |{48007}                              |                0|                    1|
48008        |48008         |48008                                |{48008}                              |                0|                    1|
48009        |48009         |{48009,48047,48060,48076,48142,48183}|{48076,48183,48009,48142,48047,48060}|                6|                    6|
48010        |48010         |48010                                |{48010}                              |                0|                    1|
48012        |48012         |{48012}                              |{48012}                              |                1|                    1|
48013        |48013         |48013                                |{48013}                              |                0|                    1|
48015        |48015         |48015                                |{48015}                              |                0|                    1|
48016        |48016         |48016                                |{48016}                              |                0|                    1|
48017        |48017         |{48017,48033}                        |{48033,48017}                        |                2|                    2|
48018        |48018         |48018                                |{48018}                              |                0|                    1|
48019        |48019         |48019                                |{48019}                              |                0|                    1|
48020        |48020         |48020                                |{48020}                              |                0|                    1|
48021        |48021         |48021                                |{48021}                              |                0|                    1|
48025        |48025         |48025                                |{48025}                              |                0|                    1|
48026        |48026         |48026                                |{48026}                              |                0|                    1|
48027        |48027         |{48014,48023,48027,48040,48093,48164}|{48023,48164,48040,48093,48027,48014}|                6|                    6|
48028        |48028         |48028                                |{48028}                              |                0|                    1|
48029        |48029         |48029                                |{48029}                              |                0|                    1|
48030        |48030         |48030                                |{48030}                              |                0|                    1|
48031        |48031         |48031                                |{48031}                              |                0|                    1|
48032        |48032         |48032                                |{48032}                              |                0|                    1|
48034        |48034         |48034                                |{48034}                              |                0|                    1|
48036        |48036         |48036                                |{48036}                              |                0|                    1|
48037        |48037         |48037                                |{48037}                              |                0|                    1|
48038        |48038         |{48038,48184}                        |{48038,48184}                        |                2|                    2|
48039        |48039         |48039                                |{48039}                              |                0|                    1|
48041        |48041         |48041                                |{48041}                              |                0|                    1|
48042        |48042         |48042                                |{48042}                              |                0|                    1|
48043        |48043         |48043                                |{48043}                              |                0|                    1|
48044        |48044         |48044                                |{48044}                              |                0|                    1|
48045        |48045         |48045                                |{48045}                              |                0|                    1|
48046        |48046         |48046                                |{48046}                              |                0|                    1|
48048        |48048         |48048                                |{48048}                              |                0|                    1|
48050        |48050         |{48022,48050}                        |{48022,48050}                        |                2|                    2|
48051        |48051         |48051                                |{48051}                              |                0|                    1|
48053        |48053         |48053                                |{48053}                              |                0|                    1|
48054        |48054         |48054                                |{48054}                              |                0|                    1|
48055        |48055         |48055                                |{48055}                              |                0|                    1|
48056        |48056         |48056                                |{48056}                              |                0|                    1|
48058        |48058         |48058                                |{48058}                              |                0|                    1|
48059        |48059         |48059                                |{48059}                              |                0|                    1|
48061        |48061         |{48061,48186}                        |{48061,48186}                        |                2|                    2|
48063        |48063         |48063                                |{48063}                              |                0|                    1|
48064        |48064         |48064                                |{48064}                              |                0|                    1|
48065        |48065         |48065                                |{48065}                              |                0|                    1|
48067        |48067         |48067                                |{48067}                              |                0|                    1|
48068        |48068         |48068                                |{48068}                              |                0|                    1|
48069        |48069         |48069                                |{48069}                              |                0|                    1|
48070        |48070         |48070                                |{48070}                              |                0|                    1|
48071        |48071         |48071                                |{48071}                              |                0|                    1|
48072        |48072         |48072                                |{48072}                              |                0|                    1|
48073        |48073         |48073                                |{48073}                              |                0|                    1|
48074        |48074         |48074                                |{48074}                              |                0|                    1|
48075        |48075         |48075                                |{48075}                              |                0|                    1|
48077        |48077         |48077                                |{48077}                              |                0|                    1|
48079        |48079         |48079                                |{48079}                              |                0|                    1|
48080        |48080         |48080                                |{48080}                              |                0|                    1|
48081        |48081         |48081                                |{48081}                              |                0|                    1|
48082        |48082         |48082                                |{48082}                              |                0|                    1|
48083        |48083         |48083                                |{48083}                              |                0|                    1|
48085        |48085         |48085                                |{48085}                              |                0|                    1|
48086        |48086         |48086                                |{48086}                              |                0|                    1|
48087        |48087         |{48087,48120}                        |{48120,48087}                        |                2|                    2|
48088        |48088         |48088                                |{48088}                              |                0|                    1|
48089        |48089         |48089                                |{48089}                              |                0|                    1|
48090        |48090         |48090                                |{48090}                              |                0|                    1|
48091        |48091         |48091                                |{48091}                              |                0|                    1|
48092        |48092         |48092                                |{48092}                              |                0|                    1|
48094        |48094         |{48094,48125,48154,48180,48195}      |{48180,48125,48195,48154,48094}      |                5|                    5|
48095        |48095         |48095                                |{48095}                              |                0|                    1|
48096        |48096         |48096                                |{48096}                              |                0|                    1|
48097        |48097         |48097                                |{48097}                              |                0|                    1|
48098        |48098         |48098                                |{48098}                              |                0|                    1|
48099        |48099         |{48049,48099,48113}                  |{48049,48099}                        |                3|                    2|
48100        |48100         |48100                                |{48100}                              |                0|                    1|
48103        |48103         |48103                                |{48103}                              |                0|                    1|
48104        |48104         |48104                                |{48104}                              |                0|                    1|
48105        |48105         |{48062,48105}                        |{48105,48062}                        |                2|                    2|
48106        |48106         |48106                                |{48106}                              |                0|                    1|
48107        |48107         |48107                                |{48107}                              |                0|                    1|
48108        |48108         |48108                                |{48108}                              |                0|                    1|
48110        |48110         |48110                                |{48110}                              |                0|                    1|
48111        |48111         |48111                                |{48111}                              |                0|                    1|
48112        |48112         |48112                                |{48112}                              |                0|                    1|
48115        |48115         |48115                                |{48115}                              |                0|                    1|
48116        |48116         |{48066,48116,48172}                  |{48116,48066,48172}                  |                3|                    3|
48117        |48117         |48117                                |{48117}                              |                0|                    1|
48119        |48119         |48119                                |{48119}                              |                0|                    1|
48121        |48121         |48121                                |{48121}                              |                0|                    1|
48123        |48123         |48123                                |{48123}                              |                0|                    1|
48124        |48124         |48124                                |{48124}                              |                0|                    1|
48126        |48126         |{48078,48126}                        |{48078,48126}                        |                2|                    2|
48127        |48127         |{48057,48127,48133,48189,48197}      |{48133,48057,48127,48197,48189}      |                5|                    5|
48128        |48128         |48128                                |{48128}                              |                0|                    1|
48129        |48129         |48129                                |{48129}                              |                0|                    1|
48130        |48130         |48130                                |{48130}                              |                0|                    1|
48131        |48131         |48131                                |{48131}                              |                0|                    1|
48132        |48132         |48132                                |{48132}                              |                0|                    1|
48135        |48135         |48135                                |{48135}                              |                0|                    1|
48136        |48136         |48136                                |{48136}                              |                0|                    1|
48137        |48137         |48137                                |{48137}                              |                0|                    1|
48138        |48138         |48138                                |{48138}                              |                0|                    1|
48139        |48139         |{48084,48139}                        |{48084,48139}                        |                2|                    2|
48140        |48140         |48140                                |{48140}                              |                0|                    1|
48141        |48141         |48141                                |{48141}                              |                0|                    1|
48144        |48144         |48144                                |{48144}                              |                0|                    1|
48145        |48145         |48145                                |{48145}                              |                0|                    1|
48146        |48146         |{48101,48122,48146}                  |{48146,48101,48122}                  |                3|                    3|
48147        |48147         |48147                                |{48147}                              |                0|                    1|
48148        |48148         |48148                                |{48148}                              |                0|                    1|
48149        |48149         |48149                                |{48149}                              |                0|                    1|
48150        |48150         |48150                                |{48150}                              |                0|                    1|
48151        |48151         |48151                                |{48151}                              |                0|                    1|
48152        |48152         |{48134,48152}                        |{48134,48152}                        |                2|                    2|
48153        |48153         |48153                                |{48153}                              |                0|                    1|
48155        |48155         |48155                                |{48155}                              |                0|                    1|
48156        |48156         |48156                                |{48156}                              |                0|                    1|
48157        |48157         |48157                                |{48157}                              |                0|                    1|
48158        |48158         |48158                                |{48158}                              |                0|                    1|
48160        |48160         |48160                                |{48160}                              |                0|                    1|
48161        |48161         |48161                                |{48161}                              |                0|                    1|
48163        |48163         |48163                                |{48163}                              |                0|                    1|
48165        |48165         |48165                                |{48165}                              |                0|                    1|
48166        |48166         |{48162,48166}                        |{48162,48166}                        |                2|                    2|
48167        |48167         |48167                                |{48167}                              |                0|                    1|
48168        |48168         |48168                                |{48168}                              |                0|                    1|
48169        |48169         |48169                                |{48169}                              |                0|                    1|
48170        |48170         |48170                                |{48170}                              |                0|                    1|
48171        |48171         |48171                                |{48171}                              |                0|                    1|
48173        |48173         |48173                                |{48173}                              |                0|                    1|
48174        |48174         |48174                                |{48174}                              |                0|                    1|
48175        |48175         |48175                                |{48175}                              |                0|                    1|
48176        |48176         |48176                                |{48176}                              |                0|                    1|
48177        |48177         |48177                                |{48177}                              |                0|                    1|
48178        |48178         |48178                                |{48178}                              |                0|                    1|
48179        |48179         |48179                                |{48179}                              |                0|                    1|
48181        |48181         |48181                                |{48181}                              |                0|                    1|
48182        |48182         |48182                                |{48182}                              |                0|                    1|
48185        |48185         |48185                                |{48185}                              |                0|                    1|
48187        |48187         |48187                                |{48187}                              |                0|                    1|
48188        |48188         |48188                                |{48188}                              |                0|                    1|
48190        |48190         |48190                                |{48190}                              |                0|                    1|
48191        |48191         |48191                                |{48191}                              |                0|                    1|
48192        |48192         |48192                                |{48192}                              |                0|                    1|
48193        |48193         |48193                                |{48193}                              |                0|                    1|
48194        |48194         |48194                                |{48194}                              |                0|                    1|
48198        |48198         |48198                                |{48198}                              |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048;
--> Updated Rows	22

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048)
order by date_eff, com_ap
/*
com_av|ncc_av                      |com_ap|ncc_ap         |date_eff  |arrete_classement                 |nom_commune_arrete_classement  |insee_commune_arrete_classement|
------|----------------------------|------|---------------|----------|----------------------------------|-------------------------------|-------------------------------|
48052 |COMBRET                     |48004 |ALTIER         |1964-11-01|1974-02-20                        |ALTIER                         |48004                          |
48114 |PLANCHAMP                   |48015 |PIED DE BORNE  |1964-11-01|1974-02-20                        |PIED-DE-BORNE                  |48015                          |
48159 |SAINT JEAN CHAZORNE         |48015 |PIED DE BORNE  |1964-11-01|1974-02-20                        |PIED-DE-BORNE                  |48015                          |
48143 |SAINTE COLOMBE DE MONTAUROUX|48070 |GRANDRIEU      |1965-02-01|1974-02-20                        |GRANDRIEU                      |48070                          |
48109 |PARADE                      |48074 |HURES LA PARADE|1971-02-15|1974-02-20                        |HURES-LA-PARADE                |48074                          |
48118 |PRADES                      |48146 |SAINTE ENIMIE  |1972-10-01|{1974-02-20,1974-02-20,1974-02-20}|{MONTBRUN,QUEZAC,SAINTE-ENIMIE}|{48101,48122,48146}            |
48024 |BERC                        |48012 |MONTS VERTS    |1973-01-01|{1974-02-20}                      |{"LES MONTS-VERTS"}            |{48012}                        |
48006 |ARCOMIE                     |48012 |MONTS VERTS    |1973-01-01|{1974-02-20}                      |{"LES MONTS-VERTS"}            |{48012}                        |
48102 |MONTJEZIEU                  |48034 |CANOURGUE      |1973-01-01|1974-02-20                        |LA CANOURGUE                   |48034                          |
48011 |AUXILLAC                    |48034 |CANOURGUE      |1973-01-01|1974-02-20                        |LA CANOURGUE                   |48034                          |
48035 |CAPELLE                     |48034 |CANOURGUE      |1973-01-01|1974-02-20                        |LA CANOURGUE                   |48034                          |
48196 |VILLARD                     |48039 |CHANAC         |1973-07-01|1974-02-20                        |CHANAC                         |48039                          |
48024 |BERC                        |48012 |MONTS VERTS    |1990-07-17|{1974-02-20}                      |{"LES MONTS-VERTS"}            |{48012}                        |
48006 |ARCOMIE                     |48012 |MONTS VERTS    |1990-07-17|{1974-02-20}                      |{"LES MONTS-VERTS"}            |{48012}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---048
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- En attente d'un avis de la DDT
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN

departement := '048';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019 as
	SELECT 
	''' || departement || '''::varchar(3) AS code_dep,
	t1.insee_cog2019::varchar(5) AS insee_2019,
	t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
	t2.nom_com::varchar AS nom_min_2019,
	t1.nb_evenements_cog::integer,
	t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
	''En expertise''::varchar AS agreg_nom_classement , 
	''En expertise''::varchar AS agreg_arretes,
	''En expertise''::varchar AS agreg_classement_85,
	''Expertis''::varchar(8) AS classement_2019,
	commentaires::TEXT AS agreg_commentaires,
	''n_adm_exp_cog_commune_000_2019''::varchar(254) AS source_geom,
	t2.geom::geometry(''MULTIPOLYGON'',2154)
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_' || departement || '_final AS t1
	JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
	ON t1.insee_cog2019 = t2.insee_com
	ORDER BY insee_2019;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- B] Cr�ation de la table des communes de 1985
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp AS
	SELECT
	''' || departement || '''::varchar(3) AS code_dep,
	insee_comm::varchar(5) AS insee_classement,
	nom_commun::varchar(100) AS nom_commune,
	date_decis::varchar AS arrete_classement,
	''Expertis''::varchar(8) AS classement_1985,
	commentair::TEXT AS commentaires
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '
	ORDER BY insee_classement;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- Une fois avis DDT obtenu : 
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN
departement := '048';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 

------------------------------------------------------------------------------------------------------------------------------------------------
---- 30/11/2020 - Tout le d�partement est zon� donc on int�gre sans accord de la DDT
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                          |count|
-------------------------------------------------------|-----|
totalit�                                               |  134|
{totalit�}                                             |    1|
{totalit�,totalit�}                                    |   10|
{totalit�,totalit�,totalit�}                           |    3|
{totalit�,totalit�,totalit�,totalit�,totalit�}         |    2|
{totalit�,totalit�,totalit�,totalit�,totalit�,totalit�}|    2|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_048_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_048_2019 as
SELECT 
'048'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
'totalit�'::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_048_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 152

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_048_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  152|
*/

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_048_2019 FOR VALUES IN ('048');
--> Updated Rows	0

---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 RENAME TO l_liste_commune_montagne_1985_048_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
totalit�|  186|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 AS
SELECT
'048'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'
	WHEN partie = 'p' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048_old
ORDER BY insee_classement;
--> 186

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  186|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_048 FOR VALUES IN ('048');
--> Updated Rows	0