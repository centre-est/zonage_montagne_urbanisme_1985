---- 20/08/2020
---- 066 ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066  WHERE date_decis IS NULL;

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 SET date_decis='20/02/1974,28/04/1976' WHERE date_decis IS NULL;
--> Updated Rows	1
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	121

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066;
--> 125

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019                |insee_commune_arrete_classement|nom_commune_arrete_classement        |arrete_classement      |classement_commune |commentaires                                           |nb_evenements_cog|
-------------|-----------------------------------|-------------------------------|-------------------------------------|-----------------------|-------------------|-------------------------------------------------------|-----------------|
66005        |ANGOUSTRINE VILLENEUVE DES ESCALDES|{66005}                        |{ANGOUSTRINE-VILLENEUVE-DES-ESCALDES}|{1974-02-20}           |{totalit�}         |{NULL}                                                 |                1|
66010        |AYGUATEBIA TALAU                   |{66010,66200}                  |{AYGUATEBIA,TALAU}                   |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,"Pas pr�sent dans fichier d�origine"}            |                2|
66047        |CAUDIES DE CONFLENT                |{66047}                        |{CAUDIES}                            |{1974-02-20}           |{totalit�}         |{"Ecrit CAUDIES-DE-MONT-LOUIS dans l�arr�t� d�origine"}|                1|
66051        |CLARA VILLERACH                    |{66051}                        |{CLARA}                              |{1974-02-20}           |{totalit�}         |{NULL}                                                 |                1|
66063        |CLUSES                             |{66063}                        |{"LES CLUSES"}                       |{1976-04-28}           |{totalit�}         |{"Ecrit l�Ecluse dans l�arr�t� de 1976"}               |                1|
66099        |LLAURO                             |{66134}                        |{PASSA-LLAURO-TORDIERES}             |{1976-04-28}           |{P}                |{"Fraction LLAURO"}                                    |                1|
66100        |LLO                                |{66100,66167}                  |{LLO,SAILLAGOUSE}                    |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                            |                2|
66130        |OSSEJA                             |{66130}                        |{OSSEJA}                             |{1974-02-20}           |{totalit�}         |{NULL}                                                 |                1|
66134        |PASSA                              |{66134}                        |{PASSA-LLAURO-TORDIERES}             |{1976-04-28}           |{P}                |{"Fraction LLAURO"}                                    |                1|
66161        |RIA SIRACH                         |{66161}                        |{RIA-SIRACH-URBANYA}                 |{1974-02-20}           |{totalit�}         |{NULL}                                                 |                1|
66167        |SAILLAGOUSE                        |{66100,66167}                  |{LLO,SAILLAGOUSE}                    |{1974-02-20,1974-02-20}|{totalit�,totalit�}|{NULL,NULL}                                            |                2|
66184        |SAINT MARTIN DE FENOUILLET         |{66184}                        |{SAINT-MARTIN}                       |{1976-04-28}           |{totalit�}         |{NULL}                                                 |                1|
66211        |TORDERES                           |{66134}                        |{PASSA-LLAURO-TORDIERES}             |{1976-04-28}           |{P}                |{"Fraction LLAURO"}                                    |                1|
66219        |URBANYA                            |{66161}                        |{RIA-SIRACH-URBANYA}                 |{1974-02-20}           |{totalit�}         |{NULL}                                                 |                1|
66220        |VALCEBOLLERE                       |{66130}                        |{OSSEJA}                             |{1974-02-20}           |{totalit�}         |{NULL}                                                 |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 113 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	128

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '66' ---066
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes|nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|------------------|-----------------|---------------------|
66001        |66001         |66001                          |{66001}           |                0|                    1|
66003        |66003         |66003                          |{66003}           |                0|                    1|
66004        |66004         |66004                          |{66004}           |                0|                    1|
66005        |66005         |{66005}                        |{66005}           |                1|                    1|
66006        |66006         |66006                          |{66006}           |                0|                    1|
66007        |66007         |66007                          |{66007}           |                0|                    1|
66008        |66008         |66008                          |{66008}           |                0|                    1|
66009        |66009         |66009                          |{66009}           |                0|                    1|
66010        |66010         |{66010,66200}                  |{66010}           |                2|                    1|
66013        |66013         |66013                          |{66013}           |                0|                    1|
66016        |66016         |66016                          |{66016}           |                0|                    1|
66018        |66018         |66018                          |{66018}           |                0|                    1|
66020        |66020         |66020                          |{66020}           |                0|                    1|
66022        |66022         |66022                          |{66022}           |                0|                    1|
66025        |66025         |66025                          |{66025}           |                0|                    1|
66027        |66027         |66027                          |{66027}           |                0|                    1|
66029        |66029         |66029                          |{66029}           |                0|                    1|
66032        |66032         |66032                          |{66032}           |                0|                    1|
66034        |66034         |66034                          |{66034}           |                0|                    1|
66035        |66035         |66035                          |{66035}           |                0|                    1|
66036        |66036         |66036                          |{66036}           |                0|                    1|
66039        |66039         |66039                          |{66039}           |                0|                    1|
66040        |66040         |66040                          |{66040}           |                0|                    1|
66043        |66043         |66043                          |{66043}           |                0|                    1|
66045        |66045         |66045                          |{66045}           |                0|                    1|
66046        |66046         |66046                          |{66046}           |                0|                    1|
66047        |66047         |{66047}                        |{66047}           |                1|                    1|
66048        |66048         |66048                          |{66048}           |                0|                    1|
66049        |66049         |66049                          |{66049}           |                0|                    1|
66051        |66051         |{66051}                        |{66051}           |                1|                    1|
66053        |66053         |66053                          |{66053}           |                0|                    1|
66054        |66054         |66054                          |{66054}           |                0|                    1|
66057        |66057         |66057                          |{66057}           |                0|                    1|
66060        |66060         |66060                          |{66060}           |                0|                    1|
66061        |66061         |66061                          |{66061}           |                0|                    1|
66062        |66062         |66062                          |{66062}           |                0|                    1|
66063        |66063         |{66063}                        |{66063}           |                1|                    1|
66064        |66064         |66064                          |{66064}           |                0|                    1|
66066        |66066         |66066                          |{66066}           |                0|                    1|
66067        |66067         |66067                          |{66067}           |                0|                    1|
66068        |66068         |66068                          |{66068}           |                0|                    1|
66072        |66072         |66072                          |{66072}           |                0|                    1|
66073        |66073         |66073                          |{66073}           |                0|                    1|
66074        |66074         |66074                          |{66074}           |                0|                    1|
66075        |66075         |66075                          |{66075}           |                0|                    1|
66076        |66076         |66076                          |{66076}           |                0|                    1|
66077        |66077         |66077                          |{66077}           |                0|                    1|
66078        |66078         |66078                          |{66078}           |                0|                    1|
66080        |66080         |66080                          |{66080}           |                0|                    1|
66081        |66081         |66081                          |{66081}           |                0|                    1|
66082        |66082         |66082                          |{66082}           |                0|                    1|
66083        |66083         |66083                          |{66083}           |                0|                    1|
66085        |66085         |66085                          |{66085}           |                0|                    1|
66086        |66086         |66086                          |{66086}           |                0|                    1|
66090        |66090         |66090                          |{66090}           |                0|                    1|
66091        |66091         |66091                          |{66091}           |                0|                    1|
66093        |66093         |66093                          |{66093}           |                0|                    1|
66095        |66095         |66095                          |{66095}           |                0|                    1|
66098        |66098         |66098                          |{66098}           |                0|                    1|
66099        |66099         |{66134}                        |{66099}           |                1|                    1|
66100        |66100         |{66100,66167}                  |{66100}           |                2|                    1|
66102        |66102         |66102                          |{66102}           |                0|                    1|
66105        |66105         |66105                          |{66105}           |                0|                    1|
66106        |66106         |66106                          |{66106}           |                0|                    1|
66109        |66109         |66109                          |{66109}           |                0|                    1|
66113        |66113         |66113                          |{66113}           |                0|                    1|
66116        |66116         |66116                          |{66116}           |                0|                    1|
66117        |66117         |66117                          |{66117}           |                0|                    1|
66119        |66119         |66119                          |{66119}           |                0|                    1|
66120        |66120         |66120                          |{66120}           |                0|                    1|
66122        |66122         |66122                          |{66122}           |                0|                    1|
66123        |66123         |66123                          |{66123}           |                0|                    1|
66124        |66124         |66124                          |{66124}           |                0|                    1|
66125        |66125         |66125                          |{66125}           |                0|                    1|
66126        |66126         |66126                          |{66126}           |                0|                    1|
66128        |66128         |66128                          |{66128}           |                0|                    1|
66130        |66130         |{66130}                        |{66130}           |                1|                    1|
66132        |66132         |66132                          |{66132}           |                0|                    1|
66137        |66137         |66137                          |{66137}           |                0|                    1|
66139        |66139         |66139                          |{66139}           |                0|                    1|
66142        |66142         |66142                          |{66142}           |                0|                    1|
66146        |66146         |66146                          |{66146}           |                0|                    1|
66147        |66147         |66147                          |{66147}           |                0|                    1|
66148        |66148         |66148                          |{66148}           |                0|                    1|
66150        |66150         |66150                          |{66150}           |                0|                    1|
66151        |66151         |66151                          |{66151}           |                0|                    1|
66152        |66152         |66152                          |{66152}           |                0|                    1|
66153        |66153         |66153                          |{66153}           |                0|                    1|
66154        |66154         |66154                          |{66154}           |                0|                    1|
66155        |66155         |66155                          |{66155}           |                0|                    1|
66156        |66156         |66156                          |{66156}           |                0|                    1|
66157        |66157         |66157                          |{66157}           |                0|                    1|
66159        |66159         |66159                          |{66159}           |                0|                    1|
66160        |66160         |66160                          |{66160}           |                0|                    1|
66161        |66161         |{66161}                        |{66161}           |                1|                    1|
66166        |66166         |66166                          |{66166}           |                0|                    1|
66167        |66167         |{66100,66167}                  |{66167}           |                2|                    1|
66179        |66179         |66179                          |{66179}           |                0|                    1|
66181        |66181         |66181                          |{66181}           |                0|                    1|
66183        |66183         |66183                          |{66183}           |                0|                    1|
66184        |66184         |{66184}                        |{66184}           |                1|                    1|
66188        |66188         |66188                          |{66188}           |                0|                    1|
66191        |66191         |66191                          |{66191}           |                0|                    1|
66192        |66192         |66192                          |{66192}           |                0|                    1|
66193        |66193         |66193                          |{66193}           |                0|                    1|
66194        |66194         |66194                          |{66194}           |                0|                    1|
66196        |66196         |66196                          |{66196}           |                0|                    1|
66197        |66197         |66197                          |{66197}           |                0|                    1|
66198        |66198         |66198                          |{66198}           |                0|                    1|
66199        |66199         |66199                          |{66199}           |                0|                    1|
66201        |66201         |66201                          |{66201}           |                0|                    1|
66202        |66202         |66202                          |{66202}           |                0|                    1|
66203        |66203         |66203                          |{66203}           |                0|                    1|
66204        |66204         |66204                          |{66204}           |                0|                    1|
66206        |66206         |66206                          |{66206}           |                0|                    1|
66209        |66209         |66209                          |{66209}           |                0|                    1|
66215        |66215         |66215                          |{66215}           |                0|                    1|
66216        |66216         |66216                          |{66216}           |                0|                    1|
66218        |66218         |66218                          |{66218}           |                0|                    1|
66219        |66219         |{66161}                        |{66219}           |                1|                    1|
66220        |66220         |{66130}                        |{66220}           |                1|                    1|
66221        |66221         |66221                          |{66221}           |                0|                    1|
66222        |66222         |66222                          |{66222}           |                0|                    1|
66223        |66223         |66223                          |{66223}           |                0|                    1|
66232        |66232         |66232                          |{66232}           |                0|                    1|
66234        |66234         |66234                          |{66234}           |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066;
--> Updated Rows	128

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap      |insee_comm|nom_commun            |pointage|partie  |date_decis|date_eff  |mod|
------|------------|----------|----------------------|--------|--------|----------|----------|---|
66099 |LLAURO      |66134     |PASSA-LLAURO-TORDIERES|x       |P       |1976-04-28|1989-09-30|21 |
66100 |LLO         |66100     |LLO                   |x       |totalit�|1974-02-20|1984-03-28|21 |
66100 |LLO         |66167     |SAILLAGOUSE           |x       |totalit�|1974-02-20|1984-03-28|21 |
66130 |OSSEJA      |66130     |OSSEJA                |x       |totalit�|1974-02-20|1985-01-01|21 |
66134 |PASSA       |66134     |PASSA-LLAURO-TORDIERES|x       |P       |1976-04-28|1989-09-30|21 |
66161 |RIA SIRACH  |66161     |RIA-SIRACH-URBANYA    |x       |totalit�|1974-02-20|1983-02-21|21 |
66167 |SAILLAGOUSE |66167     |SAILLAGOUSE           |x       |totalit�|1974-02-20|1984-03-28|21 |
66211 |TORDERES    |66134     |PASSA-LLAURO-TORDIERES|x       |P       |1976-04-28|1989-09-30|21 |
66219 |URBANYA     |66161     |RIA-SIRACH-URBANYA    |x       |totalit�|1974-02-20|1983-02-21|21 |
66220 |VALCEBOLLERE|66130     |OSSEJA                |x       |totalit�|1974-02-20|1985-01-01|21 |
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066)
order by date_eff, com_ap
/*
com_av|ncc_av                 |com_ap|ncc_ap                             |date_eff  |arrete_classement    |nom_commune_arrete_classement        |insee_commune_arrete_classement|
------|-----------------------|------|-----------------------------------|----------|---------------------|-------------------------------------|-------------------------------|
66110 |MONTALBA D AMELIE      |66003 |AMELIE LES BAINS PALALDA           |1963-01-01|20/02/1974,28/04/1976|AMELIE-LES-BAINS-PALALDA             |66003                          |
66220 |VALCEBOLLERE           |66130 |OSSEJA                             |1972-04-24|{1974-02-20}         |{OSSEJA}                             |{66130}                        |
66220 |VALCEBOLLERE           |66220 |VALCEBOLLERE                       |1972-04-24|{1974-02-20}         |{OSSEJA}                             |{66130}                        |
66163 |RIUNOGUES              |66106 |MAUREILLAS LAS ILLAS               |1972-06-20|1974-02-20           |MAUREILLAS-LAS-ILLAS                 |66106                          |
66087 |ILLAS                  |66106 |MAUREILLAS LAS ILLAS               |1972-06-20|1974-02-20           |MAUREILLAS-LAS-ILLAS                 |66106                          |
66229 |VILLENEUVE DES ESCALDES|66005 |ANGOUSTRINE VILLENEUVE DES ESCALDES|1973-03-01|{1974-02-20}         |{ANGOUSTRINE-VILLENEUVE-DES-ESCALDES}|{66005}                        |
66099 |LLAURO                 |66099 |LLAURO                             |1973-03-01|{1976-04-28}         |{PASSA-LLAURO-TORDIERES}             |{66134}                        |
66211 |TORDERES               |66134 |PASSA LLAURO TORDERES              |1973-03-01|{1976-04-28}         |{PASSA-LLAURO-TORDIERES}             |{66134}                        |
66099 |LLAURO                 |66134 |PASSA LLAURO TORDERES              |1973-03-01|{1976-04-28}         |{PASSA-LLAURO-TORDIERES}             |{66134}                        |
66219 |URBANYA                |66161 |RIA SIRACH URBANYA                 |1973-03-01|{1974-02-20}         |{RIA-SIRACH-URBANYA}                 |{66161}                        |
66211 |TORDERES               |66211 |TORDERES                           |1973-03-01|{1976-04-28}         |{PASSA-LLAURO-TORDIERES}             |{66134}                        |
66031 |CALDEGAS               |66025 |BOURG MADAME                       |1973-06-01|1974-02-20           |BOURG-MADAME                         |66025                          |
66229 |VILLENEUVE DES ESCALDES|66005 |ANGOUSTRINE VILLENEUVE DES ESCALDES|1983-03-01|{1974-02-20}         |{ANGOUSTRINE-VILLENEUVE-DES-ESCALDES}|{66005}                        |
66220 |VALCEBOLLERE           |66220 |VALCEBOLLERE                       |1985-01-01|{1974-02-20}         |{OSSEJA}                             |{66130}                        |
66099 |LLAURO                 |66099 |LLAURO                             |1989-09-30|{1976-04-28}         |{PASSA-LLAURO-TORDIERES}             |{66134}                        |
66211 |TORDERES               |66211 |TORDERES                           |1989-09-30|{1976-04-28}         |{PASSA-LLAURO-TORDIERES}             |{66134}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---066
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 24/11/2020
---- En attente d'un avis de la DDT
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN

departement := '066';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019 as
	SELECT 
	''' || departement || '''::varchar(3) AS code_dep,
	t1.insee_cog2019::varchar(5) AS insee_2019,
	t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
	t2.nom_com::varchar AS nom_min_2019,
	t1.nb_evenements_cog::integer,
	t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
	''En expertise''::varchar AS agreg_nom_classement , 
	''En expertise''::varchar AS agreg_arretes,
	''En expertise''::varchar AS agreg_classement_85,
	''Expertis''::varchar(8) AS classement_2019,
	commentaires::TEXT AS agreg_commentaires,
	''n_adm_exp_cog_commune_000_2019''::varchar(254) AS source_geom,
	t2.geom::geometry(''MULTIPOLYGON'',2154)
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_' || departement || '_final AS t1
	JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
	ON t1.insee_cog2019 = t2.insee_com
	ORDER BY insee_2019;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- B] Cr�ation de la table des communes de 1985
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
	CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp AS
	SELECT
	''' || departement || '''::varchar(3) AS code_dep,
	insee_comm::varchar(5) AS insee_classement,
	nom_commun::varchar(100) AS nom_commune,
	date_decis::varchar AS arrete_classement,
	''Expertis''::varchar(8) AS classement_1985,
	commentair::TEXT AS commentaires
	FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '
	ORDER BY insee_classement;
';
RAISE NOTICE '%', req;
EXECUTE(req);

req := '
	ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000
		ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp
		FOR VALUES IN (''' || departement || ''');
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- Une fois avis DDT obtenu : 
------------------------------------------------------------------------------------------------------------------------------------------------
;DO $$
DECLARE
departement					character VARYING(3);
req 						text;
BEGIN
departement := '066';
---- A] Cr�ation de la table des communes de 2019
req := '
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_' || departement || '_2019;
	DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_' || departement || '_temp;
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 

------------------------------------------------------------------------------------------------------------------------------------------------
---- 25/11/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85      |count|
-------------------|-----|
non class�e        |    2|
P                  |    3|
totalit�           |  111|
{totalit�}         |   11|
{totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_066_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_066_2019 as
SELECT 
'066'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
case
	WHEN t1.classement_85 = 'non class�e' THEN 'non class�e'
	WHEN t1.classement_85 = 'P' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_066_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 128

SELECT classement_2019, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_066_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
non clas       |    2|
totalit�       |  123|
partie         |    3|
 */

---- Suppression des non class�es :
delete from a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_066_2019 where classement_2019 = 'non clas';
--> Updated Rows	2

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_066_2019 FOR VALUES IN ('066');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 RENAME TO l_liste_commune_montagne_1985_066_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |    4|
totalit�|  121|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 AS
SELECT
'066'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'
	WHEN partie = 'p' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066_old
ORDER BY insee_classement;
--> 125

SELECT classement_1985, count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  121|
partie         |    4|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_066 FOR VALUES IN ('066');
--> Updated Rows	0