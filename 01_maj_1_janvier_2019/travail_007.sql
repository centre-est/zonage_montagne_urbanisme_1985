---- 08/07/2020
----- ARDECHE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 SET partie = 'totalit�' WHERE pointage = 'x' AND partie IS NULL;
--> Updated Rows	206

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 WHERE pointage = 'x';
--> 208

DELETE FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 WHERE pointage IS NULL;
--> 118

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019                    |insee_commune_arrete_classement|nom_commune_arrete_classement                            |arrete_classement                 |classement_commune          |commentaires                                                                                                                                                                                                                                                   |nb_evenements_cog|
-------------|---------------------------------------|-------------------------------|---------------------------------------------------------|----------------------------------|----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
07006        |ALBON D ARDECHE                        |{07006}                        |{ALBON}                                                  |{1974-02-20}                      |{totalit�}                  |{NULL}                                                                                                                                                                                                                                                         |                1|
07011        |VALLEES DANTRAIGUES ASPERJOC           |{07011,07016}                  |{ANTRAIGUES(-SUR-VOLANE),ASPERJOC}                       |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{ANTRAIGUES-SUR-VOLANE,NULL}                                                                                                                                                                                                                                   |                2|
07039        |BOZAS                                  |{07039}                        |{BOZAS}                                                  |{1974-02-20}                      |{totalit�}                  |{"arr�t� de 1974�: Ecrit BOSAS"}                                                                                                                                                                                                                               |                1|
07083        |DUNIERE SUR EYRIEUX                    |{07083}                        |{DUNIERES-SUR-EYRIEUX}                                   |{1976-04-28}                      |{totalit�}                  |{NULL}                                                                                                                                                                                                                                                         |                1|
07103        |SAINT JULIEN D INTRES                  |{07103,07252}                  |{INTRES,SAINT-JULIEN-BOUTIERES}                          |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                                                                                    |                2|
07112        |LABASTIDE SUR BESORGUES                |{07112}                        |{LA-BASTIDE-DE-JUVINAS}                                  |{1974-02-20}                      |{totalit�}                  |{"Arr�t� de 1974�: Ecrit LABASTIDE-SUR-BESORGUES"}                                                                                                                                                                                                             |                1|
07132        |LARGENTIERE                            |{07132}                        |{LARGENTIERE}                                            |{1976-04-28}                      |{P}                         |{"fraction ancienne commune de TAURIERS (voir � TAURIERS)"}                                                                                                                                                                                                    |                1|
07147        |MALARCE SUR LA THINES                  |{07125,07147,07320}            |{LAFIGERE,MALARCE(-SUR-LA-THINES),THINES}                |{1974-02-20,1974-02-20,1974-02-20}|{totalit�,totalit�,totalit�}|{"manquante sur fichier d�origine","Arr�t� de 1974�: Ecrit seulement MALARCE","Oubli� dans fichier d�origine"}                                                                                                                                                 |                3|
07165        |BELSENTES                              |{07165,07256}                  |{NONIERES,SAINT-JULIEN-LABROUSSE}                        |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                                                                                    |                2|
07216        |SAINT BARTHELEMY GROZON                |{07216}                        |{SAINT-BARTHELEMY-LE-PIN}                                |{1974-02-20}                      |{totalit�}                  |{NULL}                                                                                                                                                                                                                                                         |                1|
07262        |SAINT LAURENT LES BAINS LAVAL D AURELLE|{07135,07262}                  |{LAVAL-D'AURELLE,SAINT-LAURENT-LES-BAINS}                |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{NULL,NULL}                                                                                                                                                                                                                                                    |                2|
07270        |SAINT MARTIN SUR LAVEZON               |{07270,07271}                  |{SAINT-MARTIN-LE-SUPERIEUR,SAINT-MARTIN-L'INFERIEUR}     |{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{"SAINT-MARTIN-LE-SUPERIEUR (07270) absorbe SAINT-MARTIN-L'INFERIEUR (07271) et suite � fusion devient SAINT-MARTIN-SUR-LAVEZON","SAINT-MARTIN-LE-SUPERIEUR (07270) absorbe SAINT-MARTIN-L'INFERIEUR (07271) et suite � fusion devient SAINT-MARTIN-SUR-LAVEZON|                2|
07284        |SAINT PIERRE SAINT JEAN                |{07246,07284}                  |{SAINT-JEAN-DE-POURCHARESSE,SAINT-PIERRE-LE-DECHAUSSELAT}|{1974-02-20,1974-02-20}           |{totalit�,totalit�}         |{"Oubli� dans fichier d�origine","Fichier d�origine�: ecrit SAINT-PIERRE-SAINT-JEAN"}                                                                                                                                                                          |                2|
07318        |TAURIERS                               |{07132}                        |{LARGENTIERE}                                            |{1976-04-28}                      |{P}                         |{"fraction ancienne commune de TAURIERS (voir � TAURIERS)"}                                                                                                                                                                                                    |                1|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 99 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	201

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '07' ---007
ORDER BY insee_cog2019, insee_com;

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007;
--> Updated Rows	22

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap     |insee_comm|nom_commun |pointage|partie|date_decis|date_eff  |mod|
------|-----------|----------|-----------|--------|------|----------|----------|---|
07132 |LARGENTIERE|07132     |LARGENTIERE|x       |P     |1976-04-28|1989-01-01|21 |
07318 |TAURIERS   |07132     |LARGENTIERE|x       |P     |1976-04-28|1989-01-01|21 |
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007)
order by date_eff, com_ap
/*
com_av|ncc_av    |com_ap|ncc_ap      |date_eff  |arrete_classement|nom_commune_arrete_classement|insee_commune_arrete_classement|
------|----------|------|------------|----------|-----------------|-----------------------------|-------------------------------|
07180 |POUZAT    |07204 |SAINT AGREVE|1973-07-01|1974-02-20       |SAINT-AGREVE                 |07204                          |
07057 |CHASSAGNES|07334 |VANS        |1973-07-01|1974-02-20       |LES VANS                     |07334                          |
07164 |NAVES     |07334 |VANS        |1973-07-01|1974-02-20       |LES VANS                     |07334                          |
07043 |BRAHIC    |07334 |VANS        |1973-07-01|1974-02-20       |LES VANS                     |07334                          |
07318 |TAURIERS  |07132 |LARGENTIERE |1974-12-01|{1976-04-28}     |{LARGENTIERE}                |{07132}                        |
07318 |TAURIERS  |07318 |TAURIERS    |1974-12-01|{1976-04-28}     |{LARGENTIERE}                |{07132}                        |
07318 |TAURIERS  |07318 |TAURIERS    |1989-01-01|{1976-04-28}     |{LARGENTIERE}                |{07132}                        |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '07' ---007
ORDER BY insee_cog2019, insee_com;






------------------------------------------------------------------------------------------------------------------------------------------------
---- 23/11/202023
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85               |count|
----------------------------|-----|
p                           |    1|
totalit�                    |  187|
{totalit�}                  |    5|
{totalit�,totalit�}         |    6|
{totalit�,totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_007_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_007_2019 as
SELECT 
'007'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'p' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�}' THEN 'totalit�' 
	/*
	WHEN t1.classement_85 = 'P' THEN 'partie'
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�} non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} Non class�e' THEN 'partie'
	WHEN t1.classement_85 = '{totalit�} non class�e, non class�e' THEN 'partie'
  */
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_007_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 200

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_007_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  199|
partie         |    1|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_007_2019 FOR VALUES IN ('007');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 RENAME TO l_liste_commune_montagne_1985_007_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
p       |    1|
P       |    1|
totalit�|  206|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 AS
SELECT
'007'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'p' THEN 'partie'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007_old
ORDER BY insee_classement;
--> 208

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 GROUP BY classement_1985;
/*
classement_1985|count
---------------|-----
totalit�       |  206
partie         |    2
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_007 FOR VALUES IN ('007');
--> Updated Rows	0

