---- 21/08/2020
----- SAVOIE ----
---- IMPORT donn�es
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 ALTER COLUMN date_decis TYPE varchar(21);
	--USING to_date(date_decis,'YYYY-MM-DD')::varchar;

SELECT * FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073  WHERE date_decis IS NULL;

UPDATE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 SET date_decis='28/04/1976,29/01/1982' WHERE date_decis IS NULL;
--> Updated Rows	24
	
UPDATE  a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 SET partie = 'totalit�' WHERE partie IS NULL;
--> Updated Rows	216

SELECT count(*) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073;
--> 255

---- Quelles sont les communes o� il s'est pass� quelquechose dans le COG :
with requete1 as (
					with requete0 as (
									select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
									FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 AS t3
									LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
									ON t3.insee_comm = t4.com_av
									where t4.date_eff >= t3.date_decis
									ORDER BY t4.com_ap
									)
					SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
									FROM requete0 AS t0
									JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
									ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
				)
select 	com_ap::varchar AS insee_cog2019,
		nom_com_m::varchar as nom_commune_cog2019,
		array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
		array_agg(nom_commun)::varchar AS nom_commune_arrete_classement,
		array_agg(date_decis)::varchar AS arrete_classement,
		array_agg(partie)::varchar AS classement_commune,
		array_agg(commentair)::varchar AS commentaires,
		count(pointage)::integer AS nb_evenements_COG
from requete1
group by com_ap, nom_com_m
order by com_ap, nom_com_m
/*
insee_cog2019|nom_commune_cog2019      |insee_commune_arrete_classement|nom_commune_arrete_classement                                               |arrete_classement                                       |classement_commune                            |commentaires                                                                                                                                                                  |nb_evenements_cog|
-------------|-------------------------|-------------------------------|----------------------------------------------------------------------------|--------------------------------------------------------|----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
73003        |GRAND AIGUEBLANCHE       |{73003,73045,73266}            |{AIGUEBLANCHE,"LE BOIS",SAINT-OYEN}                                         |{20/02/1974,20/02/1974,20/02/1974}                      |{totalit�,totalit�,totalit�}                  |{NULL,NULL,NULL}                                                                                                                                                              |                3|
73006        |AIME LA PLAGNE           |{73006,73126,73169}            |{AIME,GRANIER,MONTGIROD}                                                    |{20/02/1974,20/02/1974,20/02/1974}                      |{totalit�,totalit�,totalit�}                  |{NULL,NULL,NULL}                                                                                                                                                              |                3|
73010        |ENTRELACS                |{73062,73108,73238}            |{CESSENS,EPERSY,SAINT-GERMAIN-LA-CHAMBOTTE}                                 |{20/02/1974,20/02/1974,20/02/1974}                      |{totalit�,totalit�,totalit�}                  |{NULL,NULL,NULL}                                                                                                                                                              |                3|
73135        |TOUR EN MAURIENNE        |{73080,73135,73203}            |{"LE CHATEL",HERMILLON,PONTAMAFREY-MONTPASCAL}                              |{20/02/1974,20/02/1974,20/02/1974}                      |{totalit�,totalit�,totalit�}                  |{NULL,NULL,NULL}                                                                                                                                                              |                3|
73150        |PLAGNE TARENTAISE        |{73038,73093,73150,73305}      |{BELLENTRE,"LA COTE-D'AIME",MACOT-LA-PLAGNE,VALEZAN}                        |{20/02/1974,20/02/1974,20/02/1974,20/02/1974}           |{totalit�,totalit�,totalit�,totalit�}         |{NULL,NULL,NULL,NULL}                                                                                                                                                         |                4|
73187        |LECHERE                  |{73046,73112,73187}            |{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                                  |{20/02/1974,20/02/1974,20/02/1974}                      |{totalit�,totalit�,totalit�}                  |{"Ecrit BONNEVAL-TARENTAISE dans l�arr�t� de 1974",NULL,NULL}                                                                                                                 |                3|
73212        |VAL D ARC                |{73002,73212}                  |{AIGUEBELLE,RANDENS}                                                        |{20/02/1974,20/02/1974}                                 |{totalit�,totalit�}                           |{NULL,NULL}                                                                                                                                                                   |                2|
73215        |VALGELON LA ROCHETTE     |{73111}                        |{ETABLE}                                                                    |{20/02/1974}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                        |                1|
73220        |SAINT ALBAN D HURTIERES  |{73220}                        |{SAINT-ALBAN-D�HURTIERES}                                                   |{20/02/1974}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                        |                1|
73227        |COURCHEVEL               |{73198,73227}                  |{"LA PERRIERE",SAINT-BON-TARENTAISE}                                        |{20/02/1974,20/02/1974}                                 |{totalit�,totalit�}                           |{NULL,"Ecrit Saint-Bon dans l�arr�t� de 1974"}                                                                                                                                |                2|
73235        |SAINT FRANCOIS LONGCHAMP |{73163,73167,73235}            |{MONTAIMONT,MONTGELLAFREY,SAINT-FRANCOIS-LONGCHAMP}                         |{20/02/1974,20/02/1974,20/02/1974}                      |{totalit�,totalit�,totalit�}                  |{NULL,NULL,NULL}                                                                                                                                                              |                3|
73236        |SAINT GENIX LES VILLAGES |{73260}                        |{SAINT-MAURICE-DE-ROTHERENS}                                                |{"20/02/1974�28/04/1976"}                               |{totalit�}                                    |{"Arr�t� de 1974�: Hameaux de Beyrin, Borgex, Bornet, Rive, Vieille-Cure, Mauchamp, Cupied, La Mare�Arr�t� de 1976�: reste du territoire non class� par arr�t� du 20/02/1974"}|                1|
73237        |SAINT GEORGES D HURTIERES|{73237}                        |{SAINT-GEORGES-D�-HURTIERES}                                                |{20/02/1974}                                            |{totalit�}                                    |{NULL}                                                                                                                                                                        |                1|
73257        |BELLEVILLE               |{73244,73257,73321}            |{SAINT-JEAN-DE-BELLEVILLE,SAINT-MARTIN-DE-BELLEVILLE,VILLARLURIN}           |{20/02/1974,20/02/1974,20/02/1974}                      |{totalit�,totalit�,totalit�}                  |{NULL,NULL,NULL}                                                                                                                                                              |                3|
73263        |SAINT OFFENGE            |{73263,73264}                  |{SAINT-OFFENGE-DESSOUS,SAINT-OFFENGE-DESSUS}                                |{20/02/1974,20/02/1974}                                 |{totalit�,totalit�}                           |{NULL,NULL}                                                                                                                                                                   |                2|
73284        |SALINS FONTAINE          |{73115,73284}                  |{FONTAINE-LE-PUITS,SALINS-LES-THERMES}                                      |{20/02/1974,20/02/1974}                                 |{totalit�,totalit�}                           |{NULL,NULL}                                                                                                                                                                   |                2|
73290        |VAL CENIS                |{73056,73143,73144,73287,73290}|{BRAMANS,LANSLEBOURG-MONT-CENIS,LANSLEVILLARD,SOLLIERES-SARDIERES,TERMIGNON}|{20/02/1974,20/02/1974,20/02/1974,20/02/1974,20/02/1974}|{totalit�,totalit�,totalit�,totalit�,totalit�}|{NULL,NULL,NULL,NULL,NULL}                                                                                                                                                    |                5|
*/

--- Quelles communes n'ont pas chang�s ?
SELECT 
		insee_comm::varchar AS insee_cog2019,
		nom_commun::varchar as nom_commune_cog2019,
		insee_comm::varchar AS insee_commune_1985,
		nom_commun::varchar AS nom_commune_1985,
		date_decis::varchar AS arretes,
		partie::varchar AS classement_85,
		0::integer AS nb_evenements_cog,
		commentair::varchar AS commentaires,
		NULL::varchar AS code_evenement_cog
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 
			where insee_comm not in (WITH 
										extraction as (
												select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
												FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 AS t1
												join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
												ON t1.insee_comm = t2.com_av
												where t2.date_eff >= t1.date_decis 
												order by t2.com_ap
														),
										array_avant AS (
												select array_agg(insee_comm) AS liste from extraction
													),
										insee_avant AS (
												SELECT UNNEST(liste) FROM array_avant
													),
										insee_apres AS (
												SELECT com_ap FROM extraction
												)
										(SELECT * FROM insee_avant
										UNION
										SELECT * FROM insee_apres)
										)
ORDER BY insee_comm;
--> 210 lignes

---- Requ�te finale : com_fusionnee
---- Fusion commune
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073 AS 
WITH requete_non_classe AS (
		WITH com_fusionnee AS (
							with requete1 as (
												with requete0 as (
																select DISTINCT t4.com_ap, t3.insee_comm, t3.nom_commun, t3.pointage, t3.partie, t3.date_decis, t3.commentair
																FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 AS t3
																LEFT join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
																ON t3.insee_comm = t4.com_av
																where t4.date_eff >= t3.date_decis
																ORDER BY t4.com_ap
																)
												SELECT 	t0.com_ap, t1.nom_com_m, t0.insee_comm, t0.nom_commun, t0.pointage, t0.partie, t0.date_decis, t0.commentair--, t2.MOD, t2.typecom_ap, t2.typecom_av 
																FROM requete0 AS t0
																JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t1
																ON t0.com_ap = t1.insee_com ORDER BY t0.com_ap
											)
								select 	com_ap::varchar AS insee_cog2019,
										nom_com_m::varchar as nom_commune_cog2019,
										array_agg(insee_comm)::varchar AS insee_commune_arrete_classement,
										array_agg(nom_commun)::varchar AS nom_commune_arrete_classement ,
										array_agg(date_decis)::varchar AS arrete_classement,
										array_agg(partie)::varchar AS classement_85,
										count(pointage)::integer AS nb_evenements_cog,
										array_agg(commentair)::varchar AS commentaires
								from requete1
								group by com_ap, nom_com_m
								order by com_ap
					)
		(SELECT * FROM com_fusionnee)
		UNION 
		---- Commune Inchang�e
		(SELECT 
				t4.insee_com::varchar AS insee_cog2019,
				t4.nom_com_m::varchar as nom_commune_cog2019,
				insee_comm::varchar AS insee_commune_arrete_classement,
				nom_commun::varchar AS nom_commune_arrete_classement ,
				date_decis::varchar AS arrete_classement,
				partie::varchar AS classement_85,
				0::integer AS nb_evenements_cog,
				commentair::varchar AS commentaires
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 AS t3
		JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 as t4
		ON t3.insee_comm = t4.insee_com
					where t3.insee_comm not in (WITH 
												extraction as (
														select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t1.commentair, t2.MOD, t2.typecom_ap, t2.typecom_av 
														FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 AS t1
														join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
														ON t1.insee_comm = t2.com_av
														where t2.date_eff >= t1.date_decis 
														order by t2.com_ap
																),
												array_avant AS (
														select array_agg(insee_comm) AS liste from extraction
															),
												insee_avant AS (
														SELECT UNNEST(liste) FROM array_avant
															),
												insee_apres AS (
														SELECT com_ap FROM extraction
														)
												(SELECT * FROM insee_avant
												UNION
												SELECT * FROM insee_apres)
												)
		ORDER BY insee_comm)
		)
SELECT * FROM requete_non_classe ORDER BY insee_COG2019;
--> Updated Rows	224

--- Comparaison 1  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = '73' ---073
ORDER BY insee_cog2019, insee_com;
/*
insee_cog2019|fred_insee_com|insee_commune_arrete_classement|fred_anciens_codes                   |nb_evenements_cog|fred_nb_anciennes_com|
-------------|--------------|-------------------------------|-------------------------------------|-----------------|---------------------|
73001        |73001         |73001                          |{73001}                              |                0|                    1|
73003        |73003         |{73003,73045,73266}            |{73045,73266,73003}                  |                3|                    3|
73004        |73004         |73004                          |{73004}                              |                0|                    1|
73005        |73005         |73005                          |{73005}                              |                0|                    1|
73006        |73006         |{73006,73126,73169}            |{73006,73169,73126}                  |                3|                    3|
73010        |73010         |{73062,73108,73238}            |{73239,73062,73108,73010,73158,73238}|                3|                    6|
73011        |73011         |73011                          |{73011}                              |                0|                    1|
73012        |73012         |73012                          |{73012}                              |                0|                    1|
73013        |73013         |73013                          |{73013}                              |                0|                    1|
73014        |73014         |73014                          |{73014}                              |                0|                    1|
73015        |73015         |73015                          |{73015}                              |                0|                    1|
73017        |73017         |73017                          |{73017}                              |                0|                    1|
73019        |73019         |73019                          |{73019}                              |                0|                    1|
73020        |73020         |73020                          |{73020}                              |                0|                    1|
73021        |73021         |73021                          |{73021}                              |                0|                    1|
73022        |73022         |73022                          |{73022}                              |                0|                    1|
73023        |73023         |73023                          |{73023}                              |                0|                    1|
73025        |73025         |73025                          |{73025}                              |                0|                    1|
73026        |73026         |73026                          |{73026}                              |                0|                    1|
73027        |73027         |73027                          |{73027}                              |                0|                    1|
73029        |73029         |73029                          |{73029}                              |                0|                    1|
73032        |73032         |73032                          |{73032}                              |                0|                    1|
73033        |73033         |73033                          |{73033}                              |                0|                    1|
73034        |73034         |73034                          |{73034}                              |                0|                    1|
73036        |73036         |73036                          |{73036}                              |                0|                    1|
73039        |73039         |73039                          |{73039}                              |                0|                    1|
73040        |73040         |73040                          |{73040}                              |                0|                    1|
73042        |73042         |73042                          |{73042}                              |                0|                    1|
73043        |73043         |73043                          |{73043}                              |                0|                    1|
73047        |73047         |73047                          |{73047}                              |                0|                    1|
73048        |73048         |73048                          |{73048}                              |                0|                    1|
73049        |73049         |73049                          |{73049}                              |                0|                    1|
73051        |73051         |73051                          |{73051}                              |                0|                    1|
73052        |73052         |73052                          |{73052}                              |                0|                    1|
73054        |73054         |73054                          |{73054}                              |                0|                    1|
73055        |73055         |73055                          |{73055}                              |                0|                    1|
73057        |73057         |73057                          |{73057}                              |                0|                    1|
73058        |73058         |73058                          |{73058}                              |                0|                    1|
73059        |73059         |73059                          |{73059}                              |                0|                    1|
73061        |73061         |73061                          |{73061}                              |                0|                    1|
73063        |73063         |73063                          |{73063}                              |                0|                    1|
73067        |73067         |73067                          |{73067}                              |                0|                    1|
73069        |73069         |73069                          |{73069}                              |                0|                    1|
73070        |73070         |73070                          |{73070}                              |                0|                    1|
73071        |73071         |73071                          |{73071}                              |                0|                    1|
73072        |73072         |73072                          |{73072}                              |                0|                    1|
73074        |73074         |73074                          |{73074}                              |                0|                    1|
73075        |73075         |73075                          |{73075}                              |                0|                    1|
73077        |73077         |73077                          |{73077}                              |                0|                    1|
73078        |73078         |73078                          |{73078}                              |                0|                    1|
73081        |73081         |73081                          |{73081}                              |                0|                    1|
73083        |73083         |73083                          |{73083}                              |                0|                    1|
73086        |73086         |73086                          |{73086}                              |                0|                    1|
73088        |73088         |73088                          |{73088}                              |                0|                    1|
73090        |73090         |73090                          |{73090}                              |                0|                    1|
73091        |73091         |73091                          |{73091}                              |                0|                    1|
73092        |73092         |73092                          |{73092}                              |                0|                    1|
73094        |73094         |73094                          |{73094}                              |                0|                    1|
73095        |73095         |73095                          |{73095}                              |                0|                    1|
73096        |73096         |73096                          |{73096}                              |                0|                    1|
73097        |73097         |73097                          |{73097}                              |                0|                    1|
73098        |73098         |73098                          |{73098}                              |                0|                    1|
73101        |73101         |73101                          |{73101}                              |                0|                    1|
73103        |73103         |73103                          |{73103}                              |                0|                    1|
73104        |73104         |73104                          |{73104}                              |                0|                    1|
73105        |73105         |73105                          |{73105}                              |                0|                    1|
73106        |73106         |73106                          |{73106}                              |                0|                    1|
73107        |73107         |73107                          |{73107}                              |                0|                    1|
73109        |73109         |73109                          |{73109}                              |                0|                    1|
73110        |73110         |73110                          |{73110}                              |                0|                    1|
73113        |73113         |73113                          |{73113}                              |                0|                    1|
73114        |73114         |73114                          |{73114}                              |                0|                    1|
73116        |73116         |73116                          |{73116}                              |                0|                    1|
73117        |73117         |73117                          |{73117}                              |                0|                    1|
73119        |73119         |73119                          |{73119}                              |                0|                    1|
73120        |73120         |73120                          |{73120}                              |                0|                    1|
73122        |73122         |73122                          |{73122}                              |                0|                    1|
73123        |73123         |73123                          |{73123}                              |                0|                    1|
73128        |73128         |73128                          |{73128}                              |                0|                    1|
73130        |73130         |73130                          |{73130}                              |                0|                    1|
73131        |73131         |73131                          |{73131}                              |                0|                    1|
73132        |73132         |73132                          |{73132}                              |                0|                    1|
73135        |73135         |{73080,73135,73203}            |{73135,73080,73203}                  |                3|                    3|
73138        |73138         |73138                          |{73138}                              |                0|                    1|
73139        |73139         |73139                          |{73139}                              |                0|                    1|
73140        |73140         |73140                          |{73140}                              |                0|                    1|
73142        |73142         |73142                          |{73142}                              |                0|                    1|
73145        |73145         |73145                          |{73145}                              |                0|                    1|
73146        |73146         |73146                          |{73146}                              |                0|                    1|
73147        |73147         |73147                          |{73147}                              |                0|                    1|
73149        |73149         |73149                          |{73149}                              |                0|                    1|
73150        |73150         |{73038,73093,73150,73305}      |{73093,73305,73150,73038}            |                4|                    4|
73152        |73152         |73152                          |{73152}                              |                0|                    1|
73153        |73153         |73153                          |{73153}                              |                0|                    1|
73154        |73154         |73154                          |{73154}                              |                0|                    1|
73156        |73156         |73156                          |{73156}                              |                0|                    1|
73157        |73157         |73157                          |{73157}                              |                0|                    1|
73160        |73160         |73160                          |{73160}                              |                0|                    1|
73161        |73161         |73161                          |{73161}                              |                0|                    1|
73162        |73162         |73162                          |{73162}                              |                0|                    1|
73164        |73164         |73164                          |{73164}                              |                0|                    1|
73166        |73166         |73166                          |{73166}                              |                0|                    1|
73168        |73168         |73168                          |{73168}                              |                0|                    1|
73170        |73170         |73170                          |{73170}                              |                0|                    1|
73173        |73173         |73173                          |{73173}                              |                0|                    1|
73175        |73175         |73175                          |{73175}                              |                0|                    1|
73176        |73176         |73176                          |{73176}                              |                0|                    1|
73177        |73177         |73177                          |{73177}                              |                0|                    1|
73178        |73178         |73178                          |{73178}                              |                0|                    1|
73179        |73179         |73179                          |{73179}                              |                0|                    1|
73180        |73180         |73180                          |{73180}                              |                0|                    1|
73181        |73181         |73181                          |{73181}                              |                0|                    1|
73182        |73182         |73182                          |{73182}                              |                0|                    1|
73184        |73184         |73184                          |{73184}                              |                0|                    1|
73186        |73186         |73186                          |{73186}                              |                0|                    1|
73187        |73187         |{73046,73112,73187}            |{73112,73046,73187}                  |                3|                    3|
73188        |73188         |73188                          |{73188}                              |                0|                    1|
73189        |73189         |73189                          |{73189}                              |                0|                    1|
73190        |73190         |73190                          |{73190}                              |                0|                    1|
73191        |73191         |73191                          |{73191}                              |                0|                    1|
73192        |73192         |73192                          |{73192}                              |                0|                    1|
73193        |73193         |73193                          |{73193}                              |                0|                    1|
73194        |73194         |73194                          |{73194}                              |                0|                    1|
73196        |73196         |73196                          |{73196}                              |                0|                    1|
73197        |73197         |73197                          |{73197}                              |                0|                    1|
73201        |73201         |73201                          |{73201}                              |                0|                    1|
73202        |73202         |73202                          |{73202}                              |                0|                    1|
73205        |73205         |73205                          |{73205}                              |                0|                    1|
73206        |73206         |73206                          |{73206}                              |                0|                    1|
73207        |73207         |73207                          |{73207}                              |                0|                    1|
73208        |73208         |73208                          |{73208}                              |                0|                    1|
73210        |73210         |73210                          |{73210}                              |                0|                    1|
73211        |73211         |73211                          |{73211}                              |                0|                    1|
73212        |73212         |{73002,73212}                  |{73002,73212}                        |                2|                    2|
73214        |73214         |73214                          |{73214}                              |                0|                    1|
73215        |73215         |{73111}                        |{73215,73111}                        |                1|                    2|
73216        |73216         |73216                          |{73216}                              |                0|                    1|
73218        |73218         |73218                          |{73218}                              |                0|                    1|
73219        |73219         |73219                          |{73219}                              |                0|                    1|
73220        |73220         |{73220}                        |{73220}                              |                1|                    1|
73221        |73221         |73221                          |{73221}                              |                0|                    1|
73223        |73223         |73223                          |{73223}                              |                0|                    1|
73224        |73224         |73224                          |{73224}                              |                0|                    1|
73226        |73226         |73226                          |{73226}                              |                0|                    1|
73227        |73227         |{73198,73227}                  |{73227,73198}                        |                2|                    2|
73228        |73228         |73228                          |{73228}                              |                0|                    1|
73229        |73229         |73229                          |{73229}                              |                0|                    1|
73230        |73230         |73230                          |{73230}                              |                0|                    1|
73231        |73231         |73231                          |{73231}                              |                0|                    1|
73232        |73232         |73232                          |{73232}                              |                0|                    1|
73233        |73233         |73233                          |{73233}                              |                0|                    1|
73234        |73234         |73234                          |{73234}                              |                0|                    1|
73235        |73235         |{73163,73167,73235}            |{73167,73235,73163}                  |                3|                    3|
73236        |73236         |{73260}                        |{73260,73236,73127}                  |                1|                    3|
73237        |73237         |{73237}                        |{73237}                              |                1|                    1|
73241        |73241         |73241                          |{73241}                              |                0|                    1|
73242        |73242         |73242                          |{73242}                              |                0|                    1|
73243        |73243         |73243                          |{73243}                              |                0|                    1|
73245        |73245         |73245                          |{73245}                              |                0|                    1|
73246        |73246         |73246                          |{73246}                              |                0|                    1|
73247        |73247         |73247                          |{73247}                              |                0|                    1|
73248        |73248         |73248                          |{73248}                              |                0|                    1|
73250        |73250         |73250                          |{73250}                              |                0|                    1|
73252        |73252         |73252                          |{73252}                              |                0|                    1|
73253        |73253         |73253                          |{73253}                              |                0|                    1|
73254        |73254         |73254                          |{73254}                              |                0|                    1|
73255        |73255         |73255                          |{73255}                              |                0|                    1|
73256        |73256         |73256                          |{73256}                              |                0|                    1|
73257        |73257         |{73244,73257,73321}            |{73321,73257,73244}                  |                3|                    3|
73258        |73258         |73258                          |{73258}                              |                0|                    1|
73259        |73259         |73259                          |{73259}                              |                0|                    1|
73261        |73261         |73261                          |{73261}                              |                0|                    1|
73262        |73262         |73262                          |{73262}                              |                0|                    1|
73263        |73263         |{73263,73264}                  |{73264,73263}                        |                2|                    2|
73265        |73265         |73265                          |{73265}                              |                0|                    1|
73267        |73267         |73267                          |{73267}                              |                0|                    1|
73268        |73268         |73268                          |{73268}                              |                0|                    1|
73269        |73269         |73269                          |{73269}                              |                0|                    1|
73270        |73270         |73270                          |{73270}                              |                0|                    1|
73271        |73271         |73271                          |{73271}                              |                0|                    1|
73272        |73272         |73272                          |{73272}                              |                0|                    1|
73273        |73273         |73273                          |{73273}                              |                0|                    1|
73274        |73274         |73274                          |{73274}                              |                0|                    1|
73275        |73275         |73275                          |{73275}                              |                0|                    1|
73276        |73276         |73276                          |{73276}                              |                0|                    1|
73277        |73277         |73277                          |{73277}                              |                0|                    1|
73278        |73278         |73278                          |{73278}                              |                0|                    1|
73280        |73280         |73280                          |{73280}                              |                0|                    1|
73281        |73281         |73281                          |{73281}                              |                0|                    1|
73282        |73282         |73282                          |{73282}                              |                0|                    1|
73284        |73284         |{73115,73284}                  |{73284,73115}                        |                2|                    2|
73285        |73285         |73285                          |{73285}                              |                0|                    1|
73286        |73286         |73286                          |{73286}                              |                0|                    1|
73289        |73289         |73289                          |{73289}                              |                0|                    1|
73290        |73290         |{73056,73143,73144,73287,73290}|{73143,73144,73056,73287,73290}      |                5|                    5|
73292        |73292         |73292                          |{73292}                              |                0|                    1|
73293        |73293         |73293                          |{73293}                              |                0|                    1|
73294        |73294         |73294                          |{73294}                              |                0|                    1|
73296        |73296         |73296                          |{73296}                              |                0|                    1|
73298        |73298         |73298                          |{73298}                              |                0|                    1|
73299        |73299         |73299                          |{73299}                              |                0|                    1|
 */

---- Mise en place du fichier final :
drop table if exists a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073_final;
create table a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073_final
( like a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073 including all);
insert into a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073_final
select * from a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073;
--> Updated Rows	224

---- Identification des r�tablissement � corriger � la main : 
select distinct t2.com_ap , t2.ncc_ap, t1.insee_comm, t1.nom_commun, t1.pointage, t1.partie, t1.date_decis, t2.date_eff, t2.mod
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 AS t1
join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t2
ON t1.insee_comm = t2.com_av
where t2.date_eff >= t1.date_decis and t2.mod = '21'
order by t2.com_ap;
/*
com_ap|ncc_ap |insee_comm|nom_commun|pointage|partie  |date_decis|date_eff  |mod|
------|-------|----------|----------|--------|--------|----------|----------|---|
 */

---- Identification des op�rations faites sur les communes non class�e
---- A v�rifier sur fusion ou fusion simple et corriger � la main.
WITH sous_requete AS (
		select DISTINCT t4.com_av, t4.ncc_av, t4.com_ap, t4.ncc_ap, t4.date_eff, t3.arrete_classement, t3.nom_commune_arrete_classement, t3.insee_commune_arrete_classement
		FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073 AS t3
		join a_dcap_actu_arretes_montagne_dhup.n_evenement_communes_cog_01012019 as t4
		ON t3.insee_cog2019 = t4.com_ap
		ORDER BY t4.com_av
		)
SELECT * FROM sous_requete
WHERE com_av NOT IN (select insee_comm FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073)
order by date_eff, com_ap
/*
com_av|ncc_av                  |com_ap|ncc_ap                    |date_eff  |arrete_classement                 |nom_commune_arrete_classement                                    |insee_commune_arrete_classement|
------|------------------------|------|--------------------------|----------|----------------------------------|-----------------------------------------------------------------|-------------------------------|
73195 |OUTRECHAISE             |73303 |UGINE                     |1964-02-10|20/02/1974                        |UGINE                                                            |73303                          |
73279 |SAINT SIGISMOND         |73011 |ALBERTVILLE               |1965-01-01|25/07/1985                        |ALBERTVILLE                                                      |73011                          |
73134 |HAUTEVILLE GONDON       |73054 |BOURG SAINT MAURICE       |1965-01-01|20/02/1974                        |BOURG-SAINT-MAURICE                                              |73054                          |
73165 |MONT DENIS              |73250 |SAINT JULIEN MONT DENIS   |1965-02-15|20/02/1974                        |SAINT-JULIEN-MONT-DENIS                                          |73250                          |
73009 |ALBANNE                 |73173 |MONTRICHER ALBANNE        |1970-01-01|20/02/1974                        |MONTRICHER-ALBANNE                                               |73173                          |
73251 |SAINT LAURENT DE LA COTE|73257 |SAINT MARTIN DE BELLEVILLE|1971-01-01|{20/02/1974,20/02/1974,20/02/1974}|{SAINT-JEAN-DE-BELLEVILLE,SAINT-MARTIN-DE-BELLEVILLE,VILLARLURIN}|{73244,73257,73321}            |
73136 |HERY                    |73303 |UGINE                     |1971-01-01|20/02/1974                        |UGINE                                                            |73303                          |
73037 |BELLECOMBE              |73003 |AIGUEBLANCHE              |1971-04-22|{20/02/1974,20/02/1974,20/02/1974}|{AIGUEBLANCHE,"LE BOIS",SAINT-OYEN}                              |{73003,73045,73266}            |
73319 |VILLARGEREL             |73003 |AIGUEBLANCHE              |1971-04-22|{20/02/1974,20/02/1974,20/02/1974}|{AIGUEBLANCHE,"LE BOIS",SAINT-OYEN}                              |{73003,73045,73266}            |
73125 |GRAND COEUR             |73003 |AIGUEBLANCHE              |1971-04-22|{20/02/1974,20/02/1974,20/02/1974}|{AIGUEBLANCHE,"LE BOIS",SAINT-OYEN}                              |{73003,73045,73266}            |
73185 |NAVES                   |73187 |LECHERE                   |1972-06-30|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73209 |PUSSY                   |73187 |LECHERE                   |1972-06-30|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73199 |PETIT COEUR             |73187 |LECHERE                   |1972-06-30|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73060 |CELLIERS                |73187 |LECHERE                   |1972-06-30|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73102 |DOUCY                   |73187 |LECHERE                   |1972-06-30|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73024 |AVANCHERS               |73003 |AIGUEBLANCHE              |1972-07-18|{20/02/1974,20/02/1974,20/02/1974}|{AIGUEBLANCHE,"LE BOIS",SAINT-OYEN}                              |{73003,73045,73266}            |
73035 |BEAUNE                  |73261 |SAINT MICHEL DE MAURIENNE |1972-09-13|20/02/1974                        |SAINT-MICHEL-DE-MAURIENNE                                        |73261                          |
73295 |THYL                    |73261 |SAINT MICHEL DE MAURIENNE |1972-09-13|20/02/1974                        |SAINT-MICHEL-DE-MAURIENNE                                        |73261                          |
73174 |MONTROND                |73013 |ALBIEZ MONTROND           |1972-09-20|20/02/1974                        |ALBIEZ-MONTROND                                                  |73013                          |
73016 |ANSIGNY                 |73010 |ALBENS                    |1972-09-29|{20/02/1974,20/02/1974,20/02/1974}|{CESSENS,EPERSY,SAINT-GERMAIN-LA-CHAMBOTTE}                      |{73062,73108,73238}            |
73291 |TESSENS                 |73006 |AIME                      |1972-12-06|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73325 |VILLETTE                |73006 |AIME                      |1972-12-06|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73291 |TESSENS                 |73006 |AIME                      |1973-02-12|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73325 |VILLETTE                |73006 |AIME                      |1973-02-12|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73148 |LONGEFOY                |73006 |AIME                      |1973-02-12|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73325 |VILLETTE                |73006 |AIME LA PLAGNE            |2016-01-01|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73148 |LONGEFOY                |73006 |AIME LA PLAGNE            |2016-01-01|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73291 |TESSENS                 |73006 |AIME LA PLAGNE            |2016-01-01|{20/02/1974,20/02/1974,20/02/1974}|{AIME,GRANIER,MONTGIROD}                                         |{73006,73126,73169}            |
73016 |ANSIGNY                 |73010 |ENTRELACS                 |2016-01-01|{20/02/1974,20/02/1974,20/02/1974}|{CESSENS,EPERSY,SAINT-GERMAIN-LA-CHAMBOTTE}                      |{73062,73108,73238}            |
73172 |MONTPASCAL              |73135 |TOUR EN MAURIENNE         |2019-01-01|{20/02/1974,20/02/1974,20/02/1974}|{"LE CHATEL",HERMILLON,PONTAMAFREY-MONTPASCAL}                   |{73080,73135,73203}            |
73102 |DOUCY                   |73187 |LECHERE                   |2019-01-01|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73199 |PETIT COEUR             |73187 |LECHERE                   |2019-01-01|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73060 |CELLIERS                |73187 |LECHERE                   |2019-01-01|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73209 |PUSSY                   |73187 |LECHERE                   |2019-01-01|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
73185 |NAVES                   |73187 |LECHERE                   |2019-01-01|{20/02/1974,20/02/1974,20/02/1974}|{BONNEVAL,FEISSONS-SUR-ISERE,"LA LECHERE"}                       |{73046,73112,73187}            |
 */

---- Correction du fichier final :
--- pas besoin

--- Comparaison 2  avec fichier Fr�d�ric Berlioz :
SELECT insee_cog2019, insee_com AS fred_insee_com, insee_commune_arrete_classement, anciens_codes AS fred_anciens_codes, nb_evenements_cog, nb_anciennes_com AS fred_nb_anciennes_com
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073_final
FULL OUTER JOIN a_dcap_actu_arretes_montagne_dhup.n_communes_montagne85_000_2019_fb
ON insee_cog2019 = insee_com
WHERE LEFT(insee_com,2) = 'dd' ---073
ORDER BY insee_cog2019, insee_com;

------------------------------------------------------------------------------------------------------------------------------------------------
---- 22/09/2020
---- I : 2019
---- Combien d'occurences sur le classement de 2019
SELECT classement_85, count(classement_85) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073_final
GROUP BY classement_85
ORDER BY classement_85;
/*
classement_85                                 |count|
----------------------------------------------|-----|
P                                             |   33|
{P,totalit�,totalit�,P,totalit�,P}            |    1|
totalit�                                      |  175|
{totalit�}                                    |    3|
{totalit�,P}                                  |    1|
{totalit�,P,totalit�}                         |    1|
{totalit�,totalit�}                           |    4|
{totalit�,totalit�,totalit�}                  |    6|
{totalit�,totalit�,totalit�,totalit�}         |    1|
{totalit�,totalit�,totalit�,totalit�,totalit�}|    1|
 */
---- A] Cr�ation de la table des communes de 2019
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_073_2019;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_073_2019 as
SELECT 
'073'::varchar(3) AS code_dep,
t1.insee_cog2019::varchar(5) AS insee_2019,
t1.nom_commune_cog2019::varchar(100) AS nom_cog2019,
t2.nom_com::varchar AS nom_min_2019,
t1.nb_evenements_cog::integer,
t1.insee_commune_arrete_classement::varchar AS agreg_insee_classement,
t1.nom_commune_arrete_classement::varchar AS agreg_nom_classement , 
t1.arrete_classement::varchar AS agreg_arretes,
t1.classement_85 ::varchar AS agreg_classement_85,
CASE
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'
	WHEN t1.classement_85 = 'P' THEN 'partie'                                             
	WHEN t1.classement_85 = '{P,totalit�,totalit�,P,totalit�,P}' THEN 'partie'            
	WHEN t1.classement_85 = 'totalit�' THEN 'totalit�'                                     
	WHEN t1.classement_85 = '{totalit�}' THEN 'totalit�'                                    
	WHEN t1.classement_85 = '{totalit�,P}' THEN 'partie'                                  
	WHEN t1.classement_85 = '{totalit�,P,totalit�}' THEN 'partie'                        
	WHEN t1.classement_85 = '{totalit�,totalit�}' THEN 'totalit�'                           
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�}' THEN 'totalit�'                  
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�'         
	WHEN t1.classement_85 = '{totalit�,totalit�,totalit�,totalit�,totalit�}' THEN 'totalit�' 	  
	ELSE NULL
END::varchar(8) AS classement_2019,
commentaires::TEXT AS agreg_commentaires,
'n_adm_exp_cog_commune_000_2019'::varchar(254) AS source_geom,
t2.geom::geometry('MULTIPOLYGON',2154)
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_2019_073_final AS t1
JOIN r_admin_express.n_adm_exp_cog_commune_000_2019 AS t2
ON t1.insee_cog2019 = t2.insee_com
ORDER BY insee_2019;
--> 226

SELECT classement_2019, count(classement_2019) FROM a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_073_2019 GROUP BY classement_2019;
/*
classement_2019|count|
---------------|-----|
totalit�       |  190|
partie         |   36|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_000_2019 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_commune_loimontagne85_073_2019 FOR VALUES IN ('073');
--> Updated Rows	0


---- II : 1985
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 RENAME TO l_liste_commune_montagne_1985_073_old;
---- Combien d'occurences sur le classement de 1985
SELECT partie, count(partie) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073_old
GROUP BY partie
ORDER BY partie;
/*
partie  |count|
--------|-----|
P       |   39|
totalit�|  218|
 */
---- A] Cr�ation de la table des communes de 1985
DROP TABLE IF EXISTS a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073;
CREATE TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 AS
SELECT
'073'::varchar(3) AS code_dep,
insee_comm::varchar(5) AS insee_classement,
nom_commun::varchar(100) AS nom_commune,
date_decis::varchar AS arrete_classement,
CASE
	WHEN partie = 'totalit�' THEN 'totalit�'
	WHEN partie = 'P' THEN 'partie'  
	ELSE NULL
END::varchar(8) AS classement_1985,
commentair::TEXT AS commentaires
FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073_old
ORDER BY insee_classement;
--> 257

SELECT classement_1985, count(classement_1985) FROM a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 GROUP BY classement_1985;
/*
classement_1985|count|
---------------|-----|
totalit�       |  218|
partie         |   39|
 */

---- B] Attache � la table nationale
ALTER TABLE a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_000 ATTACH PARTITION a_dcap_actu_arretes_montagne_dhup.l_liste_commune_montagne_1985_073 FOR VALUES IN ('073');
--> Updated Rows	0